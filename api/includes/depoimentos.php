<?php
/* Listar depoimentos */
if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	
	// Verificar cache
	$str_xml = verificarCache( 'depoimentos', 'depoimentos'.$ID, 24 );
	
	if ($str_xml){
		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Depoimentos listados. Arquivo em cache.';
		$retorno["depoimentos"] = $str_xml;
		
	}else{
		
		$str_xml = '';
				
		$sql = 	"select top 20 DEPOIMENTOS.ID_depoimento, DEPOIMENTOS.nome_depoimento, DEPOIMENTOS.formacao_depoimento, DEPOIMENTOS.empresa_depoimento, 
				DEPOIMENTOS.resumo_depoimento, DEPOIMENTOS.email_depoimento, DEPOIMENTOS.ID_curso, CURSOS.titulo_curso , DEPOIMENTOS.corpo_depoimento 
				from DEPOIMENTOS inner join CURSOS on DEPOIMENTOS.ID_curso=CURSOS.ID_curso 
				where status_depoimento=1 and resumo_depoimento is not null and resumo_depoimento<>'' ";
		
		if ($ID)
			$sql .= " and DEPOIMENTOS.ID_curso=$ID";
				
		$sql .= " order by NewId()";
		
		$rsDep = abrirRs( $sql );
		
		if ( sqlsrv_num_rows( $rsDep ) > 0 ){
		
			while( $depoimento = sqlsrv_fetch_array( $rsDep ) ) {
				
				$sql = "select foto_usuario from USUARIOS where email_usuario='".$depoimento['email_depoimento']."'";
				$rsFoto = abrirRs( $sql );
				
				if ( sqlsrv_num_rows( $rsFoto ) > 0 ){
					$foto 			= sqlsrv_fetch_array( $rsFoto );	
					$foto_usuario 	= '/asa/_arquivos_usuarios/'.$foto["foto_usuario"];
				}
				
				$str_xml	.= 	'<depoimento codigo="'.$depoimento['ID_depoimento'].'" curso="'.$depoimento['ID_curso'].'">
									<titulo_curso><![CDATA['.$depoimento['titulo_curso'].']]></titulo_curso>
									<nome><![CDATA['.$depoimento['nome_depoimento'].']]></nome>
									<foto><![CDATA['.$foto_usuario.']]></foto>
									<formacao><![CDATA['.$depoimento['formacao_depoimento'].']]></formacao>
									<empresa><![CDATA['.$depoimento['empresa_depoimento'].']]></empresa>
									<resumo><![CDATA['.$depoimento['resumo_depoimento'].']]></resumo>
									<descricao><![CDATA['.$depoimento['corpo_depoimento'].']]></descricao>
								</depoimento>';
				
			}

			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Depoimentos listados';
			$retorno["depoimentos"] = $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'depoimentos', 'depoimentos'.$ID, $str_xml );
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= 'Nenhum depoimento encontrado';
		}
	
	}


/* Listar depoimentos paginados */
}else if ($metodo == 'listar_todos'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	iniciaPaginacao();
	
	$ID_tema	= formataVar( 'ID_tema', 'get' );
	
	$str_xml = '';
	
	// Monta a query com as condições
	$sql_where = 	"from DEPOIMENTOS with (NOLOCK) inner join CURSOS with (NOLOCK) on DEPOIMENTOS.ID_curso=CURSOS.ID_curso where status_depoimento=1";
	if ($ID_tema && is_numeric($ID_tema)){
		$sql_where .= ' and ID_tema='.$ID_tema;
	}
	
	// query para contar número de registros
	$sql_conta = "select count(1) as qtd ".$sql_where;
	$rsConta = abrirRs( $sql_conta );
	
	if ( sqlsrv_num_rows( $rsConta ) > 0 ){
		
		$quantidade = sqlsrv_fetch_array( $rsConta );
		$quantidade_registros = $quantidade['qtd'];
		
		$sql = 	"SELECT * FROM ( SELECT DEPOIMENTOS.ID_depoimento, DEPOIMENTOS.nome_depoimento, DEPOIMENTOS.formacao_depoimento, DEPOIMENTOS.empresa_depoimento, DEPOIMENTOS.data_depoimento, 
				DEPOIMENTOS.resumo_depoimento, DEPOIMENTOS.email_depoimento, DEPOIMENTOS.ID_curso, CURSOS.titulo_curso , DEPOIMENTOS.corpo_depoimento, indice = ROW_NUMBER() OVER (ORDER BY data_depoimento desc)
				".$sql_where." ) AS temp WHERE indice BETWEEN ".$paginacao_inicio." AND ".$paginacao_fim;
		$rs = abrirRs( $sql );
		
		while( $registro = sqlsrv_fetch_array( $rs ) ) {
			
			if ( is_null($registro['resumo_depoimento']) || empty($registro['resumo_depoimento']) || $registro['resumo_depoimento'] == $registro['nome_depoimento'] ){
				$registro['resumo_depoimento'] = removerHtml( $registro['corpo_depoimento'], 350 );
			}
			
			$sql = "select foto_usuario from USUARIOS where email_usuario='".$registro['email_depoimento']."'";
			$rsFoto = abrirRs( $sql );
			
			$foto_usuario 	= '';
			if ( sqlsrv_num_rows( $rsFoto ) > 0 ){
				$foto 			= sqlsrv_fetch_array( $rsFoto );	
				$foto_usuario 	= '/asa/_arquivos_usuarios/'.$foto["foto_usuario"];
			}
			
			$str_xml	.= 	'<depoimento codigo="'.$registro['ID_depoimento'].'" curso="'.$registro['ID_curso'].'">
								<titulo_curso><![CDATA['.$registro['titulo_curso'].']]></titulo_curso>
								<nome><![CDATA['.$registro['nome_depoimento'].']]></nome>
								<foto><![CDATA['.$foto_usuario.']]></foto>
								<formacao><![CDATA['.$registro['formacao_depoimento'].']]></formacao>
								<empresa><![CDATA['.$registro['empresa_depoimento'].']]></empresa>
								<resumo><![CDATA['.$registro['resumo_depoimento'].']]></resumo>
								<descricao><![CDATA['.$registro['corpo_depoimento'].']]></descricao>
								<data>'.dataBD( $registro["data_depoimento"] ).'</data>
							</depoimento>';
			
		}

		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Depoimentos listados';
		$retorno["quantidade"] 	= $quantidade_registros;
		$retorno["depoimentos"]	= $str_xml;
		
	}else{
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Nenhum depoimento encontrado';	
		$retorno["quantidade"] 	= 0;
	}



/* Ver um depoimento específico*/
}else if ($metodo == 'ver'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	if (!$ID){
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= 'Faltam parâmetros';
	}else{
		
		
		// Verificar cache
		$str_xml = verificarCache( 'depoimentos', 'depoimento_'.$ID, 24 );
		
		if ($str_xml){
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Depoimento listado. Arquivo em cache.';
			$retorno["depoimentos"] = $str_xml;
			
		}else{
		
			$str_xml = '';
	
				
			$sql = 	"select DEPOIMENTOS.*, CURSOS.titulo_curso from DEPOIMENTOS inner join CURSOS on DEPOIMENTOS.ID_curso=CURSOS.ID_curso where status_depoimento=1 and ID_depoimento=$ID";
			
			$rsDep = abrirRs( $sql );
			
			if ( sqlsrv_num_rows( $rsDep ) > 0 ){
				
				$registro = sqlsrv_fetch_array( $rsDep );
				
				if ( is_null($registro['resumo_depoimento']) || empty($registro['resumo_depoimento']) || $registro['resumo_depoimento'] == $registro['nome_depoimento'] ){
					$registro['resumo_depoimento'] = removerHtml( $registro['corpo_depoimento'], 350 );
				}
				
				$sql = "select foto_usuario from USUARIOS where email_usuario='".$registro['email_depoimento']."'";
				$rsFoto = abrirRs( $sql );
				
				$foto_usuario 	= '';
				if ( sqlsrv_num_rows( $rsFoto ) > 0 ){
					$foto 			= sqlsrv_fetch_array( $rsFoto );	
					$foto_usuario 	= '/asa/_arquivos_usuarios/'.$foto["foto_usuario"];
				}
				
				$str_xml	.= 	'<depoimento codigo="'.$registro['ID_depoimento'].'" curso="'.$registro['ID_curso'].'">
									<titulo_curso><![CDATA['.$registro['titulo_curso'].']]></titulo_curso>
									<nome><![CDATA['.$registro['nome_depoimento'].']]></nome>
									<foto><![CDATA['.$foto_usuario.']]></foto>
									<formacao><![CDATA['.$registro['formacao_depoimento'].']]></formacao>
									<empresa><![CDATA['.$registro['empresa_depoimento'].']]></empresa>
									<resumo><![CDATA['.$registro['resumo_depoimento'].']]></resumo>
									<descricao><![CDATA['.$registro['corpo_depoimento'].']]></descricao>
									<data>'.dataBD( $registro["data_depoimento"] ).'</data>
								</depoimento>';
					
				
		
				
				$retorno["erro"] 		= 0;
				$retorno["mensagem"] 	= 'Depoimento listado';
				$retorno["depoimentos"]	= $str_xml;
				
				// gravar arquivo de cache
				gravarCache( 'depoimentos', 'depoimento_'.$ID, $str_xml );
				
			}else{
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= 'Nenhum depoimento encontrado';
			}
		
		}
	
	}
	
	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>