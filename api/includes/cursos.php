<?php
/* Ver dados do curso */
if ($metodo == 'ver'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	if ($ID){
		
		// Verificar cache
		$str_xml = verificarCache( 'cursos', 'curso_'.$ID, 12 );
		
		if ($str_xml){
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Curso encontrado. Arquivo em cache.';
			$retorno["curso"] 		= $str_xml;
			
		}else{
			
			if ( is_numeric($ID) == false ){
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= 'Curso não encontrado.';
				goto fim;
			}
			
			$sql = 	"select CURSOS.* from CURSOS where ID_curso=$ID";
			$rsCursos = abrirRs( $sql );
			
			if ( sqlsrv_num_rows( $rsCursos ) > 0 ){
			
				$curso = sqlsrv_fetch_array( $rsCursos );
				
				if ($curso['banner_curso']){
					$curso['banner_curso'] = '/methodus/_arquivos_banner/'.$curso['banner_curso'];
				}
				
				$str_xml = 	'<codigo>'.$ID.'</codigo>
							<status>'.$curso['status_curso'].'</status>
							<tema>'.$curso['ID_tema'].'</tema>
							<titulo><![CDATA['.$curso['titulo_curso'].']]></titulo>
							<url><![CDATA['.$curso['titulo_curso'].']]></url>
							<cor>'.$curso['cor_curso'].'</cor>
							<distancia>'.$curso['distancia_curso'].'</distancia>
							<banner><![CDATA['.$curso['banner_curso'].']]></banner>
							<resumo><![CDATA['.$curso['resumo_curso'].']]></resumo>
							<descricao><![CDATA['.$curso['conteudo_curso'].']]></descricao>
							<metatags><![CDATA['.$curso['metatags'].']]></metatags>'; //<maladireta><![CDATA['.$curso['mailing_curso'].']]></maladireta>
				
				// Variáveis do tema do curso
				if ($curso['ID_tema']){
					
					$sql = 	"select * from TEMAS_VARIAVEIS where ID_tema=".$curso['ID_tema'];
					$rsTema = abrirRs( $sql );
					
					if ( sqlsrv_num_rows( $rsTema ) > 0 ){
						
						$str_xml .= 	'<variaveis>';
						
						while( $tema = sqlsrv_fetch_array( $rsTema ) ) {
							$str_xml .= 	'<'.$tema['titulo_variavel'].' tipo="'.$tema['tipo_variavel'].'"><![CDATA['.str_replace( ']]>', '', str_replace( '<![CDATA[', '', $tema['valor_variavel'] ) ).']]></'.$tema['titulo_variavel'].'>';
						}
						
						$str_xml .= 	'</variaveis>';
					}
				}
				
				// Planos de pagamento
				$sql = "select * from CURSOS_PLANOS where ID_curso=$ID and status_plano=1 order by empresarial_plano, valor_plano";
				$rsPlano = abrirRs( $sql );
		
				if ( sqlsrv_num_rows( $rsPlano ) > 0 ){
					$str_xml .= 	'<planos>';
					while( $plano = sqlsrv_fetch_array( $rsPlano ) ) {
						$str_xml .= 	'<plano codigo="'.$plano['ID_plano'].'" empresarial="'.$plano['empresarial_plano'].'" participantes="'.$plano['participantes_plano'].'" desconto1="'.$plano['desconto1_plano'].'" desconto2="'.$plano['desconto2_plano'].'" desconto3="'.$plano['desconto3_plano'].'" parcelas="'.$plano['parcelas_plano'].'">'.formataMoeda( $plano['valor_plano'], 'base' ).'</plano>';
					}
					$str_xml .= 	'</planos>';
				}
				
				// Datas de turmas
				$sql = "select * from CURSOS_DATAS where ID_curso=$ID and status_data=1 and fim_data >= getDate() and fim_inscricoes_data >= DATEADD(day,-1,getDate())  order by inicio_data";
				$rsTurma = abrirRs( $sql );
		
				if ( sqlsrv_num_rows( $rsTurma ) > 0 ){
					$str_xml .= 	'<turmas>';
					while( $turma = sqlsrv_fetch_array( $rsTurma ) ) {
						
						if ($turma['foto_data']){
							$turma['foto_data'] = '/asa/_arquivos_turmas/'.$turma['foto_data'];
						}
						
						$str_xml .= 	'<turma codigo="'.$turma['ID_data'].'" presencial="'.$turma['presencial_data'].'">
											<inicio>'.formataData( $turma["inicio_data"], 'base' ).'</inicio>
											<fim>'.formataData( $turma["fim_data"], 'base' ).'</fim>
											<fim_inscricoes>'.formataData( $turma["fim_inscricoes_data"], 'base' ).'</fim_inscricoes>
											<horario><![CDATA['.$turma['horario_data'].']]></horario>
											<quantidade_vagas>'.$turma['vagas_data'].'</quantidade_vagas>
											<foto><![CDATA['.$turma['foto_data'].']]></foto>
											<descricao><![CDATA['.$turma['descricao_data'].']]></descricao>
										</turma>';
					}
					$str_xml .= 	'</turmas>';
				}
				
				// Perfis de aluno
				$sql = "select * from PERFIL where ID_curso=$ID and status_perfil=1 order by titulo_perfil";
				$rsPerfis = abrirRs( $sql );
		
				if ( sqlsrv_num_rows( $rsPerfis ) > 0 ){
					$str_xml .= 	'<perfis>';
					while( $perfil = sqlsrv_fetch_array( $rsPerfis ) ) {
						$str_xml .= 	'<perfil codigo="'.$perfil['ID_perfil'].'"><![CDATA['.$perfil['titulo_perfil'].']]></perfil>';
					}
					$str_xml .= 	'</perfis>';
				}
				
				// Fotos de turmas antigas
				$sql = "select top 4 foto_data, inicio_data from CURSOS_DATAS where ID_curso=$ID and status_data=1 and foto_data is not null order by inicio_data desc";
				$rsTurma = abrirRs( $sql );
		
				if ( sqlsrv_num_rows( $rsTurma ) > 0 ){
					$str_xml .= 	'<fotos>';
					while( $turma = sqlsrv_fetch_array( $rsTurma ) ) {
						
						$turma['foto_data'] = '/asa/_arquivos_turmas/'.$turma['foto_data'];
						
						
						$str_xml .= 	'<foto>
											<data>'.formataData( $turma["inicio_data"], 'base' ).'</data>
											<arquivo><![CDATA['.$turma['foto_data'].']]></arquivo>
										</foto>';
					}
					$str_xml .= 	'</fotos>';
				}
				
				
				
				$retorno["erro"] 		= 0;
				$retorno["mensagem"] 	= 'Curso encontrado';
				$retorno["curso"] 		= $str_xml;
				
				// gravar arquivo de cache
				gravarCache( 'cursos', 'curso_'.$ID, $str_xml );
				
				
			}else{
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= "Curso não encontrado";		
			}			
	
		}
	
	}else{
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= "Campos obrigatórios não preenchidos";		
	}
	
	fim:

/* Ver dados do curso */
}else if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
		
	// Verificar cache
	$str_xml = verificarCache( 'variados', 'cursos', 12 );
	
	if ($str_xml){
		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Cursos listados. Arquivo em cache.';
		$retorno["cursos"] 		= $str_xml;
		
	}else{
		
		
		$str_xml = '';
				
		$sql = 	"select ID_curso, titulo_curso, cor_curso, resumo_curso 
				from CURSOS where status_curso=1 order by ordem_curso, titulo_curso";
		$rsCursos = abrirRs( $sql );
		
		if ( sqlsrv_num_rows( $rsCursos ) > 0 ){
		
			while( $curso = sqlsrv_fetch_array( $rsCursos ) ) {
				
				$str_xml	.= 	'<curso codigo="'.$curso['ID_curso'].'">
									<titulo><![CDATA['.$curso['titulo_curso'].']]></titulo>
									<cor><![CDATA['.$curso['cor_curso'].']]></cor>
									<resumo><![CDATA['.$curso['resumo_curso'].']]></resumo>
								</curso>';
				
			}

			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Cursos listados';
			$retorno["cursos"] 		= $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'variados', 'cursos', $str_xml );
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= 'Nenhum curso encontrado';
		}
	
	}


	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>