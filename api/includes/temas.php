<?php	
// Forçar exibição em XML
if ( $formato == '' )
	$formato = 'xml';

if ($ID){
	
	// Verificar cache
	$str_xml = verificarCache( 'temas', 'tema_'.$ID, 24 );
	
	if ($str_xml){
		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Tema encontrado. Arquivo em cache.';
		$retorno["tema"]	 	= $str_xml;
		
	}else{
		
		if ( is_numeric($ID) == false ){
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= 'Tema não encontrado.';
			goto fim;
		}
			
		$sql = 	"select * from TEMAS_VARIAVEIS where ID_tema=$ID";
		$rsTema = abrirRs( $sql );
		
		$str_xml = '';
		
		if ( sqlsrv_num_rows( $rsTema ) > 0 ){
			
			while( $tema = sqlsrv_fetch_array( $rsTema ) ) {
				$str_xml .= 	'<'.$tema['titulo_variavel'].' tipo="'.$tema['tipo_variavel'].'"><![CDATA['.str_replace( ']]>', '', str_replace( '<![CDATA[', '', $tema['valor_variavel'] ) ).']]></'.$tema['titulo_variavel'].'>';
			}
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Tema encontrado';
			$retorno["tema"] 		= $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'temas', 'tema_'.$ID, $str_xml );
			
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= "Tema não encontrado";	
		}			

	}

}else{
	
	// Verificar cache
	$str_xml = verificarCache( 'temas', 'temas', 24 );
	
	if ($str_xml){
		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Temas encontrados. Arquivo em cache.';
		$retorno["temas"]	 	= $str_xml;
		
	}else{
			
		$sql = 	"select ID_tema, titulo_tema from TEMAS where status_tema=1 order by ordem_tema";
		$rsTema = abrirRs( $sql );
		
		$str_xml = '';
		
		if ( sqlsrv_num_rows( $rsTema ) > 0 ){
			
			while( $tema = sqlsrv_fetch_array( $rsTema ) ) {
				$str_xml .= 	'<tema codigo="'.$tema['ID_tema'].'"><![CDATA['.$tema['titulo_tema'].']]></tema>';
			}
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Temas encontrados';
			$retorno["temas"] 		= $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'temas', 'temas', $str_xml );
			
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= "Temas não encontrados";	
		}			

	}
			
}

fim:

?>