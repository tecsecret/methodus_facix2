<?php
/* Listar Livros */
if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';	
		
	// Verificar cache
	$str_xml = verificarCache( 'variados', 'recomendacoes', 24 );
	
	if ($str_xml){
		
		$retorno["erro"] 			= 0;
		$retorno["mensagem"] 		= 'Recomendações listadas. Arquivo em cache.';
		$retorno["recomendacoes"] 	= $str_xml;
		
	}else{
		
		$str_xml = '';
		
		$sql = 	"select * from EMPRESAS_RECOMENDADAS where status_recomendacao=1 order by titulo_recomendacao";
		$rs = abrirRs( $sql );
		
		if ( sqlsrv_num_rows( $rs ) > 0 ){
		
			while( $registro = sqlsrv_fetch_array( $rs ) ) {
				
				if ($registro['logo_recomendacao']){
					$registro['logo_recomendacao'] = '/asa/_arquivos/empresas/'.$registro['logo_recomendacao'];
				}
				
				$str_xml .= 	'<empresa codigo="'.$registro['ID_recomendacao'].'">
									<data>'.dataBD( $registro["data_recomendacao"] ).'</data>
									<titulo><![CDATA['.$registro['titulo_recomendacao'].']]></titulo>
									<telefone><![CDATA['.$registro['telefone_recomendacao'].']]></telefone>
									<email><![CDATA['.$registro['email_recomendacao'].']]></email>
									<site><![CDATA['.$registro['site_recomendacao'].']]></site>
									<logo><![CDATA['.$registro['logo_recomendacao'].']]></logo>
									<descricao><![CDATA['.$registro['descricao_recomendacao'].']]></descricao>
								</empresa>';
						
			}
			
			
			$retorno["erro"] 			= 0;
			$retorno["mensagem"] 		= 'Recomendações listadas';
			$retorno["recomendacoes"] 	= $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'variados', 'recomendacoes', $str_xml );
			
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= "Nenhuma recomendação encontrada";		
		}			

	}

	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>