<?php	
// Forçar exibição em XML
if ( $formato == '' )
	$formato = 'xml';

if ($ID){
	
	// Verificar cache
	$str_xml = verificarCache( 'conteudos', 'conteudo_'.$ID, 24 );
	
	if ($str_xml){
		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Conteúdo encontrado. Arquivo em cache.';
		$retorno["conteudo"] 	= $str_xml;
		
	}else{
		
		if ( is_numeric($ID) == false ){
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= 'Conteúdo não encontrado.';
			goto fim;
		}
		
		$sql = 	"select * from CONTEUDOS where ID_conteudo=$ID";
		$rs = abrirRs( $sql );
		
		if ( sqlsrv_num_rows( $rs ) > 0 ){
		
			$conteudo = sqlsrv_fetch_array( $rs );
			
			if ($conteudo['banner_conteudo']){
				$conteudo['banner_conteudo'] = '/methodus/_arquivos_banner/'.$conteudo['banner_conteudo'];
			}
			
			$str_xml = 	'<codigo>'.$ID.'</codigo>
						<status>'.$conteudo['status_conteudo'].'</status>
						<ID_tema>'.$conteudo['ID_tema'].'</ID_tema>
						<titulo><![CDATA['.$conteudo['titulo_conteudo'].']]></titulo>
						<banner><![CDATA['.$conteudo['banner_conteudo'].']]></banner>
						<texto_banner><![CDATA['.$conteudo['texto_banner_conteudo'].']]></texto_banner>
						<corpo><![CDATA['.$conteudo['corpo_conteudo'].']]></corpo>
						<metatags><![CDATA['.$conteudo['metatags'].']]></metatags>';
			
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Conteúdo encontrado';
			$retorno["conteudo"] 	= $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'conteudos', 'conteudo_'.$ID, $str_xml );
			
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= "Conteúdo não encontrado";		
		}			

	}

}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Campos obrigatórios não preenchidos";		
}

fim:

?>