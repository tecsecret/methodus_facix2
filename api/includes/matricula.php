<?php
/* Cadastrar alunos e criar matrícula */
if ($metodo == 'inscricao'){

	$participantes	= formataVar( 'participantes', 'post' );
	$ID_curso		= formataVar( 'ID_curso', 'post' );;
	$ID_data		= formataVar( 'ID_data', 'post' );
	$ID_plano		= formataVar( 'ID_plano', 'post' );
	
	// Verificar campos obrigatórios
	if ( empty($participantes) || empty($ID_curso) || empty($ID_data) || empty($ID_plano) ){
		
		$retorno["erro"] = 1;
		$retorno["mensagem"] = 'Campos obrigatórios não preenchidos';
		
	}else{
		
		// Variável que vai formatar a mensagem enciada para os professores
		$mensagem	= '<b>Nova solicitação de inscrição</b><BR /><BR />';
		
		
		// verificar dados do curso escolhido, para Mailing e LOG
		$xml_curso 	= executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID_curso ) );
		$curso 		= lerXML( $xml_curso );
			
		if ( $curso->erro == 0 ){
			
			$curso				= $curso->curso;
			$titulo_curso		= $curso->titulo;
			$distancia_curso	= $curso->distancia;
			
			if ( isset( $curso->planos->plano ) == false ){
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= 'Erro ao encontrar o plano escolhido';
				goto fim;		
			}else{
				$plano_escolhido = $curso->planos->xpath("plano[@codigo='".$ID_plano."']");
				if ( count( $plano_escolhido ) >= 1 ){
					$plano_escolhido = $plano_escolhido[0];	
				}else{
					$retorno["erro"] 		= 1;
					$retorno["mensagem"] 	= 'Erro ao encontrar o plano escolhido';
					goto fim;		
				}
			}
			
			if ( isset( $curso->turmas->turma ) == false ){
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= 'Erro ao encontrar a turma escolhida';
				goto fim;			
			}else{
				$data_escolhida = $curso->turmas->xpath("turma[@codigo='".$ID_data."']");
				if ( count( $data_escolhida ) >= 1 ){
					$data_escolhida = $data_escolhida[0];	
				}else{
					$retorno["erro"] 		= 1;
					$retorno["mensagem"] 	= 'Erro ao encontrar a turma escolhida';
					goto fim;			
				}	
			}
			
			// verificar se é plano empresarial
			if ( $plano_escolhido['empresarial'] == '1' ){
				$plano_empresarial = true;	
			}else{
				$plano_empresarial = false;		
			}
			
			if ( $distancia_curso == '0' ){
				$mensagem	.= '<b>Curso: </b>'.$titulo_curso.' / '.$data_escolhida->horario.' - de '.formataData( $data_escolhida->inicio, 'tela' ) . ' à ' . formataData( $data_escolhida->fim, 'tela' ).'<BR /><BR />';
			}else{
				$mensagem	.= '<b>Curso: </b>'.$titulo_curso.' / Curso à distância<BR /><BR />';
			}
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= 'Erro ao encontrar o curso escolhido';
			goto fim;	
		}
		
		//Incluir a inscrição
		$sql = 	"insert into CURSOS_INSCRICOES (data_inscricao, ID_curso, ID_plano, ID_data, online_inscricao, pago_inscricao) values (getDate(), $ID_curso, $ID_plano, $ID_data, 0, 0)";
		executarQuery( $sql );
		
		$sql = "select top 1 ID_inscricao from CURSOS_INSCRICOES where ID_curso=$ID_curso and ID_plano=$ID_plano and ID_data=$ID_data order by ID_inscricao desc";
		$rs = abrirRs( $sql );
			
		if ( sqlsrv_num_rows( $rs ) > 0 ){
			$inscricao = sqlsrv_fetch_array( $rs );
			$ID_inscricao = $inscricao['ID_inscricao'];
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= 'Erro ao cadastrar nova inscrição';
			goto fim;		
		}
		
		// Se o plano escolhido é empresarial, cadastrar empresa
		if ( $plano_empresarial == true ){
			
			$razao				= formataVar( 'razao', 'post' );
			$cnpj				= formataVar( 'cnpj', 'post' );;
			$endereco_empresa	= formataVar( 'endereco_empresa', 'post' );
			$cidade_empresa		= formataVar( 'cidade_empresa', 'post' );
			$cep_empresa		= formataVar( 'cep_empresa', 'post' );;
			$estado_empresa		= formataVar( 'estado_empresa', 'post' );
			
			$mensagem	.= '<table border="0" cellspacing="0" cellpadding="2">_
							<TR><TD><b>Empresa:</b></TD><TD>'.$razao.'</TD></TR>
							<TR><TD><b>CNPJ:</b></TD><TD>'.$cnpj.'</TD></TR>
							<TR><TD><b>Endereço:</b></TD><TD>'.$endereco_empresa.'</TD></TR>
							<TR><TD><b>CEP:</b></TD><TD>'.$cep_empresa.'</TD></TR>
							<TR><TD><b>Cidade:</b></TD><TD>'.$cidade_empresa.'</TD></TR>
							<TR><TD><b>Estado:</b></TD><TD>'.$estado_empresa.'</TD></TR>
							</table><hr width="100%" />';
			
			// Tratar variáveis da empresa
			$cnpj				= formataCNPJ( $cnpj, 'base' );
			$cep_empresa		= formataCEP( $cep_empresa, 'base' );
			$razao				= charSet( $razao );
			$endereco_empresa	= charSet( $endereco_empresa );
			$cidade_empresa		= charSet( $cidade_empresa );
							
			// verificar se empresa já existe
			$sql = "select ID_empresa from EMPRESAS where cnpj_empresa='{$cnpj}'";
			$rs = abrirRs( $sql );
			
			if ( sqlsrv_num_rows( $rs ) > 0 ){
				
				$empresa 	= sqlsrv_fetch_array( $rs );
				$ID_empresa = $empresa['ID_empresa'];
				
				$sql = 	"update EMPRESAS set razao_empresa='{$razao}', endereco_empresa='{$endereco_empresa}', cep_empresa='{$cep_empresa}', 
						cidade_empresa='{$cidade_empresa}', estado_empresa='{$estado_empresa}' where ID_empresa=$ID_empresa";
				executarQuery( $sql );
				
			}else{
				
				$sql = 	"insert into EMPRESAS (data_empresa, razao_empresa, cnpj_empresa, endereco_empresa, cep_empresa, cidade_empresa, estado_empresa) 
						values (getDate(),'{$razao}','{$cnpj}','{$endereco_empresa}','{$cep_empresa}','{$cidade_empresa}','{$estado_empresa}')";
				executarQuery( $sql );
				
				$sql = "select ID_empresa from EMPRESAS where cnpj_empresa='{$cnpj}'";
				$rs = abrirRs( $sql );
				
				if ( sqlsrv_num_rows( $rs ) > 0 ){
					$empresa 	= sqlsrv_fetch_array( $rs );
					$ID_empresa = $empresa['ID_empresa'];
				}
					
			}
			
			// Cadastrar empresa na inscrição
			if ( isset($ID_empresa) ){
				$sql = "insert into CURSOS_INSCRICOES_EMPRESAS (ID_inscricao, ID_empresa) values ($ID_inscricao,$ID_empresa)";
				executarQuery( $sql );
			}
	
		}
		
		
		// Fazer loop para cadastrar todos os participantes
		for ( $i=1; $i <= intval($participantes); $i++ ){
			
			$nome		= formataVar( 'nome_'.$i, 'post' );
			$nascimento	= formataVar( 'nascimento_'.$i, 'post' );;
			$cpf		= formataVar( 'cpf_'.$i, 'post' );
			$rg			= formataVar( 'rg_'.$i, 'post' );
			$tel		= formataVar( 'tel_'.$i, 'post' );;
			$email		= formataVar( 'email_'.$i, 'post' );
			$endereco	= formataVar( 'endereco_'.$i, 'post' );
			$cidade		= formataVar( 'cidade_'.$i, 'post' );
			$cep		= formataVar( 'cep_'.$i, 'post' );
			$estado		= formataVar( 'estado_'.$i, 'post' );
			
			$mensagem	.= 	'<table border="0" cellspacing="0" cellpadding="2">
							<TR><TD colspan="2"><b>Participante '.$i.'</b></TD></TR>
							<TR><TD><b>Nome:</b></TD><TD>'.$nome.'</TD></TR>
							<TR><TD><b>CPF:</b></TD><TD>'.$cpf.'</TD></TR>
							<TR><TD><b>RG:</b></TD><TD>'.$rg.'</TD></TR>
							<TR><TD><b>Nascimento:</b></TD><TD>'.$nascimento.'</TD></TR>
							<TR><TD><b>E-mail:</b></TD><TD>'.$email.'</TD></TR>
							<TR><TD><b>Telefone:</b></TD><TD>'.$tel.'</TD></TR>
							<TR><TD><b>Endereço:</b></TD><TD>'.$endereco.'</TD></TR>
							<TR><TD><b>Cidade:</b></TD><TD>'.$cidade.'</TD></TR>
							<TR><TD><b>CEP:</b></TD><TD>'.$cep.'</TD></TR>
							<TR><TD><b>Estado:</b></TD><TD>'.$estado.'</TD></TR>
							</table><hr width=""100%"" />';
							
			// Tratar variáveis do participante
			$cpf		= formataCPF( $cpf, 'base' );
			$cep		= formataCEP( $cep, 'base' );
			$tel		= formataTelefone( $tel, 'base' );
			
			// verificar se aluno já existe
			$sql = "select top 1 ID_usuario, entrou_usuario, interesses_usuario from USUARIOS where email_usuario='".$email."' or cpf_usuario='".$cpf."' order by ID_usuario desc";
			$rs = abrirRs( $sql );
					
			if ( sqlsrv_num_rows( $rs ) > 0 ){	
				$aluno 					= sqlsrv_fetch_array( $rs );
				$ID_usuario				= $aluno['ID_usuario'];
			}else{
				$ID_usuario				= null;
			}
			
			//campos comuns 
			$campos["nome"] 		= $nome;							
			$campos["email"] 		= $email;							
			$campos["cpf"] 			= $cpf;							
			$campos["rg"] 			= $rg;							
			$campos["nascimento"] 	= $nascimento;							
			$campos["ddd_tel"] 		= $tel['ddd'];							
			$campos["tel"] 			= $tel['numero'];							
			$campos["endereco"] 	= $endereco;							
			$campos["cep"] 			= $cep;							
			$campos["cidade"] 		= $cidade;							
			$campos["estado"] 		= $estado;								
			
			// Incluir ou atualizar cadastro do aluno				
			$usuario = new Usuario( $campos );
			$cadastro = $usuario->Cadastro( $ID_usuario );
			
			if ( $cadastro['erro'] == 0 && isset($cadastro['id']) ){
				$ID_usuario = $cadastro['id'];
			}
			
			// Cadastrar participante na inscrição
			if ( isset($ID_usuario) ){
				$sql = "insert into CURSOS_INSCRICOES_USUARIOS (ID_inscricao, ID_usuario) values ($ID_inscricao,$ID_usuario)";
				executarQuery( $sql );
			}
		
			//Enviar dados para RDStation
			$identificador_curso = identificadorCurso( $titulo_curso );
			cadastrarRdstation( 'aluno_'.$identificador_curso, $nome, $email, '', $titulo_curso );
		
		}
		
		// Enviar email para os professores com dados da nova matrícula
		enviaEmail( 'Nova solicitação de inscrição', $mensagem, $email_principal, 'Methodus', $email, $nome, 1 );
		
		
		$retorno["erro"] 			= 0;
		$retorno["ID_inscricao"] 	= $ID_inscricao;
		$retorno["mensagem"] 		= 'Cadastro realizado com sucesso';
		
	}
	

/* Alterar meio e forma de pagamento */
}elseif ($metodo == 'meio'){

	/*BLACK FRIDAY 2020*/
	$singleInCash = 30;
    $singleInPeriod = 20;

    $groupInCash = 35;
    $groupInPeriod = 25;

	$ID_inscricao	= formataVar( 'ID_inscricao', 'post' );
	$forma			= formataVar( 'forma', 'post' );
	$meio			= formataVar( 'meio', 'post' );
	
	if ( !$ID_inscricao || !$forma ){
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= "Campos obrigatórios não preenchidos";		
	}else{
		
		// Dados da inscrição
		$xml_inscricao 	= executaPagina( 'api/', array( 'a'=>'matricula', 'metodo'=>'ver', 'ID'=>$ID_inscricao ) );
		$inscricao 		= lerXML( $xml_inscricao );
			
		if ( $inscricao->erro == 0 ){
			
			$inscricao			= $inscricao->inscricao;
			$ID_curso			= $inscricao->ID_curso;
			$ID_plano			= $inscricao->ID_plano;
			$ID_data			= $inscricao->ID_data;
			$qtd_inscritos		= count( $inscricao->participantes->participante );
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= 'Erro ao consultar a inscrição';
			goto fim;	
		}
		
		// Dados do curso
		$xml_curso 	= executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID_curso ) );
		$curso 		= lerXML( $xml_curso );
			
		if ( $curso->erro == 0 ){
			
			$curso				= $curso->curso;
			$planos_curso		= $curso->planos;
			$turmas_curso		= $curso->turmas->turma;
			
			if ( isset( $curso->planos->plano ) == false ){
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= 'Erro ao consultar os planos do curso';
				goto fim;			
			}else{
					
				$plano_escolhido = $curso->planos->xpath("plano[@codigo='".$ID_plano."']");
				if ( count( $plano_escolhido ) >= 1 ){
					$plano_escolhido = $plano_escolhido[0];	
				}else{
					$retorno["erro"] 		= 1;
					$retorno["mensagem"] 	= 'Erro ao consultar o plano escolhido';
					goto fim;		
				}
					
			}
			
			if ( isset( $curso->turmas->turma ) == false ){
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= 'Erro ao consultar turmas do curso';
				goto fim;		
			}else{
					
				$data_escolhida = $curso->turmas->xpath("turma[@codigo='".$ID_data."']");
				if ( count( $data_escolhida ) >= 1 ){
					$data_escolhida = $data_escolhida[0];	
				}else{
					$retorno["erro"] 		= 1;
					$retorno["mensagem"] 	= 'Erro ao consultar a turma escolhida';
					goto fim;			
				}
					
			}
			
			// verificar se é plano empresarial
			if ( $plano_escolhido['empresarial'] == '1' ){
				$plano_empresarial = true;	
			}else{
				$plano_empresarial = false;		
			}
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= 'Erro ao consultar o curso';
			goto fim;		
		}
		
		// Verificar preço a vista e a prazo
		if ( $qtd_inscritos <= 1 || $plano_escolhido['desconto2'] <= 0 ){
			$investimento_aprazo = ( $plano_escolhido - ($plano_escolhido * ( $singleInPeriod / 100 )) );
			
			if ( $plano_escolhido['desconto1'] > 0 ){
				$investimento_avista = ( $plano_escolhido - ($plano_escolhido * ( $singleInCash / 100 )) );
			}else{
				$investimento_avista = $investimento_aprazo;	
			}

			$investimento_avista = $investimento_avista * $qtd_inscritos;
			$investimento_aprazo = $investimento_aprazo * $qtd_inscritos;
			
		}else{
			
			$investimento_avista = ( $plano_escolhido - ($plano_escolhido * ( $groupInCash / 100 )) );
			
			if ( $plano_escolhido['desconto3'] > 0 ){
				$investimento_aprazo = ( $plano_escolhido - ($plano_escolhido * ( $groupInPeriod / 100 )) );
			}else{
				$investimento_aprazo = $investimento_avista;	
			}

			$investimento_avista = $investimento_avista * $qtd_inscritos;
			$investimento_aprazo = $investimento_aprazo * $qtd_inscritos;
		}
		
		// se escolheu plano à vista setar valor a vista, senão à prazo
		if ( $forma <= 1 ){
			$valor = $investimento_avista;
		}else{
			$valor = $investimento_aprazo;	
		}
		
		
		//atualizar a inscrição
		$sql = 	"update CURSOS_INSCRICOES set pagto_forma='".$forma."', pagto_meio='".$meio."', pagto_valor='".$valor."' where ID_inscricao=$ID_inscricao";
		executarQuery( $sql );
		
		// Atualizar o XML
		removerCache( 'inscricoes', 'inscricao_'.$ID_inscricao );
		
		$retorno["erro"] 			= 0;
		$retorno["mensagem"] 		= 'Inscrição atualizada';
		$retorno["ID_inscricao"] 	= $ID_inscricao;
		
	}


/* Efetuar pagamento via iPag */
}elseif ($metodo == 'pagamento'){

	$ID_inscricao		= formataVar( 'ID_inscricao', 'post' ); //'108'; //
	$meio				= formataVar( 'meio', 'post' ); //'amex'; //
	$cartao_nome		= formataVar( 'cartao_nome', 'post' ); //'Renan Silva'; //
	$cartao_numero		= formataVar( 'cartao_numero', 'post' ); //'34534534'; //
	$cartao_seguranca	= formataVar( 'cartao_seguranca', 'post' ); //'3445'; //
	$cartao_validade	= formataVar( 'cartao_validade', 'post' ); //'03/20'; //
	$cupom_desconto 	= formataVar( 'cupom', 'post' ); //cupomDesconto'; //
	$bf_discount 	    = formataVar( 'bf_discount', 'post' ); //cupomDesconto'; //

	
	/*$arquivo = fopen('meuarquivo.txt','w');
	fwrite($arquivo, $cupom_desconto);
	fclose($arquivo);*/

	if ( !$ID_inscricao || !$meio || !$cartao_nome || !$cartao_numero || !$cartao_seguranca || !$cartao_validade ){
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= "Campos obrigatórios não preenchidos";	
		goto fim;	
	}
	
	// Tratar a validade
	$temp_validade = explode('/',$cartao_validade);
	if ( count($temp_validade) < 2 ){
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= 'Vencimento do cartão digitado incorretamente';
		goto fim;	
	}else{
		$cartao_validade_mes = $temp_validade[0];
		$cartao_validade_ano = substr( $temp_validade[1], -2 );
	}
	
	// verificar qual o código do meio de pagamento
	$xml_meio	 	= lerXML( '../facix2/_xml/pagamentos_meio.xml' );
	$meio_escolhido = $xml_meio->xpath("registro[@ipag='".$meio."']");
	$meio_escolhido	= $meio_escolhido[0]['codigo'];
	
	//atualizar a inscrição
	$sql = 	"update CURSOS_INSCRICOES set pagto_meio='".$meio_escolhido."' where ID_inscricao=$ID_inscricao";
	executarQuery( $sql );
	
	// Dados da inscrição
	$xml_inscricao 	= executaPagina( 'api/', array( 'a'=>'matricula', 'metodo'=>'ver', 'ID'=>$ID_inscricao ) );
	$inscricao 		= lerXML( $xml_inscricao );
		
	if ( $inscricao->erro == 0 ){
		
		$inscricao			= $inscricao->inscricao;
		$forma				= $inscricao->pagamento->forma;
		$valor				= $inscricao->pagamento->valor;
		$participante		= $inscricao->participantes->participante[0];
		
		if ( $inscricao->empresa ){
			$cobrado_nome = $inscricao->empresa->razao;
			$cobrado_doc = $inscricao->empresa->cnpj;
		}else{
			$cobrado_nome = $participante->nome;
			$cobrado_doc = $participante->cpf;
		}
		$cobrado_email = $participante->email;
		
	}else{
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= 'Erro ao acessar os dados da inscrição';
		goto fim;
	}



	if($cupom_desconto=='1'){
		$valor=(strval($valor-($valor*0.1)));
		if(isset($bf_discount)){
			$oldValor = $valor;
			$valor = strval($bf_discount);
		}
	} else {
		// $valor = strval($valor);
		// if(isset($bf_discount)){
		// 	$oldValor = $valor;
		// 	$valor= strval($bf_discount);
		// }
	}

	
	//dados do pedido
	$fields = array(
		'identificacao' => $ipag_login,
		'pedido' => $ID_inscricao,
		'operacao' => 'Pagamento',
		'url_retorno' => 'http://api.methodus.com.br/ipag/'.$ID_inscricao.'/',
		'retorno_tipo' => 'xml',
		'valor' => $valor,
		'nome' => $cobrado_nome,
		'doc' => $cobrado_doc,
		'email' => $cobrado_email,
		'metodo'=> $meio,
		'parcelas'=> $forma,
		'nome_cartao'=> $cartao_nome,
		'num_cartao'=> $cartao_numero,
		'cvv_cartao'=> $cartao_seguranca,
		'mes_cartao'=> $cartao_validade_mes,
		'ano_cartao'=> $cartao_validade_ano
	);
	
	$fields_string ='';
	foreach($fields as $key=>$value) { 
		$fields_string .= $key.'='.urlencode($value).'&'; 
	}
	$fields_string = rtrim($fields_string, '&');
	
	$arquivo = fopen('meuarquivo.txt','w');
	fwrite($arquivo, $fields_string );
	fclose($arquivo);

	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, $ipag_api.'pagamento' );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields_string );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)' );
	curl_setopt( $ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1 );
	
	$retorno_ipag = curl_exec( $ch );
	curl_close( $ch );
	
	/*echo('DEBUG: '.$retorno_ipag);
	exit();*/
	
	$retorno_ipag	= lerXML( $retorno_ipag );
	
	/*
	Exemplo de retorno do iPag:
	<retorno>
	  <id_transacao>1006993069000957C34A</id_transacao>
	  <valor>1.00</valor>
	  <num_pedido>teste201703141349</num_pedido>
	  <status_pagamento>8</status_pagamento>
	  <mensagem_transacao>Transação autorizada</mensagem_transacao>
	  <metodo>amex</metodo>
	  <operadora>cielo</operadora>
	  <operadora_mensagem>Transação Autorizada</operadora_mensagem>
	  <id_librepag>1430595</id_librepag>
	  <autorizacao_id>123456</autorizacao_id>
	  <url_autenticacao></url_autenticacao>
	</retorno>
	*/
	
	$id_transacao		= '';
	$id_librepag		= '';
	$autorizacao_id		= '';
	$status_pagamento 	= '';
	$mensagem_transacao = '';
	$operadora_mensagem = '';
	
	/*$id_transacao		= '1006993069000957C34A';
	$id_librepag		= '1430595';
	$autorizacao_id		= '123456';
	$status_pagamento 	= '8';
	$mensagem_transacao = 'Transação Autorizada';
	$operadora_mensagem = 'Transação Autorizada';*/
	
	if (isset($retorno_ipag->id_transacao))
		$id_transacao 		= $retorno_ipag->id_transacao;
		
	if (isset($retorno_ipag->id_librepag))
		$id_librepag 		= $retorno_ipag->id_librepag;
		
	if (isset($retorno_ipag->autorizacao_id))
		$autorizacao_id 	= $retorno_ipag->autorizacao_id;
		
	if (isset($retorno_ipag->status_pagamento))
		$status_pagamento 		= $retorno_ipag->status_pagamento;
		
	if (isset($retorno_ipag->mensagem_transacao))
		$mensagem_transacao 	= $retorno_ipag->mensagem_transacao;
	else
		$mensagem_transacao 	= 'Serviço indisponível, tente novamente em instantes.';
		
	if (isset($retorno_ipag->operadora_mensagem))
		$operadora_mensagem 	= $retorno_ipag->operadora_mensagem;
	
	switch ($status_pagamento) {
		case 5:
		case 8:
			
			// tratar forma de pagamento
			$forma = substr( ('0'.$forma), -2 );
			$forma = 'A'.$forma;
			
			// verificar qual o código do meio de pagamento
			$xml_status 		= lerXML( '../facix2/_xml/pagamentos_cod_status.xml' );
			$status_escolhido 	= $xml_status->xpath("registro[@ipag='".$status_pagamento."']");
			$status_escolhido	= $status_escolhido[0]['codigo'];
			
			// criar o pagamento se já não existir
			$sql = "select ID_pagamento from PAGAMENTOS where ID_inscricao=".$ID_inscricao." and identificacao_pagamento='ipag".$id_transacao."'";
			$rs = abrirRs( $sql );
					
			if ( sqlsrv_num_rows( $rs ) > 0 ){	
				$pagamento 		= sqlsrv_fetch_array( $rs );
				$ID_pagamento	= $pagamento['ID_pagamento'];
				
				$sql = 	"update PAGAMENTOS set cod_status_pagamento='".$status_escolhido."', data_pagamento=getDate(), forma_pagamento='".$forma."', meio_pagamento='".$meio_escolhido."', valor_pagamento=".$valor.", 
						parcelas_pagamento=';p1:0;p2:0;p3:0;p4:0;p5:0;p6:0;p7:0;p8:0;p9:0;p10:0;p11:0;p12:0' where ID_pagamento=".$ID_pagamento;
				executarQuery( $sql );
				
			}else{
				
				$sql = 	"insert into PAGAMENTOS (ID_inscricao, identificacao_pagamento, cod_status_pagamento, data_pagamento, forma_pagamento, meio_pagamento, valor_pagamento, parcelas_pagamento) 
						values (".$ID_inscricao.",'ipag".$id_transacao."','".$status_escolhido."',getDate(),'".$forma."','".$meio_escolhido."',".$valor.",';p1:0;p2:0;p3:0;p4:0;p5:0;p6:0;p7:0;p8:0;p9:0;p10:0;p11:0;p12:0')";
				executarQuery( $sql );
				
			}
			
			liberarAcesso( $ID_inscricao );
			enviarContrato( $ID_inscricao );
			conversaoRDStation( $cobrado_email, null, $ID_inscricao, $valor );
			
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Transação autorizada. Obrigado! Em breve entraremos em contato.';
			break;
		default:
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= $mensagem_transacao.' '.$operadora_mensagem;
			break;
	}	
	
	
	

/* Ver dados da matrícula */
}elseif ($metodo == 'ver'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
		
	if (!$ID){
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= "Campos obrigatórios não preenchidos";		
	}else{
		
		// Verificar cache
		$str_xml = verificarCache( 'inscricoes', 'inscricao_'.$ID, 12 );
		
		if ($str_xml){
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Inscrição encontrada. Arquivo em cache.';
			$retorno["inscricao"] 	= $str_xml;
			
		}else{
		
			
		
			// Dados da inscrição
			$sql = "select * from CURSOS_INSCRICOES where ID_inscricao=$ID";
			$rs = abrirRs( $sql );
			
			if ( sqlsrv_num_rows( $rs ) > 0 ){
				
				$inscricao = sqlsrv_fetch_array( $rs );

				if ( is_null($inscricao['pagto_forma']) || is_null($inscricao['pagto_meio']) || is_null($inscricao['pagto_valor']) ){
					// Dados da inscrição
					$sql = "select top 1 * from PAGAMENTOS where ID_inscricao=$ID";
					$rs_pagto = abrirRs( $sql );

					if ( sqlsrv_num_rows( $rs ) > 0 ){
						$pagto = sqlsrv_fetch_array( $rs_pagto );

						if ( is_null($inscricao['pagto_forma']) ){
							$inscricao['pagto_forma'] = str_replace('B','',str_replace('A','',$pagto['forma_pagamento']));
						}
						if ( is_null($inscricao['pagto_meio']) ){
							$inscricao['pagto_meio'] = $pagto['meio_pagamento'];
						}
						if ( is_null($inscricao['pagto_valor']) ){
							$inscricao['pagto_valor'] = $pagto['valor_pagamento'];
						}
					}
				}
				
				$str_xml = 	'<codigo>'.$ID.'</codigo>
							<data>'.formataData( $inscricao['data_inscricao'], 'base' ).' '.formataHora( $inscricao['data_inscricao'], 'base' ).'</data>
							<ID_curso>'.$inscricao['ID_curso'].'</ID_curso>
							<ID_plano>'.$inscricao['ID_plano'].'</ID_plano>
							<ID_data>'.$inscricao['ID_data'].'</ID_data>
							<numero>'.$inscricao['numero_inscricao'].'</numero>
							<banir>'.$inscricao['banir_inscricao'].'</banir>
							<ID_codigo>'.$inscricao['ID_codigo'].'</ID_codigo>
							<online>'.$inscricao['online_inscricao'].'</online>
							<contrato>'.formataData( $inscricao['contrato_inscricao'], 'base' ).' '.formataHora( $inscricao['contrato_inscricao'], 'base' ).'</contrato>
							<pagamento pago="'.$inscricao['pago_inscricao'].'">
								<forma>'.$inscricao['pagto_forma'].'</forma>
								<meio>'.$inscricao['pagto_meio'].'</meio>
								<valor>'. formataMoeda( $inscricao['pagto_valor'], 'base' ) .'</valor>
								<descontos>
									<desconto>'.$inscricao['desconto_inscricao'].'</desconto>
									<desconto_dinheiro>'.$inscricao['desconto_dinheiro_inscricao'].'</desconto_dinheiro>
								</descontos>
							</pagamento>';
				
				// Dados de empresa se existir
				$sql = 	"select ID_inscricao_empresa, EMPRESAS.ID_empresa, razao_empresa, cnpj_empresa from CURSOS_INSCRICOES_EMPRESAS 
						inner join EMPRESAS on CURSOS_INSCRICOES_EMPRESAS.ID_empresa = EMPRESAS.ID_empresa where ID_inscricao=$ID";
				$rs = abrirRs( $sql );
				
				if ( sqlsrv_num_rows( $rs ) > 0 ){
					$empresa = sqlsrv_fetch_array( $rs );
					
					$str_xml .= '<empresa relacao="'.$empresa['ID_inscricao_empresa'].'">
									<codigo>'.$empresa['ID_empresa'].'</codigo>
									<razao><![CDATA['.$empresa['razao_empresa'].']]></razao>
									<cnpj>'.formataCNPJ( $empresa['cnpj_empresa'], 'tela' ).'</cnpj>
								</empresa>';	
				}
				
				// Dados dos inscritos
				$sql = 	"select ID_inscricao_usuario, USUARIOS.ID_usuario, obs_inscricao_usuario, nome_usuario, cpf_usuario, email_usuario from CURSOS_INSCRICOES_USUARIOS 
						inner join USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario = USUARIOS.ID_usuario where ID_inscricao=$ID order by nome_usuario";
				$rs = abrirRs( $sql );
				
				if ( sqlsrv_num_rows( $rs ) > 0 ){
					
					$str_xml .= 	'<participantes>';
					
					while( $participante = sqlsrv_fetch_array( $rs ) ) {
					
						$str_xml .= '<participante relacao="'.$participante['ID_inscricao_usuario'].'">
										<codigo>'.$participante['ID_usuario'].'</codigo>
										<observacoes><![CDATA['.$participante['obs_inscricao_usuario'].']]></observacoes>
										<nome><![CDATA['.$participante['nome_usuario'].']]></nome>
										<cpf>'.formataCPF( $participante['cpf_usuario'], 'tela' ).'</cpf>
										<email><![CDATA['.$participante['email_usuario'].']]></email>
									</participante>';	
								
					}		
								
					$str_xml .= 	'</participantes>';
				}
							
				
				$retorno["erro"] 		= 0;
				$retorno["mensagem"] 	= 'Inscrição encontrada';
				$retorno["inscricao"] 	= $str_xml;
				
				// gravar arquivo de cache
				gravarCache( 'inscricoes', 'inscricao_'.$ID, $str_xml );
				
			}else{
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= "Inscrição não encontrada";		
			}
			
			
		}
				
	}
	

}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

	
fim:
?>