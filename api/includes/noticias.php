<?php
/* Ver dados da notícia */
if ($metodo == 'ver'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	if ($ID){
		
		// Verificar cache
		$str_xml = verificarCache( 'noticias', 'noticia_'.$ID, 24 );
		
		if ($str_xml){
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Notícia encontrada. Arquivo em cache.';
			$retorno["noticia"] 	= $str_xml;
			
		}else{
			
			if ( is_numeric($ID) == false ){
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= 'Notícia não encontrada.';
				goto fim;
			}
			
			$sql = 	"select * from NOTICIAS where ID_noticia=$ID";
			$rs = abrirRs( $sql );
			
			if ( sqlsrv_num_rows( $rs ) > 0 ){
			
				$noticia = sqlsrv_fetch_array( $rs );
				
				if ($noticia['foto']){
					$noticia['foto'] = '/methodus/_arquivos_home/'.$noticia['foto'];
				}
				
				$str_xml = 	'<codigo>'.$ID.'</codigo>
							<status>'.$noticia['status_noticia'].'</status>
							<data>'.dataBD( $noticia["data_noticia"] ).'</data>
							<titulo><![CDATA['.$noticia['titulo_noticia'].']]></titulo>
							<chamada><![CDATA['.$noticia['chamada'].']]></chamada>
							<foto><![CDATA['.$noticia['foto'].']]></foto>
							<corpo><![CDATA['.$noticia['corpo_noticia'].']]></corpo>
							<palavras><![CDATA['.$noticia['palavras'].']]></palavras>';				
				
				
				$retorno["erro"] 		= 0;
				$retorno["mensagem"] 	= 'Notícia encontrada';
				$retorno["noticia"] 	= $str_xml;
				
				// gravar arquivo de cache
				gravarCache( 'noticias', 'noticia_'.$ID, $str_xml );
				
				
			}else{
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= "Notícia não encontrada";		
			}			
	
		}
	
	}else{
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= "Campos obrigatórios não preenchidos";		
	}
	
	// Se retornou sem erros acrescentar um ao contador de visualizações
	if ( $retorno["erro"] == 0 ){
		$sql = 	"update NOTICIAS set acessos=IsNull(acessos, 0)+1 where ID_noticia=$ID";
		executarQuery( $sql );	
	}
	
	fim:


/* Listar notícias */
}else if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	iniciaPaginacao();
	
	$pesquisa	= formataVar( 'pesquisa', 'get' );
	
	$str_xml = '';
	
	// Monta a query com as condições
	$sql_where = 	"from NOTICIAS with (NOLOCK) where status_noticia=1 and data_noticia <=getDate()";
	if ($pesquisa){
		$sql_where .= " and ( titulo_noticia like '%".$pesquisa."%' or chamada like '%".$pesquisa."%' or palavras like '%".$pesquisa."%' or corpo_noticia like '%".$pesquisa."%' )";
	}
	
	// query para contar número de registros
	$sql_conta = "select count(1) as qtd ".$sql_where;
	$rsConta = abrirRs( $sql_conta );
	
	if ( sqlsrv_num_rows( $rsConta ) > 0 ){
		
		$quantidade = sqlsrv_fetch_array( $rsConta );
		$quantidade_registros = $quantidade['qtd'];
		
		$sql = 	"SELECT * FROM ( SELECT ID_noticia, titulo_noticia, data_noticia, chamada, foto, corpo_noticia, indice = ROW_NUMBER() OVER (ORDER BY data_noticia desc)
				".$sql_where." ) AS temp WHERE indice BETWEEN ".$paginacao_inicio." AND ".$paginacao_fim;
		$rs = abrirRs( $sql );
		
		while( $registro = sqlsrv_fetch_array( $rs ) ) {
			
			if ($registro['foto']){
				$registro['foto'] = '/methodus/_arquivos_home/'.$registro['foto'];
			}
			
			if ( is_null($registro['chamada']) || empty($registro['chamada']) || $registro['chamada'] == $registro['titulo_noticia'] ){
				$registro['chamada'] = removerHtml( $registro['corpo_noticia'], 350 );
			}
			
			$str_xml	.= 	'<noticia codigo="'.$registro['ID_noticia'].'">
								<titulo><![CDATA['.$registro['titulo_noticia'].']]></titulo>
								<chamada><![CDATA['.$registro['chamada'].']]></chamada>
								<foto><![CDATA['.$registro['foto'].']]></foto>
								<data>'.dataBD( $registro["data_noticia"] ).'</data>
							</noticia>';
			
		}

		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Notícias listadas';
		$retorno["quantidade"] 	= $quantidade_registros;
		$retorno["noticias"] 	= $str_xml;
		
	}else{
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Nenhuma notícia encontrada';	
		$retorno["quantidade"] 	= 0;
	}


	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>