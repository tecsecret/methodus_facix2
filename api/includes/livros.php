<?php
/* Listar Livros */
if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';

	$ID_tema	= formataVar( 'ID_tema', 'get' );
	$pesquisa	= formataVar( 'pesquisa', 'get' );	
		
	// Verificar cache
	$str_xml = verificarCache( 'variados', 'livros', 24 );
	
	if ($str_xml){
		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Livros listados. Arquivo em cache.';
		$retorno["livros"] 		= $str_xml;
		
	}else{
		
		$str_xml = '';
		
		$sql = 	"select * from LIVROS where status_livro=1 order by titulo_livro";
		$rs = abrirRs( $sql );
		
		if ( sqlsrv_num_rows( $rs ) > 0 ){
		
			while( $registro = sqlsrv_fetch_array( $rs ) ) {
				
				if ($registro['foto']){
					$registro['foto'] = '/methodus/_arquivos_home/'.$registro['foto'];
				}
				
				$str_xml .= 	'<livro codigo="'.$registro['ID_livro'].'" ID_tema="'.$registro['ID_tema'].'">
									<data>'.dataBD( $registro["data_livro"] ).'</data>
									<titulo><![CDATA['.$registro['titulo_livro'].']]></titulo>
									<editora><![CDATA['.$registro['editora'].']]></editora>
									<chamada><![CDATA['.$registro['chamada'].']]></chamada>
									<corpo><![CDATA['.$registro['corpo_livro'].']]></corpo>
									<foto><![CDATA['.$registro['foto'].']]></foto>
								</livro>';
						
			}
			
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Livros listados';
			$retorno["livros"] 		= $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'variados', 'livros', $str_xml );
			
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= "Nenhum Livro encontrado";		
		}			

	}

	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>