<?php
/* Ver dados do vídeo */
if ($metodo == 'ver'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	if ($ID){
		
		// Verificar cache
		$str_xml = verificarCache( 'videos', 'video_'.$ID, 24 );
		
		if ($str_xml){
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Vídeo encontrado. Arquivo em cache.';
			$retorno["video"] 		= $str_xml;
			
		}else{
			
			if ( is_numeric($ID) == false ){
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= 'Vídeo não encontrado.';
				goto fim;
			}
			
			$sql = 	"select * from VIDEOS where ID_video=$ID";
			$rs = abrirRs( $sql );
			
			if ( sqlsrv_num_rows( $rs ) > 0 ){
			
				$video = sqlsrv_fetch_array( $rs );
				
				if ($video['foto_video']){
					$video['foto_video'] = '/methodus/_arquivos_videos/'.$video['foto_video'];
				}
				if ($video['arquivo_video']){
					$video['arquivo_video'] = '/facix2/ftp/Uploads/videos/'.$video['arquivo_video'];
				}
				
				$str_xml = 	'<codigo>'.$ID.'</codigo>
							<ID_tema>'.$video['ID_tema'].'</ID_tema>
							<status>'.$video['status_video'].'</status>
							<data>'.dataBD( $video["data_video"] ).'</data>
							<titulo><![CDATA['.$video['titulo_video'].']]></titulo>
							<chamada><![CDATA['.$video['chamada_video'].']]></chamada>
							<foto><![CDATA['.$video['foto_video'].']]></foto>
							<corpo><![CDATA['.$video['corpo_video'].']]></corpo>
							<arquivo><![CDATA['.$video['arquivo_video'].']]></arquivo>';				
				
				
				$retorno["erro"] 		= 0;
				$retorno["mensagem"] 	= 'Vídeo encontrado';
				$retorno["video"] 		= $str_xml;
				
				// gravar arquivo de cache
				gravarCache( 'videos', 'video_'.$ID, $str_xml );
				
				
			}else{
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= "Vídeo não encontrado";		
			}			
	
		}
	
	}else{
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= "Campos obrigatórios não preenchidos";		
	}
	
	// Se retornou sem erros acrescentar um ao contador de visualizações
	if ( $retorno["erro"] == 0 ){
		$sql = 	"update VIDEOS set acessos=IsNull(acessos, 0)+1 where ID_video=$ID";
		executarQuery( $sql );	
	}
	
	fim:


/* Listar vídeos */
}else if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	iniciaPaginacao();
	
	$ID_tema	= formataVar( 'ID_tema', 'get' );
	$pesquisa	= formataVar( 'pesquisa', 'get' );
	
	$str_xml = '';
	
	// Monta a query com as condições
	$sql_where = 	"from VIDEOS with (NOLOCK) where status_video=1 and data_video <=getDate()";
	if ($ID_tema && is_numeric($ID_tema)){
		$sql_where .= ' and ID_tema='.$ID_tema;
	}
	if ($pesquisa){
		$sql_where .= " and ( titulo_video like '%".$pesquisa."%' or chamada_video like '%".$pesquisa."%' or corpo_video like '%".$pesquisa."%' )";
	}
	
	// query para contar número de registros
	$sql_conta = "select count(1) as qtd ".$sql_where;
	$rsConta = abrirRs( $sql_conta );
	
	if ( sqlsrv_num_rows( $rsConta ) > 0 ){
		
		$quantidade = sqlsrv_fetch_array( $rsConta );
		$quantidade_registros = $quantidade['qtd'];
		
		$sql = 	"SELECT * FROM ( SELECT ID_video, titulo_video, data_video, ID_tema, chamada_video, foto_video, corpo_video, indice = ROW_NUMBER() OVER (ORDER BY data_video desc)
				".$sql_where." ) AS temp WHERE indice BETWEEN ".$paginacao_inicio." AND ".$paginacao_fim;
		$rs = abrirRs( $sql );
		
		while( $registro = sqlsrv_fetch_array( $rs ) ) {
			
			if ($registro['foto_video']){
				$registro['foto_video'] = '/methodus/_arquivos_videos/'.$registro['foto_video'];
			}
			
			if ( is_null($registro['chamada_video']) || empty($registro['chamada_video']) || $registro['chamada_video'] == $registro['titulo_video'] ){
				$registro['chamada_video'] = removerHtml( $registro['corpo_video'], 350 );
			}
			
			$str_xml	.= 	'<video codigo="'.$registro['ID_video'].'" ID_tema="'.$registro['ID_tema'].'">
								<titulo><![CDATA['.$registro['titulo_video'].']]></titulo>
								<chamada><![CDATA['.$registro['chamada_video'].']]></chamada>
								<foto><![CDATA['.$registro['foto_video'].']]></foto>
								<data>'.dataBD( $registro["data_video"] ).'</data>
							</video>';
			
		}

		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Vídeos listados';
		$retorno["quantidade"] 	= $quantidade_registros;
		$retorno["videos"]	 	= $str_xml;
		
	}else{
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Nenhum Vídeo encontrado';	
		$retorno["quantidade"] 	= 0;
	}


	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>