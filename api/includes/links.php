<?php
/* Listar links */
if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';

		
	// Verificar cache
	$str_xml = verificarCache( 'variados', 'links', 24 );
	
	if ($str_xml){
		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Links listados. Arquivo em cache.';
		$retorno["links"] 		= $str_xml;
		
	}else{
		
		$str_xml = '';
		
		$sql = 	"select * from LINKS where status_link=1 order by titulo_link";
		$rs = abrirRs( $sql );
		
		if ( sqlsrv_num_rows( $rs ) > 0 ){
		
			while( $registro = sqlsrv_fetch_array( $rs ) ) {
				
				if ( substr( $registro['url_link'], 0, 4 ) != 'http' ){
					$registro['url_link'] = 'http://'.$registro['url_link'];
				}
				
				$str_xml .= 	'<link codigo="'.$registro['ID_link'].'">
									<data>'.dataBD( $registro["data_link"] ).'</data>
									<titulo><![CDATA['.$registro['titulo_link'].']]></titulo>
									<url><![CDATA['.$registro['url_link'].']]></url>
									<corpo><![CDATA['.$registro['corpo_link'].']]></corpo>
								</link>';
						
			}
			
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Links listados';
			$retorno["links"] 		= $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'variados', 'links', $str_xml );
			
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= "Nenhum link encontrado";		
		}			

	}

	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>