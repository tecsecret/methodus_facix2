<?php
/* Ver dados do curso */
if ($metodo == 'cadastro'){


	$nome		= formataVar( 'nome', 'post' );
	$email		= formataVar( 'email', 'post' );;
	$telefone	= formataVar( 'telefone', 'post' );
	$celular	= formataVar( 'celular', 'post' );
	$ID_curso	= formataVar( 'curso', 'post' );
	$conheceu 	= formataVar( 'conheceu', 'post' );
	$perfil		= formataVar( 'perfil', 'post' );
	$origem		= formataVar( 'origem', 'post' );
	$webhook	= formataVar( 'webhook', 'post' );

	if ( strpos( $email, 'winter@example.com' ) !== false ){
		exit('ERRO');
	}
	
	// Verificar campos obrigatórios
	if ( empty($nome) || empty($email) || empty($origem) || empty($ID_curso) ){
		
		$retorno["erro"] = 1;
		$retorno["mensagem"] = 'Campos obrigatórios não preenchidos';
		
	}else{		
		
		// verificar se aluno já existe
		$sql = "select top 1 ID_usuario, entrou_usuario, interesses_usuario from USUARIOS where email_usuario='".$email."' order by ID_usuario desc";
		$rs = abrirRs( $sql );
				
		if ( sqlsrv_num_rows( $rs ) > 0 ){	
			$aluno 					= sqlsrv_fetch_array( $rs );
			$ID						= $aluno['ID_usuario'];
			$campos["entrou"] 		= $aluno['entrou_usuario'].' '.$origem.';';
			$campos["interesses"] 	= $aluno['interesses_usuario'].'id'.$ID_curso.';';
			
		}else{
			$campos["nome"] 		= $nome;							
			$campos["email"] 		= $email;	
			$campos["entrou"] 		= ''.$origem.';';
			$campos["interesses"] 	= 'id'.$ID_curso.';';
			$ID						= null;
		}
		
		//campos comuns 
		if ( !empty($perfil) )
			$campos["ID_perfil"] 	= $perfil;	
		if ( !empty($conheceu) )
			$campos["conheceu"] 	= $conheceu;
		
		// Incluir ou atualizar cadastro do aluno				
		$usuario = new Usuario( $campos );
		$cadastro = $usuario->Cadastro( $ID );
		
		
		// verificar dados do curso escolhido, para Mailing e LOG
		$xml_curso 	= executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID_curso ) );
		$curso 		= lerXML( $xml_curso );
			
		if ( $curso->erro == 0 ){
			
			$curso				= $curso->curso;
			$titulo_curso		= $curso->titulo;
			$variaveis_curso	= $curso->variaveis;
			$perfis_curso		= $curso->perfis;
				
		}else{
			$titulo_curso = 'Curso não encontrado';	
		}
		
		// Enviar email para Methodus avisando sobre cadastro
		if ( is_null($ID) )
			$mensagem	=	'Um novo cadastro foi feito através do website. ';
		else
			$mensagem	=	'Uma pessoa já cadastrada no Facix, preencheu um formulário do website e atualizou seus dados. ';
			
			
		$mensagem	.=	'Seguem mais informações abaixo:<BR /><BR /><table border="0" cellpadding="3" cellspacing="0">
						<TR><TD><b>Nome: </b></TD><TD>'.$nome.'</TD></TR>
						<TR><TD><b>E-mail: </b></TD><TD><a href="mailto:'.$email.'">'.$email.'</a></TD></TR>
						<TR><TD><b>Área de interesse: </b></TD><TD>'.$titulo_curso.'</TD></TR>
						<TR><TD><b>Origem: </b></TD><TD>'.$origem.'</TD></TR>';
		
		$titulo_perfil = null;
		if ( !empty($perfil) && isset($perfis_curso) ){
			
			$titulo_perfil 	= $perfis_curso->xpath("//perfil[@codigo='".$perfil."']");
			if (count( $titulo_perfil )>0){
				$titulo_perfil = $titulo_perfil[0];
				$mensagem	.=	'<TR><TD><b>Perfil: </b></TD><TD>'.$titulo_perfil.'</TD></TR>';
			}else{
				$titulo_perfil = null;	
			}
		}
		
			
		$mensagem	.=	'</table>';
		
		enviaEmail( 'Cadastro - '.$origem, $mensagem, $email_principal, 'Methodus', $email, $nome, 1 );
		
		//Enviar dados para RDStation
		if ( empty($webhook) ){
			$retorno_rd = cadastrarRdstation( strtolower($origem), $nome, $email, $titulo_perfil, $titulo_curso );
		}
		
		//echo $retorno_rd;
		//exit();
		
		$retorno["erro"] = 0;
		$retorno["mensagem"] = 'Cadastro realizado com sucesso. ';
		
	}
	
	

}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}
?>