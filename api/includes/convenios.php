<?php
$tipo			= formataVar( 'tipo', 'post' );
$razao			= formataVar( 'razao', 'post' );
$nome			= formataVar( 'nome', 'post' );
$cargo			= formataVar( 'cargo', 'post' );
$endereco		= formataVar( 'endereco', 'post' );
$cep			= formataVar( 'cep', 'post' );
$cidade			= formataVar( 'cidade', 'post' );
$estado			= formataVar( 'estado', 'post' );
$email			= formataVar( 'email', 'post' );
$telefone		= formataVar( 'telefone', 'post' );
$funcionarios	= formataVar( 'funcionarios', 'post' );
$cursos			= formataVar( 'cursos', 'post' );

// Verificar campos obrigatórios
if ( empty($tipo) || empty($razao) || empty($nome) || empty($cargo) || empty($endereco) || empty($cep) || empty($cidade) || empty($estado) || empty($email) ){
	
	$retorno["erro"] = 1;
	$retorno["mensagem"] = 'Campos obrigatórios não preenchidos';
	
}else{
	
	if ($tipo == "In Company"){
		$id_tipo = 1;
	}else{
		$id_tipo = 2;
	}
	
	// Tratar cursos
	$str_cursos = '';
	foreach( $cursos as $curso ) {
		$str_cursos .= ' '.$curso.',';
	}
	$str_cursos = trim( $str_cursos, ',' );
	
	$mensagem	=	'Uma empresa solicitou '.$tipo.' através do web site.<BR /><BR /><table border="0" cellpadding="3" cellspacing="0">
					<TR><TD><b>Empresa: </b></TD><TD>'. $razao .'</TD></TR>
					<TR><TD><b>Nome: </b></TD><TD>'. $nome .'</TD></TR>
					<TR><TD><b>Cargo: </b></TD><TD>'. $cargo .'</TD></TR>
					<TR><TD><b>Endereço: </b></TD><TD>'. $endereco .'</TD></TR>
					<TR><TD><b>CEP: </b></TD><TD>'. $cep .'</TD></TR>
					<TR><TD><b>Cidade: </b></TD><TD>'. $cidade .'</TD></TR>
					<TR><TD><b>Estado: </b></TD><TD>'. $estado .'</TD></TR>
					<TR><TD><b>E-mail: </b></TD><TD><a href="mailto:'. $email .'">'. $email .'</a></TD></TR>
					<TR><TD><b>Telefone: </b></TD><TD>'. $telefone .'</TD></TR>
					<TR><TD><b>Quantidade de funcionários: </b></TD><TD>'. $funcionarios .' funcionários</TD></TR>
					<TR><TD valign="top"><b>Cursos de interesse: </b></TD><TD>'. $str_cursos .'</TD></TR>
					</table>';
	
	// Enviar e-mail
	enviaEmail( 'Empresa - '.$tipo, $mensagem, $email_principal, 'Methodus', $email, $nome, 1 );
	
	// Formatar variáveis
	$telefone 	= formataTelefone( $telefone );
	$telefone 	= $telefone["ddd"].$telefone["numero"];
	$cep 		= formataCEP( $cep, 'base' );
	
	
	// Cadastrar empresa no Facix	
	$sql = "select top 1 ID_empresa from EMPRESAS where razao_empresa like '%".$razao."%' order by ID_empresa desc";
	$rs = abrirRs( $sql );
			
	if ( sqlsrv_num_rows( $rs ) <= 0 ){	
		$sql = 	"insert into EMPRESAS (data_empresa,razao_empresa,nome_empresa,cargo_empresa,endereco_empresa,cep_empresa,cidade_empresa,estado_empresa,tipo_empresa,funcionarios_empresa,interesses_empresa, email_empresa, telefone_empresa) _
				 values ('".dataBD('agora')."','".$razao."','".$nome."','".$cargo."','".$endereco."','".$cep."','".$cidade."','".$estado."',".$id_tipo.",'".$funcionarios."','".$str_cursos."','".$email."','".$telefone."')";
		executarQuery( $sql );
	}
	
	
	$retorno["erro"] = 0;
	$retorno["mensagem"] = 'Dados enviados com sucesso';
	
}

?>