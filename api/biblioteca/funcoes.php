<?php
/* FUNÇÕES BÁSICAS PARA TODAS AS PÁGINAS */
ob_start();

// Codigicação em UTF-8
//header('Content-type: text/html; charset=ISO-8859-1',true);
header('Content-Type: text/html; charset=UTF-8',true); 
//header('Content-type: text/html; charset=SQL_Latin1_General_CP850_CI_AI');

// Para interromper o código no caso de enviarem alguma injeção de sql
pararInjection();

// Conecta ao banco de dados
function conexaoBD(){
	
	global $BD_servidor, $BD_base, $BD_login, $BD_senha, $IP_servidor, $api_chave, $email_principal, $ipag_api, $ipag_login;
	include_once 'conexao.php';

	$config 	= array("Database" => $BD_base, "UID" => $BD_login, "PWD" => $BD_senha);
	$conexao 	= sqlsrv_connect($BD_servidor, $config);

	if ($conexao){
		return $conexao; //"Conectato com sucesso!";
	}
	else{
		
		die( print_r( sqlsrv_errors(), true));
		return 0; //"Não foi possível conectar ao banco de dados!";
	}
	
}

// Função para permitir ou não acesso às APIs do siet
function permissaoApi(){
	
	global $IP_servidor, $api_chave;
	
	$servidor_referencia 	= serverReferer();
	$servidor_atual			= $_SERVER['SERVER_NAME'];
	$IP_requisicao			= ipCliente();

	if ( ( strpos( $servidor_referencia, $servidor_atual ) === false && strpos( $servidor_referencia, 'homologacao.methodus.com.br' ) === false && strpos( $servidor_referencia, 'methodus.com.br' ) === false) && strpos( $IP_servidor, $IP_requisicao ) === false ){
		$api_chave_enviada = formataVar( 'api_chave' );
		
		if ( $api_chave != $api_chave_enviada ){
			ob_end_clean();
			header('HTTP/1.0 404 Not Found');
    		echo "<h1>Error 404 Not Found</h1>";
			echo "The page that you have requested could not be found.ggg";
			//echo '<HR /><h1>DEBUG:</h1>Servidor de referência: '.$servidor_referencia.' | Servidor da API: '.$servidor_atual.'<BR />';
			//echo 'IP do servidor: '.$IP_servidor.' | IP do cliente: '.$IP_requisicao.'';
			//echo 'chave enviada = '.$api_chave_enviada . ' Chave='.$api_chave;
			exit;	
		}
	}
}

function serverReferer(){
	
	if(isset($_SERVER['HTTP_REFERER'])){
		// Use given referrer
		$referrer   = $_SERVER['HTTP_REFERER'];
	
	} else {
		// No referrer
		$referrer   = '';
	}
	
	return $referrer;
		
}

// IP do computador de acesso
function ipCliente() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

// Coloca uma query SQL em um recordset
function abrirRs( $sql, $params = array() ){
	
	global $conexao;
	
	$options 	= array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
	$rs 		= sqlsrv_query($conexao, $sql, $params, $options);
	
	return $rs;
}

// Executa uma query SQL
function executarQuery( $sql ){
	
	global $conexao;
	
	$params 	= array();
	$options 	= array();
	$rs 		= sqlsrv_query($conexao, $sql, $params, $options);
	
	return sqlsrv_rows_affected( $rs );
}

// Função para criar uma query em SQL
function querySQL( $tabela, $dados, $id ){
	
	if ( isset($id) ){
		// query para Update
		
		$sql = "update ".$tabela." set ";
		
		foreach ($dados as $campo => $valor) {
          	$sql.= " ".$campo."='".$valor."',";
        }
		
		$sql = substr( $sql, 0, (strlen($sql)-1) );
		
		$sql.= " where";
		
		foreach ($id as $campo => $valor) {
			if ($valor == "null" || $valor == "getDate()")
				$sql.= " and ".$campo."=".$valor." ";
			else
        		$sql.= " and ".$campo."='".$valor."' ";
        }
		
		$sql = str_replace( "where and", "where", $sql );
		
	}else{
		// query para Insert
		
		$sql = "insert into ".$tabela." ( ";
		
		foreach ($dados as $campo => $valor) {
          	$sql.= " ".$campo.",";
        }
		
		$sql = substr( $sql, 0, (strlen($sql)-1) );
		
		$sql.= " ) values (";
		
		foreach ($dados as $campo => $valor) {
			if ($valor == "null" || $valor == "getDate()")
				$sql.= " ".$valor.",";
			else
        		$sql.= " '".$valor."',";
        }
		
		$sql = substr( $sql, 0, (strlen($sql)-1) );
		
		$sql.= " )";
		
	}
	
	return $sql;
	
}

// Função para limpar uma variável e prevenir SQL Injection
function formataVar( $variavel, $metodo='*', $tipo='normal' ) { 
	
	// apenas para testes
	//$metodo='*';
	
	if ($metodo == 'post'){
		$texto = @$_POST[$variavel];
	}elseif ($metodo == 'get'){
		$texto = @$_GET[$variavel];
	}elseif ($metodo == 'var'){
		$texto = $variavel;
	}else{
		if (isset($_GET[$variavel])) {
		  $texto = @$_GET[$variavel];
		}else{
		  $texto = @$_POST[$variavel];
		}	
	}
	
	switch ($tipo){
		
		case 'textarea':
			
			$texto = str_replace( "'", "''", $texto );
			$texto = str_replace( '"', '""', $texto );
			$texto = str_replace( "<", "&lt;", $texto );
			$texto = str_replace( ">", "&gt;", $texto );
			$texto = str_replace( chr(10), "<BR />", $texto );
			
			break;
		
		case 'html':
			
			$texto = str_replace( "'", "''", $texto );
			
			break;
		
		case 'tela':
			
			$texto = str_replace( "<BR>", chr(10), $texto );
			$texto = str_replace( "<BR />", chr(10), $texto );
			$texto = str_replace( "<br>", chr(10), $texto );
			$texto = str_replace( "<br />", chr(10), $texto );
			
			break;
		
		// Se é uma variável normal
		default:
			$texto = str_replace( "'", "''", $texto );
			$texto = str_replace( '"', '""', $texto );
			$texto = str_replace( "<", "&lt;", $texto );
			$texto = str_replace( ">", "&gt;", $texto );
	
	}
	
	pararInjection($texto);

    return $texto; 
} 

// Função para limpar uma variável e prevenir SQL Injection
function pararInjection($texto=null) { 
	
	$palavras_malditas = array( 'select ', 'update ', 'drop table', 'insert into', 'show ', ' table ', 'cast(', 'delete from ', 'xp_', 'exec ', 'exec(', 'declare ', 'truncate', 'case when', '1=1--' );
	$str_erro = '';
	
	if ( $texto ){
		
		// se é um array, implodir em uma string separada por virgulas
		if ( is_array($texto) ){
			$texto = implode(',',$texto);	
		}
		
		$texto = strtolower($texto);
		foreach($palavras_malditas as $palavra){
			if ( strpos( $texto, $palavra ) ){
				$str_erro .= '- Carregou uma variável com texto: '.$texto.' | Palavra proibida: '.$palavra.PHP_EOL;	
			}
		}
	
	}else{
		
		//POST
		foreach ($_POST as $key => $value) {
			
			// se é um array, implodir em uma string separada por virgulas
			if ( is_array($value) ){
				$texto = implode(',',$value);	
			}else{
				$texto = $value	;
			}
			
			$texto = strtolower($texto);
			foreach($palavras_malditas as $palavra){
				if ( strpos( $texto, $palavra ) ){
					$str_erro .= 'POST: '.$key.': '.$texto.' | Palavra proibida: '.$palavra.PHP_EOL;		
				}
			}
		}
		
		//GET
		foreach ($_GET as $key => $value) {
			
			// se é um array, implodir em uma string separada por virgulas
			if ( is_array($value) ){
				$texto = implode(',',$value);	
			}else{
				$texto = $value	;
			}
			
			$texto = strtolower($texto);
			foreach($palavras_malditas as $palavra){
				if ( strpos( $texto, $palavra ) ){
					$str_erro .= 'GET: '.$key.': '.$texto.' | Palavra proibida: '.$palavra.PHP_EOL;	
				}
			}
		}
		
		//COOKIES
		foreach ($_COOKIE as $key => $value) {
			
			// se é um array, implodir em uma string separada por virgulas
			if ( is_array($value) ){
				$texto = implode(',',$value);	
			}else{
				$texto = $value	;
			}
			
			$texto = strtolower($texto);
			foreach($palavras_malditas as $palavra){
				if ( strpos( $texto, $palavra ) ){
					$str_erro .= 'COOKIE: '.$key.': '.$texto.' | Palavra proibida: '.$palavra.PHP_EOL;
				}
			}
		}
			
	}
	
	// se tem erro, não executar página e gravar LOG
	if ( empty($str_erro)==false ){
		//criarLog( $str_erro );
		exit('ERRO: falha nos inputs de dados.');	
	}
}


// Formatar CPF
function formataCPF( $cpf, $acao='base' ){
	
	if ($cpf){
	
		if ($acao == 'base'){
			$temp	= $cpf;
			$temp	= str_replace('.','',$temp);
			$temp	= str_replace('-','',$temp);
			
		}else{
			$temp	= formataCPF( $cpf );
			if ( strlen($temp) < 11 ){
				$temp = 'CPF inválido';	
			}else{
				$temp = substr($temp,0,3).'.'.substr($temp,3,3).'.'.substr($temp,6,3).'-'.substr($temp,9,2);	
			}
		}
		
		return $temp;
	
	}else{
		return '';	
	}
}

// Formatar CNPJ
function formataCNPJ( $cnpj, $acao='base' ){
	
	if ($cnpj){
	
		if ($acao == 'base'){
			$temp	= $cnpj;
			$temp	= str_replace('.','',$temp);
			$temp	= str_replace('-','',$temp);
			$temp	= str_replace('/','',$temp);
			
		}else{
			$temp	= formataCNPJ( $cnpj );
			if ( strlen($temp) < 14 ){
				$temp = 'CNPJ inválido';	
			}else{
				$temp = substr($temp,0,2).'.'.substr($temp,2,3).'.'.substr($temp,5,3).'/'.substr($temp,8,4).'-'.substr($temp,12,2);	
			}
		}
		
		return $temp;
	
	}else{
		return '';	
	}
}

// Formatar CEP
function formataCEP( $cep, $acao='base' ){
	
	if ($cep){
	
		if ($acao == 'base'){
			$temp	= $cep;
			$temp	= str_replace('.','',$temp);
			$temp	= str_replace('-','',$temp);
			
		}else{
			$temp	= formataCEP( $cep );
			if ( strlen($temp) < 8 ){
				$temp = 'CEP inválido';	
			}else{
				$temp = substr($temp,0,5).'-'.substr($temp,5,3);	
			}
		}
		
		return $temp;
	
	}else{
		return '';	
	}
}

// Função para exibir data
function formataData( $data=null, $tipo='tela' ){
	
	if ( $data ){
	
		if ( $data == 'agora' )
			$data=null;
			
		switch ($tipo){
			case 'tela':
				$tipo = 'd/m/Y';
				break;
			case 'eua':
				$tipo = 'm/d/Y';
				break;
			case 'base':
				$tipo = 'Y-m-d';
				break;
		}
		
		if ($data){
			if ( is_string($data) ){
				$data = strtotime( $data );
				return date( $tipo, $data );
			}else{
				if (is_a($data, 'DateTime')==false)  //if ( !($data instanceof DateTime) )
					return date_format( new DateTime($data), $tipo );
				else
					return date_format( $data, $tipo );
			}
		}else
			return date( $tipo );
	
	}else{
		return '';	
	}
		
}

// Função para exibir hora
function formataHora( $hora=null, $tipo='tela' ){
	
	if ( $hora ){
	
		if ( $hora == 'agora' )
			$hora=null;
			
		switch ($tipo){
			case 'tela': // 23h45
				$tipo = 'H\hi';
				break;
			case 'completo': // 23h45m12s
				$tipo = 'H\hi\ms\s';
				break;
			case 'base': // 23:45:12
				$tipo = 'H:i:s';
				break;
		}
		
		if ($hora){
			if ( is_string($hora) ){
				$hora = strtotime( $hora );
				return date( $tipo, $hora );
			}else{
				if (is_a($hora, 'DateTime')==false)
					return date_format( new DateTime($hora), $tipo );
				else
					return date_format( $hora, $tipo );
			}
		}else
			return date( $tipo );
	
	}else{
		return '';	
	}
		
}

// Função para formatar data e hora para base de dados
function dataBD( $data=null ){
	
	if ( $data ){
		//exit('teste: '.strpos( substr($data,0,5), '-' ) );
		if ( is_string($data) && strpos( substr($data,0,5), '-' ) === FALSE && $data != 'agora' ) //if ( is_string($data) && validarData($data) == false )
			$data = converteData($data);
			
		return formataData( $data, 'base' ).' '.formataHora( $data, 'base' );
	
	}else{
		return '';	
	}
		
}


//Validar se é data
function validarData($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

// Converter data brasileira para universal
function converteData( $data=null ){
	
	if ($data){
		//$temp = substr($data,3,2).'/'.substr($data,0,2).'/'.substr($data,6,4);
		$temp = substr($data,6,4).'-'.substr($data,3,2).'-'.substr($data,0,2);
		
		// se tem hora
		if ( strlen($data) > 10 ){
			$temp.= ' '.substr($data,11,2).':'.substr($data,14,2).':'.substr($data,17,2);
		}
		
		return $temp;
	}else{
		return null;
	}
		
}

// Função para verificar se arquivo existe
function checaArquivo( $arquivo ){
	if (file_exists($arquivo)) {
		return 1;
	} else {
		return 0;
	}	
}

// Função para remover um arquivo
function apagarArquivo( $arquivo ){
	
	if (checaArquivo($arquivo)) {
		
		if (unlink($arquivo)){
			return 1;
		}else{
			return 0;
		}
		
	} else {
		return -1;
	}	
	
}

// Função para enviar um e-mail
function enviaEmail( $assunto=NULL, $conteudo=NULL, $paraEmail=NULL, $paraNome=NULL, $deEmail=NULL, $deNome=NULL, $layout=1, $anexo=NULL ){

	if ( strpos( $deEmail, 'winter@example.com' ) !== false || strpos( $conteudo, 'winter@example.com' ) !== false ){
		exit('ERRO');
	}
	
	global $emailSMTP, $emailPorta, $emailLogin, $emailConta, $emailSenha, $emailCabecalho, $emailRodape;
	include_once 'variaveis_email.php';
	
	if(!isset($assunto) || !$assunto)		{ $assunto = 'SEM ASSUNTO'; }
	if(!isset($conteudo) || !$conteudo)		{ $conteudo = ' '; }
	if(!isset($paraEmail) || !$paraEmail)	{ $paraEmail = $emailConta; }
	if(!isset($paraNome) || !$paraNome)		{ $paraNome = $paraEmail; }
	if(!isset($deEmail) || !$deEmail)		{ $deEmail = $emailConta; }
	if(!isset($deNome) || !$deNome)			{ $deNome = $emailConta; }	

	include_once('phpMailer/class.phpmailer.php');
	
	$mail = new PHPMailer();
	
	$mail->IsSMTP();
	
	$mail->Host       = $emailSMTP;
	$mail->SMTPDebug  = false;				// false. para testes, usar 2
	$mail->SMTPAuth   = true;			// Autenticação de SMTP
	//$mail->SMTPSecure = 'tls';
	$mail->SMTPKeepAlive = true;  
	$mail->Mailer 	  = "smtp"; 
	$mail->CharSet 	  = 'utf-8';
	$mail->Port       = $emailPorta;
	$mail->Username   = $emailLogin;
	$mail->Password   = $emailSenha;
	
	// Para Skymail, o from precisa sempre ser o e-mail  de autenticação
	$mail->SetFrom($emailConta, $deNome); //$mail->SetFrom($deEmail, $deNome);
	$mail->AddReplyTo($deEmail, $deNome);
	$mail->AddAddress($paraEmail, $paraNome);
	$mail->Subject = $assunto;
	
	//anexando todos os arquivos para envio
	if(isset($anexo) && $anexo){
		
		if (is_array($anexo)){
		
			foreach($anexo as $key => $value) {
				$mail->AddAttachment($value);
			}
		
		}else{
			$mail->AddAttachment($anexo);
		}
	}
	
	// Se precisa adicionar o layout
	if ( $layout == 1 ){
		$conteudo = $emailCabecalho . $conteudo . $emailRodape;
	}
	
	$mail->IsHTML(true);
	$mail->MsgHTML( $conteudo );

	if($mail->Send())
		return 1;
	else
		return 0;
}

// Função para redimensionar e/ou cortar uma imagem
function tratarImagem( $arquivo, $salvar, $largura, $altura, $modo=2 ){
	
	require_once '../biblioteca/wideimage/WideImage.php';
	
	$arquivo = corrigePastaCliente( $arquivo );
	$salvar = corrigePastaCliente( $salvar );
	
	if ( checaArquivo( $arquivo ) ){
		
		$image = WideImage::load( $arquivo );
		list($largura_original, $altura_original) = getimagesize( $arquivo );
		
		// esticar desproporcionalmente
		if ( $modo == 1 || $modo == 'esticar' ){
			
			$image = $image->resize( $largura, $altura, 'fill' );
			
		// esticar proporcionalmente
		}elseif ( $modo == 2 || $modo == 'proporcional' ){
			
			$image = $image->resize( $largura, $altura );
			
		// recortar
		}elseif ( $modo == 3 || $modo == 'recortar' ){
			
			$prop_original 	= ( $altura_original / $largura_original );
			$prop_nova 		= ( $altura / $largura );
			
			if ( $prop_original > $prop_nova ) {
				
				$esquerda 	= 0;
				$topo 		= ( ( $altura_original * $largura / $largura_original ) - $altura ) / 2;
				
			}else{
				
				$esquerda 	= ( ( $largura_original * $altura / $altura_original ) - $largura ) / 2;
				$topo 		= 0;
				
			}
			
			$image = $image->resize( $largura, $altura, 'outside' );
			$image = $image->crop($esquerda, $topo, $largura, $altura);
					
		}
		
		$image->saveToFile( $salvar );
		return 1;
		
	}else{
		return 0;	
	}
	
}

// Ler XML
function lerXML( $arquivo ){
	
	if ( strpos($arquivo,'<?xml') === false ){
	
		if ( checaArquivo($arquivo) ) {
			$xml = simplexml_load_file($arquivo);
			return($xml);
		} else {
			return(0);
		}
	
	}else{
		$xml = simplexml_load_string($arquivo);
		return($xml);	
	}
	
}

// Converter charset de uma string
function charSet( $texto, $tipo='iso' ){
	
	if ( $tipo == 'utf8' ){
		$texto = iconv( "ISO-8859-1", "UTF-8//TRANSLIT", $texto );
	}else{
		$texto = iconv( "UTF-8", "ISO-8859-1//TRANSLIT", $texto );
	}
	
	return $texto;
		
}

// Diferença entre datas
function diferencaDatas( $data1, $data2, $periodo='horas' ){
	
	if ( $data1 && $data2 ){
		
		$data1 = strtotime( $data1 );
		$data2 = strtotime( $data2 );
		//return $data2;
		switch ($periodo) {
			case 'dias':
				$fator = 86400;
				break;
			case 'meses':
				$fator = 2592000;
				break;
			case 'anos':
				$fator = 31536000;
				break;
			case 'horas':
				$fator = 3600;
				break;
			case 'minutos':
				$fator = 60;
				break;
			case 'segundos':
				$fator = 1;
				break;
		}
		
		$diferenca = floor( ($data2-$data1) / $fator );
		return($diferenca);
		
	}else{
		return(0);	
	}
	
}

// Adiciona dias a uma data
function dateAdd( $data, $periodo, $tipo="d" ){
	
	$data = dataBD($data,'base');
	
	$data_temp = strtotime($data);
	
	if ( $tipo == 'd' ){
		$periodo = 24 * 3600 * $periodo;
	}else if ( $tipo == 'm' ){
		return adicionaMes( $data, $periodo );
		//$periodo = 24 * 3600 * 30 * $periodo;
	}else if ( $tipo == 'y' ){
		$periodo = 24 * 3600 * 365 * $periodo;	
	}else if ( $tipo == 'h' ){
		$periodo = 3600 * $periodo;	
	}else if ( $tipo == 'n' ){
		$periodo = 60 * $periodo;
	}
	
	$data_temp = $data_temp + $periodo;
	
	$data_temp = date('Y-m-d H:i:s', $data_temp );
	
	return $data_temp;
	
}

// Para adicionar meses em uma data, considerando meses com dias que não existem
function adicionaMes( $data, $mes=0 ){

	$monthToAdd = $mes;
	
	$d1 = DateTime::createFromFormat('Y-m-d H:i:s', $data);
	
	$year = $d1->format('Y');
	$month = $d1->format('n');
	$day = $d1->format('d');
	
	$year += floor($monthToAdd/12);
	$monthToAdd = $monthToAdd%12;
	$month += $monthToAdd;
	if($month > 12) {
		$year ++;
		$month = $month % 12;
		if($month === 0)
			$month = 12;
	}
	
	if(!checkdate($month, $day, $year)) {
		$d2 = DateTime::createFromFormat('Y-n-j', $year.'-'.$month.'-1');
		$d2->modify('last day of');
	}else {
		$d2 = DateTime::createFromFormat('Y-n-d', $year.'-'.$month.'-'.$day);
	}
	$d2->setTime($d1->format('H'), $d1->format('i'), $d1->format('s'));
	return $d2->format('Y-m-d H:i:s');

}



// Executar uma página externa e entregar o resultado
function executaPagina( $pagina, $dados='', $metodo='get', $charset='utf-8' ){
	
	global $url_site;
	
	if (strpos($pagina,'http://') === false && strpos($pagina,'https://') === false){
		if ($url_site)
			$pagina = $url_site."/../".$pagina;
		else
			$pagina = "http://".$_SERVER['SERVER_NAME']."/".$pagina;
	}
	
	if ($metodo == 'post'){
		
		$opts = array(
			'http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded; charset='.$charset,
				'content' => http_build_query($dados)
			),
			'ssl' => array(
				'verify_peer'=>false,
				'verify_peer_name'=>false
			)
		);
		
		$context 	= stream_context_create($opts);
		$resultado 	= file_get_contents($pagina, false, $context);
		
	}else{
		
		if ( $dados ){
			$str_query = "?";
			foreach ($dados as $campo => $valor) {
				$str_query.= "&".$campo."=".$valor;
			}
		
			$str_query = str_replace('?&','?',$str_query);
			$str_query = preg_replace("/ /", "%20", $str_query);
		}else{
			$str_query = '';
			$dados = array();
		}
		
		$opts = array(
			'http' => array(
				'method'  => 'GET',
				'header'  => 'Content-type: text/html; charset='.$charset,
				'content' => http_build_query($dados)
			),
			'ssl' => array(
				'verify_peer'=>false,
				'verify_peer_name'=>false
			)
		);
		
		$context 	= stream_context_create($opts);
		$resultado = file_get_contents($pagina.$str_query, false, $context);
	}
	
	return $resultado;
		
}


// Verificar arquivo de cache
function verificarCache( $tipo, $nome_arquivo, $periodo=6 ){
	
	global $forcar;
	
	if ( $tipo && $nome_arquivo ){
		
		$arquivo_cache = 'xml/'.$tipo.'/'.$nome_arquivo.'.xml';
		
		if ( checaArquivo( $arquivo_cache ) ){
			
			$data_modificacao = dataBD( date('Y-m-d H:i:s', filemtime( $arquivo_cache ) ) );
			$diferenca = diferencaDatas( $data_modificacao, dataBD('agora'), 'horas' );
			//exit( 'Data do arquivo: '.$data_modificacao.' / Data agora: '.dataBD('agora').' / Diferença: '.$diferenca );
			
			if ( ($diferenca >= $periodo && $forcar != 2) || $forcar == 1 ){
				return 0; // Arquivo passou do prazo do cache e precisa ser refeito
			}else{
				$str_xml = file_get_contents( $arquivo_cache ); 
				return $str_xml;
			}
			
		}else{
			return 0; //  Não existe o arquivo, precisa cria-lo
		}
			
	}else{
		return 0;
	}
		
}

// Gravar arquivo de cache
function gravarCache( $tipo, $nome_arquivo, $conteudo ){
	
	if ( $tipo && $nome_arquivo && $conteudo ){
		
		$arquivo_cache = 'xml/'.$tipo.'/'.$nome_arquivo.'.xml';
		
		$gravarArquivo = fopen($arquivo_cache, 'w+');
		fwrite($gravarArquivo, str_replace('	','',$conteudo));
		fclose($gravarArquivo);
			
	}else{
		return 0;
	}
		
}

// Remover arquivo de cache
function removerCache( $tipo, $nome_arquivo ){
	
	if ( $tipo && $nome_arquivo ){
		
		$arquivo_cache = 'xml/'.$tipo.'/'.$nome_arquivo.'.xml';
		
		return apagarArquivo( $arquivo_cache );
			
	}else{
		return 0;
	}
		
}


// Encodar string para texto de URL
function encodarURL( $texto ){
	
	$texto = str_replace( ' ', '-', $texto );
	
	$texto = str_replace( 'á', 'a', $texto );
	$texto = str_replace( 'ã', 'a', $texto );
	$texto = str_replace( 'à', 'a', $texto );
	$texto = str_replace( 'â', 'a', $texto );
	$texto = str_replace( 'ä', 'a', $texto );
	
	$texto = str_replace( 'é', 'e', $texto );
	$texto = str_replace( 'ê', 'e', $texto );
	$texto = str_replace( 'è', 'e', $texto );
	$texto = str_replace( 'ë', 'e', $texto );
	
	$texto = str_replace( 'í', 'i', $texto );
	$texto = str_replace( 'î', 'i', $texto );
	$texto = str_replace( 'ì', 'i', $texto );
	$texto = str_replace( 'ï', 'i', $texto );
	
	$texto = str_replace( 'ó', 'o', $texto );
	$texto = str_replace( 'ô', 'o', $texto );
	$texto = str_replace( 'õ', 'o', $texto );
	$texto = str_replace( 'ò', 'o', $texto );
	$texto = str_replace( 'ö', 'o', $texto );
	
	$texto = str_replace( 'ú', 'u', $texto );
	$texto = str_replace( 'û', 'u', $texto );
	$texto = str_replace( 'ù', 'u', $texto );
	$texto = str_replace( 'ü', 'u', $texto );
	
	$texto = str_replace( 'ç', 'c', $texto );
	
	$texto = str_replace( ',', '', $texto );
	$texto = str_replace( '.', '', $texto );
	$texto = str_replace( '?', '', $texto );
	$texto = str_replace( '!', '', $texto );
	$texto = str_replace( '\'', '', $texto );
	$texto = str_replace( '"', '', $texto );
	
	$texto = strtolower( $texto );
	$texto = urlencode( $texto );	
	
	return $texto;
}

// Formata Vídeos embedados do Youtube e Vimeo 
function videosEmbedados( $video ){
	
	$video = str_replace('?rel=0','',$video);
	
	if ( strpos( $video, 'youtube.com/embed' ) ){
		
		return $video;
		
	}else if ( strpos( $video, 'youtu.be' ) ){
		
		$temp 		= explode( "/", $video );
		$id_video 	= $temp[ count($temp)-1 ];
		
		return "https://www.youtube.com/embed/".$id_video."?rel=0&amp;wmode=transparent&amp;autoplay=0&amp;showinfo=0";
		
	}else if ( strpos( $video, 'youtube.com' ) ){
		
		$temp 		= explode( "?", $video );
		$temp2 		= explode( "&", $temp[ 1 ] );
		
		for ($i=0; $i<count($temp2); $i++){
			if ( strpos( $temp2[$i], 'v=' ) >= 0 ){
				$id_video 	= str_replace('v=','',$temp2[$i]);
				break;
			}
		}
		
		return "https://www.youtube.com/embed/".$id_video."?rel=0&amp;wmode=transparent&amp;autoplay=0&amp;showinfo=0";
			
	}else if ( strpos( $video, 'vimeo.com' ) ){
		
		$temp 		= explode( "/", $video );
		$id_video 	= $temp[ count($temp)-1 ];
		
		return "https://player.vimeo.com/video/".$id_video."?title=0&amp;byline=0&amp;portrait=0&amp;color=4fa336&amp;autoplay=0";
		
	}else{
		
		return $video;
		
	}
	
}

// Para formatar moeda
function formataMoeda( $valor=0, $tipo='tela' ){
	
	$valor = floatval($valor);
	
	if ( $tipo == 'base' ){
		return number_format($valor, 2, '.', '');
	}else{
		return number_format($valor, 2, ',', '.');	
	}
}

// Para formatar nome e sobrenome
function formataNome( $nome, $sobrenome=null ){
	
	if ( $sobrenome ){
		return $nome.' '.$sobrenome;
	}else{
		$partes 		= explode(" ", $nome);
		$primeiro_nome 	= array_shift($partes);
		$sobrenome		= implode(" ", $partes);
		
		$nome["nome"] 		= $primeiro_nome;
		$nome["sobrenome"] 	= $sobrenome;
		
		return $nome;
	}
}

// Para formatar nome e sobrenome
function formataTelefone( $numero, $tipo='base', $ddd=null ){
	
	$retorno = str_replace(' ','',$ddd.$numero);
	$retorno = str_replace('(','',$retorno);
	$retorno = str_replace(')','',$retorno);
	$retorno = str_replace('-','',$retorno);
	$retorno = str_replace('.','',$retorno);
	
	$ddd		= substr($retorno,0,2);
	$numero		= substr($retorno,2);
	
	if ( $tipo == 'base' ){
		
		$telefone["ddd"] 	= $ddd;
		$telefone["numero"] = $numero;
		
		return $telefone;
		
	}else{
		
		if ( strlen( $numero ) >= 9 ){
			$numero	= substr($numero,0,5).'-'.substr($numero,5);
		}else{
			$numero	= substr($numero,0,4).'-'.substr($numero,4);	
		}
		
		return '('.$ddd.') '.$numero;
	}
}


// Para efetuar um pagamento com a Braspag
function matricularInstudo( $ID_inscricao ){
	
	global $instudo_ws_url, $instudo_ws_login, $instudo_ws_senha;
	
	// verifica dados da inscrição
	$xml_inscricao 	= executaPagina( 'api/', array( 'a'=>'matriculas', 'metodo'=>'ver', 'ID'=>$ID_inscricao ) );
	$xml_inscricao 	= lerXML( $xml_inscricao );
	
	if ( $xml_inscricao->erro == 0 ){
		
		$inscricao 				= $xml_inscricao->matricula;
		
		$ID_usuario				= $inscricao->ID_usuario;
		$ID_plano				= $inscricao->ID_plano;
		$ID_curso				= $inscricao->ID_curso;
		$ID_instudo_matricula	= $inscricao->matricula->ID_instudo;
		$instudo_tipo_matricula	= $inscricao->matricula->ID_instudo["tipo"];
			
	}else{
		return("ERRO: ".$xml_inscricao->mensagem);	
	}
	
	// Se inscrição já tem um registro no Instudo
	if ( $ID_instudo_matricula != '' ){
		return("ERRO: A inscrição já tem um registro no Instudo. ID = ".$ID_instudo_matricula);
	}
	
	// Relacionar dados do aluno //
	$xml_aluno 	= executaPagina( 'api/', array( 'a'=>'alunos', 'metodo'=>'ver', 'ID'=>$ID_usuario ) );
	$xml_aluno 	= lerXML( $xml_aluno );
	
	if ( $xml_aluno->erro > 0 ){
		return("ERRO: ".$xml_aluno->mensagem);	
	}
	
	$aluno 				= $xml_aluno->aluno;
	
	$nome				= str_replace( "'", "", $aluno->nome );
	$email				= $aluno->email;
	$login				= $aluno->login;
	$nascimento			= formataData( $aluno->nascimento, 'base' );
	$cpf				= formataCPF( $aluno->cpf, 'base' );
	$ddd_telefone		= $aluno->telefones->telefone->ddd;
	$telefone			= $aluno->telefones->telefone->numero;
	$ddd_celular		= $aluno->telefones->celular->ddd;
	$celular			= $aluno->telefones->celular->numero;
	$cidade				= str_replace( "'", "", $aluno->cidade );
	$estado				= $aluno->estado;
	$pais				= str_replace( "'", "", $aluno->pais );
	$sexo				= $aluno->sexo;
	$ID_instudo_aluno	= $aluno->ID_instudo;
	
	// tratar variáveis
	if ( $ddd_celular != '' && $celular != '' ){
		$ddd_telefone 	= $ddd_celular;
		$telefone 		= $celular;
	}
	
	// Se usuário ainda não tem ID no Instudo
	if ( $ID_instudo_aluno == '' ){
		
		// descobrir senha do aluno
		$sql= "select senha_usuario from USUARIOS where ID_usuario=$ID_usuario";
		$rs = abrirRs( $sql );
			
		if ( sqlsrv_num_rows( $rs ) > 0 ){	
			$aluno	= sqlsrv_fetch_array( $rs );
			$senha = $aluno["senha_usuario"];	
		}else{
			$senha = '123456';	
		}
		
		$cliente = new SoapClient($instudo_ws_url);
		 
		$args = array( 'usuario' => $instudo_ws_login,
					   'senha'  => $instudo_ws_senha,
					   'nome_aluno' => $nome,
					   'email_aluno' => $email,
					   'senha_aluno' => $senha,
					   'sexo' => $sexo,
					   'ddd_telefone' => $ddd_telefone,
					   'telefone' => $telefone,
					   'nascimento' => $nascimento,
					   'cidade' => $cidade,
					   'estado' => $estado,
					   'pais' => $pais,
					   'cpf' => $cpf );
		 
		$retorno = $cliente->cadastrar_aluno( $args );
		
		if ( $retorno->erro == 0 ){
			$ID_instudo_aluno = $retorno->ID_aluno;
			
			$sql = "update USUARIOS set ID_instudo=$ID_instudo_aluno where ID_usuario=$ID_usuario";
			executarQuery( $sql );
			
			// remove cache do XML
			removerCache( 'alunos', 'aluno_'.$ID_usuario );
			
		}else{
			return("ERRO: Erro ao cadastrar aluno no Instudo. ".$retorno->msg);	
		}
			
	}
	
	
	// Se ainda não foi criado aluno no Instudo
	if ( $ID_instudo_aluno == '' ){
		return("ERRO: Erro ao encontrar aluno no Instudo.");	
	}else{
		
		// verificar relações entre o curso comprado e as turmas do Instudo
		$sql = "select ID_instudo from CURSOS_PLANOS_INSTUDO where ID_plano=$ID_plano order by ordem";
		$rs = abrirRs( $sql );
		
		if ( sqlsrv_num_rows( $rs ) > 0 ){
			
			$matricula = new SoapClient($instudo_ws_url);
					
			if ( sqlsrv_num_rows( $rs ) > 1 ){	
				
				$grupo = true;
				
				// Descobrir titulo do curso //
				$xml_curso 	= executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID_curso ) );
				$xml_curso 	= lerXML( $xml_curso );
				
				if ( $xml_curso->erro > 0 ){
					return("ERRO: ".$xml_curso->mensagem);	
				}
				$curso 			= $xml_curso->curso;
				$titulo_curso	= $curso->titulo;
				
				$str_turmas = '';
				while( $instudo = sqlsrv_fetch_array( $rs ) ) {
					$str_turmas .= $instudo["ID_instudo"].',';
				}
				$str_turmas = trim($str_turmas,",");
				
				$args = array( 	'usuario' => $instudo_ws_login,
								'senha'  => $instudo_ws_senha,
								'ID_aluno' => $ID_instudo_aluno,
								'titulo' => $titulo_curso,
								'ID_turmas' => $str_turmas,
								'ID_curso_cliente' => $ID_curso,
								'ID_loja' => 'novo_'.$ID_inscricao );
				
				$retorno = $matricula->matricular_aluno_grupo( $args );
				
			}else if ( sqlsrv_num_rows( $rs ) == 1 ){	
				
				$grupo = false;
				
				$instudo = sqlsrv_fetch_array( $rs );
				
				$args = array( 	'usuario' => $instudo_ws_login,
								'senha'  => $instudo_ws_senha,
								'ID_aluno' => $ID_instudo_aluno,
								'ID_turma' => $instudo["ID_instudo"],
								'ID_loja' => 'novo_'.$ID_inscricao );
				
				$retorno = $matricula->matricular_aluno( $args );
				
			}
			
			if ( $retorno->erro == 0 ){
				
				if ( $grupo == true ){
					$ID_instudo_matricula = $retorno->ID_grupo;
					$str_campo = 'ID_grupo';
				}else{
					$ID_instudo_matricula = $retorno->ID_matricula;
					$str_campo = 'ID_matricula';
				}
				
				$sql = "update CURSOS_INSCRICOES set instudo_inscricao=1, ID_instudo_matricula=$ID_instudo_matricula, instudo_tipo='".$str_campo."' where ID_inscricao=$ID_inscricao";
				executarQuery( $sql );
				
				// remove cache do XML
				removerCache( 'matriculas', 'matricula_'.$ID_inscricao );
				
			}else{
				return("ERRO: Erro ao cadastrar aluno no Instudo. ".$retorno->msg);	
			}
			
		}else{
			return("ERRO: Este curso não tem vínculo com uma turma no Instudo.");		
		}
		
		
		return 1;
			
	}
	
	
}



// Retorna o IP do usuário
function ipUsuario(){
	
	$http_client_ip       = '';
	$http_x_forwarded_for = '';
	$remote_addr          = '';
	
	if ( isset($_SERVER['HTTP_CLIENT_IP']) )
		$http_client_ip       = $_SERVER['HTTP_CLIENT_IP'];
	
	if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )
		$http_x_forwarded_for = $_SERVER['HTTP_X_FORWARDED_FOR'];
	
	if ( isset($_SERVER['REMOTE_ADDR']) )
		$remote_addr          = $_SERVER['REMOTE_ADDR'];
	 
	/* VERIFICO SE O IP REALMENTE EXISTE NA INTERNET */
	if(!empty($http_client_ip)){
		$ip = $http_client_ip;
		/* VERIFICO SE O ACESSO PARTIU DE UM SERVIDOR PROXY */
	} elseif(!empty($http_x_forwarded_for)){
		$ip = $http_x_forwarded_for;
	} else {
		/* CASO EU NÃO ENCONTRE NAS DUAS OUTRAS MANEIRAS, RECUPERO DA FORMA TRADICIONAL */
		$ip = $remote_addr;
	}
	 
	return $ip;	
}

// Formatar uma variável para pesquisar em banco
function formataPesquisa( $texto ){
	
	$texto = strtolower( $texto );
	$texto = str_replace( ' ', '%', $texto );
	
	$texto = str_replace( 'ã', 'a', $texto );
	$texto = str_replace( 'á', 'a', $texto );
	$texto = str_replace( 'à', 'a', $texto );
	$texto = str_replace( 'â', 'a', $texto );
	$texto = str_replace( 'a', '[a,ã,á,à,â]', $texto );
	
	$texto = str_replace( 'é', 'e', $texto );
	$texto = str_replace( 'è', 'e', $texto );
	$texto = str_replace( 'ê', 'e', $texto );
	$texto = str_replace( 'e', '[e,é,è,ê]', $texto );
	
	$texto = str_replace( 'í', 'i', $texto );
	$texto = str_replace( 'ì', 'i', $texto );
	$texto = str_replace( 'î', 'i', $texto );
	$texto = str_replace( 'i', '[i,í,ì,î]', $texto );
	
	$texto = str_replace( 'õ', 'o', $texto );
	$texto = str_replace( 'ó', 'o', $texto );
	$texto = str_replace( 'ò', 'o', $texto );
	$texto = str_replace( 'ô', 'o', $texto );
	$texto = str_replace( 'o', '[o,õ,ó,ò,ô]', $texto );
	
	$texto = str_replace( 'ú', 'u', $texto );
	$texto = str_replace( 'ù', 'u', $texto );
	$texto = str_replace( 'û', 'u', $texto );
	$texto = str_replace( 'u', '[u,ú,ù,û]', $texto );
	
	$texto = str_replace( 'ç', 'c', $texto );
	$texto = str_replace( 'c', '[c,ç]', $texto );
	
	return $texto;
}

// Prepara arquivos de upload
function preparaArquivos(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

// Função para mover um arquivo de upload
function moverUpload( $arquivo, $salvar ){
	
	if ( move_uploaded_file( $arquivo, $salvar ) ) {
		return 1;
	}else{
		return 0;
	}
	
}

// Função para retornar mensagens padrões
class Mensagens {
	
	public static function Ler( $codigo ){
		
		// Ler XML de padrões //
		$xml_padroes 	= lerXML( '../facix2/_xml/mensagens_email.xml' );
		
		if ( !$xml_padroes ){
			$retorno['erro'] = 1;
		}else{
			
			$retorno = array( 'erro' => 0 );

			$conteudo = $xml_padroes->xpath("registro[@codigo='".$codigo."']");
			
			if ($conteudo){
				$retorno['erro'] 		= 0;
				$retorno['remetente'] 	= $conteudo[0]->remetente;
				$retorno['email'] 		= $conteudo[0]->email;
				$retorno['assunto'] 	= $conteudo[0]->assunto;
				$retorno['mensagem'] 	= $conteudo[0]->mensagem;
			}else{
				$retorno['erro'] = 1;
			}
				
		}
		
		return $retorno;
	}
		
}


// Função de criptografia e descriptografia
function criptografar($frase, $chave, $crypt=true)
{
	$retorno = "";
	
	$frase = strval($frase);
	
	if ($frase=='') 
		return '';
	
	if($crypt){
		$string = $frase;
		$i = strlen($string)-1;
		$j = strlen($chave);
		
		do{
			$retorno .= ($string{$i} ^ $chave{$i % $j});
		}while ($i--);

		$retorno = strrev($retorno);
		$retorno = base64_encode($retorno);
	}else{
		$string = base64_decode($frase);
		$i = strlen($string)-1;
		$j = strlen($chave);

		do{
			$retorno .= ($string{$i} ^ $chave{$i % $j});
		}while ($i--);
		
		$retorno = strrev($retorno);
	}
		
	return $retorno;
}


// Função para escrever um LOG em TXT no servidor com qualquer informação
function criarLog( $texto, $pasta='' ){
	$data_atual = formataData('agora','base');
	
	$arquivo_log = $pasta.'log_'.$data_atual.'.txt';
	
	$referer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'';
	
	$texto = 	PHP_EOL.'DATA: '.formataData('agora','base').' as '.formataHora('agora','base').PHP_EOL.
				'IP: '.ipCliente().PHP_EOL.
				'URL: '.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].PHP_EOL.
				'REFERER: '.urlencode($referer.'').PHP_EOL.
				$texto.PHP_EOL.'--------------------';
	
	$fp = fopen( $arquivo_log, "a+" );
	fwrite( $fp, $texto );
	
}



// Função para identificar parâmetros de origem e se existirem, gravar em cookie
function origemAcesso(){
	$utm_source 	= formataVar( 'utm_source', 'GET' );
	$utm_medium 	= formataVar( 'utm_medium', 'GET' );
	$utm_campaign 	= formataVar( 'utm_campaign', 'GET' );
	
	if ($utm_source)
		$cookie_origem['utm_source'] 	= $utm_source;
	if ($utm_medium)
		$cookie_origem['utm_medium'] 	= $utm_medium;
	if ($utm_campaign)
		$cookie_origem['utm_campaign'] 	= $utm_campaign;
	
	if ( isset($cookie_origem) ) {
		setcookie('origem', serialize($cookie_origem), time() + (86400 * 30), "/" ); // 86400 = 1 dia
	}
	
}


// Detecta se esta usando UTF-8
function detectaUTF8( $string ){
	
	if ($string){
	
		if (preg_match('!!u', $string)){
		   // this is utf-8
		   return true;
		}else {
		   // definitely not utf-8
		   return false;
		}
	
	}else{
		return true;	
	}
		
}

// Inicializa Variáveis de paginação
function iniciaPaginacao(){
	
	global $paginacao_inicio, $paginacao_tamanho, $paginacao_fim;
	
	$paginacao_inicio	= formataVar( 'inicio', 'get' );
	$paginacao_tamanho	= formataVar( 'paginacao', 'get' );	
	
	if (!$paginacao_inicio){
		$paginacao_inicio = 1;	
	}
	if (!$paginacao_tamanho){
		$paginacao_tamanho = 20;	
	}
	$paginacao_fim	= ($paginacao_inicio + $paginacao_tamanho ) - 1;
}

// Remover código HTML de uma string e opcionalmente limitar número de caracteres
function removerHtml( $string='', $limite=null ){
	
	$string = strip_tags($string);
	
	if ($limite){
		if ( strlen($string) > $limite ){
			$string = substr( $string, 0, $limite ).'...';
		}	
	}
	return $string;
		
}

// Liberar acesso aos alunos da inscrição
function liberarAcesso( $ID_inscricao ){
	
	$sql = "select numero_inscricao, ID_curso from CURSOS_INSCRICOES where ID_inscricao=$ID_inscricao";
	$rs = abrirRs( $sql );
					
	if ( sqlsrv_num_rows( $rs ) > 0 ){	
		$inscricao = sqlsrv_fetch_array( $rs );
		
		// Se não tem número de inscrição, adicionar um
		if ( is_null( $inscricao['numero_inscricao'] ) ){
			
			$sql = "select top 1 numero_inscricao from CURSOS_INSCRICOES where numero_inscricao is not null order by numero_inscricao desc";
			$rs_ultimo = abrirRs( $sql );
					
			if ( sqlsrv_num_rows( $rs_ultimo ) > 0 ){	
				$ultima_inscricao = sqlsrv_fetch_array( $rs_ultimo );
				$numero_inscricao = $ultima_inscricao['numero_inscricao']+1;
			}else{
				$numero_inscricao = 1;	
			}
			
			$sql = "update CURSOS_INSCRICOES set numero_inscricao=$numero_inscricao where ID_inscricao=$ID_inscricao";
			executarQuery( $sql );
				
		}
		
		// Atualizar inscrição para marca-la como paga
		$sql = "update CURSOS_INSCRICOES set pago_inscricao=1 where ID_inscricao=$ID_inscricao";
		executarQuery( $sql );
		
		$ID_curso 		= $inscricao["ID_curso"];
		$data_inicio	= dataBD( 'agora' );
		$data_fim		= dataBD( dateAdd( 'agora', 12, 'm' ) );
		
		$sql = 	"SELECT USUARIOS.ID_usuario, USUARIOS.nome_usuario, USUARIOS.cpf_usuario, USUARIOS.email_usuario FROM CURSOS_INSCRICOES INNER JOIN CURSOS_INSCRICOES_USUARIOS ON 
			  	CURSOS_INSCRICOES.ID_inscricao = CURSOS_INSCRICOES_USUARIOS.ID_inscricao INNER JOIN 
			  	USUARIOS ON CURSOS_INSCRICOES_USUARIOS.ID_usuario = USUARIOS.ID_usuario 
			  	where CURSOS_INSCRICOES_USUARIOS.ID_inscricao=$ID_inscricao";
		$rs_users = abrirRs( $sql );
					
		if ( sqlsrv_num_rows( $rs_users ) > 0 ){
			while( $participante = sqlsrv_fetch_array( $rs_users ) ) {
				$ID_usuario = $participante['ID_usuario'];

				$dados = array('nome'=>$participante['nome_usuario'],'email'=>$participante['email_usuario'],'cpf'=>$participante['cpf_usuario'],'admin'=>false);

				
				if ($ID_curso == 4 ) {
					call_api("http://157.245.215.207/usuarios",json_encode($dados),array(),"POST");
				} elseif ($ID_curso == 12 ) {
					call_api("http://104.131.87.127/usuarios",json_encode($dados),array(),"POST");
				}
				

				$sql = "select ID_usuario_curso from USUARIOS_CURSOS where ID_usuario=$ID_usuario and ID_curso=$ID_curso";
				$rs = abrirRs( $sql );
					
				if ( sqlsrv_num_rows( $rs ) > 0 ){	
					$relacao = sqlsrv_fetch_array( $rs );
					
					$sql = 	"update USUARIOS_CURSOS set inicio_usuario_curso='".$data_inicio."', fim_usuario_curso='".$data_fim."', status_usuario_curso=1
							 where ID_usuario_curso=".$relacao["ID_usuario_curso"];
					executarQuery( $sql );
					
				}else{
					
					$sql =	"insert into USUARIOS_CURSOS (ID_usuario, ID_curso, inicio_usuario_curso, fim_usuario_curso, status_usuario_curso) 
							values ($ID_usuario,$ID_curso,'".$data_inicio."','".$data_fim."',1)";
					executarQuery( $sql );
					
				}
			}
		}
			
	}
		
}

// ENVIA UM PEDIDO PARA UMA API

function call_api($url,$body = "",$params = array(),$method = 'GET',$extra_headers = array()) {

	global $url_site;
		
	$the_url = strpos($url,'http') === 0 ? $url : $url_site."/../".$url;
	
	$headers = [
	  'Accepts: application/json',
	  'Content-Type: application/json'
	];
	
	$headers = array_merge($headers,$extra_headers);
	
	
	if (!empty($params)) {
		$qs = http_build_query($params); // query string encode the parameters
		$request = $the_url."?{$qs}"; // create the request URL
	} else {
		$request = $the_url;
	}
	

	$curl = curl_init(); // Get cURL resource
	// Set cURL options
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $request,            // set the request URL
	  CURLOPT_HTTPHEADER => $headers,     // set the headers 
	  CURLOPT_POSTFIELDS => $body,		  // sets the request body
	  CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
	));

	if ($method == "POST") {
		curl_setopt($curl, CURLOPT_POST, true);
	} else {			
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
	}

	$response = curl_exec($curl); // Send the request, save the response
	// $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	$result = json_decode($response,true); // store json decoded response
	// $result = $response; // store json decoded response
	$result['rt_httpcode'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl); // Close request
	
	return $result;
}


// ENVIAR LEAD PARA A RD STATION
function cadastrarRdstation( $identificador, $nome, $email, $perfil='', $interesse='' ){
	
	$retorno 	= executaPagina( 'facix2/_php/rdstation/index.php', array( 'identificador'=>$identificador, 'nome'=>$nome, 'email'=>$email, 'perfil'=>$perfil, 'interesse'=>$interesse ) );
	return $retorno;
	
}

// Função para enviar sinal de venda para RDStation
function conversaoRDStation( $email=null, $ID_usuario=null, $ID_inscricao=null, $valor=null ){
	
	// Formatar valor
	if ($valor){
		$valor = floatval($valor);	
	}
	
	// Precisa enviar pelo menos um parâmetro identificador do usuário
	if ( !$email && !$ID_usuario && !$ID_inscricao ){
		return 0;	
	}else{
		
		$nome = '';

		if ( $email ){

			$sql = "select nome_usuario from USUARIOS where email_usuario='".$email."'";
			$rs_won = abrirRs( $sql );
					
			if ( sqlsrv_num_rows( $rs_won ) > 0 ){	
				$user_won = sqlsrv_fetch_array( $rs_won );
				$nome = $user_won["nome_usuario"];
			}

		}else if ( $ID_usuario ){
			
			$sql = "select nome_usuario, email_usuario from USUARIOS where ID_usuario=".$ID_usuario;
			$rs_won = abrirRs( $sql );
					
			if ( sqlsrv_num_rows( $rs_won ) > 0 ){	
				$user_won = sqlsrv_fetch_array( $rs_won );
				$email = $user_won["email_usuario"];
				$nome = $user_won["nome_usuario"];
			}
			
		}else if ( $ID_inscricao ){
			
			$sql = "select nome_usuario, email_usuario from CURSOS_INSCRICOES_USUARIOS inner join USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario=USUARIOS.ID_usuario where ID_inscricao=".$ID_inscricao;
			$rs_won = abrirRs( $sql );
					
			if ( sqlsrv_num_rows( $rs_won ) > 0 ){	
				$user_won = sqlsrv_fetch_array( $rs_won );
				$email = $user_won["email_usuario"];
				$nome = $user_won["nome_usuario"];
			}
				
		}

		if ( $ID_inscricao && $email ){
			
			$sql = "select titulo_curso from CURSOS_INSCRICOES inner join CURSOS on CURSOS_INSCRICOES.ID_curso=CURSOS.ID_curso where ID_inscricao=".$ID_inscricao;
			$rs_won = abrirRs( $sql );
					
			if ( sqlsrv_num_rows( $rs_won ) > 0 ){	
				$user_won = sqlsrv_fetch_array( $rs_won );

				$titulo_curso = $user_won["titulo_curso"];
				$identificador_curso = identificadorCurso( $titulo_curso );
				
				cadastrarRdstation( 'comprou_'.$identificador_curso, $nome, $email, '', $titulo_curso );
			}
		}
		
		// Se até agora não foi enviado um valor, enviar 1
		if (!$valor){
			$valor = 1;	
		}
		
		// Enviar dados para RD Station
		if ( $email && $valor ){

			$retorno = executaPagina( 'facix2/_php/rdstation/index.php', array( 'acao'=>'won', 'email'=>$email, 'valor'=>$valor ) );
			return $retorno;

		}else{
			return 0;	
		}
	}
	
}

// Função para gerar identificador do curso de acordo com título do curso
function identificadorCurso( $titulo_curso ){

	$identificador_curso = strtolower($titulo_curso);
	$identificador_curso = str_replace('curso de ','',$identificador_curso);
	$identificador_curso = str_replace(' ','_',$identificador_curso);

	if ( strpos( $identificador_curso, 'orat' ) !== false ){
		$identificador_curso = 'oratoria';
	}else if ( strpos( $identificador_curso, 'memoriza' ) !== false ){
		$identificador_curso = 'leitura_memoria';
	}else if ( strpos( $identificador_curso, 'dist' ) !== false ){
		$identificador_curso = 'leitura_distancia';
	}else if ( strpos( $identificador_curso, 'tempo' ) !== false ){
		$identificador_curso = 'tempo';
	}

	return $identificador_curso;
}

// FUnção para enviar contrato para todos os participantes
function enviarContrato( $ID_inscricao ){
	
	global $email_principal;
	
	$sql = "select titulo_curso from CURSOS_INSCRICOES inner join CURSOS on CURSOS_INSCRICOES.ID_curso=CURSOS.ID_curso where ID_inscricao=$ID_inscricao";
	$rs = abrirRs( $sql );
					
	if ( sqlsrv_num_rows( $rs ) > 0 ){	
		$inscricao = sqlsrv_fetch_array( $rs );
			
		$titulo_curso = $inscricao["titulo_curso"];
		$titulo_curso =	charSet($titulo_curso,'utf8');
		
		// verificar pagamento
		$sql = "select top 1 ID_pagamento from PAGAMENTOS where ID_inscricao=".$ID_inscricao." order by ID_pagamento desc";
		$rs = abrirRs( $sql );
				
		if ( sqlsrv_num_rows( $rs ) > 0 ){	
			$pagamento 		= sqlsrv_fetch_array( $rs );
			$ID_pagamento	= $pagamento['ID_pagamento'];
		
			$sql = "select nome_usuario, email_usuario from CURSOS_INSCRICOES_USUARIOS inner join USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario=USUARIOS.ID_usuario where ID_inscricao=$ID_inscricao";
			$rs_users = abrirRs( $sql );
							
			if ( sqlsrv_num_rows( $rs_users ) > 0 ){
				while( $participante = sqlsrv_fetch_array( $rs_users ) ) {
					
					$mensagem =	"<BR />Sr(a) ".$participante["nome_usuario"]."<BR /><BR />Sua inscrição no curso ".$titulo_curso." foi realizada com sucesso.<BR />
								<a href='http://api.methodus.com.br/facix2/contrato/contrato.asp?ID=".$ID_pagamento."'>Clique aqui</a> para visualizar e imprimir seu contrato.<BR /><BR />
								Att,<BR />Methodus Cosultoria<BR /><a href='http://www.methodus.com.br'>www.methodus.com.br</a><BR /><a href='mailto:info@methodus.com.br'>info@methodus.com.br</a>";
							
					enviaEmail( 'Contrato', $mensagem, $participante["email_usuario"], $participante["nome_usuario"], $email_principal, 'Methodus', 1 );
					enviaEmail( 'Contrato - Curso de '.$titulo_curso.' Methodus Consultoria', $mensagem, $email_principal, 'Methodus', $email_principal, 'Methodus', 1 );
					
				}
			}
			
			$sql = "update CURSOS_INSCRICOES set contrato_inscricao=getDate() where ID_inscricao=$ID_inscricao";
			executarQuery( $sql );
		
		}
	
	}
}


// Para converter uma array em JSON
function arrayJSON($struct) {
   //return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($struct));
   return json_encode( $struct, JSON_UNESCAPED_UNICODE );
}


?>