<?php
/* TODAS AS CLASSES DO SISTEMA FICAM AQUI */


// Alunos (usuários)
class Usuario{
	
	public 	$campos;
	
	public function __construct( $campos ){
		
		if ( isset($campos["nome"]) )							
			$this->nome			= $campos["nome"];
		if ( isset($campos["email"]) )							
			$this->email		= $campos["email"];
		if ( isset($campos["nascimento"]) )							
			$this->nascimento	= $campos["nascimento"];
		if ( isset($campos["cpf"]) )							
			$this->cpf			= $campos["cpf"];
		if ( isset($campos["rg"]) )							
			$this->rg			= $campos["rg"];
		if ( isset($campos["ddd_tel"]) )							
			$this->ddd_tel		= $campos["ddd_tel"];
		if ( isset($campos["tel"]) )							
			$this->tel			= $campos["tel"];
		if ( isset($campos["ddd_cel"]) )							
			$this->ddd_cel		= $campos["ddd_cel"];
		if ( isset($campos["cel"]) )							
			$this->cel			= $campos["cel"];
		if ( isset($campos["endereco"]) )							
			$this->endereco		= $campos["endereco"];
		if ( isset($campos["bairro"]) )							
			$this->bairro		= $campos["bairro"];
		if ( isset($campos["cep"]) )							
			$this->cep			= $campos["cep"];
		if ( isset($campos["cidade"]) )							
			$this->cidade		= $campos["cidade"];
		if ( isset($campos["estado"]) )							
			$this->estado		= $campos["estado"];
		if ( isset($campos["conheceu"]) )							
			$this->conheceu		= $campos["conheceu"];
		if ( isset($campos["interesses"]) )							
			$this->interesses	= $campos["interesses"];
		if ( isset($campos["entrou"]) )							
			$this->entrou		= $campos["entrou"];
		if ( isset($campos["prospeccao"]) )							
			$this->prospeccao	= $campos["prospeccao"];
		if ( isset($campos["newsletter"]) )							
			$this->newsletter	= $campos["newsletter"];
		if ( isset($campos["ID_perfil"]) )							
			$this->ID_perfil	= $campos["ID_perfil"];			
		if ( isset($campos["data_atualizacao"]) )							
			$this->data_atualizacao	= $campos["data_atualizacao"];	
	}
	
	public function Cadastro( $id ){
		
		// valores para inserir ou atualizar
		$valores = array();
		
		if ( !$id )
			$valores["data_usuario"] 		= "getDate()";
		
		if ( isset($this->nome) )
			$valores["nome_usuario"] 		= charSet( $this->nome );
		if ( isset($this->email) )
			$valores["email_usuario"] 		= $this->email;
		if ( isset($this->nascimento) )
			$valores["nascimento_usuario"] 	= dataBD($this->nascimento);
		if ( isset($this->cpf) )
			$valores["cpf_usuario"]			= formataCPF( $this->cpf );
		if ( isset($this->rg) )
			$valores["rg_usuario"] 			= $this->rg;
		if ( isset($this->ddd_tel) )
			$valores["ddd_tel_usuario"] 	= $this->ddd_tel;
		if ( isset($this->tel) )
			$valores["tel_usuario"] 		= $this->tel;
		if ( isset($this->ddd_cel) )
			$valores["ddd_cel_usuario"] 	= $this->ddd_cel;
		if ( isset($this->cel) )
			$valores["cel_usuario"] 		= $this->cel;
		if ( isset($this->endereco) )
			$valores["endereco_usuario"] 	= charSet( $this->endereco );
		if ( isset($this->bairro) )
			$valores["bairro_usuario"] 		= charSet( $this->bairro );
		if ( isset($this->cep) )
			$valores["cep_usuario"] 		= $this->cep;
		if ( isset($this->cidade) )
			$valores["cidade_usuario"] 		= charSet( $this->cidade );	
		if ( isset($this->estado) )
			$valores["estado_usuario"] 		= $this->estado;	
		if ( isset($this->newsletter) )
			$valores["newsletter_usuario"] 	= $this->newsletter;	
		if ( isset($this->conheceu) )
			$valores["conheceu_usuario"] 	= $this->conheceu;	
		if ( isset($this->interesses) )
			$valores["interesses_usuario"] 	= $this->interesses;	
		if ( isset($this->entrou) )
			$valores["entrou_usuario"] 		= $this->entrou;	
		if ( isset($this->prospeccao) )
			$valores["prospeccao_usuario"] 	= $this->prospeccao;	
		if ( isset($this->ID_perfil) )
			$valores["ID_perfil"] 			= $this->ID_perfil;	
		
		// sempre atualizar a data de atualização do aluno	
		$valores["data_atualizacao_usuario"] = dataBD('agora'); //"getDate()";	
		
		if ( $id ){
			
			if ( count( $valores ) ){
				
				// Verificar se índices não serão duplicados
				if ( isset($this->email) || isset($this->cpf) ){
					
					$sql = "select top 1 ID_usuario, email_usuario, cpf_usuario from USUARIOS where ID_usuario<>'$id' and (ID_usuario=0 ";
					
					if ( isset($this->email) )
						$sql.= " or email_usuario='".$valores["email_usuario"]."' ";
					if ( isset($this->cpf) )
						$sql.= " or cpf_usuario='".$valores["cpf_usuario"]."' ";
						
					$sql.= ")";
					
					$rs = abrirRs( $sql );
					
					if ( sqlsrv_num_rows( $rs ) > 0 ){
						
						$usuario = sqlsrv_fetch_array( $rs );
						if ( $usuario['email_usuario'] == $valores["email_usuario"] ){
							$str_erro = 'Este e-mail já esta cadastrado';	
						}else if ( $usuario['cpf_usuario'] == $valores["cpf_usuario"] ){
							$str_erro = 'Este CPF já esta cadastrado';	
						}
						
						$retorno = array(	'erro' => 1,
											'mensagem' => $str_erro
										);
						return $retorno;
					}
						
				}
				
				// Atualizar registro
				$sql = querySQL( 'USUARIOS', $valores, array("ID_usuario"=>$id) );
				$afetados = executarQuery( $sql );
				
				if ( $afetados > 0 ){
					
					$retorno = array(	'erro' => 0,
										'mensagem' => 'Aluno atualizado com sucesso',
										'id' => $id
									);
									
				}else{
					
					$retorno = array(	'erro' => 1,
										'mensagem' => 'Aluno não encontrado'
									);
				}
			
			}else{
				
				$retorno = array(	'erro' => 1,
									'mensagem' => 'Pelo menos um parâmetro deve ser enviado para atualizar os dados'
								);		
					
			}
			return $retorno;
			
			
		}else{
			
			// Campos obrigatórios
			if ( !isset( $this->email ) || !isset( $this->nome ) ){
				$retorno = array(	'erro' => 1,
									'mensagem' => 'Faltam campos obrigatórios'
								);	
				return $retorno;
			}
			
			// Verificar se registro já existe
			$sql = 	"select top 1 ID_usuario, email_usuario, cpf_usuario from USUARIOS 
					where email_usuario='".$valores["email_usuario"]."' ";
			
			if ( isset( $this->cpf ) ){
				$sql.=	" or (cpf_usuario='".$valores["cpf_usuario"]."' and cpf_usuario<>'' and cpf_usuario is not null)";
			}
			$rs = abrirRs( $sql );
			
			if ( sqlsrv_num_rows( $rs ) > 0 ){
				
				$usuario = sqlsrv_fetch_array( $rs );
				if ( $usuario['email_usuario'] == $valores["email_usuario"] ){
					$str_erro = 'Este e-mail já esta cadastrado';		
				}else if ( $usuario['cpf_usuario'] == $valores["cpf_usuario"] ){
					$str_erro = 'Este CPF já esta cadastrado';	
				}
				
				$retorno = array(	'erro' => 1,
									'mensagem' => $str_erro
								);
				return $retorno;
			}
			
			// Inserir registro			
			$sql = querySQL( 'USUARIOS', $valores, null );
			executarQuery( $sql );
			
			// Para descobrir o ID do novo registro
			$sql = "select top 1 ID_usuario from USUARIOS where email_usuario='".$this->email."' order by ID_usuario desc";
			$rs = abrirRs( $sql );
			
			if ( sqlsrv_num_rows( $rs ) > 0 ){
				$usuario = sqlsrv_fetch_array( $rs );
				$id = $usuario['ID_usuario'];
				
				$retorno = array(	'erro' => 0,
									'mensagem' => 'Aluno cadastrado com sucesso',
									'id' => $id
								);
			}else{
				$retorno = array(	'erro' => 1,
									'mensagem' => 'Aluno foi cadastrado, mas houve um erro ao recuperar o ID do aluno'
								);
			}
			return $retorno;
			
		}
		
		
	}
	
/*	public static function Dados( $id ){
		
		$sql 		= "SELECT * FROM USUARIOS where ID_usuario=".$id;
		$rs 		= abrirRs( $sql );
		
		if ( sqlsrv_num_rows( $rs ) > 0 ){
			
			$usuario = sqlsrv_fetch_array( $rs );
			
			$retorno = array(	'erro' 			=> 0,
								'id' 			=> $id,
								'data' 			=> $usuario['data_usuario'],
								'nome' 			=> $usuario['nome_usuario'],
								'email' 		=> $usuario['email_usuario'],
                        		'login' 		=> $usuario['login_usuario'],
                        		'senha' 		=> $usuario['senha_usuario'],
								'nascimento'	=> $usuario['nascimento_usuario'],
								'cpf' 			=> $usuario['cpf_usuario'],
								'rg' 			=> $usuario['rg_usuario'],
								'ddd_tel' 		=> $usuario['ddd_tel_usuario'],
								'tel' 			=> $usuario['tel_usuario'],
								'ddd_cel' 		=> $usuario['ddd_cel_usuario'],
								'cel' 			=> $usuario['cel_usuario'],
								'endereco' 		=> $usuario['endereco_usuario'],
								'bairro' 		=> $usuario['bairro_usuario'],
								'cep' 			=> $usuario['cep_usuario'],
								'cidade' 		=> $usuario['cidade_usuario'],
								'estado' 		=> $usuario['estado_usuario'],
								'pais' 			=> $usuario['pais_usuario'],
								'newsletter' 	=> $usuario['newsletter_usuario'],
								'conheceu' 		=> $usuario['conheceu_usuario'],
								'sexo'			=> $usuario['sexo_usuario'],
								'observacoes'	=> $usuario['observacoes_usuario'],
								'contatar' 		=> $usuario['contatar_usuario'],
								'privado' 		=> $usuario['privado_usuario'],
								'ID_instudo' 	=> $usuario['ID_instudo']
							);
							
		}else{
			$retorno = array(	'erro' => 1,
								'mensagem' => 'Aluno não encontrado'
							);
		}
		
		return $retorno;
	}*/
	
/*	public static function Remover( $id ){
		
		// Remover registro
		$sql = "delete from USUARIOS where ID_usuario=".$id;
		$afetados = executarQuery( $sql );
		
		if ( $afetados > 0 ){
			
			$retorno = array(	'erro' => 0,
								'mensagem' => 'Aluno removido'
							);
							
		}else{
			$retorno = array(	'erro' => 1,
								'mensagem' => 'Aluno não encontrado'
							);
		}
		
		return $retorno;
	}*/
		
}


// Classe para transformar um array em XML
class Array2XML {

    private static $xml = null;
	private static $encoding = 'UTF-8';

    /**
     * Initialize the root XML node [optional]
     * @param $version
     * @param $encoding
     * @param $format_output
     */
    public static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true) {
        self::$xml = new DomDocument($version, $encoding);
        self::$xml->formatOutput = $format_output;
		self::$encoding = $encoding;
    }

    /**
     * Convert an Array to XML
     * @param string $node_name - name of the root node to be converted
     * @param array $arr - aray to be converterd
     * @return DomDocument
     */
    public static function &createXML($node_name, $arr=array()) {
        $xml = self::getXMLRoot();
        $xml->appendChild(self::convert($node_name, $arr));

        self::$xml = null;    // clear the xml node in the class for 2nd time use.
        return $xml;
    }

    /**
     * Convert an Array to XML
     * @param string $node_name - name of the root node to be converted
     * @param array $arr - aray to be converterd
     * @return DOMNode
     */
    private static function &convert($node_name, $arr=array()) {

        //print_arr($node_name);
        $xml = self::getXMLRoot();
        $node = $xml->createElement($node_name);

        if(is_array($arr)){
            // get the attributes first.;
            if(isset($arr['@atributos'])) {
                foreach($arr['@atributos'] as $key => $value) {
                    if(!self::isValidTagName($key)) {
                        throw new Exception('[Array2XML] Illegal character in attribute name. attribute: '.$key.' in node: '.$node_name);
                    }
                    $node->setAttribute($key, self::bool2str($value));
                }
                unset($arr['@atributos']); //remove the key from the array once done.
            }

            // check if it has a value stored in @value, if yes store the value and return
            // else check if its directly stored as string
            if(isset($arr['@value'])) {
                $node->appendChild($xml->createTextNode(self::bool2str($arr['@value'])));
                unset($arr['@value']);    //remove the key from the array once done.
                //return from recursion, as a note with value cannot have child nodes.
                return $node;
            } else if(isset($arr['@cdata'])) {
                $node->appendChild($xml->createCDATASection(self::bool2str($arr['@cdata'])));
                unset($arr['@cdata']);    //remove the key from the array once done.
                //return from recursion, as a note with cdata cannot have child nodes.
                return $node;
            }
        }

        //create subnodes using recursion
        if(is_array($arr)){
            // recurse to get the node for that key
            foreach($arr as $key=>$value){
                if(!self::isValidTagName($key)) {
                    throw new Exception('[Array2XML] Illegal character in tag name. tag: '.$key.' in node: '.$node_name);
                }
                if(is_array($value) && is_numeric(key($value))) {
                    // MORE THAN ONE NODE OF ITS KIND;
                    // if the new array is numeric index, means it is array of nodes of the same kind
                    // it should follow the parent key name
                    foreach($value as $k=>$v){
                        $node->appendChild(self::convert($key, $v));
                    }
                } else {
                    // ONLY ONE NODE OF ITS KIND
                    $node->appendChild(self::convert($key, $value));
                }
                unset($arr[$key]); //remove the key from the array once done.
            }
        }

        // after we are done with all the keys in the array (if it is one)
        // we check if it has any text value, if yes, append it.
        if(!is_array($arr)) {
            $node->appendChild($xml->createTextNode(self::bool2str($arr)));
        }

        return $node;
    }

    /*
     * Get the root XML node, if there isn't one, create it.
     */
    private static function getXMLRoot(){
        if(empty(self::$xml)) {
            self::init();
        }
        return self::$xml;
    }

    /*
     * Get string representation of boolean value
     */
    private static function bool2str($v){
        //convert boolean to text value.
        $v = $v === true ? 'true' : $v;
        $v = $v === false ? 'false' : $v;
        return $v;
    }

    /*
     * Check if the tag name or attribute name contains illegal characters
     * Ref: http://www.w3.org/TR/xml/#sec-common-syn
     */
    private static function isValidTagName($tag){
        $pattern = '/^[a-z_]+[a-z0-9\:\-\.\_]*[^:]*$/i';
        return preg_match($pattern, $tag, $matches) && $matches[0] == $tag;
    }
}
?>