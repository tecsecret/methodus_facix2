<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Methodus - Página não encontrada</title>
</head>
<%
str_url = "http://"&Request.ServerVariables("SERVER_NAME")&Request.ServerVariables("URL")&"?"&replace(Request.ServerVariables("QUERY_STRING"),"'","")
%>
<body>
	<center>
    	<img src="http://www.methodus.com.br/site/_img/methodus_logotipo.png" /><BR />
        <h1>Página não encontrada</h1><BR />
        <b>URL acessada:</b> <%=str_url%><BR />
        <a href="http://www.methodus.com.br">Voltar para home</a><BR />
        <script type="text/javascript">
			function ir_home(){
				document.location = 'http://www.methodus.com.br';	
			}
			
			setTimeout("ir_home()",15000);
		</script>
    </center>
</body>
</html>
<%
if instr( str_url, "artigo" ) then
	url_redirect = "http://www.methodus.com.br/artigos.html"
	
elseif instr( str_url, "biblioteca" ) then
	url_redirect = "http://www.methodus.com.br/livros.html"
	
elseif instr( str_url, "alta-performance" ) then
	url_redirect = "http://www.methodus.com.br/leitura-dinamica.html"
	
elseif instr( str_url, "/c/" ) then
	url_redirect = replace( str_url, "/c/", "/site/" )
	
elseif instr( str_url, "/methodus/" ) then
	url_redirect = replace( str_url, "/methodus/", "/site/" )
		
elseif instr( str_url, "facix2" ) then
	url_redirect = "http://www.methodus.com.br/facix2/"
	
elseif instr( str_url, "cursos" ) then
	url_redirect = "http://www.methodus.com.br/cursos.html"

elseif instr( str_url, "agenda" ) then
	url_redirect = "http://www.methodus.com.br/calendario.html"

elseif instr( str_url, "contato" ) then
	url_redirect = "http://www.methodus.com.br/contato.html"
			
end if

if not url_redirect = empty then
	response.Clear()
	Response.Status="301 Moved Permanently"
	Response.AddHeader "Location",url_redirect
	response.End()
end if
%>