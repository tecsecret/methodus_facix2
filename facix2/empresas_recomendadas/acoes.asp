<!-- #include file="../_base/config.asp" -->
<!-- #INCLUDE FILE = "../_base/componente_upload.asp" -->
<%
call checalogado()

'' Vari�veis do componente de Upload ''
Dim oFO, oProps, oFile, i, item, oMyName
Set oFO = New FileUpload

Set oProps = oFO.GetUploadSettings
with oProps

	.Extensions = Array("jpg","gif","png")
	.UploadDirectory = Server.Mappath("../../asa/_arquivos/empresas/")
	.AllowOverWrite = true
	.MaximumFileSize = 5000000  '5 Mb
	.MininumFileSize = 1     '1 bytes
	.UploadDisabled = false
	
End with

oFO.ProcessUpload
'''''''''''''''''''''''''''''''''''''''



acao 		= 	formatar("acao",1,2)

titulo 		=	formatar(oFO.Form("titulo"),1,4)
descricao 	=	formatar(oFO.Form("descricao"),3,4)
ativo 		=	formatar(oFO.Form("ativo"),1,4)
telefone 	=	formatar(oFO.Form("telefone"),1,4)
email 		=	formatar(oFO.Form("email"),1,4)
site		=	formatar(oFO.Form("site"),1,4)

ID 			= 	formatar(oFO.Form("ID"),1,4)
if ID=empty then
	ID 		= 	formatar("ID",1,2)
end if


'INCLUS�O
if acao="incluir" then

	if titulo=empty or ativo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	
	'' Para fazer upload do arquivo ''
	set oFile 				= oFO.File("logo")
	'oFile.FileName			= ID&"."&oFile.FileExtension
	nome_arquivo 			= FORMATA_HORA( now(), 1 ) &"_"& oFile.FileName
	oFile.FileName			= nome_arquivo
	
	if not oFile.FileExtension=empty then
		if lcase(oFile.FileExtension) <> "jpg" and lcase(oFile.FileExtension) <> "gif" and lcase(oFile.FileExtension) <> "png" then
			'erro de extens�o
		else
			oFile.SaveAsFile
			
			'' Para formatar a imagem '''''''''''''''
			call FORMATA_IMAGEM( "../../asa/_arquivos/empresas/"& nome_arquivo, "../../asa/_arquivos/empresas/"& nome_arquivo, 320, 240, 2 )
			
		end if
	else
		nome_arquivo = ""
	end if
	'''''''''''''''''''''''''''''''''
	
	
	sql="insert into EMPRESAS_RECOMENDADAS (titulo_recomendacao, status_recomendacao, descricao_recomendacao, telefone_recomendacao, email_recomendacao, site_recomendacao, logo_recomendacao)"&_
		" values ('"&titulo&"',"&ativo&",'"&descricao&"','"&telefone&"','"&email&"','"&site&"','"&nome_arquivo&"')"
	conexao.execute(sql)

	session("sucesso") = "0001"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
'EXCLUIR
elseif acao="excluir" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if

	sql="select logo_recomendacao from EMPRESAS_RECOMENDADAS where ID_recomendacao="&ID&" and logo_recomendacao<>''"
	set rs = ABRIR_RS(sql)

	if not rs.EOF then
		call EXCLUIR_ARQUIVO( "../../asa/_arquivos/empresas/"& rs("logo_recomendacao") )
	end if
	
	sql="delete from EMPRESAS_RECOMENDADAS where ID_recomendacao="&ID
	conexao.execute(sql)
	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))

'EDITAR
elseif acao="editar" then

	if ID=empty or titulo=empty or ativo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	'' Para fazer upload do arquivo ''
	set oFile 				= oFO.File("logo")
	'oFile.FileName			= ID&"."&oFile.FileExtension
	nome_arquivo 			= FORMATA_HORA( now(), 1 ) &"_"& oFile.FileName
	oFile.FileName			= nome_arquivo
	
	if not oFile.FileExtension=empty then
		if lcase(oFile.FileExtension) <> "jpg" and lcase(oFile.FileExtension) <> "gif" and lcase(oFile.FileExtension) <> "png" then
			'erro de extens�o
		else
			oFile.SaveAsFile

			'' para remover imagem antiga
			sql="select logo_recomendacao from EMPRESAS_RECOMENDADAS where ID_recomendacao="&ID&" and logo_recomendacao<>''"
			set rs = ABRIR_RS(sql)

			if not rs.EOF then
				call EXCLUIR_ARQUIVO( "../../asa/_arquivos/empresas/"& rs("logo_recomendacao") )
			end if
			
			'' Para formatar a imagem '''''''''''''''
			call FORMATA_IMAGEM( "../../asa/_arquivos/empresas/"& nome_arquivo, "../../asa/_arquivos/empresas/"& nome_arquivo, 320, 240, 2 )
			
			sql="update EMPRESAS_RECOMENDADAS set logo_recomendacao='"&nome_arquivo&"' where ID_recomendacao="&ID
			conexao.execute(sql)
			
		end if
	else
		nome_arquivo = ""
	end if
	'''''''''''''''''''''''''''''''''

	sql="update EMPRESAS_RECOMENDADAS set titulo_recomendacao='"&titulo&"', descricao_recomendacao='"&descricao&"', status_recomendacao="&ativo&", "&_
		" telefone_recomendacao='"&telefone&"',email_recomendacao='"&email&"',site_recomendacao='"&site&"'"&_
		" where ID_recomendacao="&ID
	conexao.execute(sql)

	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")

'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>