<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Enviar Mala Direta@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>

<%
ID 	= 	formatar("ID",1,2)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_enviar_maladireta" method="post" action="maladireta_acoes.asp?acao=enviar" onSubmit="document.a_enviar_maladireta.enviar.disabled = true;">
	<input type="hidden" name="ID" value="<%=ID%>" />
    <input type="hidden" name="novo_envio" value="1" />
	<tr>
		<td class="titulo_02" width="50%"><b>Alunos</b></td>
		<td class="titulo_02" width="50%"><b>Banco de e-mails</b></td>
	</tr>
	<tr>
		<td>
			<!--<input type="checkbox" name="alunos" value="1" /> Enviar para banco de alunos cadastrados -->
            <!--<option value="3">Alunos (N�o Pagos)</option> -->
            <select name="alunos">
                <option value="">Nenhum Aluno</option>
                <option value="1">Todos</option>
                <option value="2">Alunos</option>
                <option value="4">Interessados</option>
            </select>
		</td>
		<td>
			<!--<input type="checkbox" name="banco" value="1" /> Enviar para banco de e-mails cadastrados -->
            <%
			sql="select titulo_grupo, ID_grupo from MALA_DIRETA_GRUPOS where status_grupo="&verdade&""
			set rs_cat= conexao.execute(sql)
			%>
            
            <input id="label_0" type="checkbox" name="banco" value="0" /> <label for="label_0">E-mails sem grupo</label> <BR />
            
			<%do while not rs_cat.EOF%>
                <input id="label_<%=rs_cat("ID_grupo")%>" type="checkbox" name="banco" value="<%=rs_cat("ID_grupo")%>" /> <label for="label_<%=rs_cat("ID_grupo")%>"><%=Server.HTMLEncode(rs_cat("titulo_grupo"))%></label>
                <BR />
            <%rs_cat.movenext:loop%>
		</td>
	</tr>
    <tr>
		<td><b>Apenas interessados em</b></td>
		<td><b>&nbsp;</b></td>
	</tr>
	<tr>
		<td>
            <select name="interesse">
                <option value="">Sem filtro por interesse</option>
                <%
				sql = 	"SELECT titulo_curso, ID_curso FROM CURSOS where status_curso="&verdade&" order by distancia_curso asc, titulo_curso asc"
				set rs = conexao.execute(sql)
				
				do while not rs.EOF
				%>
			
					<option value="id<%=rs("ID_curso")%>"><%=rs("titulo_curso")%></option>
				
				<%
				rs.movenext:loop
				%>
            </select>
		</td>
        <td>&nbsp;</td>
    </tr>
	<tr>
		<td class="titulo_02" colspan="2"><b>E-mails</b> (separe e-mails com ';')</td>
	</tr>
	<tr>
		<td colspan="2" align="right">
			<textarea name="emails" style="width:100%; height:200px;"></textarea><BR />
			<input type="checkbox" name="gravar" value="1" /> Salvar esses e-mails no banco de e-mails.
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<BR />
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input name="enviar" type="submit" value="Enviar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->