<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_principal_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Definir a vari�vel do bot�o voltar ' - ' 0: sem bot�o; 1:Voltar no hist�rico; 2: session(voltar) '
	voltar	=	PAGINA_VOLTAR( "../_principal/" )
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("P�gina Principal@../_principal/;Matr�culas@default.asp;Relat�rio Anual@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	
	session("voltar") = SESSION_VOLTAR()
	%>


<%
ano			=	formatar("ano",1,2)
ano2		=	formatar("ano2",1,2)

if ano=empty then
	ano = year(now())
end if
if ano2=empty then
	ano2 = year(now())
end if

estatus		=	formatar("estatus",1,2)
forma		=	formatar("forma",1,2)
pesquisa	=	formatar("pesquisa",1,2)

if pesquisa = empty then
	estatus = "3"
end if

cor			= 	"#FFFFFF"
%>


<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
    
    <!-- IN�CIO DA PESQUISA -->
	<tr id="tr_pesquisa02">
		<TD bgcolor="#FFFFDD" align="center">
		
			<table border="0" cellpadding="3" cellspacing="0" style="width:100%">
			<form name="pesquisa" method="get" action="relatorio_anual.asp">
				<input type="hidden" name="pesquisa" value="1" />
				<TR>
					<TD align="right"><b>de:</b> </TD>
					<TD>
						<select name="ano" style="width:100px;">
							<%
							sql_ano = "select top 1 data_inscricao from CURSOS_INSCRICOES order by data_inscricao"
							set rs_ano = ABRIR_RS(sql_ano)
							
							if not rs_ano.EOF then
								ano_inicio = year( rs_ano("data_inscricao") )
							else
								ano_inicio = 2000
							end if
							
                            for i = ano_inicio to year(now())
                            %>
                            	<option value="<%=i%>"<%if cint(ano)=i then%> selected<%end if%>><%=i%></option>
							<%next%>
						</select>
					</TD>	
					<TD align="right"><b>Status:</b> </TD>
					<TD>
						<select name="estatus" style="width:250px;">
							<option value="">Indiferente</option>
							<%
                            call CONEXAO_XML("../../facix2/_xml/pagamentos_cod_status.xml")
                            set registros = xmlDoc.documentElement
                            
                            for each registro in registros.childNodes
                            %>
                            	<option value="<%=registro.selectSingleNode("@codigo").text%>"<%if estatus=registro.selectSingleNode("@codigo").text then%> selected<%end if%>><%=registro.text%></option>
							<%next%>
						</select>
					</TD>
					<TD rowspan="2"><input type="submit" value="Pesquisar" style="width:110px; height:50px;" /></TD>
				</TR>
				<TR>
					<TD align="right"><b>�:</b> </TD>
					<TD>
						<select name="ano2" style="width:100px;">
							<%
                            for i = ano_inicio to year(now())
                            %>
                            	<option value="<%=i%>"<%if cint(ano2)=i then%> selected<%end if%>><%=i%></option>
							<%next%>
						</select>
					</TD>
									
					<TD align="right"><b>Meio de pagamento:</b> </TD>
					<TD>
						<select name="forma" style="width:250px;">
							<option value="">Indiferente</option>
                            <%
                            call CONEXAO_XML("../../facix2/_xml/pagamentos_meio.xml")
                            set registros = xmlDoc.documentElement
                            
                            for each registro in registros.childNodes
                            %>
                            	<option value="<%=registro.selectSingleNode("@codigo").text%>"<%if forma=registro.selectSingleNode("@codigo").text then%> selected<%end if%>><%=registro.text%></option>
							<%next%>
						</select>
					</TD>
				</TR>
			</form>
			</table>
		
		</TD>
	</tr>
	<!-- FIM DA PESQUISA -->

    <%
	for conta_ano = cint(ano) to cint(ano2)
	
		'' Criando query SQL considerando pesquisa ''
		sql="SELECT count(1) as qtd FROM CURSOS_INSCRICOES inner join CURSOS ON CURSOS_INSCRICOES.ID_curso = CURSOS.ID_curso inner join PAGAMENTOS on PAGAMENTOS.ID_inscricao = CURSOS_INSCRICOES.ID_inscricao "&_
			" inner join CURSOS_INSCRICOES_USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_inscricao = CURSOS_INSCRICOES.ID_inscricao inner join USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario=USUARIOS.ID_usuario "&_
			" where (CURSOS_INSCRICOES.ID_curso=[ID_CURSO] ) and YEAR(data_inscricao) = "& conta_ano &""
		
		if not estatus=empty then
			sql= sql + " and ( cod_status_pagamento='"&estatus&"' )"
		end if
		
		if not forma=empty then
			sql= sql + " and ( pagto_meio='"& forma &"')"
		end if
		
		'''''''''''''''''''''''''''''''''''''''''''''
		response.Write("<!--"&sql&"-->")
		%>
        
        
        <tr bgcolor="#CCCCCC">
            <td align="center"><b>Relat�rio do ano <%=conta_ano%></b></td>
        </tr>
        
        <tr bgcolor="#FFFFFF">
            <td align="center">
                
                <table border="1" cellpadding="4" cellspacing="0" width="100%">
                    <TR>
                        <TH width="250">
                            Curso / M�s
                        </TH>
                        <%
                        for i = 1 to 12
                            response.Write("<TH>"&i&"</TH>")
                        next
                        %>
                        <TH width="50">Total</TH>
                    </TR>
                    
                    <%
                    sql_curso = "select titulo_curso, ID_curso from CURSOS where status_curso="&verdade&" order by titulo_curso"
                    set rs_cat = conexao.execute(sql_curso)
                    
                    do while not rs_cat.EOF
                    %>
                    <TR>
                        <TD>
                            <%=rs_cat("titulo_curso")%>
                        </TD>
                        <%
                        for i = 1 to 12
                            sql_temp = replace( sql, "[ID_CURSO]", rs_cat("ID_curso") )
                            sql_temp = sql_temp & " and MONTH(data_inscricao)="&i
                            set rs = ABRIR_RS(sql_temp)
                            
                            if i = 12 then
                                dia2 = "31"
                            else
                                dia2 = day( dateadd( "d", -1, ano&"-"&(i+1)&"-1" ) )
                            end if
                            
                            response.Write("<TD><a href=""default.asp?pesquisa=1&ID_curso="&rs_cat("ID_curso")&"&estatus="&estatus&"&forma="&forma&"&dia1=1&mes1="&i&"&ano1="&conta_ano&"&dia2="&day( dateadd("d",-1,(dateadd("m",1,(conta_ano&"-"&i&"-1")))) )&"&mes2="&i&"&ano2="&conta_ano&""">"&rs("qtd")&"</a></TD>")
                        next
                        %>
                        <TD bgcolor="#FFFFCC">
                            <%
                            sql_temp = replace( sql, "[ID_CURSO]", rs_cat("ID_curso") )
                            set rs = ABRIR_RS(sql_temp)
                            
                            response.Write(rs("qtd"))
                            %>
                        </TD>
                    </TR>
                    <%rs_cat.movenext:loop%>
                    
                    <TR>
                        <TD bgcolor="#FFFFCC">
                            Total
                        </TD>
                        <%
                        for i = 1 to 12
                            sql_temp = replace( sql, "=[ID_CURSO]", "<>0" )
                            sql_temp = sql_temp & " and MONTH(data_inscricao)="&i
                            set rs = ABRIR_RS(sql_temp)
                            
                            response.Write("<TD bgcolor=""#FFFFCC"">"&rs("qtd")&"</TD>")
                        next
                        %>
                        <TD bgcolor="#FFFFCC">
                            <b>
                            <%
                            sql_temp = replace( sql, "=[ID_CURSO]", "<>0" )
                            set rs = ABRIR_RS(sql_temp)
                            
                            response.Write(rs("qtd"))
                            %>
                            </b>
                        </TD>
                    </TR>
                    
                </table>
                
            </td>
        </tr>
    
    <%next%>
	
</table>


<!--#include file="../_base/paginacao_links.asp"-->


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_principal_02.asp" -->