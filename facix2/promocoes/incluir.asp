<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Incluir C�digo Promocional@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>

<script language="javascript" src="../_base/js/chainedselects.js"></script>

<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE PROMO��O
function checa(formulario)
{
	campo	=	[
					formulario.codigo,
					formulario.dia,
					formulario.mes,
					formulario.ano,
					formulario.desconto
				];
	n_campo	=	[
					"C�digo",
					"Data",
					"Desconto"
				];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);

	if (checa_caracter(campo[1],n_campo[1],"0123456789")==false)
		return(false);
	if (checa_caracter(campo[2],n_campo[1],"0123456789")==false)
		return(false);
	if (checa_caracter(campo[3],n_campo[1],"0123456789")==false)
		return(false);
		
	if (checa_nulo(campo[4],n_campo[2])==false)
		return(false);
	if (checa_caracter(campo[4],n_campo[2],"0123456789,.")==false)
		return(false);
	
	return(true);
}
/////////////////////////////////////////////////////////////

function aleatorio(inferior,superior)
{ 
   numPossibilidades = superior - inferior 
   aleat = Math.random() * numPossibilidades 
   aleat = Math.floor(aleat) 
   return parseInt(inferior) + aleat 
} 


function gera_codigo()
{ 
   hexadecimal = new Array("1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","X","Z");
   cor_aleatoria = ""; 
   for (i=0;i<8;i++){ 
      posarray = aleatorio(0,hexadecimal.length);
      cor_aleatoria += hexadecimal[posarray];
   } 
   //return cor_aleatoria 
   document.getElementById("codigo").value = cor_aleatoria;
}

</script>

<%
ID_aluno	=	formatar("ID_aluno",1,2)

if ID_aluno = empty then
	ID_aluno = 0
end if
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_incluir_promocao" method="post" action="acoes.asp?acao=incluir" onSubmit="return checa(this);">
    <tr>
		<td colspan="2"><b>T�tulo (observa��o):</b></td>
	</tr>
	<tr>
		<TD colspan="2">
			<input type="text" name="observacoes" style="width:100%;" maxlength="250" />
		</TD>
	</tr>
	<tr>
		<td width="50%"><b>C�digo:</b></td>
		<td width="50%"><b>Desconto:</b></td>
	</tr>
	<tr>
		<TD>
			<input type="text" name="codigo" id="codigo" style="width:50%;" maxlength="50" /> <input type="button" value="Gerar Aleat�rio" onClick="return gera_codigo();" />
		</TD>
		<td>
            <input type="text" name="desconto" style="width:50px;" maxlength="7" />
            <input type="radio" name="dinheiro" value="<%=verdade%>"/> R$ 
			<input type="radio" name="dinheiro" value="<%=falso%>" checked  /> %
		</td>
	</tr>
	<tr>
		<td><b>Status:</b></td>
		<td><b>Expira��o:</b></td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="ativo" value="<%=verdade%>" checked /> Ativo 
			<input type="radio" name="ativo" value="<%=falso%>" /> Inativo
		</td>
		<td>
			<input type="text" name="dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" /> / 
			<input type="text" name="mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" /> / 
			<input type="text" name="ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" /> 
		</td>
	</tr>
    <tr>
		<td><b>Reutiliz�vel:</b> (se sim, poder� ser usado por v�rias pessoas at� a data de expira��o)</td>
        <td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="reutilizavel" value="<%=verdade%>" /> Sim
			<input type="radio" name="reutilizavel" value="<%=falso%>" checked /> N�o
		</td>
        <td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<BR />
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Incluir" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>

<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->