<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar C�digo Promocional@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>

<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// EDI��O DE PROMO��O
function checa(formulario)
{
	campo	=	[
					formulario.codigo,
					formulario.dia,
					formulario.mes,
					formulario.ano,
					formulario.desconto
				];
	n_campo	=	[
					"C�digo",
					"Data",
					"Desconto"
				];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);

	if (checa_caracter(campo[1],n_campo[1],"0123456789")==false)
		return(false);
	if (checa_caracter(campo[2],n_campo[1],"0123456789")==false)
		return(false);
	if (checa_caracter(campo[3],n_campo[1],"0123456789")==false)
		return(false);
		
	if (checa_nulo(campo[4],n_campo[2])==false)
		return(false);
	if (checa_caracter(campo[4],n_campo[2],"0123456789,.")==false)
		return(false);
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID 	= 	formatar("ID",1,2)

sql="select * from PROMOCOES_CODIGOS where ID_codigo="&ID
set rs= conexao.execute(sql)

if isnull(rs("vencimento_codigo")) = false then
	dia = day(rs("vencimento_codigo"))
	mes = month(rs("vencimento_codigo"))
	ano = year(rs("vencimento_codigo"))
end if
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_editar_promocao" method="post" action="acoes.asp?acao=editar" onSubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
    <tr>
		<td colspan="2"><b>T�tulo (observa��o):</b></td>
	</tr>
	<tr>
		<TD colspan="2">
			<input type="text" name="observacoes" style="width:100%;" maxlength="250" value="<%=rs("titulo_codigo")%>" />
		</TD>
	</tr>
	<tr>
		<td width="50%"><b>C�digo:</b></td>
		<td width="50%"><b>Desconto:</b></td>
	</tr>
	<tr>
		<TD>
			<input type="text" name="codigo" style="width:100%;" maxlength="50" value="<%=rs("texto_codigo")%>" />
		</TD>
		<td>
			<input type="text" name="desconto" style="width:50px;" maxlength="7" value="<%=rs("desconto_codigo")%>" /> 
            <input type="radio" name="dinheiro" value="<%=verdade%>"<%if rs("dinheiro_codigo")=true then%> checked<%end if%> /> R$ 
			<input type="radio" name="dinheiro" value="<%=falso%>"<%if rs("dinheiro_codigo")=false or isnull(rs("dinheiro_codigo")) then%> checked<%end if%>  /> %
		</td>
	</tr>
    <tr>
		<td><b>Status:</b></td>
		<td><b>Expira��o:</b></td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="ativo" value="<%=verdade%>"<%if rs("status_codigo")=true then%> checked<%end if%> /> Ativo 
			<input type="radio" name="ativo" value="<%=falso%>"<%if rs("status_codigo")=false then%> checked<%end if%> /> Inativo
		</td>
		<td>
			<input type="text" name="dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=dia%>" /> / 
			<input type="text" name="mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=mes%>" /> / 
			<input type="text" name="ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=ano%>" /> 
		</td>
	</tr>
    <tr>
		<td><b>Reutiliz�vel:</b> (se sim, poder� ser usado por v�rias pessoas at� a data de expira��o)</td>
        <td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="reutilizavel" value="<%=verdade%>"<%if rs("reutilizavel_codigo")=true then%> checked<%end if%> /> Sim
			<input type="radio" name="reutilizavel" value="<%=falso%>"<%if rs("reutilizavel_codigo")=false or isnull(rs("reutilizavel_codigo")) then%> checked<%end if%> /> N�o
		</td>
        <td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<BR />
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Editar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->