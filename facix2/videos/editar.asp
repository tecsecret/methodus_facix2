<%@ codepage="1252" language="VBScript" %>
<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar Video@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>
	<script src="../_ckeditor/ckeditor.js"></script>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// EDI��O DE ARTIGO
function checa(formulario)
{
	campo=[
			formulario.titulo
		  ];
	n_campo=[
			"T�tulo"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID 	= 	formatar("ID",1,2)
sql="select * from VIDEOS where ID_video="&ID
set rs= conexao.execute(sql)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_editar_artigo" method="post" action="acoes.asp?acao=editar" onSubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
	<tr>
		<td class="titulo_02" width="50%"><b>T�tulo</b></td>
		<td class="titulo_02" width="50%"><b>Status</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="titulo" style="width:100%" value="<%=Server.HTMLEncode(rs("titulo_video"))%>" />
		</td>
		<td>
			<input type="radio" name="ativo" value="<%=verdade%>"<%if rs("status_video")=true then%> checked<%end if%> /> Ativo 
			<input type="radio" name="ativo" value="<%=falso%>"<%if rs("status_video")=false then%> checked<%end if%> /> Inativo
		</td>
	</tr>
    <tr>
		<td class="titulo_02" width="50%"><b>Categoria</b></td>
		<td class="titulo_02" width="50%"><b>V�deo</b></td>
	</tr>
	<tr>
		<td>
			<%
			sql="select titulo_tema, ID_tema from TEMAS where status_tema="&verdade&""
			set rs_tema= conexao.execute(sql)
			%>
			<select name="tema" style="width:100%">
				<option value="0">Sem Categoria</option>
				<%do while not rs_tema.EOF%>
					<option value="<%=rs_tema("ID_tema")%>"<%if rs_tema("ID_tema")=rs("ID_tema") then%> selected="selected"<%end if%>><%=rs_tema("titulo_tema")%></option>
				<%rs_tema.movenext:loop%>
			</select>
		</td>
		<td>
        	<%
            'Configura��es do script:
            
            'Parametro passado pela URL quando se clica em uma determinada pasta ou arquivo:
            ParametroPasta = "anexos"
            
            'A session abaixo deve ser alterada para o caminho f�sico onde estar�o os arquivos e subdiret�rios a serem acessados:
            Session("Path") = Server.Mappath("../ftp/Uploads/videos")
            Path = Session("Path")
            'A vari�vel abaixo deve ser alterada para o caminho virtual das pastas no servidor:
            PathVirtualArquivos = ("../ftp/Uploads")
            'A vari�vel abaixo deve ser alterada para o caminho virtual deste script:
            PathScript = ("../ftp/default.asp")
            
            'Criando o objeto Filesystem Object:
            SET FSO = Server.CreateObject("Scripting.FileSystemObject")
            'Setando o caminho que ser� indicado como pasta raiz:
            Set Pasta = FSO.GetFolder(""&Path&"")
            'Solicitando a lista de arquivos para a pasta raiz:
            set arquivos = pasta.files
            'Setando a pasta raiz:
            set Raiz = Pasta
            'Verificando as subpastas da pasta raiz:
            Set Pastas = Raiz.SubFolders
            
            'Verificando o Nome da pasta Raiz:
            Nome = pasta.name
            
            'Verificando se h� subpastas:
            
            contador = 0
            for each subpastas in pastas
            contador = contador+1
            next
            %>        
        	
            <select name="video" style="width:100%;">
            	<option value="">-- Nenhum --</option>
                <%for each arquivo in arquivos%>
                	<%
					caminho = PathVirtualArquivos&"/"&ParametroPasta
					if ParametroPasta <> "" then
						caminho = caminho &"/"
					end if
					caminho = caminho & arquivo.name
					%>
                	<option value="<%=arquivo.name%>"<%if arquivo.name = rs("arquivo_video") then%> selected="selected"<%end if%>><%=Server.HTMLEncode(arquivo.name)%></option>
                <%next%>
            </select>
		</td>
	</tr>
	<tr>
		<td class="titulo_02" colspan="2"><b>Conte�do</b></td>
	</tr>
	<tr>
		<td colspan="2">
            <textarea id="editor1" name="corpo"><%=rs("corpo_video")%></textarea>
			<script>
                CKEDITOR.replace( 'editor1', {
                    height: '240px',
                    toolbar: 'Padrao'			
                });
            </script>	
		</td>
	</tr>
    <TR>
        <TD><b>Data para Publica��o:</b></TD>
        <TD>&nbsp;</TD>
    </TR>
    <TR>
        <TD>
            <input type="text" name="pub_dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=day(rs("data_video"))%>" /> / 
            <input type="text" name="pub_mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=month(rs("data_video"))%>" /> / 
            <input type="text" name="pub_ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=year(rs("data_video"))%>" />
            �s
            <input type="text" name="pub_hora" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=hour(rs("data_video"))%>" />h
            <input type="text" name="pub_min" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=minute(rs("data_video"))%>" />m
        </TD>
        <td>&nbsp;</td>
    </TR>
    <tr>
		<td><b>Imagem para home</b></td>
        <td valign="bottom"><b>Chamada para home</b></td>
	</tr>
    <tr>
		<td valign="top">
			<%call EXIBE_UPLOAD( "foto", "../../../methodus/_arquivos_videos/", "jpg,gif,png" )%>
            <%if not rs("foto_video")=empty then%>
				Arquivo atual: <a href="../../methodus/_arquivos_videos/<%=rs("foto_video")%>" target="_blank"><%=rs("foto_video")%></a>
			<%end if%>
		</td>
        <td valign="top">
        	<%
            palavras = ""
			if isnull(rs("chamada_video"))=false then
				palavras = Server.HTMLEncode(rs("chamada_video"))
			end if
            %>
			<textarea name="chamada" style="width:100%; height:50px;"><%=palavras%></textarea>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Editar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->