<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/cls_webservice.asp" -->
<!-- #include file="../_base/md5.asp" -->
<%
If (Request.Form.Count > 0) Then
	
	'' LOG
	call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "ETAPA 1: Foi chamada a campainha"  )
	
	'' script para montar chave de seguran�a ''
	string_seguranca = ""
	for j=1 to Request.Form.Count-1
		string_seguranca = string_seguranca & request.Form(j)
		'temp_mail = temp_mail &"<BR />"& request.Form(j)
	next
	
	'call ENVIAR_EMAIL( "renan@pensadigital.com.br", "renan@pensadigital.com.br", "ERRO - I-pagare", "Pedido completo: "&temp_mail )
	string_seguranca = md5( md5("h1l2t8d0") & string_seguranca )
	'''''''''''''''''''''''''''''''''''''''''''
	
	'' request da chave enviada pela i-pagare
	chave	= formatar("chave",1,1)
	
	
	'' se a chave montade for igual a enviada pode continuar
	'if chave = string_seguranca then

		'' Variaveis enviadas sempre
		uid_pedido			= formatar("uid_pedido",1,1)
		codigo_pedido		= formatar("codigo_pedido",1,1)
		codigo_status		= formatar("codigo_status",1,1)
		data_status			= formatar("data_status",1,1)
		hora_status			= formatar("hora_status",1,1)
		'' Variaveis enviadas quando "aguardando pagamento" ou "pagamento confirmado"
		codigo_pagamento	= formatar("codigo_pagamento",1,1)
		forma_pagamento		= formatar("forma_pagamento",1,1)
		valor_pagamento		= formatar("valor_pagamento",1,1)
		
		'' LOG
		call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "ETAPA 2: UID do pedido: "&uid_pedido&" / C�digo do pedido: "&codigo_pedido  )
		
		''##########################################################################''
		'' DOCUMENTA��O B�SICA SOBRE A I-PAGARE
		'' ------------------------------------
		'' Codigo do Status (int):
		'' 1 - Aguardando Pagamento
		'' 2 - Aguardando confirma��o de pagamento
		'' 3 - Pagamento confirmado
		'' 4 - Cancelado
		'' 5 - Pagamento expirado
		'' ------------------------------------
		'' Codigo do Pagamento (int):
		'' 2 - Mastercard - Komerci
		'' 11 - American Express � WebPOS
		'' 1 - Diners - Komerc
		'' 7 - Visa � Verified by Visa
		'' ------------------------------------
		'' Formas de Pagamento (varchar):
		'' A01 - � vista		| 
		'' A02 - 2x sem juros	| B02 - 2x com juros
		'' A03 - 3x sem juros	| B03 - 3x com juros
		'' A04 - 4x sem juros	| B04 - 4x com juros
		'' A05 - 5x sem juros	| B05 - 5x com juros
		'' A06 - 6x sem juros	| B06 - 6x com juros
		'' A07 - 7x sem juros	| B07 - 7x com juros
		'' A08 - 8x sem juros	| B08 - 8x com juros
		'' A09 - 9x sem juros	| B09 - 9x com juros
		'' A10 - 10x sem juros	| B10 - 10x com juros
		'' A11 - 11x sem juros	| B11 - 11x com juros
		'' A12 - 12x sem juros	| B12 - 12x com juros
		'' ------------------------------------
		'' Valor do pagamento:
		'' � enviado no formato sem virgulas ou pontos - R$ 50,00 -> 5000
		'' ------------------------------------
		'' Data do status:
		'' � enviado no formato AAAAMMDD
		'' ------------------------------------
		'' Hora do status:
		'' � enviado no formato HHMMSS
		'' ------------------------------------
		''##########################################################################''
		
		
		'' Para formatar o campo de Valor ''
		if not valor_pagamento=empty then
			valor_pagamento = replace(replace(valor_pagamento,".",""),",","")
			valor_pagamento = left( valor_pagamento, (len(valor_pagamento)-2) ) &"."& right( valor_pagamento, 2 )
		else
			valor_pagamento = 0
		end if
		''''''''''''''''''''''''''''''''''''
		
		'' Para formatar o campo de Data  ''
		'data_status = mid(data_status,1,4) & "-" & mid(data_status,5,2) & "-" & mid(data_status,7,2)
		'data_status = mid(data_status,7,2) & "/" & mid(data_status,5,2) & "/" & mid(data_status,1,4)
		data_status = FORMATA_DATA( dateadd( "m", 1, now() ) , 3 )
		'hora_status = mid(hora_status,1,2) & ":" & mid(hora_status,3,2) & ":" & mid(hora_status,5,2)
		hora_status = FORMATA_HORA( now() , 4 )
		''''''''''''''''''''''''''''''''''''

		
		'' verificar se j� existe registro de pagamento com esse c�digo ''
		sql = "select * from PAGAMENTOS where ID_inscricao="&codigo_pedido&" and identificacao_pagamento='"&uid_pedido&"'"
		set rs = conexao.execute(sql)
		
		if not rs.EOF then
		
			ID_pagamento = rs("ID_pagamento")
			
			'' Atualizar o registro de pagamento j� existente
			sql = 	"update PAGAMENTOS set cod_status_pagamento='"&codigo_status&"', data_pagamento='"&data_status&" "&hora_status&"',"&_
					" forma_pagamento='"&forma_pagamento&"', meio_pagamento='"&codigo_pagamento&"', valor_pagamento="&valor_pagamento&""
					
			if codigo_status=3 or codigo_status="3" then
				'sql = sql & ", parcelas_pagamento=';p1:1;p2:1;p3:1;p4:1;p5:1;p6:1;p7:1;p8:1;p9:1;p10:1;p11:1;p12:1'"
				sql = sql & ", parcelas_pagamento=';p1:0;p2:0;p3:0;p4:0;p5:0;p6:0;p7:0;p8:0;p9:0;p10:0;p11:0;p12:0'"
			end if 
					
			sql = sql & " where ID_pagamento="&ID_pagamento
			conexao.execute(sql)
			
			'' LOG
			call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "ETAPA 3: Fez Update do pagamento de ID "&ID_pagamento  )
			
			if codigo_status=3 or codigo_status="3" then
				call LIBERAR_ACESSO( codigo_pedido )
				call ENVIAR_CONTRATO( ID_pagamento )
				call CONVERSAO_RDSTATION( null, null, codigo_pedido, valor_pagamento )
			end if
			
			'' LOG
			call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "ETAPA 4: FIM com sucesso "&ID_pagamento  )
			
			response.Write("OK")
		
		else
			
			'' Criar um novo registro de pagamento para o pedido
			sql =	"insert into PAGAMENTOS (ID_inscricao, identificacao_pagamento, cod_status_pagamento, data_pagamento, "&_
					"forma_pagamento, meio_pagamento, valor_pagamento"
					
			if codigo_status=3 or codigo_status="3" then
				sql = sql & ", parcelas_pagamento"
			end if 	
					
			sql = sql & ") values ("&codigo_pedido&",'"&uid_pedido&"','"&codigo_status&"','"&data_status&" "&hora_status&"', "&_
						"'"&forma_pagamento&"','"&codigo_pagamento&"',"&valor_pagamento
					
			if codigo_status=3 or codigo_status="3" then
				'sql = sql & ", ';p1:1;p2:1;p3:1;p4:1;p5:1;p6:1;p7:1;p8:1;p9:1;p10:1;p11:1;p12:1'"
				sql = sql & ", ';p1:0;p2:0;p3:0;p4:0;p5:0;p6:0;p7:0;p8:0;p9:0;p10:0;p11:0;p12:0'"
			end if 		
			
			sql = sql & ") select @@identity as NovoId"
			set rs = conexao.execute(sql).nextrecordset
			
			ID_pagamento = rs("NovoId")
			
			'' LOG
			call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "ETAPA 3: Fez Insert do pagamento de ID "&ID_pagamento  )
			
			if codigo_status=3 or codigo_status="3" then
				call LIBERAR_ACESSO( codigo_pedido )
				call ENVIAR_CONTRATO( ID_pagamento )
				call CONVERSAO_RDSTATION( null, null, codigo_pedido, valor_pagamento )
			end if
			
			'' LOG
			call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "ETAPA 4: FIM com sucesso "&ID_pagamento  )
			
			response.Write("OK")
		
		end if
		
		
		
	'else
	
		'response.Write ( "ERRO - Chave errada") ': "&chave&" / "&string_seguranca )
		
		'' Se a chave n�o for igual mandar email para Renan (APENAS PARA TESTES)
		'call ENVIAR_EMAIL( "renan@pensadigital.com.br", "renan@pensadigital.com.br", "ERRO - I-pagare", "Chave errada: "&chave&" / "&string_seguranca )
	
	'end if

else
	response.Write ( "ERRO - Sem Par�metros" )
end if
%>