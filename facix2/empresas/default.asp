<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_principal_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Definir a vari�vel do bot�o voltar ' - ' 0: sem bot�o; 1:Voltar no hist�rico; 2: session(voltar) '
	voltar	=	PAGINA_VOLTAR( "../_principal/" )
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("P�gina Principal@../_principal/;Empresas@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	
	session("voltar") = SESSION_VOLTAR()
	%>


<%
pesquisa	=	formatar("pesquisa",1,2)
nome		=	formatar("nome",1,2)
estado		=	formatar("estado",1,2)
tipo		=	formatar("tipo",1,2)
dia1		=	formatar("dia1",1,2)
mes1		=	formatar("mes1",1,2)
ano1		=	formatar("ano1",1,2)
dia2		=	formatar("dia2",1,2)
mes2		=	formatar("mes2",1,2)
ano2		=	formatar("ano2",1,2)
data_inicio =	( dia1&"/"&mes1&"/"&ano1&" 00:00:00" )
data_fim	=	( dia2&"/"&mes2&"/"&ano2&" 23:59:59" )

cor			= 	"#FFFFFF"


'' Se foi feita alguma pesquisa, carregar a p�gina j� com a janela de pesquisa aberta ''
if pesquisa = "1" then
	status_01 = "none"
	status_02 = "block"
else
	status_01 = "block"
	status_02 = "none"
end if
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Criando query SQL considerando pesquisa ''
sql="select ID_empresa, razao_empresa, tipo_empresa from EMPRESAS where ID_empresa > 0"

	if not nome=empty then
		sql=sql+" and razao_empresa like '%"&FORMATAR_PESQUISA(nome,true)&"%'"
	end if
	if not estado=empty then
		sql=sql+" and estado_empresa='"&estado&"'"
	end if

	if isdate(data_inicio) then
		sql = sql & " and data_empresa >= '"&DATA_BD( data_inicio )&"'"
	end if
	
	if isdate(data_fim) then
		sql = sql & " and data_empresa <= '"&DATA_BD( data_fim )&"'"
	end if
	
	if not tipo = empty then
		
		select case tipo
		
			case "0"
				sql = sql & " and tipo_empresa is null"
				
			case else
				sql = sql & " and tipo_empresa="&tipo&""
		
		end select
		
	end if

sql_conta = replace( sql, "select ID_empresa, razao_empresa, tipo_empresa", "select count(ID_empresa) as qtd" )
set rs_conta = conexao.execute(sql_conta)

sql=sql&" order by razao_empresa"
'''''''''''''''''''''''''''''''''''''''''''''

session("sql") = sql
%>
<!-- #include file="../_base/paginacao.asp" -->


<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	<tr id="tr_menu" bgcolor="#EEEEEE" style="display:<%=status_01%>;">
		<TD colspan="4" align="center" height="30">
			<a href="javascript:" onClick="exibir_acao( 'incluir.asp' );" class="link_02"><img src="../_img/icone_incluir.gif" width="32" height="32" align="absmiddle" border="0" /> Incluir nova Empresa</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="javascript:" onClick="abrir_pesquisa(true);" class="link_02"><img src="../_img/icone_pesquisar.gif" width="38" height="32" align="absmiddle" border="0" /> Pesquisar Empresas</a>
		</TD>
	</tr>
	
	<!-- IN�CIO DA PESQUISA -->
	<tr id="tr_pesquisa01" style="display:<%=status_02%>;">
		<TD colspan="4" height="5" bgcolor="#EEEEEE" align="center"><a href="javascript:" onClick="abrir_pesquisa(false);" class="link_04">(X) Fechar Pesquisa</a></TD>
	</tr>
	<tr id="tr_pesquisa02" style="display:<%=status_02%>;">
		<TD colspan="4" bgcolor="#FFFFDD" align="center">
		
			
			<table border="0" cellpadding="3" cellspacing="0">
			<form name="pesquisa" method="get" action="default.asp">
				<input type="hidden" name="pesquisa" value="1" />
				<TR>
                	<TD align="right"><b>Empresa:</b> </TD>
					<TD><input type="text" name="nome" style="width:300px;" value="<%=nome%>" /></TD>
					<TD align="right"><b>Estado:</b> </TD>
					<TD>
						<select name="estado">
							<option value="">Todos</option>
						<%
						call CONEXAO_XML("../_xml/estados.xml")
						set estados = xmlDoc.documentElement
						
						for each registro in estados.childNodes
						%>
							<option value="<%=registro.text%>"<%if registro.text=estado then%> selected<%end if%>><%=registro.text%></option>
						<%next%>
						</select>
					</TD>
					
					<TD rowspan="3"><input type="submit" value="Pesquisar" style="width:90px; height:70px;" /></TD>
				</TR>
                <TR>
					<TD align="right" colspan="2"><b>Cadastro:</b> entre
						
						<input type="text" name="dia1" style="width:25px;" value="<%=dia1%>" maxlength="2" /> / 
						<input type="text" name="mes1" style="width:25px;" value="<%=mes1%>" maxlength="2" /> / 
						<input type="text" name="ano1" style="width:45px;" value="<%=ano1%>" maxlength="4" />
						
						e
						
						<input type="text" name="dia2" style="width:25px;" value="<%=dia2%>" maxlength="2" /> / 
						<input type="text" name="mes2" style="width:25px;" value="<%=mes2%>" maxlength="2" /> / 
						<input type="text" name="ano2" style="width:45px;" value="<%=ano2%>" maxlength="4" />
						
					</TD>
                    <TD align="right"><b>Tipo:</b> </TD>
					<TD>
						<select name="tipo">
							<option value="">Todos</option>
							<option value="0"<%if tipo="0" then%> selected<%end if%>>Relacionadas a inscri��o</option>
                            <option value="1"<%if tipo="1" then%> selected<%end if%>>In Company</option>
                            <option value="2"<%if tipo="2" then%> selected<%end if%>>Conv�nios</option>
						</select>
					</TD>
				</TR>
			</form>
			</table>
		
		</TD>
	</tr>
	<!-- FIM DA PESQUISA -->
	
	<tr bgcolor="#CCCCCC">
		<td align="center" class="titulo_02"><b>Empresa</b></td>
		<td align="center" class="titulo_02"><b>Tipo</b></td>
        <td align="center" class="titulo_02" width="80"><b>A��es</b></td>
	</tr>
	<%Count = 0
	do while not rs.EOF And Count < rs.PageSize%>
	<tr bgcolor="<%=cor%>" onMouseOut="mouse_out(this,'<%=cor%>');" onMouseOver="mouse_over(this,'#FFFFEE');">
		<td style="cursor:pointer;" onClick="abrir_pesquisa(false); exibir_acao( 'empresa_default.asp?ID=<%=rs("ID_empresa")%>' );"><%=rs("razao_empresa")%></td>
		<td style="cursor:pointer;" onClick="abrir_pesquisa(false); exibir_acao( 'empresa_default.asp?ID=<%=rs("ID_empresa")%>' );">
		
			<%if rs("tipo_empresa")=1 then%>
            	In Company
            <%elseif rs("tipo_empresa")=2 then%>
            	Conv�nio
            <%else%>
            	Relacionada a uma inscri��o
            <%end if%>
        
        </td>
		<td align="center">	
            <a href="javascript:" onClick="abrir_pesquisa(false); exibir_acao( 'editar.asp?ID=<%=rs("ID_empresa")%>' );"><img src="../_img/icone_editar.gif" width="15" height="16" border="0" align="absmiddle" alt="Editar" /></a>
			
            <%if isnull(rs("tipo_empresa"))=false then%>
                 | 	
                <a href="#" onClick="return confirmar('acoes.asp?ID=<%=rs("ID_empresa")%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>
			<%end if%>
        </td>
	</tr>
	<%
	if cor="#FFFFFF" then
		cor= "#EEEEEE"
	else
		cor= "#FFFFFF"
	end if
	
	Count = Count + 1
	rs.movenext:loop
	%>
	
	<%
	conta= Count
	do while conta < 20
	%>
	<tr bgcolor="<%=cor%>">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td>&nbsp;</td>
	</tr>
	<%
	if cor="#FFFFFF" then
		cor= "#EEEEEE"
	else
		cor= "#FFFFFF"
	end if
	
	conta = conta + 1
	loop
	%>
	
</table>
<font color="#FF0000"><%=rs_conta("qtd")%> registro(s) retornados</font>
 | 
<a href="exportar_default.asp"><img src="../_img/icone_salvar.gif" width="14" height="14" border="0" align="absmiddle" /> Exportar emails listados</a>


<!-- COME�O DOS LINKS DE PAGINA��O -->
	<%if count <> 0 then%>
	<BR>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<TR>
		<TD align="center">
			<%if totalpages > 1 then%>
				<%if cint(curpage) > 1 then%><a href="default.asp?pesquisa=<%=pesquisa%>&estado=<%=estado%>&nome=<%=nome%>&email=<%=email%>&dia1=<%=dia1%>&mes1=<%=mes1%>&ano1=<%=ano1%>&dia2=<%=dia2%>&mes2=<%=mes2%>&ano2=<%=ano2%>&tipo=<%=tipo%>&curpage=<%=curpage-1%>" class="link_01"><%else%><font color="#CCCCCC"><%end if%>
					� P�gina Anterior
				<%if cint(curpage) > 1 then%></a><%else%></font><%end if%>
				| 
				<%if cint(curpage) < totalpages then%><a href="default.asp?pesquisa=<%=pesquisa%>&estado=<%=estado%>&nome=<%=nome%>&email=<%=email%>&dia1=<%=dia1%>&mes1=<%=mes1%>&ano1=<%=ano1%>&dia2=<%=dia2%>&mes2=<%=mes2%>&ano2=<%=ano2%>&tipo=<%=tipo%>&curpage=<%=curpage+1%>" class="link_01"><%else%><font color="#CCCCCC"><%end if%>
					Pr�xima P�gina �
				<%if cint(curpage) < totalpages then%></a><%else%></font><%end if%>
				<img src="../img/b.gif" width="1" height="20" align="texttop" />
			<%else%>
				&nbsp;
			<%end if%>
		</TD>
		</TR>
		<TR>
		<TD align="center">
			P�gina 
			<%if totalpages > 1 then%>
				<select name="paginas" class="entrada" onChange="javascript:document.location=paginas.value">
				<%
				conta=1
				do while conta <= totalpages
				%>
					<option value="default.asp?pesquisa=<%=pesquisa%>&estado=<%=estado%>&nome=<%=nome%>&email=<%=email%>&dia1=<%=dia1%>&mes1=<%=mes1%>&ano1=<%=ano1%>&dia2=<%=dia2%>&mes2=<%=mes2%>&ano2=<%=ano2%>&tipo=<%=tipo%>&curpage=<%=conta%>"<%if cint(curpage)=conta then%> selected<%end if%>><%=conta%></option>
				<%
				conta=conta+1
				loop
				%>
				</select>
			<%else%>
				1
			<%end if%>
			<%if totalpages > 1 then%>
				 de <%=totalpages%>
			<%end if%>
		</TD> 
		</TR>
	</table>	 
	<%end if%>
<!-- FIM DOS LINKS DE PAGINA��O -->


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_principal_02.asp" -->