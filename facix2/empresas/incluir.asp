<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Incluir Empresa@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

///////////////////////////
function checa(formulario)
{
	campo=[	
				formulario.razao,
				formulario.cnpj,
				formulario.email,
				formulario.telefone,
				formulario.nome,
				formulario.cargo,
				formulario.endereco,
				formulario.bairro,
				formulario.cidade,
				formulario.cep1,
				formulario.cep2,
				formulario.funcionarios
			];
	n_campo=[
				"Raz�o",
				"CNPJ",
				"E-mail",
				"Telefone",
				"Nome",
				"Cargo",
				"Endere�o",
				"Bairro",
				"Cidade",
				"CEP",
				"CEP",
				"N�mero de Funcion�rios"
			];


	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
		
	if (campo[1].value != "")
	{
		if ( checa_cnpj( campo[1] ) == false )
		{
			return(false);
		}
	}
	
	if (campo[2].value != "")
	{
		if (checa_email(campo[2],n_campo[2])==false)
			return(false);
	}
	
	if (checa_caracter(campo[3],n_campo[3],"1234567890- .")==false)
		return(false);	
		
	if (checa_caracter(campo[9],n_campo[9],"1234567890")==false)
		return(false);
	if (checa_caracter(campo[10],n_campo[10],"1234567890")==false)
		return(false);
		
	if (checa_caracter(campo[11],n_campo[11],"1234567890")==false)
		return(false);


	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_editar" method="post" action="acoes.asp?acao=incluir" onSubmit="return checa(this);">
	<tr>
		<td>
		
			<table border="0" cellpadding="1" cellspacing="0" width="100%">
				<TR>
					<TD width="50%"><b>Raz�o:</b></TD>
					<TD width="50%"><b>CNPJ:</b></TD>
				</TR>
				<TR>
					<TD>
						<input type="text" name="razao" style="width:100%" maxlength="240" />
					</TD>
					<TD>
						<input type="text" name="cnpj" style="width:100%" maxlength="20" />
                    </TD>
				</TR>
				<TR>
					<TD width="50%"><b>E-mail:</b></TD>
					<TD width="50%"><b>Tipo</b></TD>
				</TR>
				<TR>
					<TD><input type="text" name="email" style="width:100%" maxlength="150" /></TD>
					<TD> 
                        <select name="tipo">
                            <option value="1">In Company</option>
                            <option value="2">Conv�nio</option>
                        </select>
                    </TD>
				</TR>
				<TR>
					<TD width="50%"><b>Telefone:</b></TD>
					<TD width="50%"><b>Nome do Contato:</b></TD>
				</TR>
				<TR>
					<TD><input type="text" name="telefone" style="width:100%" maxlength="15" /></TD>
					<TD><input type="text" name="nome" style="width:100%" maxlength="200" /></TD>
				</TR>
                <TR>
					<TD width="50%"><b>Cargo do Contato:</b></TD>
					<TD width="50%">&nbsp;</TD>
				</TR>
				<TR>
					<TD><input type="text" name="cargo" style="width:100%" maxlength="200" /></TD>
					<TD>&nbsp;
                    	
                    </TD>
				</TR>
				<TR>
					<TD width="50%"><b>Endere�o:</b></TD>
					<TD width="50%"><b>Bairro:</b></TD>
				</TR>
				<TR>
					<TD><input type="text" name="endereco" style="width:100%" maxlength="180" /></TD>
					<TD><input type="text" name="bairro" style="width:100%" maxlength="90" /></TD>
				</TR>
				<TR>
					<TD width="50%"><b>Cidade:</b></TD>
					<TD width="50%"><b>CEP:</b></TD>
				</TR>
				<TR>
					<TD><input type="text" name="cidade" style="width:100%" maxlength="90" /></TD>
					<TD><input type="text" name="cep1" style="width:50px" maxlength="5" onKeyUp="return autoTab(this, 5, event);" /> - <input type="text" name="cep2" style="width:30px" maxlength="3" /></TD>
				</TR>
				<TR>
					<TD width="50%"><b>Estado:</b></TD>
					<TD width="50%"><b>N�mero de Funcion�rios:</b></TD>
				</TR>
				<TR>
					<TD>
						<select name="estado">
						<%
						call CONEXAO_XML("../_xml/estados.xml")
						set estados = xmlDoc.documentElement
						
						for each registro in estados.childNodes
						%>
							<option value="<%=registro.text%>"<%if registro.text="SP" then%> selected<%end if%>><%=registro.text%></option>
						<%next%>
						</select>
					</TD>
					<TD><input type="text" name="funcionarios" style="width:100%" maxlength="5" /></TD>
				</TR>
				<TR>
					<TD width="50%"><b>Colaborador 01:</b></TD>
					<TD width="50%"><b>Colaborador 02:</b></TD>
				</TR>
				<TR>
					<TD><input type="text" name="colaborador1" style="width:100%" maxlength="150" /></TD>
					<TD><input type="text" name="colaborador2" style="width:100%" maxlength="150" /></TD>
				</TR>
				<TR>
					<TD width="50%"><b>Colaborador 03:</b></TD>
					<TD width="50%"><b>�rea de atua��o:</b></TD>
				</TR>
				<TR>
					<TD><input type="text" name="colaborador3" style="width:100%" maxlength="150" /></TD>
					<TD><input type="text" name="area" style="width:100%" maxlength="150" /></TD>
				</TR>
				<TR>
					<TD width="50%"><b>Quantidade de colaboradores:</b></TD>
					<TD width="50%">&nbsp;</TD>
				</TR>
				<TR>
					<TD><input type="text" name="qtd_colaboradores" style="width:100%" maxlength="50" /></TD>
					<TD>&nbsp;</TD>
				</TR>
                <TR>
					<TD colspan="2">
						<b>Interesses:</b><BR>
						<textarea name="interesses" style="width:100%; height:100px;"></textarea>
					</TD>
				</TR>
			</table>
			
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Incluir" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->