<%
'Bloco de c�digo que faz com que a p�gina n�o seja armazenada em cache, evitando que o usu�rio
'visualize arquivos/pastas que n�o est�o mais no servidor ou n�o visualize arquivos/pastas novos:

Response.Buffer=true
Response.AddHeader "cache-control", "private"
Response.AddHeader "pragma", "no-cache"
Response.ExpiresAbsolute = #January 1, 1990 00:00:01#
Response.Expires=Now()-1
Response.AddHeader "Cache-Control", "must-revalidate"
Response.AddHeader "Cache-Control", "no-cache"
%>



<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_principal_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->


<%
'Configura��es do script:

'Parametro passado pela URL quando se clica em uma determinada pasta ou arquivo:
ParametroPasta = Request.QueryString("Pasta")

'A session abaixo deve ser alterada para o caminho f�sico onde estar�o os arquivos e subdiret�rios a serem acessados:
Session("Path") = Server.Mappath("../ftp/Uploads/"&ParametroPasta)
Path = Session("Path")
'A vari�vel abaixo deve ser alterada para o caminho virtual das pastas no servidor:
PathVirtualArquivos = ("../ftp/Uploads")
'A vari�vel abaixo deve ser alterada para o caminho virtual deste script:
PathScript = ("../ftp/default.asp")

'Criando o objeto Filesystem Object:
SET FSO = Server.CreateObject("Scripting.FileSystemObject")
'Setando o caminho que ser� indicado como pasta raiz:
Set Pasta = FSO.GetFolder(""&Path&"")
'Solicitando a lista de arquivos para a pasta raiz:
set arquivos = pasta.files
'Setando a pasta raiz:
set Raiz = Pasta
'Verificando as subpastas da pasta raiz:
Set Pastas = Raiz.SubFolders

'Verificando o Nome da pasta Raiz:
Nome = pasta.name

'Verificando se h� subpastas:

contador = 0
for each subpastas in pastas
contador = contador+1
next
%>


	<%
	' Definir a vari�vel do bot�o voltar ' - ' 0: sem bot�o; 1:Voltar no hist�rico; 2: session(voltar) '
	voltar	=	PAGINA_VOLTAR( "../_principal/" )
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("P�gina Principal@../_principal/;FTP@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	
	session("voltar") = SESSION_VOLTAR()
	%>


<%
cor			= 	"#FFFFFF"
%>



<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	<tr id="tr_menu" bgcolor="#EEEEEE" style="display:block;">
		<TD colspan="4" align="center" height="30">
			<%if ParametroPasta = "" then%>
			<a href="javascript:" onClick="exibir_acao( 'pasta_incluir.asp?pasta=<%=ParametroPasta%>' );" class="link_02"><img src="../_img/icone_gerenciar.gif" width="41" height="32" align="absmiddle" border="0" /> Incluir Pasta</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
			<%end if%>
			
			<a href="javascript:" onClick="exibir_acao( 'incluir.asp?pasta=<%=ParametroPasta%>' );" class="link_02"><img src="../_img/icone_incluir.gif" width="32" height="32" align="absmiddle" border="0" /> Incluir Arquivo</a>
		</TD>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td align="center"><b>Arquivo</b></td>
        <td align="center"><b>Caminhoo</b></td>
		<td align="center"><b>Tamanho</b></td>
		<td align="center"><b>A��es</b></td>
	</tr>
	
	<%if ParametroPasta <> "" then%>
	<tr bgcolor="<%=cor%>">
		<td colspan="4"><a href="<%=PathScript%>"><img src="img/pasta_up.gif" width="16" height="18" border="0" align="absmiddle" /> Um n�vel acima ...</a></td>
	</tr>
	<%end if%>

	<%if contador >= 0 then%>
	
		
	<%for each subpastas in Pastas%>
	<tr bgcolor="<%=cor%>">
		<td colspan="3"><a href="<%=PathScript%>?Pasta=<%=ParametroPasta%><%=subpastas.name%>"><img src="img/pasta.png" width="16" height="14" align="absmiddle" border="0" /> <%=subpastas.name%></a></td>
		<td align="center">
			<a href="#" onClick="return confirmar('acoes.asp?pasta=<%=subpastas.name%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>
		</td>
	</tr>
	<%
	if cor="#FFFFFF" then
		cor= "#EEEEEE"
	else
		cor= "#FFFFFF"
	end if
	
	next
	%>
	
	
	<%for each arquivo in arquivos%>
	<tr bgcolor="<%=cor%>">
		<td>
			<a href="<%=PathVirtualArquivos%>/<%=ParametroPasta%><%if ParametroPasta <> "" then%>/<%end if%><%=arquivo.name%>" target="_blank">
			
				<%
				if right(arquivo.name,3) = "zip" then
				%>
					<img src="img/arquivo_zip.gif" width="15" height="16" border="0" align="absmiddle" />
				<%
				elseif right(arquivo.name,3) = "pdf" then
				%>
					<img src="img/arquivo_pdf.gif" width="16" height="16" border="0" align="absmiddle" />
				<%
				elseif right(arquivo.name,3) = "exe" then
				%>
					<img src="img/arquivo_exe.gif" width="18" height="18" border="0" align="absmiddle" />
				<%
				else
				%>
					<img src="img/arquivo_documento.gif" width="12" height="14" border="0" align="absmiddle" />
				<%end if%>
			
			<%=arquivo.name%>
			
			</a>
		</td>
        <TD>
            <%
			caminho = PathVirtualArquivos&"/"&ParametroPasta
			if ParametroPasta <> "" then
				caminho = caminho &"/"
			end if
			caminho = caminho & arquivo.name
			
			response.Write(Server.MapPath(caminho))
			%>
        </TD>
		<td align="center">
			<%
			tamanho = 0
			
			if arquivo.size <= 999 then
				tamanho = arquivo.size & " bytes"
			elseif arquivo.size <= 999999 then
				tamanho = formatnumber((arquivo.size/1024),2) & " Kb"
			else
				tamanho = formatnumber(((arquivo.size/1024)/1024),2) & " Mb"
			end if
			%>
		
			<%=tamanho%>
		</td>
		<td align="center">
			<a href="#" onClick="return confirmar('acoes.asp?pasta=<%=ParametroPasta%>&ID=<%=arquivo.name%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>
		</td>
	</tr>
	<%
	if cor="#FFFFFF" then
		cor= "#EEEEEE"
	else
		cor= "#FFFFFF"
	end if
	
	next
	%>
	
	<%end if%>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_principal_02.asp" -->