<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_principal_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Definir a vari�vel do bot�o voltar ' - ' 0: sem bot�o; 1:Voltar no hist�rico; 2: session(voltar) '
	voltar	=	PAGINA_VOLTAR( "../_principal/" )
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("P�gina Principal@../_principal/;Recomenda��es@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	
	session("voltar") = SESSION_VOLTAR()
	%>


<%
palavra_chave	=	formatar("palavra_chave",1,2)
convertidos		=	formatar("convertidos",1,2)
livro			=	formatar("livro",1,2)
solicitou		=	formatar("solicitou",1,2)
dia1			=	formatar("dia1",1,2)
mes1			=	formatar("mes1",1,2)
ano1			=	formatar("ano1",1,2)
dia2			=	formatar("dia2",1,2)
mes2			=	formatar("mes2",1,2)
ano2			=	formatar("ano2",1,2)
entregue		=	formatar("entregue",1,2)
lembrete		=	formatar("lembrete",1,2)

if dia1 = empty or mes1=empty or ano1=empty then
	dia1 = day(dateadd("d",-60,now()))
	mes1 = month(dateadd("d",-60,now()))
	ano1 = year(dateadd("d",-60,now()))
end if

if dia2 = empty or mes2=empty or ano2=empty then
	dia2 = day(now())
	mes2 = month(now())
	ano2 = year(now())
end if

data_inicio 	=	( dia1&"/"&mes1&"/"&ano1&" 00:00:00" )
data_fim		=	( dia2&"/"&mes2&"/"&ano2&" 23:59:59" )
cor				= 	"#FFFFFF"

'' Criando query SQL considerando pesquisa ''
sql	=	"select RECOMENDACOES.*, U1.nome_usuario as nome1, U2.nome_usuario as nome2, "&_
		"(select top 1 data_inscricao from CURSOS_INSCRICOES inner join CURSOS_INSCRICOES_USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_inscricao = CURSOS_INSCRICOES.ID_inscricao where CURSOS_INSCRICOES_USUARIOS.ID_usuario=RECOMENDACOES.ID_usuario2) as comprou "&_
		"from RECOMENDACOES inner join USUARIOS as U1 on RECOMENDACOES.ID_usuario1 = U1.ID_usuario inner join USUARIOS as U2 on RECOMENDACOES.ID_usuario2 = U2.ID_usuario where data_recomendacao>='"&DATA_BD(data_inicio)&"' and data_recomendacao<='"&DATA_BD(data_fim)&"' "
		' and data_inscricao>=RECOMENDACOES.data_recomendacao
if not palavra_chave=empty then
	sql=sql+" and (U1.nome_usuario like '%"&FORMATAR_PESQUISA(palavra_chave,true)&"%' or U2.nome_usuario like '%"&FORMATAR_PESQUISA(palavra_chave,true)&"%' or U1.email_usuario like '%"&FORMATAR_PESQUISA(palavra_chave,true)&"%' or U2.email_usuario like '%"&FORMATAR_PESQUISA(palavra_chave,true)&"%')"
end if

if not convertidos=empty then
	sql=sql+" and exists (select top 1 1 from CURSOS_INSCRICOES inner join CURSOS_INSCRICOES_USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_inscricao = CURSOS_INSCRICOES.ID_inscricao where CURSOS_INSCRICOES_USUARIOS.ID_usuario=RECOMENDACOES.ID_usuario2) "
	' and data_inscricao>=RECOMENDACOES.data_recomendacao
end if

if not livro=empty then
	sql=sql+" and (entregue_recomendacao is null or entregue_recomendacao="&falso&") "
end if

if not entregue=empty then
	sql=sql+" and (entregue2_recomendacao is null or entregue2_recomendacao="&falso&") "
end if

if not solicitou=empty then
	sql=sql+" and (livros_recomendacao is not null and livros_recomendacao<>'') "
end if

if lembrete=empty then
	sql=sql+" and (U1.email_usuario <> U2.email_usuario) "
end if

sql=sql+" order by nome1, data_recomendacao desc"
'''''''''''''''''''''''''''''''''''''''''''''
'response.Write(sql)

set rs = ABRIR_RS(sql)
%>


<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	
	<!-- IN�CIO DA PESQUISA -->
	<tr id="tr_pesquisa02">
		<TD bgcolor="#FFFFDD" align="center">
		
			
			<table border="0" cellpadding="3" cellspacing="0">
			<form name="pesquisa" method="get" action="default.asp">
				<TR>
					<TD align="right"><b>Nome / E-mail:</b> </TD>
					<TD><input type="text" name="palavra_chave" style="width:200px;" value="<%=palavra_chave%>" maxlength="200" /></TD>
					<TD><input type="checkbox" name="convertidos" value="1"<%if convertidos="1" then%> checked="checked"<%end if%> />Apenas recomenda��es convertidas em venda</TD>
					<TD rowspan="3"><input type="submit" value="Pesquisar" style="width:90px; height:70px;" /></TD>
				</TR>
				<TR>
					<TD colspan="3"><input type="checkbox" name="solicitou" value="1"<%if solicitou="1" then%> checked="checked"<%end if%> />Apenas recomenda��es que o livro j� foi solicitado</TD>
				</TR>
                <TR>
					<TD colspan="3"><input type="checkbox" name="livro" value="1"<%if livro="1" then%> checked="checked"<%end if%> />Apenas recomenda��es que o livro n�o foi enviado</TD>
				</TR>
                <TR>
					<TD colspan="3"><input type="checkbox" name="entregue" value="1"<%if entregue="1" then%> checked="checked"<%end if%> />Apenas recomenda��es que o livro n�o foi entregue</TD>
				</TR>
                <TR>
					<TD colspan="3"><input type="checkbox" name="lembrete" value="1"<%if lembrete="1" then%> checked="checked"<%end if%> />Incluir recomenda��es entre mesmo e-mail</TD>
				</TR>
				<TR>
					<TD align="right"><b>Data:</b> </TD>
					<TD>
						entre 
						<input type="text" name="dia1" style="width:25px;" value="<%=dia1%>" maxlength="2" /> / 
						<input type="text" name="mes1" style="width:25px;" value="<%=mes1%>" maxlength="2" /> / 
						<input type="text" name="ano1" style="width:45px;" value="<%=ano1%>" maxlength="4" />
					</TD>
					<TD>
						e 
						<input type="text" name="dia2" style="width:25px;" value="<%=dia2%>" maxlength="2" /> / 
						<input type="text" name="mes2" style="width:25px;" value="<%=mes2%>" maxlength="2" /> / 
						<input type="text" name="ano2" style="width:45px;" value="<%=ano2%>" maxlength="4" />
					</TD>
				</TR>
			</form>
			</table>
		
		</TD>
	</tr>
	<!-- FIM DA PESQUISA -->
	
	<tr bgcolor="#CCCCCC">
		<td align="center" class="titulo_02"><b>Recomenda��es</b></td>
	
	<%
	ID_temp = 0
	total_indicacoes = 0
	do while not rs.EOF
	
		if ID_temp <> rs("ID_usuario1") then
			ID_temp = rs("ID_usuario1")
			conta_indicados = 1
			response.Write("</TD></TR><TR><TD bgcolor='"&cor&"'><font size='3' color='#FF3300'><b>"&rs("nome1")&"</b></font> <a href=""javascript:"" onClick=""exibir_acao( '../usuarios/editar.asp?ID="&rs("ID_usuario1")&"' );"">Editar</a>")
			if cor="#FFFFFF" then
				cor= "#EEEEEE"
			else
				cor= "#FFFFFF"
			end if
		end if
		
		'' email de cobran�a de livro enviado
		if isnull(rs("email_recomendacao"))=false and (isnull(rs("livros_recomendacao")) or rs("livros_recomendacao")=empty) and (isnull(rs("entregue_recomendacao")) or rs("entregue_recomendacao")=false ) then
			rec_cor = "#CCCC00"
			
		'' livros solicitados
		elseif (isnull(rs("livros_recomendacao"))=false and not rs("livros_recomendacao")=empty) and (isnull(rs("entregue_recomendacao")) or rs("entregue_recomendacao")=false ) then
			rec_cor = "#FF6666"
		
		'' livros enviados mas n�o entregues
		elseif (isnull(rs("entregue_recomendacao"))=false and rs("entregue_recomendacao")=true ) and (isnull(rs("entregue2_recomendacao")) or rs("entregue2_recomendacao")=false) then
			rec_cor = "#33CCFF"
		
		'' livros enviados
		elseif (isnull(rs("entregue_recomendacao"))=false and rs("entregue_recomendacao")=true ) then
			rec_cor = "#00CC33"	
		
		else
			rec_cor = ""	
		end if
		%>
	
		<div style="padding:3px; position:relative; clear:both; margin:auto; width:740px; border-top:#CCC solid 1px;<%if not rec_cor = empty then%> background:<%=rec_cor%>;<%end if%>">
			<div style="display:inline-block; width:350px; float:left;">
				<b>indicado <%=conta_indicados%>:</b> <%=rs("nome2")%> <a href="javascript:" onClick="exibir_acao( '../usuarios/editar.asp?ID=<%=rs("ID_usuario2")%>' );">Editar</a>
			</div>
			<div style="display:inline-block; width:320px; float:left;">
				<b>Livros:</b> <%=rs("livros_recomendacao")%> 
                <a href="javascript:" onClick="exibir_acao( 'editar.asp?ID=<%=rs("ID_recomendacao")%>' );">Editar</a> | 
                <a href="javascript:" onClick="return confirmar('acoes.asp?ID=<%=rs("ID_recomendacao")%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>
                
                <%if isnull(rs("email_recomendacao")) and isnull(rs("comprou"))=false then%>
                 | <a href="acoes.asp?acao=enviar&ID=<%=rs("ID_recomendacao")%>"><img src="../_img/icone_email.gif" width="16" height="10" border="0" title="Enviar e-mail" align="absmiddle" /></a>
                <%end if%>
			</div>
		</div>
		<%
		total_indicacoes =total_indicacoes + 1
		conta_indicados = conta_indicados + 1
	rs.movenext:loop
	%>
	
	</td></tr>
    
    <TR>
    	<TD bgcolor="#FFFFCC" align="center">
        	<b>Total de indica��es: <%=total_indicacoes%></b>
        </TD>
    </TR>
	
</table>

<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_principal_02.asp" -->