<!-- #include file="../_base/config.asp" -->
<%
call checalogado()

acao 			= 	formatar("acao",1,2)

endereco 		=	formatar("endereco",1,1)
bairro 			=	formatar("bairro",1,1)
cidade 			=	formatar("cidade",1,1)
cep1 			=	formatar("cep1",1,1)
cep2 			=	formatar("cep2",1,1)
estado 			=	formatar("estado",1,1)
livros			=	formatar("livros",2,1)
observacoes		=	formatar("observacoes",2,1)
entregue		=	formatar("entregue",1,1)
entregue2		=	formatar("entregue2",1,1)

ID 				= 	formatar("ID",1,3)

cep				=	cep1&cep2
''''''''''''''''''''''''''''''''''''''''''''

'EDITAR
if acao="editar" then

	if ID=empty or entregue=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql="update RECOMENDACOES set livros_recomendacao='"&livros&"',endereco_recomendacao='"&endereco&"',bairro_recomendacao='"&bairro&"',cep_recomendacao='"&cep&"',cidade_recomendacao='"&cidade&"',estado_recomendacao='"&estado&"',entregue_recomendacao="&entregue&",entregue2_recomendacao="&entregue2&",observacoes_recomendacao='"&observacoes&"' where ID_recomendacao="&ID
	conexao.execute(sql)

	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")


'ENVIAR EMAIL 
elseif acao="enviar" then
	
	if ID=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql	=	"select RECOMENDACOES.*, U1.nome_usuario as nome1, U1.email_usuario as email1, U2.nome_usuario as nome2, "&_
			"(select top 1 titulo_curso from CURSOS_INSCRICOES inner join CURSOS_INSCRICOES_USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_inscricao = CURSOS_INSCRICOES.ID_inscricao inner join CURSOS on CURSOS_INSCRICOES.ID_curso=CURSOS.ID_curso where CURSOS_INSCRICOES_USUARIOS.ID_usuario=RECOMENDACOES.ID_usuario2 and data_inscricao>=RECOMENDACOES.data_recomendacao) as titulo_curso "&_
			"from RECOMENDACOES inner join USUARIOS as U1 on RECOMENDACOES.ID_usuario1 = U1.ID_usuario inner join USUARIOS as U2 on RECOMENDACOES.ID_usuario2 = U2.ID_usuario where ID_recomendacao="&ID
	set rs = ABRIR_RS(sql)
	
	if not rs.EOF then
	
		mensagem = 		"<BR /><b>[NOME_ALUNO]</b>,<BR /><BR />"&_
						"Voc� indicou o Curso de [CURSO] e [NOME_INDICADO] est� fazendo o treinamento.<BR /><BR />"&_
						"Voc� tem direito a uma cortesia em livros at� o valor de R$ 50,00. <BR /><BR />"&_
						"Envie-nos o t�tulo, autor e editora, bem como seu endere�o para que possamos encaminh�-lo a voc�.<BR /><BR />"&_
						"Grato por sua indica��o!<BR /><BR />"&_
						"Prof. Alcides Schotten<BR />"&_
						"Diretor Executivo<BR />"&_
						"www.methodus.com.br<BR />"&_
						"(11) 3288 2777 / 3285 1052<BR />"&_
						"Av. Paulista, 2202 � cj 134<BR />"&_
						"Metr� Consola��o"
						
		email_aluno = rs("email1")
		mensagem = replace( mensagem, "[NOME_ALUNO]", rs("nome1")&"" )
		mensagem = replace( mensagem, "[CURSO]", rs("titulo_curso")&"" )
		mensagem = replace( mensagem, "[NOME_INDICADO]", rs("nome2")&"" )
					
		
		call ENVIAR_EMAIL_DEDICADO( email_aluno, "Voc� tem direito a uma cortesia por sua indica��o - Methodus", mensagem )
		
		call ENVIAR_EMAIL_DEDICADO( "info@methodus.com.br", "Voc� ganhou uma cortesia pela sua indica��o", mensagem )
		
		sql="update RECOMENDACOES set email_recomendacao='"&DATA_BD(now())&"' where ID_recomendacao="&ID
		conexao.execute(sql)
	
	end if

	session("sucesso") = "0002"
	response.Redirect(session("voltar"))
	

'EXCLUIR
elseif acao="excluir" then
	
	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if
	
	sql="DELETE FROM RECOMENDACOES where ID_recomendacao="&ID
	conexao.execute(sql)
	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))


'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>