<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar Recomenda��o@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

///////////////////////////
function checa(formulario)
{

	if (checa_caracter(formulario.cep1,"CEP","1234567890")==false)
		return(false);
	if (checa_caracter(formulario.cep2,"CEP","1234567890")==false)
		return(false);


	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID		=	formatar("ID",1,2)
campo	=	formatar("campo",1,2)

sql	=	"select RECOMENDACOES.*, U1.nome_usuario as nome1, U1.email_usuario as email1, U1.endereco_usuario, U1.bairro_usuario, U1.cep_usuario, U1.cidade_usuario, U1.estado_usuario, U1.ddd_tel_usuario as ddd_tel1, U1.tel_usuario as tel1, U1.ddd_cel_usuario as ddd_cel1, U1.cel_usuario as cel1, U2.nome_usuario as nome2, U2.email_usuario as email2, U2.ddd_tel_usuario as ddd_tel2, U2.tel_usuario as tel2, U2.ddd_cel_usuario as ddd_cel2, U2.cel_usuario as cel2, "&_
		"(select top 1 data_inscricao from CURSOS_INSCRICOES inner join CURSOS_INSCRICOES_USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_inscricao = CURSOS_INSCRICOES.ID_inscricao where CURSOS_INSCRICOES_USUARIOS.ID_usuario=RECOMENDACOES.ID_usuario2 and data_inscricao>=RECOMENDACOES.data_recomendacao) as comprou "&_
		"from RECOMENDACOES inner join USUARIOS as U1 on RECOMENDACOES.ID_usuario1 = U1.ID_usuario inner join USUARIOS as U2 on RECOMENDACOES.ID_usuario2 = U2.ID_usuario where ID_recomendacao="&ID

set rs = conexao.execute(sql)

if isnull(rs("endereco_recomendacao")) or rs("endereco_recomendacao")=empty then
	endereco = rs("endereco_usuario")
else
	endereco = rs("endereco_recomendacao")
end if

if isnull(rs("bairro_recomendacao")) or rs("bairro_recomendacao")=empty then
	bairro = rs("bairro_usuario")
else
	bairro = rs("bairro_recomendacao")
end if

if isnull(rs("cep_recomendacao")) or rs("cep_recomendacao")=empty then
	cep = rs("cep_usuario")&""
else
	cep = rs("cep_recomendacao")&""
end if

if isnull(rs("cidade_recomendacao")) or rs("cidade_recomendacao")=empty then
	cidade = rs("cidade_usuario")
else
	cidade = rs("cidade_recomendacao")
end if

if isnull(rs("estado_recomendacao")) or rs("estado_recomendacao")=empty then
	estado = rs("estado_usuario")
else
	estado = rs("estado_recomendacao")
end if
%>


<table border="0" cellpadding="1" cellspacing="0" width="100%">
<form name="a_editar" method="post" action="acoes.asp?acao=editar" onSubmit="return checa(this);">
<input type="hidden" name="ID" value="<%=ID%>" />
	
	<TR>
		<TD width="50%"><b>Quem indicou:</b></TD>
		<TD width="50%"><b>Indicado:</b></TD>
	</TR>
	<TR>
		<TD>
			<%=rs("nome1")%><BR />
			<a href="maitlto:<%=rs("email1")%>"><%=rs("email1")%></a><BR />
			<%if isnull(rs("tel1")) = false and not rs("tel1")=empty then%>(<%=rs("ddd_tel1")%>) <%=rs("tel1")%><BR /><%end if%>
			<%if isnull(rs("cel1")) = false and not rs("cel1")=empty then%>(<%=rs("ddd_cel1")%>) <%=rs("cel1")%><BR /><%end if%>
		</TD>
		<TD>
			<%=rs("nome2")%><BR />
			<a href="maitlto:<%=rs("email2")%>"><%=rs("email2")%></a><BR />
			<%if isnull(rs("tel2")) = false and not rs("tel2")=empty then%>(<%=rs("ddd_tel2")%>) <%=rs("tel2")%><BR /><%end if%>
			<%if isnull(rs("cel2")) = false and not rs("cel2")=empty then%>(<%=rs("ddd_cel2")%>) <%=rs("cel2")%><BR /><%end if%>
		</TD>
	</TR>
	
	<TR>
		<TD colspan="2">
			<b>Livros:</b><BR>
			<textarea name="livros" style="width:100%; height:100px;"><%=formatar(rs("livros_recomendacao")&"",4,4)%></textarea>
		</TD>
	</TR>
	<TR>
		<TD width="50%"><b>Endere�o:</b></TD>
		<TD width="50%"><b>Bairro:</b></TD>
	</TR>
	<TR>
		<TD>
			<input type="text" name="endereco" style="width:100%" maxlength="200" value="<%=endereco%>" />
		</TD>
		<TD>
			<input type="text" name="bairro" style="width:100%" maxlength="200" value="<%=bairro%>" />
		</TD>
	</TR>
	<TR>
		<TD width="50%"><b>Cidade:</b></TD>
		<TD width="50%"><b>CEP:</b></TD>
	</TR>
	<TR>
		<TD><input type="text" name="cidade" style="width:100%" maxlength="90" value="<%=cidade%>" /></TD>
		<TD><input type="text" name="cep1" style="width:50px" maxlength="5" onKeyUp="return autoTab(this, 5, event);" value="<%=left(cep,5)%>" /> - <input type="text" name="cep2" style="width:30px" maxlength="3" value="<%=right(cep,3)%>" /></TD>
	</TR>
	<TR>
		<TD width="50%"><b>Estado:</b></TD>
		<TD width="50%"><b>Livros foram enviados?</b></TD>
	</TR>
	<TR>
		<TD>
			<select name="estado">
				<option value="">--</option>
				<%
				call CONEXAO_XML("../_xml/estados.xml")
				set estados = xmlDoc.documentElement
				
				for each registro in estados.childNodes
				%>
					<option value="<%=registro.text%>"<%if registro.text=estado then%> selected<%end if%>><%=registro.text%></option>
				<%next%>
			</select>
		</TD>
		<TD><input type="radio" name="entregue" value="<%=verdade%>"<%if rs("entregue_recomendacao")=true then%> checked<%end if%> /> Sim <input type="radio" name="entregue" value="<%=falso%>"<%if rs("entregue_recomendacao")=false or isnull(rs("entregue_recomendacao")) then%> checked<%end if%> /> N�o</TD>
	</TR>
    <TR>
		<TD width="50%">&nbsp;</TD>
		<TD width="50%"><b>Livros foram entregues?</b></TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
		<TD><input type="radio" name="entregue2" value="<%=verdade%>"<%if rs("entregue2_recomendacao")=true then%> checked<%end if%> /> Sim <input type="radio" name="entregue2" value="<%=falso%>"<%if rs("entregue2_recomendacao")=false or isnull(rs("entregue2_recomendacao")) then%> checked<%end if%> /> N�o</TD>
	</TR>
	<TR>
		<TD colspan="2">
			<b>Observa��es:</b><BR>
			<textarea name="observacoes" style="width:100%; height:100px;"><%=formatar(rs("observacoes_recomendacao")&"",4,4)%></textarea>
		</TD>
	</TR>

	<tr>
		<td align="center" colspan="2">
			<BR />
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Editar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->