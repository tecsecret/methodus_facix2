<%@ codepage="1252" language="VBScript" %>
<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar Conteudo@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>
	<script src="../_ckeditor/ckeditor.js"></script>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE M�DULOS ADMINISTRATIVOS
function checa(formulario)
{
	campo=[
			formulario.titulo
		  ];
	n_campo=[
			"T�tulo"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
		
	formulario.botao_enviar.disabled = true;
	formulario.botao_enviar.value = "Processando";
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID 	= 	formatar("ID",1,2)
sql="select * from CONTEUDOS where ID_conteudo="&ID
set rs= conexao.execute(sql)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form ENCTYPE="multipart/form-data" name="a_editar_conteudo" method="post" action="acoes.asp?acao=editar" onSubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
	<tr>
		<td class="titulo_02" width="50%"><b>T�tulo</b></td>
		<td class="titulo_02" width="50%"><b>Sub-Categoria</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="titulo" style="width:100%" value="<%=Server.HTMLEncode(rs("titulo_conteudo"))%>" />
		</td>
		<td>
			<%
			ID 	= 	formatar("ID",1,2)
			
			sql="select titulo_categoria, ID_categoria from CATEGORIAS where SUB_categoria=0"
			set rs_cat= conexao.execute(sql)
			%>
			<select name="categoria" style="width:100%">
				<%do while not rs_cat.EOF%>
					<option value="<%=rs_cat("ID_categoria")%>"<%if rs_cat("ID_categoria")=rs("ID_categoria") then%> selected<%end if%>><%=Server.HTMLEncode(rs_cat("titulo_categoria"))%></option>
				<%rs_cat.movenext:loop%>
			</select>
		</td>
	</tr>
    <tr>
		<td class="titulo_02" colspan="2"><b>Texto do Banner</b></td>
	</tr>
	<tr>
		<td colspan="2">	
            <textarea id="editor1" name="texto_banner"><%=rs("texto_banner_conteudo")%></textarea>
			<script>
                CKEDITOR.replace( 'editor1', {
                    height: '140px',
                    toolbar: 'Basico'			
                });
            </script>
		</td>
	</tr>
    <tr>
		<td class="titulo_02" colspan="2"><b>Imagem do Banner</b></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="file" name="arquivo1" style="width:100%" />
		</td>
	</tr>
    <%if not rs("banner_conteudo")=empty then%>
    <TR>
		<td colspan="2">
			Arquivo atual: <a href="../../methodus/_arquivos_banner/<%=rs("banner_conteudo")%>" target="_blank"><%=rs("banner_conteudo")%></a>
		</td>
    </TR>
	<%end if%>
	<tr>
		<td class="titulo_02" colspan="2"><b>Conte�do</b></td>
	</tr>
	<tr>
		<td colspan="2">	
            <textarea id="editor2" name="conteudo"><%=rs("corpo_conteudo")%></textarea>
			<script>
                CKEDITOR.replace( 'editor2', {
                    height: '350px',
                    toolbar: 'Padrao'			
                });
            </script>
		</td>
	</tr>
	<tr>
		<td><b>Status</b></td>
        <td class="titulo_02" width="50%"><b>Tema</b></td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="ativo" value="1"<%if rs("status_conteudo")=true then%> checked<%end if%> /> Ativo 
			<input type="radio" name="ativo" value="0"<%if rs("status_conteudo")=false then%> checked<%end if%> /> Inativo
		</td>
        <td>
			<%
			sql="select titulo_tema, ID_tema from TEMAS where status_tema="&verdade&""
			set rs_tema= conexao.execute(sql)
			%>
			<select name="tema" style="width:100%">
				<option value="null">Sem Categoria</option>
				<%do while not rs_tema.EOF%>
					<option value="<%=rs_tema("ID_tema")%>"<%if rs_tema("ID_tema")=rs("ID_tema") then%> selected="selected"<%end if%>><%=rs_tema("titulo_tema")%></option>
				<%rs_tema.movenext:loop%>
			</select>
		</td>
	</tr>
    <tr>
		<td class="titulo_02" colspan="2"><b>Meta-Tags</b> (apenas para administra��o)</td>
	</tr>
	<tr>
		<td colspan="2">
			<%
            metatags = ""
			if isnull(rs("metatags"))=false then
				metatags = Server.HTMLEncode(rs("metatags"))
			end if
            %>
			<textarea name="metatags" style="width:100%; height:60px;"><%=metatags%></textarea>	
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" name="botao_enviar" value="Editar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->