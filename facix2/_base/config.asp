<%
''''''''''''''''''''''''''''''' CONFIGURA��ES '''''''''''''''''''''''''''''''''
response.Buffer 	= true
%>
<!-- #include file="../_base/conexao.asp" -->
<%
'' PREVEN��O DE INJE��O DE SQL ''
FUNCTION sqlInjection()

	'Palavras que ser�o barradas caso encontradas nos request
	palavrasDoMal = array("insert", "drop ", "update", "cast(")
	erro_injecao = ""
	
	' Verificando o que � passado pelo request.queryString
	' for each item in request.QueryString
	' 	for j = lbound(palavrasDoMal) to ubound(palavrasDoMal)
	' 		if instr(lcase(Request.QueryString(item)), lcase(palavrasDoMal(j))) > 0 then
	' 			erro_injecao = erro_injecao & " | querystring: "&Request.QueryString(item)&""
	' 		end if
	' 	next
	' next

	'Verificando o que � enviado por request.form
	' for each item in request.form
	' 	for j = lbound(palavrasDoMal) to ubound(palavrasDoMal)
	' 		if instr(lcase(Request.form(item)), lcase(palavrasDoMal(j))) > 0 then
	' 			erro_injecao = erro_injecao & " | form: "&Request.form(item)&""
	' 		end if
	' 	next
	' next

	' Verifica o que est� sendo passado via cookies
	' for each item in request.Cookies
	' 	for j = lbound(palavrasDoMal) to ubound(palavrasDoMal)
	' 		if instr(lcase(Request.Cookies(item)), lcase(palavrasDoMal(j))) > 0 then
	' 			erro_injecao = erro_injecao & " | cookie: "&Request.Cookies(item)&""
	' 		end if
	' 	next
	' next
	
	if not erro_injecao = "" then
		'call ENVIAR_EMAIL( "renan@pensadigital.com.br", "renan@pensadigital.com.br", website_01&" - Inje��o de c�digo", website_01&"<BR />"&erro_injecao )
		response.Clear()
		response.Redirect("../../")
	end if

END FUNCTION

sqlInjection()


FUNCTION LimpaLixo( input )

    dim lixo
    dim textoOK

    lixo = array ( "select" , "drop " ,  "update" , "--" , "insert" , "delete" ,  "xp_" ) '"<script"

    textoOK = input

     for j = 0 to uBound(lixo)
          textoOK = replace( textoOK ,  lixo(j) , "")
     next

     LimpaLixo = textoOK

END FUNCTION
'''''''''''''''''''''''''''''''''

'' Formata��o de INPUTS ''
FUNCTION FORMATAR( var, tipo, metodo )

	if metodo= 1 then ' Post
		formatar= request.form(var)
	elseif metodo= 2 then ' Querystring
		formatar= request.querystring(var)
	elseif metodo= 4 then ' componente de upload
		formatar= var
	else ' N�o especificado
		formatar= request(var)
	end if
	
	if tipo= 2 then ' Textarea
	
		formatar= replace(formatar,"'","''")
		formatar= replace(formatar,chr(34),"""")
		formatar= replace(formatar,"<","&lt;")
		formatar= replace(formatar,">","&gt;")
		formatar= replace(formatar,Chr(10),"<br />")
		formatar= replace(formatar,Chr(32) & Chr(32)," &nbsp;")
		
	elseif tipo= 3 then ' Textarea em HTML
		
		formatar= replace(formatar,"'","''")
		
	elseif tipo= 4 then ' Do banco para o textarea
		
		formatar= replace(formatar,"<br />",Chr(10))
		formatar= replace(formatar,"<BR />",Chr(10))
		formatar= replace(formatar,"<BR>",Chr(10))
		
	else ' Campo de texto normal ou variavel de entrada qualquer
		
		formatar= replace(formatar,"'","''")
		formatar= replace(formatar,chr(34),"""")
		formatar= replace(formatar,"<","&lt;")
		formatar= replace(formatar,">","&gt;")
		
	end if
	
	formatar= trim(formatar)
	
	formatar= LimpaLixo( formatar )
	
END FUNCTION
''''''''''''''''''''''''''

'' Formata datas ''
FUNCTION FORMATA_DATA( data, tipo )
	
	'' Tipos de datas
	'' 1- Brasileiro (21/12/2005)
	'' 2- Americano (12/21/2005)
	'' 3- Universal para BD (2005-12-21)
	
	if isdate(data)=false then
		Response.Clear()
		session("erro")="0006"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
		'response.redirect("../_erro/default.asp?erro=0006")
	end if
	
	if year(data)<1900 then
		Response.Clear()
		session("erro")="0006"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	'' temp: corre��o de fuso-hor�rio ''
'	if datediff("n",data,now()) > 2 or datediff("n",data,now()) < -2 then
'		'' n�o fa�a nada, pois a data n�o � a de agora
'	else
'		data = dateadd( "h", 3, cdate(data) )
'	end if
	''''''''''''''''''''''''''''''''''''
	
	select case tipo
		case 1
			FORMATA_DATA = FormatoData(data,"dd/mm/aaaa")
		case 2
			FORMATA_DATA = FormatoData(data,"mm/dd/aaaa")
		case else
			FORMATA_DATA = FormatoData(data,"aaaa-mm-dd")
	end select
	
END FUNCTION
'''''''''''''''''''

'' Formata hora ''
FUNCTION FORMATA_HORA( hora, tipo )
	
	'' Tipos de horas
	'' 1- 9h15m
	'' 2- 9:15h
	'' 3- 9:15
	'' 4- 9:15:05
	
	'' temp: corre��o de fuso-hor�rio ''
'	if isdate(hora) then
'		if datediff("n",hora,now()) > 2 or datediff("n",hora,now()) < -2 then
'			'' n�o fa�a nada, pois a data n�o � a de agora
'		else
'			hora = dateadd( "h", 3, cdate(hora) )
'		end if
'	end if
	''''''''''''''''''''''''''''''''''''

	hora= timevalue(hora)
	
	select case tipo
		case 1
			FORMATA_HORA = FormatoHora(hora,"hhhmmm")
		case 2
			FORMATA_HORA = FormatoHora(hora,"hh:mmh")
		case 3
			FORMATA_HORA = FormatoHora(hora,"hh:mm")
		case else
			FORMATA_HORA = FormatoHora(hora,"hh:mm:ss")
	end select
	
END FUNCTION
'''''''''''''''''''

'' Fun��es para formata��o de data e hora ''
Function FormatoData(data,formato)

	if isdate(data)=false then
		Response.Clear()
		session("erro")="0006"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
		'response.redirect("../_erro/default.asp?erro=0006")
	end if

    Dia = Right("0" & Day(data), 2)
    Mes = Right("0" & Month(data), 2)    
    Ano = Year(data)

    DataFinal = Replace(formato, "NMdd", WeekDayName(WeekDay(Date)))
    DataFinal = Replace(DataFinal, "NMmm", MonthName(Month(Date)))
    DataFinal = Replace(DataFinal, "ABmm", MonthName(Month(Date), True))
    DataFinal = Replace(DataFinal, "dd", Dia)
    DataFinal = Replace(DataFinal, "mm", Mes)
    DataFinal = Replace(DataFinal, "aaaa", Ano)
    DataFinal = Replace(DataFinal, "aa", Mid(Ano, 3, 2))

    FormatoData = DataFinal

End Function

Function FormatoHora(hora_total,formato)
	
    Hora 		= Right("0" & Hour(hora_total), 2)
    Minuto 		= Right("0" & Minute(hora_total), 2)
    Segundo 	= Right("0" & Second(hora_total), 2)

    HoraFinal 	= Replace(formato, "hh", Hora)
    HoraFinal 	= Replace(HoraFinal, "mm", Minuto)
    HoraFinal 	= Replace(HoraFinal, "ss", Segundo)

    FormatoHora = HoraFinal

End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Formata��o para pesquisas ''
FUNCTION FORMATAR_PESQUISA( var, condicao )
	pesquisa = REPLACE( var, "*", "%" )
	pesquisa = REPLACE( pesquisa,"a","[a,�,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"e","[e,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"i","[i,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"o","[o,�,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"u","[u,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"c","[c,�]")
	pesquisa = REPLACE( pesquisa,"A","[A,�,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"E","[E,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"I","[I,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"O","[O,�,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"U","[U,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"C","[C,�]")
	
	if condicao then
		pesquisa = REPLACE( pesquisa, " ", "%" )
	end if
	
	FORMATAR_PESQUISA = pesquisa
END FUNCTION
''''''''''''''''''''''''''''''''

'' Formatar Moeda para banco de dados ''
FUNCTION FORMATA_MOEDA( valor, parametro )

	if parametro = true then
	
		valor = replace( valor, ".", "")
		valor = replace( valor, ",", ".")
		
	else
	
		valor = formatnumber( valor, 2 )
		
	end if
	
	FORMATA_MOEDA = valor

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''

'' Formata��o para dinheiro ''
FUNCTION FORMATAR_DINHEIRO( var )
	FORMATAR_DINHEIRO = REPLACE( var, ".", "" )
	FORMATAR_DINHEIRO = REPLACE( FORMATAR_DINHEIRO, ",", "." )
END FUNCTION
''''''''''''''''''''''''''''''''

'' Testa se um componente existe instalado no servidor ''
Function TestObject( comIdentity )
	On Error Resume Next
	TestObject = False
	Err.Clear
	Set xTestObj = Server.CreateObject( comIdentity )
	If Err = 0 Then TestObject = True
	Set xTestObj = Nothing
	Err.clear
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Tira c�digo HTML de qualquer string ''
FUNCTION tira_codigo( var,quantidade )

      Do While True
            ini = InStr(1,var,"<")

            If ini = 0 Then
                  Exit Do
            end if

            fim = InStr(ini,var,">")

            If fim = 0 Then
                  Exit Do
            end if

            parcial = Mid(var,ini,(fim-ini)+1)
            var = Replace(var,parcial,"")
      Loop
	  
      tira_codigo = left(var,quantidade) 
	  
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''

'' CRIPTOGRAFIA SEGURA (DESTRUTIVA) ''
FUNCTION criptografar(senha)

	var = senha
	
	var= lcase(var)
	i = 1
	total= 0
	do while i<= len(var)
		numero= asc(mid(var,i,1))
		total= ((total+numero)*11.7)
		i= i+1
	loop
	var= total
	var= (var/71)
	var= (var*var)
	var= (var*1529439)
	var= fix(var)
	var= left(var,16)
	var= cstr(var)
	var= replace(var,",","")
	
	criptografar= var
	
END FUNCTION
''''''''''''''''''''''''''''''''''''''

'' SETA PASTAS PERMITIDAS PARA O USU�RIO ''
FUNCTION permissoes_usuario()

	dim sql
	
	usuario_departamentos = ""
	sql="select * from PRESTADOR_DEPARTAMENTOS where ID_prestador="&session("admin")
	set rs_permissao= conexao.execute(sql)
	do while not rs_permissao.EOF
		usuario_departamentos = usuario_departamentos & rs_permissao("ID_departamento")

		rs_permissao.movenext
		if not rs_permissao.EOF then
			usuario_departamentos = usuario_departamentos & ","
		end if
	loop
	
	permissoes_usuario = usuario_departamentos

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''

'' CHECA SE EST� LOGADO E QUAL � A PERMISS�O ''
SUB checalogado()
	
	ID_admin= session("admin")
	
	if ID_admin=empty then
		checa_logado = false
	else

		areas= Request.ServerVariables("PATH_INFO")
		area_a= split(areas,"/")
		area= area_a(ubound(area_a)-1)
		
		sql="select ID_pasta from PASTAS where arquivo_pasta='"&area&"'"
		set rs_permissao= conexao.execute(sql)
		
		if rs_permissao.EOF then
			checa_logado = true
		else
		
			ID_pasta= rs_permissao("ID_pasta")
			
			parte = Split(permissoes_usuario(), ",", -1, 1)
			qtd = UBound (parte)
			i=0
			sql= "select * from PERMISSOES where (ID_departamento=0"
			do while i <= qtd
				sql= sql+" or ID_departamento="&trim(parte(i))
				i= i+1
			loop
			sql= sql+") and ID_pasta="&ID_pasta
			set rs_permissao= conexao.execute(sql)
			
			
			if not rs_permissao.EOF then
				checa_logado = true
			else
				checa_logado = false
			end if
		
		end if
		
	end if
	
	set rs_permissao= nothing
	
	if checa_logado = false then
		Response.Clear()
		session("erro") = "0003"
		if session("voltar")=empty then
			response.redirect("https://api.methodus.com.br/facix2/_login/default.asp")
		else
			response.redirect(session("voltar"))
		end if
		response.End()
	end if 
	'response.Write("<BR /><BR />"&area&"<BR />"&usuario_departamentos&"<BR />"&ID_pasta&"<BR />"&checa_logado)
	
END SUB
''''''''''''''''''''''''''''''''''''''''''''''

'' CRIA��O DO MENU MIGALGA DE P�O ''
FUNCTION cria_titulo(var)
	
	cria_titulo= "<table border=""0"" cellpadding=""5"" cellspacing=""0"" width=""100%""><TR><TD bgcolor=""#EEEEEE"">"
	
	parte = Split(var, ";", -1, 1)
	qtd = UBound (parte)
	i=0
	do while i <= qtd
	
		parte2 = Split(parte(i), "@", -1, 1)
		if parte2(1)="" then
			cria_titulo= cria_titulo&" <b>"&parte2(0)&"</b>"
		else
			cria_titulo= cria_titulo&" <a href="""&parte2(1)&""" class=""link_01"">"&parte2(0)&"</a>"
		end if
		
		i= i+1
		
		if i <= qtd then
			cria_titulo= cria_titulo&" &gt;&gt; "
		end if
	loop
	
	cria_titulo= cria_titulo&"</TD></TR></table><BR />"
	
END FUNCTION
''''''''''''''''''''''''''''''''''''''

'' fun��es para montar a session com url completa ''
FUNCTION SESSION_VOLTAR()

		SESSION_VOLTAR = "https://"&Request.ServerVariables("SERVER_NAME")&Request.ServerVariables("URL")&"?"&Request.ServerVariables("QUERY_STRING")

END FUNCTION
''''''''''''''''''''''''''''''''''''''''''''''''''''

'' fun��o para determinar qual ser� a vari�vel do link voltar ''
FUNCTION PAGINA_VOLTAR( tipo )
	
	select case tipo
	case "0"
		PAGINA_VOLTAR = ""
	case "1"
		PAGINA_VOLTAR = "javascript: history.back(1);"
	case "2"
		PAGINA_VOLTAR = session("voltar")
	case else
		PAGINA_VOLTAR = tipo
	end select
	
END FUNCTION
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para formatar uma quantidade total de segundos em HH:MM:SS ''
FUNCTION CALCULA_TEMPO(total)
					
	hora 			= fix(total/3600)
	minuto 			= fix((total - (hora*3600))/60)
	segundo 		= total - ((hora*3600)+(minuto*60))
						
	CALCULA_TEMPO 	= hora&":"&right("0"&minuto,2)&":"&right("0"&segundo,2)
						
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para formatar uma hora em HH:MM:SS em segundos''
FUNCTION CALCULA_SEGUNDOS(tempo)

	parte 	= Split(tempo, ":", -1, 1)
					
	hora 	= parte(0)*3600
	minuto 	= parte(1)*60
	segundo = parte(2)
						
	CALCULA_SEGUNDOS = hora + minuto + segundo
						
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para abrir conex�o com um XML ''
set xmlDoc=server.createObject("microsoft.xmldom")
SUB CONEXAO_XML( caminho )

	xmlDoc.async = false
	caminho = Server.MapPath(caminho)
	xmlDoc.load(caminho)

END SUB
''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para montar e-mail ''
FUNCTION MONTA_EMAIL(conteudo)	
	
	'strCabecalho 	= "<html><head><title>Methodus</title><style type=""text/css"">body,td,th {font-family: Arial, Helvetica, sans-serif;font-size: 11px;color: #000000;}body {background-color: #96c1c8;background-image: url(http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_fundo.jpg);background-repeat: repeat-x;margin-left: 10px;margin-top: 10px;margin-right: 10px;margin-bottom: 10px;}.menu_util{font-size:8px;color:#666666;text-decoration:none;}.menu_util:hover{text-decoration:underline;}</style></head><body><div style=""width:100%; position:relative; cursor:default;"" align=""center""><div style=""width:600px; position:relative; text-align:left;""><table border=""0"" width=""600"" cellpadding=""0"" cellspacing=""0"" align=""center""><TR><TD width=""18""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_borda_01.gif"" width=""18"" height=""108"" /></TD><TD width=""564"" bgcolor=""#FFFFFF""><div style=""position:relative; left:0px; background:url(http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_topo.gif); width:513px; height:108px;""><BR /><BR /><table border=""0"" width=""100%"" cellpadding=""0"" cellspacing=""0""><TR><TD align=""center""><a href=""http://www.methodus.com.br""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_logo.gif"" width=""222"" height=""40"" border=""0"" /></a><BR /><font color=""#BBBBBB"" style=""font-size:12px;"">Aprimorando Talentos</font></TD><TD><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/bullet_01.gif"" width=""35"" height=""63"" border=""0"" align=""left"" hspace=""10"" /></TD><TD><div><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/b.gif"" width=""1"" height=""5"" /></div><font style=""color:#0a477e""><b style=""font-size:16px"">11 3288.2777</b><BR />Av. Paulista 2202  Cj 134<BR />S�o Paulo, SP<BR />Metr� Consola��o<BR /></font></TD><TD align=""right""><a href=""http://www.methodus.com.br"" class=""menu_util""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/menu_util_home.gif"" width=""11"" height=""11"" border=""0"" align=""absmiddle"" /> &nbsp; WEB SITE</a> </TD></TR></table></div></div>            	</TD><TD width=""18""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_borda_02.gif"" width=""18"" height=""108"" /></TD></TR></table></div><div style=""width:600px; position:relative; text-align:left; background-color:#FFFFFF;""><table border=""0"" width=""569"" cellpadding=""0"" cellspacing=""0"" align=""center""><TR><TD valign=""top"">"
	
	'strRodape 		= "</TD></TR></table></div><div style=""width:600px; position:relative; text-align:left; background-color:#FFFFFF; height:30px;""><div><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/b.gif"" width=""1"" height=""5"" /></div><table border=""0"" width=""95%"" cellpadding=""3"" cellspacing=""5"" align=""center""><TR><TD align=""center"" bgcolor=""#336699""><font style=""color:#FFFFFF; font-size:9px;"">Copyright 2008 | Todos os direitos reservados Methodus.</font></TD></TR></table></div><div style=""width:600px; position:relative; text-align:left;""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_rodape_borda.gif"" width=""600"" height=""17"" /></div></div></body></html>"
	
	strCabecalho 	= "<html><head><title>Methodus</title><style type=""text/css"">td, div, body { font-family:Arial, Helvetica, sans-serif; font-size:12px; }</style></head><body bgcolor=""#96c1c8""><div style=""background-color:#FFFFFF; width:600px; position:relative; text-align:left;"" align=""center""><div style=""position:relative; height:60px;""><div style=""position:relative; top:5px; left:10px; float:left; height:60px; width:250px; text-align:center;""><font style=""color:#203682; font-family:'Times New Roman'; font-size:34px; font-weight:bold; line-height:32px;"">METHODUS</font><BR /><font style=""color:#CCCCCC; font-family:'Arial'; font-size:13px; font-weight:bold; line-height:12px;"">Aprimorando Talentos</font><BR /><a href=""http://www.methodus.com.br""><font style=""color:#203682; font-family:Arial; font-size:10px; line-height:11px;"">www.methodus.com.br</font></a></div><div style=""position:relative; float:right; top:15px; right:20px; height:60px;""><font style=""color:#203682; font-family:Arial; font-size:12px;"">Av. Paulista 2202 Cj 134<BR />S�o Paulo, SP - Metr� Consola��o<BR /><b>11 3288 2777 / 3285 1052</b></font></div></div><div style=""position:relative; width:100%;""><div style=""padding:15px;"">"
	
	strRodape 		= "</div></div></div></body></html>"
	
	MONTA_EMAIL = strCabecalho & conteudo & strRodape

END FUNCTION
'''''''''''''''''''''''''''''''

'' Fun��es para enviar e-mail com CDONTS ou CDOSYS ''
SUB ENVIAR_EMAIL( de, para, assunto, mensagem )
	
	'' para enviar IP no caso de mensagens para os professores
	if para = "info@methodus.com.br" then
		mensagem = replace( mensagem&"", "</body>", "IP do udu�rio: "&request.ServerVariables("REMOTE_ADDR")&"</body>" )
	end if
	

	Set CDOMail = Server.CreateObject("CDO.Message")
	Set CDOCon = Server.CreateObject ("CDO.Configuration")
	
	'Host
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = dedicado_smtp '"smtp.gmail.com"
	'Porta
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = dedicado_porta '25
	'SSL
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = dedicado_ssl 'True
	'Porta do CDO
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	'SMTP Autenticado = 1
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	'Usu�rio
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = dedicado_login '"contato@methodus.com.br"
	'Senha
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = dedicado_senha '"contato123"
	'Timeout (segundos)
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
	CDOCon.Fields.update
	Set CDOMail.Configuration = CDOCon
	
	CDOMail.From 		= "Methodus <"&dedicado_email&">"
	CDOMail.To 			= para
	CDOMail.Subject 	= assunto
	CDOMail.HtmlBody 	= mensagem
	
	'on error resume next
	'Enviando
	CDOMail.Send
	
	'Finalizando
	Set CDOMail = Nothing
	Set CDOCon = Nothing




	'If TestObject("CDONTS.NewMail") Then
		'CDONTS
	'	Set EnviarMail = Server.CreateObject("CDONTS.NewMail")
	'	EnviarMail.Body = mensagem
	'	EnviarMail.Importance = 1
	'	EnviarMail.BodyFormat = 0
	'	EnviarMail.MailFormat = 0
	'else
		'CDOSYS
	'	Set EnviarMail = CreateObject("CDO.Message")
	'	EnviarMail.HtmlBody = mensagem
	'end if
	
	'EnviarMail.From = de
	'EnviarMail.To = para
	'EnviarMail.Subject = assunto
	
	'EnviarMail.Send
	'Set EnviarMail = Nothing
	
		'Set EnviarMail = Server.CreateObject("Persits.MailSender")
		'EnviarMail.Host = "mail.methodus.com.br"
		'EnviarMail.Username = "contato@methodus.com.br"
		'EnviarMail.Password = "contato123"
		'EnviarMail.From = "contato@methodus.com.br"
		'EnviarMail.FromName = "Methodus"
		'EnviarMail.AddAddress para, para
		'EnviarMail.Subject = assunto
		'EnviarMail.IsHTML = True
		'EnviarMail.Body = mensagem
		'EnviarMail.Timestamp 	= Now()
		'On Error Resume Next
		'EnviarMail.Send
		
		'Set EnviarMail = nothing	
						
END SUB

Function TestObject( comIdentity )
	On Error Resume Next
	TestObject = False
	Err.Clear
	Set xTestObj = Server.CreateObject( comIdentity )
	If Err = 0 Then TestObject = True
	Set xTestObj = Nothing
	Err.clear
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para enviar e-mail com ASP MAIL ''
SUB ENVIAR_EMAIL_DEDICADO( para, assunto, mensagem )
	
	'' para enviar IP no caso de mensagens para os professores
	if para = "info@methodus.com.br" then
		mensagem = replace( mensagem&"", "</body>", "IP do udu�rio: "&request.ServerVariables("REMOTE_ADDR")&"</body>" )
	end if
	
	Set CDOMail = Server.CreateObject("CDO.Message")
	Set CDOCon = Server.CreateObject ("CDO.Configuration")
	
	'Host
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = dedicado_smtp '"66.135.61.51" '"smtp.cursoleituradinamica.com"
	'Porta
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = dedicado_porta '25
	'SSL
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = dedicado_ssl 'True
	'Porta do CDO
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	'SMTP Autenticado = 1
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	'Usu�rio
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = dedicado_login '"info@cursoleituradinamica.com" '"postmaster@cursoleituradinamica.com"
	'Senha
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = dedicado_senha '"methodus12344"
	'Timeout (segundos)
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
	CDOCon.Fields.update
	Set CDOMail.Configuration = CDOCon
	
	CDOMail.From 		= "Methodus <"&dedicado_email&">"
	CDOMail.To 			= para
	CDOMail.Subject 	= assunto
	CDOMail.HtmlBody 	= mensagem
	
	'Enviando
	'On Error Resume Next
	CDOMail.Send
	
	'Finalizando
	Set CDOMail = Nothing
	Set CDOCon = Nothing

	'Dim EnviarMail2
	
	'Set EnviarMail2 = Server.CreateObject("Persits.MailSender")
	'EnviarMail2.Host = "mail.methodus.com.br"
	'EnviarMail2.Username = "contato@methodus.com.br"
	'EnviarMail2.Password = "contato123"
	'EnviarMail2.From = "contato@methodus.com.br"
	'EnviarMail2.FromName = "Methodus"
	'EnviarMail2.AddAddress para, "Aluno"
	'EnviarMail2.Subject = assunto
	'EnviarMail2.IsHTML = True
	'EnviarMail2.Body = mensagem
	'EnviarMail2.Timestamp 	= Now()
	'On Error Resume Next
	'EnviarMail2.Send
	
	'Set EnviarMail2 = nothing
						
END SUB
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para enviar e-mail com ASP MAIL ''
SUB ENVIAR_EMAIL_ANEXO( para, assunto, mensagem, anexo )
	
	'' para enviar IP no caso de mensagens para os professores
	if para = "info@methodus.com.br" then
		mensagem = replace( mensagem&"", "</body>", "IP do udu�rio: "&request.ServerVariables("REMOTE_ADDR")&"</body>" )
	end if

	Set CDOMail = Server.CreateObject("CDO.Message")
	Set CDOCon = Server.CreateObject ("CDO.Configuration")
	
	'Host
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = dedicado_smtp '"smtp.gmail.com"
	'Porta
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = dedicado_porta '25
	'SSL
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = dedicado_ssl 'True
	'Porta do CDO
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	'SMTP Autenticado = 1
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	'Usu�rio
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = dedicado_login '"contato@methodus.com.br"
	'Senha
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = dedicado_senha '"contato123"
	'Timeout (segundos)
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
	CDOCon.Fields.update
	Set CDOMail.Configuration = CDOCon
	
	CDOMail.From 		= "Methodus <"&dedicado_email&">"
	CDOMail.To 			= para
	if not anexo = empty then
		CDOMail.AddAttachment(anexo) 'anexa o arquivo
	end if
	CDOMail.Subject 	= assunto
	CDOMail.HtmlBody 	= mensagem
	
	'on error resume next
	'Enviando
	CDOMail.Send
	
	'Finalizando
	Set CDOMail = Nothing
	Set CDOCon = Nothing

	'Dim EnviarMail2
	
	'Set EnviarMail2 = Server.CreateObject("Persits.MailSender")
	'EnviarMail2.Host = "mail.methodus.com.br"
	'EnviarMail2.Username = "contato@methodus.com.br"
	'EnviarMail2.Password = "contato123"
	'EnviarMail2.From = "contato@methodus.com.br"
	'EnviarMail2.FromName = "Methodus"
	'EnviarMail2.AddAddress para, "Aluno"
	'EnviarMail2.Subject = assunto
	
	'if not anexo = empty then
	'	EnviarMail2.AddAttachment anexo
	'end if
	
	'EnviarMail2.IsHTML = True
	'EnviarMail2.Body = mensagem
	'EnviarMail2.Timestamp 	= Now()
	'On Error Resume Next
	'EnviarMail2.Send
	
	'Set EnviarMail2 = nothing
						
END SUB
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para enviar e-mail com ASP MAIL ''
SUB ENVIAR_EMAIL_DEDICADO_ANEXO( para, assunto, mensagem, anexo )
	
	'' para enviar IP no caso de mensagens para os professores
	if para = "info@methodus.com.br" then
		mensagem = replace( mensagem&"", "</body>", "IP do udu�rio: "&request.ServerVariables("REMOTE_ADDR")&"</body>" )
	end if

	Set CDOMail = Server.CreateObject("CDO.Message")
	Set CDOCon = Server.CreateObject ("CDO.Configuration")
	
	'Host
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = dedicado_smtp '"66.135.61.51" '"smtp.cursoleituradinamica.com"
	'Porta
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = dedicado_porta '25
	'SSL
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = dedicado_ssl 'True
	'Porta do CDO
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	'SMTP Autenticado = 1
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	'Usu�rio
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = dedicado_login '"info@cursoleituradinamica.com" '"postmaster@cursoleituradinamica.com"
	'Senha
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = dedicado_senha '"methodus12344"
	'Timeout (segundos)
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
	CDOCon.Fields.update
	Set CDOMail.Configuration = CDOCon
	
	CDOMail.From 		= "Methodus <"&dedicado_email&">"
	CDOMail.To 			= para

	if not anexo = empty then
		CDOMail.AddAttachment(anexo) 'anexa o arquivo
	end if
	CDOMail.Subject 	= assunto
	CDOMail.HtmlBody 	= mensagem
	
	'on error resume next
	'Enviando
	CDOMail.Send
	
	on error goto 0
	
	'Finalizando
	Set CDOMail = Nothing
	Set CDOCon = Nothing
						
END SUB
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para formatar CNPJ ''
FUNCTION FORMATA_CNPJ(cnpj, condicao)

	if condicao = true then
	
		cnpj_novo = replace( replace( replace( cnpj, ".", ""), "-", ""), "/", "")
		
		FORMATA_CNPJ = cnpj_novo
	
	else
	
		cnpj = right( "0000000000000000"&cnpj, 15)
		
		cnpj_novo = mid(cnpj, 1, 3) & "." & mid(cnpj, 4, 3) & "." & mid(cnpj, 7, 3) & "/" & mid(cnpj, 10, 4) & "-" & mid(cnpj, 14, 2)
		
		FORMATA_CNPJ = cnpj_novo
		
	end if

END FUNCTION
'''''''''''''''''''''''''''''''

'' FUN��O PARA MONTAR A MALA-DIRETA DO CURSO ''
FUNCTION MAILING_CURSO( ID_curso )

	sql = 	"SELECT titulo_curso, ID_curso, resumo_curso, distancia_curso, mailing_curso, cor_curso, mailing_ultimo_curso, mailing_fotos_curso, mailing_data_curso FROM CURSOS where ID_curso="&ID_curso&""
	set rs_curso = conexao.execute(sql)
	
	if not rs_curso.EOF then

	str_mailing = rs_curso("mailing_curso")
	
	'' para ver qual foto colocar ''
	if isnull(rs_curso("mailing_fotos_curso"))=false and rs_curso("mailing_fotos_curso") > 0 then
		
		str_utlima_foto = 1
		
		if isnull(rs_curso("mailing_data_curso")) then
			
			sql = "update CURSOS set mailing_data_curso='"&DATA_BD(now())&"', mailing_ultimo_curso="&str_utlima_foto&" where ID_curso="&ID_curso&""
			conexao.execute(sql)
			
		else
			
			if DateDiff("d", now(), rs_curso("mailing_data_curso")) > 1 or DateDiff("d", now(), rs_curso("mailing_data_curso")) < -1 then
			
				if isnull(rs_curso("mailing_ultimo_curso")) or rs_curso("mailing_ultimo_curso") >= rs_curso("mailing_fotos_curso") then
					str_utlima_foto = 1
				else
					str_utlima_foto = rs_curso("mailing_ultimo_curso") + 1
				end if
				
				sql = "update CURSOS set mailing_data_curso='"&DATA_BD(now())&"', mailing_ultimo_curso="&str_utlima_foto&" where ID_curso="&ID_curso&""
				conexao.execute(sql)
				
			else
				str_utlima_foto = rs_curso("mailing_ultimo_curso")
			end if
			
		end if
		
	end if
	
	'if cstr(ID_curso) = "4" then 
		str_mailing = replace( str_mailing, "[IMAGEM]", "http://api.methodus.com.br/site/maladireta/img/cursos/"&ID_curso&"_"&str_utlima_foto&".jpg" )
	'else
	'	str_mailing = replace( str_mailing, "[IMAGEM]", "<img src=""http://api.methodus.com.br/site/maladireta/img/cursos/"&ID_curso&"_"&str_utlima_foto&".jpg"" width=""570"" height=""231"" border=""0"" />" )
	'end if
	''''''''''''''''''''''''''''''''
	
	str_mailing = replace( str_mailing, "[TEXTO_IMAGEM]", texto_imagem )
	str_mailing = replace( str_mailing, "[TEXTO]", texto_automatico )
	
	'' Se � curso a dist�ncia ''
	if rs_curso("distancia_curso") = true then
		
		'' montar os valores do curso ''
		sql = "select top 1 * from CURSOS_PLANOS where ID_curso="&ID_curso&" and status_plano="&verdade&" and empresarial_plano="&falso&" order by ID_plano"
		set rs_plano = conexao.execute(sql)
		
		str_mailing_valor = ""
		if not rs_plano.EOF then
		
			'str_mailing_valor = str_mailing_valor & "<div align=""center"">"
            'str_mailing_valor = str_mailing_valor & "    <strong style=""FONT-SIZE: 26px; COLOR: "&rs_curso("cor_curso")&""">R$ "&FORMATA_MOEDA( rs_plano("valor_plano"), false )&"</strong> "
            'str_mailing_valor = str_mailing_valor & "</div>"
        	
			str_mailing_valor = str_mailing_valor & ""
			
			
			str_mailing_valor = str_mailing_valor & "<div>"
            str_mailing_valor = str_mailing_valor & "<b>Investimento:</b> <font style=""FONT-SIZE: 18px; COLOR: "&rs_curso("cor_curso")&";""><b>R$ "&FORMATA_MOEDA( rs_plano("valor_plano"), false )&"</b></font><BR /><a href=""http://www.methodus.com.br/site/investimento/cursos.asp?curso="&ID_curso&""">(fa�a seu pedido)</a><BR /><font style=""font-size:4px; color:#FFFFFF"">.</font><BR />"
            
			if isnull(rs_plano("desconto1_plano"))=false then
            	str_mailing_valor = str_mailing_valor & "� vista "&rs_plano("desconto1_plano")&"% de desconto: <b>R$ "&FORMATA_MOEDA( (rs_plano("valor_plano")-( (rs_plano("valor_plano")*rs_plano("desconto1_plano"))/100 )), false )&"</b>"
            end if
                
            str_mailing_valor = str_mailing_valor & "<BR />a prazo, at� "&rs_plano("parcelas_plano")&" vezes sem juros<BR /><font color="""&rs_curso("cor_curso")&""" style=""font-size:10px;"">"
            
            if rs_plano("parcelas_plano") > 1 then

            	for i=2 to rs_plano("parcelas_plano")

                	str_mailing_valor = str_mailing_valor & ""&i&" x "&FORMATA_MOEDA( ( rs_plano("valor_plano") / i ), false )&" / "
                    
                next
                
            end if

            str_mailing_valor = str_mailing_valor & "</font></div>"
			
		end if
		''''''''''''''''''''''''''''''''
		
		str_mailing = replace( str_mailing, "[VALOR]", str_mailing_valor )
		
	
	'' se � curso presencial ''	
	else
		
		'' montar as datas do curso ''
		sql = "select top 5 * from CURSOS_DATAS where ID_curso="&rs_curso("ID_curso")&" and inicio_data>='"&DATA_BD(dateadd("d",-1,now()))&"' and fim_inscricoes_data>='"&DATA_BD(now())&"' order by inicio_data"
		set rs_data= conexao.execute(sql)
		
		str_mailing_data = ""
		do while not rs_data.EOF
			
        	str_mailing_data = str_mailing_data & "<font color="""&rs_curso("cor_curso")&"""><b>&gt;&gt;</b></font> "&rs_data("horario_data")&"<BR /><div style=""padding-left:18px;""><b>"&FORMATA_DATA( rs_data("inicio_data"), 1 )&"</b> � <b>"&FORMATA_DATA( rs_data("fim_data"), 1 )&"</b></div>"
        	
		rs_data.movenext:loop
		''''''''''''''''''''''''''''''
		
		'' montar os valores do curso ''
		sql = "select top 1 * from CURSOS_PLANOS where ID_curso="&ID_curso&" and status_plano="&verdade&" and empresarial_plano="&falso&" order by ID_plano"
		set rs_plano = conexao.execute(sql)
		
		str_mailing_valor = ""
		if not rs_plano.EOF then
		
       		str_mailing_valor = str_mailing_valor & ""
			
			
			str_mailing_valor = str_mailing_valor & "<div>"
            str_mailing_valor = str_mailing_valor & "<b>Investimento:</b> <font style=""FONT-SIZE: 18px; COLOR: "&rs_curso("cor_curso")&";""><b>R$ "&FORMATA_MOEDA( rs_plano("valor_plano"), false )&"</b></font> <a href=""http://www.methodus.com.br/site/investimento/cursos.asp?curso="&ID_curso&""">(fa�a sua inscri��o)</a><BR /><font style=""font-size:4px; color:#FFFFFF"">.</font><BR />"
            
			if isnull(rs_plano("desconto1_plano"))=false then
            	str_mailing_valor = str_mailing_valor & "� vista "&rs_plano("desconto1_plano")&"% de desconto: <b>R$ "&FORMATA_MOEDA( (rs_plano("valor_plano")-( (rs_plano("valor_plano")*rs_plano("desconto1_plano"))/100 )), false )&"</b>"
            end if
                
            str_mailing_valor = str_mailing_valor & "<BR />a prazo, at� "&rs_plano("parcelas_plano")&" vezes sem juros<BR /><font color="""&rs_curso("cor_curso")&""" style=""font-size:10px;"">"
            
            if rs_plano("parcelas_plano") > 1 then

            	for i=2 to rs_plano("parcelas_plano")

                	str_mailing_valor = str_mailing_valor & ""&i&" x "&FORMATA_MOEDA( ( rs_plano("valor_plano") / i ), false )&" / "
                    
                next
                
            end if

            str_mailing_valor = str_mailing_valor & "</font></div>"
            
            if isnull(rs_plano("desconto2_plano"))=false then
                str_mailing_valor = str_mailing_valor & "<div align=""center""><font style=""font-size:10px;"">Obs.: Para mais de uma pessoa, "&rs_plano("desconto2_plano")&"% de desconto � vista ou "&rs_plano("desconto3_plano")&"% de desconto a prazo</font></div>"
            end if
        
		end if
		''''''''''''''''''''''''''''''''
		
		str_mailing = replace( str_mailing, "[DATAS]", str_mailing_data )
		str_mailing = replace( str_mailing, "[VALOR]", str_mailing_valor )
		
	end if
	
	'' para pegar �ltimos artigos ''
	if instr( 1, str_mailing, "[ARTIGOS:", 1 ) > 0 then
	
		'' Leitura dinamica
		if instr( 1, str_mailing, "[ARTIGOS:1]", 1 ) > 0 then
			sql = "select top 3 ID_artigo, titulo_artigo from ARTIGOS where ID_tema=1 and status_artigo="&verdade&" and data_artigo<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_artigo desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_artigo = ""
			do while not rsp.EOF
				str_mailing_artigo = str_mailing_artigo & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/site/artigos/detalhes.asp?ID="&rsp("ID_artigo")&""">"&rsp("titulo_artigo")&"</a><br />"
			rsp.movenext:loop
		
			str_mailing = replace( str_mailing, "[ARTIGOS:1]", str_mailing_artigo )
		end if
		
		
		'' Comunica��es
		if instr( 1, str_mailing, "[ARTIGOS:2]", 1 ) > 0 then
			sql = "select top 3 ID_artigo, titulo_artigo from ARTIGOS where ID_tema=2 and status_artigo="&verdade&" and data_artigo<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_artigo desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_artigo = ""
			do while not rsp.EOF
				str_mailing_artigo = str_mailing_artigo & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/site/artigos/detalhes.asp?ID="&rsp("ID_artigo")&""">"&rsp("titulo_artigo")&"</a><br />"
			rsp.movenext:loop
		
			str_mailing = replace( str_mailing, "[ARTIGOS:2]", str_mailing_artigo )
		end if
		
		'' Gerenciamento
		if instr( 1, str_mailing, "[ARTIGOS:3]", 1 ) > 0 then
			sql = "select top 3 ID_artigo, titulo_artigo from ARTIGOS where ID_tema=3 and status_artigo="&verdade&" and data_artigo<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_artigo desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_artigo = ""
			do while not rsp.EOF
				str_mailing_artigo = str_mailing_artigo & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/site/artigos/detalhes.asp?ID="&rsp("ID_artigo")&""">"&rsp("titulo_artigo")&"</a><br />"
			rsp.movenext:loop
		
			str_mailing = replace( str_mailing, "[ARTIGOS:3]", str_mailing_artigo )
		end if
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimas noticias ''
	if instr( 1, str_mailing, "[NOTICIAS]", 1 ) > 0 then
		
		sql = "select top 3 ID_noticia, titulo_noticia from NOTICIAS where status_noticia="&verdade&" and data_noticia<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_noticia desc"
		set rsp = conexao.execute(sql)
		
		do while not rsp.EOF
			str_mailing_noticia = str_mailing_noticia & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/site/noticias/detalhes.asp?ID="&rsp("ID_noticia")&""">"&rsp("titulo_noticia")&"</a><br />"
		rsp.movenext:loop
		
		str_mailing = replace( str_mailing, "[NOTICIAS]", str_mailing_noticia )
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimas noticias ''
	if instr( 1, str_mailing, "[ARTIGOS_CARREIRAS]", 1 ) > 0 then
		
		sql = "select top 3 ID_artigo, titulo_artigo from ARTIGOS_CARREIRAS where status_artigo="&verdade&" and data_artigo<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_artigo desc"
		set rsp = conexao.execute(sql)
		
		do while not rsp.EOF
			str_mailing_artigoc = str_mailing_artigoc & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/site/artigos_carreiras/detalhes.asp?ID="&rsp("ID_artigo")&""">"&rsp("titulo_artigo")&"</a><br />"
		rsp.movenext:loop
		
		str_mailing = replace( str_mailing, "[ARTIGOS_CARREIRAS]", str_mailing_artigoc )
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimas noticias ''
	if instr( 1, str_mailing, "[LINKS]", 1 ) > 0 then
		
		sql = "select top 3 ID_link, url_link, titulo_link from LINKS where status_link="&verdade&" and mm="&verdade&" order by data_link desc"
		set rsp = conexao.execute(sql)
		
		do while not rsp.EOF
			str_mailing_link = str_mailing_link & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href="""&rsp("url_link")&""">"&rsp("titulo_link")&"</a><br />"
		rsp.movenext:loop
		
		str_mailing = replace( str_mailing, "[LINKS]", str_mailing_link )
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimos livros ''
	if instr( 1, str_mailing, "[LIVROS:", 1 ) > 0 then
	
		'' Leitura dinamica
		if instr( 1, str_mailing, "[LIVROS:1]", 1 ) > 0 then
			sql = "select top 3 ID_livro, titulo_livro, corpo_livro from LIVROS where ID_tema=1 and status_livro="&verdade&" and mm="&verdade&" order by data_livro desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <b style=""font-size:9px;"">"&rsp("titulo_livro")&"</b><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[LIVROS:1]", str_mailing_livro )
		end if
			
		'' Comunica��es
		if instr( 1, str_mailing, "[LIVROS:2]", 1 ) > 0 then
			sql = "select top 3 ID_livro, titulo_livro, corpo_livro from LIVROS where ID_tema=2 and status_livro="&verdade&" and mm="&verdade&" order by data_livro desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <b style=""font-size:9px;"">"&rsp("titulo_livro")&"</b><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[LIVROS:2]", str_mailing_livro )
		end if
		
		'' Gerenciamento
		if instr( 1, str_mailing, "[LIVROS:3]", 1 ) > 0 then
			sql = "select top 3 ID_livro, titulo_livro, corpo_livro from LIVROS where ID_tema=3 and status_livro="&verdade&" and mm="&verdade&" order by data_livro desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <b style=""font-size:9px;"">"&rsp("titulo_livro")&"</b><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[LIVROS:3]", str_mailing_livro )
		end if
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimos Videos ''
	if instr( 1, str_mailing, "[VIDEOS:", 1 ) > 0 then
	
		'' Leitura dinamica
		if instr( 1, str_mailing, "[VIDEOS:1]", 1 ) > 0 then
			sql = "select top 3 ID_video, titulo_video from VIDEOS where ID_tema=1 and status_video="&verdade&" and chamada_video<>'' order by data_video desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/site/videos/detalhes.asp?ID="&rsp("ID_video")&""">"&rsp("titulo_video")&"</a><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[VIDEOS:1]", str_mailing_livro )
		end if
			
		'' Comunica��es
		if instr( 1, str_mailing, "[VIDEOS:2]", 1 ) > 0 then
			sql = "select top 3 ID_video, titulo_video from VIDEOS where ID_tema=2 and status_video="&verdade&" and chamada_video<>'' order by data_video desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/site/videos/detalhes.asp?ID="&rsp("ID_video")&""">"&rsp("titulo_video")&"</a><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[VIDEOS:2]", str_mailing_livro )
		end if
		
		'' Gerenciamento
		if instr( 1, str_mailing, "[VIDEOS:3]", 1 ) > 0 then
			sql = "select top 3 ID_video, titulo_video from VIDEOS where ID_tema=3 and status_video="&verdade&" and chamada_video<>'' order by data_video desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/site/videos/detalhes.asp?ID="&rsp("ID_video")&""">"&rsp("titulo_video")&"</a><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[VIDEOS:3]", str_mailing_livro )
		end if
		
	end if
	'''''''''''''''''''''''''''''''
	
	
	
	MAILING_CURSO = str_mailing
	
	end if

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''


'' FUN��O PARA ENVIAR A MALA-DIRETA DO CURSO PARA ALUNO EM HOR�RIO DETERMINADO ''
FUNCTION ENVIA_MAILING_CURSO( ID_curso, nome, email )

	tempo_atraso = 20

	ID_curso = split( ID_curso, "," )
	
	
	
	for i=0 to ubound(ID_curso)
			
		ID_temp = ID_curso(i)
		ID_temp = replace( ID_temp&"", "|o", "" )
		
		str_cursos_query = str_cursos_query & "," & ID_temp
			
	next
		
	sql = 	"SELECT ID_curso, titulo_curso FROM CURSOS where ID_curso in (0"&str_cursos_query&")"
	set rs_curso = conexao.execute(sql)
		
	do while not rs_curso.EOF
			
		titulo_curso 	= rs_curso("titulo_curso")
		assunto			= titulo_curso
		mensagem 		= ( "<b>"&nome&"</b>, seguem informa��es relevantes<BR />sobre o treinamento de <b>"&titulo_curso&"</b><BR /><BR />"& MAILING_CURSO( rs_curso("ID_curso") ) )
		mensagem 		= replace( mensagem, "[EMAIL]", email )
		'mensagem 		= ( MONTA_EMAIL( "<b>"&nome&"</b>, seguem informa��es relevantes<BR />sobre o treinamento de <b>"&titulo_curso&"</b><BR /><BR />"& MAILING_CURSO( ID_curso(i) ) ) )
	
		'call ENVIAR_EMAIL( email_01, email, assunto, mensagem )
		
		
		Set CDOMail = Server.CreateObject("CDO.Message")
		Set CDOCon = Server.CreateObject ("CDO.Configuration")
		
		'Host
		CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = dedicado_smtp '"smtp.gmail.com"
		'Porta
		CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = dedicado_porta '25
		'SSL
		CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = dedicado_ssl 'True
		'Porta do CDO
		CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		'SMTP Autenticado = 1
		CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
		'Usu�rio
		CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = dedicado_login '"contato@methodus.com.br"
		'Senha
		CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = dedicado_senha '"contato123"
		'Timeout (segundos)
		CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
		CDOCon.Fields.update
		Set CDOMail.Configuration = CDOCon
		
		CDOMail.From 		= "Methodus <"&dedicado_email&">"
		CDOMail.To 			= nome &"<"& email &">"
		CDOMail.Subject 	= assunto
		CDOMail.HtmlBody 	= mensagem
		
		on error resume next
		'Enviando
		CDOMail.Send
		
		'Finalizando
		Set CDOMail = Nothing
		Set CDOCon = Nothing
			
		
	rs_curso.movenext:loop
	
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' FUN��O PARA MONTAR A MALA-DIRETA DO CURSO ''
FUNCTION MAILING_AUTOMATICO_CURSO( ID_curso, texto_automatico, imagem, texto_imagem )

	sql = 	"SELECT titulo_curso, ID_curso, resumo_curso, distancia_curso, mailing_curso, cor_curso, mailing_ultimo_curso, mailing_fotos_curso, mailing_data_curso FROM CURSOS where ID_curso="&ID_curso&""
	set rs_curso = conexao.execute(sql)
	
	if not rs_curso.EOF then
	
		str_mailing = rs_curso("mailing_curso")
		
		'' trocando a imagem e texto
		'str_mailing = replace( str_mailing, "[IMAGEM] </a>", "[IMAGEM]</a>" )
		'str_mailing = replace( str_mailing, "[IMAGEM]</a>", "<img src=""http://www.methodus.com.br/site/maladireta/img/cursos/"&imagem&".jpg"" width=""600"" height=""339"" border=""0"" /></a><div style=""padding:15px; font-size: 12px;"">"&texto_automatico&"</div>" )
		
		str_mailing = replace( str_mailing, "[IMAGEM]", "http://api.methodus.com.br/site/maladireta/img/cursos/"&imagem&".jpg" )
		str_mailing = replace( str_mailing, "[TEXTO_IMAGEM]", texto_imagem )
		str_mailing = replace( str_mailing, "[TEXTO]", texto_automatico )
		''''''''''''''''''''''''''''''''
		
		'' Se � curso a dist�ncia ''
		if rs_curso("distancia_curso") = true then
			
			'' montar os valores do curso ''
			sql = "select top 1 * from CURSOS_PLANOS where ID_curso="&ID_curso&" and status_plano="&verdade&" and empresarial_plano="&falso&" order by ID_plano"
			set rs_plano = conexao.execute(sql)
			
			str_mailing_valor = ""
			if not rs_plano.EOF then
			
				'str_mailing_valor = str_mailing_valor & "<div align=""center"">"
				'str_mailing_valor = str_mailing_valor & "    <strong style=""FONT-SIZE: 26px; COLOR: "&rs_curso("cor_curso")&""">R$ "&FORMATA_MOEDA( rs_plano("valor_plano"), false )&"</strong> "
				'str_mailing_valor = str_mailing_valor & "</div>"
				
				str_mailing_valor = str_mailing_valor & ""
				
				
				str_mailing_valor = str_mailing_valor & "<div>"
				str_mailing_valor = str_mailing_valor & "<font style=""COLOR: "&rs_curso("cor_curso")&";""><b>R$ "&FORMATA_MOEDA( rs_plano("valor_plano"), false )&"</b></font><BR />"
				
				if isnull(rs_plano("desconto1_plano"))=false then
					str_mailing_valor = str_mailing_valor & "� vista "&rs_plano("desconto1_plano")&"% de desconto: <b>R$ "&FORMATA_MOEDA( (rs_plano("valor_plano")-( (rs_plano("valor_plano")*rs_plano("desconto1_plano"))/100 )), false )&"</b>"
				end if
					
				str_mailing_valor = str_mailing_valor & "<BR />a prazo, at� "&rs_plano("parcelas_plano")&" vezes sem juros<BR /><font color="""&rs_curso("cor_curso")&""" style=""font-size:10px;"">"
				
				if rs_plano("parcelas_plano") > 1 then
	
					for i=2 to rs_plano("parcelas_plano")
	
						str_mailing_valor = str_mailing_valor & ""&i&" x "&FORMATA_MOEDA( ( rs_plano("valor_plano") / i ), false )&" / "
						
					next
					
				end if
	
				str_mailing_valor = str_mailing_valor & "</font></div>"
				
			end if
			''''''''''''''''''''''''''''''''
			
			str_mailing = replace( str_mailing, "[VALOR]", str_mailing_valor )
			
		
		'' se � curso presencial ''	
		else
			
			'' montar as datas do curso ''
			sql = "select top 5 * from CURSOS_DATAS where ID_curso="&rs_curso("ID_curso")&" and inicio_data>='"&DATA_BD(dateadd("d",-1,now()))&"' and fim_inscricoes_data>='"&DATA_BD(now())&"' order by inicio_data"
			set rs_data= conexao.execute(sql)
			
			str_mailing_data = ""
			do while not rs_data.EOF
				
				str_mailing_data = str_mailing_data & "<font color="""&rs_curso("cor_curso")&"""><b>&gt;&gt;</b></font> "&rs_data("horario_data")&"<BR /><div style=""padding-left:18px;""><b>"&FORMATA_DATA( rs_data("inicio_data"), 1 )&"</b> � <b>"&FORMATA_DATA( rs_data("fim_data"), 1 )&"</b></div>"
				
			rs_data.movenext:loop
			''''''''''''''''''''''''''''''
			
			'' montar os valores do curso ''
			sql = "select top 1 * from CURSOS_PLANOS where ID_curso="&ID_curso&" and status_plano="&verdade&" and empresarial_plano="&falso&" order by ID_plano"
			set rs_plano = conexao.execute(sql)
			
			str_mailing_valor = ""
			if not rs_plano.EOF then
			
				str_mailing_valor = str_mailing_valor & ""
				
				
				str_mailing_valor = str_mailing_valor & "<div>"
				str_mailing_valor = str_mailing_valor & "<font style=""FONT-SIZE: 18px; COLOR: "&rs_curso("cor_curso")&";""><b>R$ "&FORMATA_MOEDA( rs_plano("valor_plano"), false )&"</b></font><BR />"
				
				if isnull(rs_plano("desconto1_plano"))=false then
					str_mailing_valor = str_mailing_valor & "� vista "&rs_plano("desconto1_plano")&"% de desconto: <b>R$ "&FORMATA_MOEDA( (rs_plano("valor_plano")-( (rs_plano("valor_plano")*rs_plano("desconto1_plano"))/100 )), false )&"</b>"
				end if
					
				str_mailing_valor = str_mailing_valor & "<BR />a prazo, at� "&rs_plano("parcelas_plano")&" vezes sem juros<BR /><font color="""&rs_curso("cor_curso")&""">"
				
				if rs_plano("parcelas_plano") > 1 then
	
					for i=2 to rs_plano("parcelas_plano")
	
						str_mailing_valor = str_mailing_valor & ""&i&" x "&FORMATA_MOEDA( ( rs_plano("valor_plano") / i ), false )&" / "
						
					next
					
				end if
	
				str_mailing_valor = str_mailing_valor & "</font></div>"
				
				if isnull(rs_plano("desconto2_plano"))=false then
					str_mailing_valor = str_mailing_valor & "<div align=""center""><font>Obs.: Para mais de uma pessoa, "&rs_plano("desconto2_plano")&"% de desconto � vista ou "&rs_plano("desconto3_plano")&"% de desconto a prazo</font></div>"
				end if
			
			end if
			''''''''''''''''''''''''''''''''
			
			str_mailing = replace( str_mailing, "[DATAS]", str_mailing_data )
			str_mailing = replace( str_mailing, "[VALOR]", str_mailing_valor )
			
		end if
		
		
		MAILING_AUTOMATICO_CURSO = str_mailing
	
	end if

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''

'' FUN��O PARA LIBERAR ACESSO DO USU�RIO AO CURSO '''''''''''''''''''''''''''''''
SUB LIBERAR_ACESSO( ID_inscricao )

	sql="select * from CURSOS_INSCRICOES where ID_inscricao="&ID_inscricao
	set rs = conexao.execute(sql)
	
	if not rs.EOF then
	
		'' descobrir qual � o �ltimo n�mero de inscri��o ''
		if isnull(rs("numero_inscricao")) then
			sql = "select top 1 * from CURSOS_INSCRICOES where numero_inscricao is not null order by numero_inscricao desc"
			set rs_ultimo = conexao.execute(sql)
			if not rs_ultimo.EOF then
				numero_inscricao = rs_ultimo("numero_inscricao")+1
			else
				numero_inscricao = 1
			end if
			
			sql="update CURSOS_INSCRICOES set numero_inscricao="&numero_inscricao&" where ID_inscricao="&rs("ID_inscricao")
			conexao.execute(sql)
		end if
		
		str_ID_inscricao = cstr(rs("ID_inscricao")&"")
	
		'' Atualizar a inscri��o ''
		sql="update CURSOS_INSCRICOES set pago_inscricao="&verdade&" where ID_inscricao="&rs("ID_inscricao")
		conexao.execute(sql)
		'''''''''''''''''''''''''''
	
		ID_curso 	= rs("ID_curso")
		data_inicio	= DATA_BD( now() )
		data_fim	= DATA_BD( dateadd( "m", 12, now() ) )
		
		sql = "SELECT USUARIOS.ID_usuario, USUARIOS.nome_usuario, USUARIOS.cpf_usuario, USUARIOS.email_usuario FROM CURSOS_INSCRICOES INNER JOIN CURSOS_INSCRICOES_USUARIOS ON "&_
			  "CURSOS_INSCRICOES.ID_inscricao = CURSOS_INSCRICOES_USUARIOS.ID_inscricao INNER JOIN "&_
			  "USUARIOS ON CURSOS_INSCRICOES_USUARIOS.ID_usuario = USUARIOS.ID_usuario "&_
			  "where CURSOS_INSCRICOES_USUARIOS.ID_inscricao="&rs("ID_inscricao")
		set rs_users = conexao.execute(sql)
		
		do while not rs_users.EOF
		
			ID_usuario = rs_users("ID_usuario")

			dados = "{""nome"":""" & rs_users("nome_usuario") & """,""email"":""" & rs_users("email_usuario") & """,""cpf"":""" & rs_users("cpf_usuario") & """,""admin"":""false""}"
			
				
			IF ID_curso = 4 THEN
				Call SEND_API_CALL("http://157.245.215.207/usuarios",dados)
			ELSEIF ID_curso = 12 THEN
				Call SEND_API_CALL("http://104.131.87.127/usuarios",dados)
			END IF
				
		
			sql="select ID_usuario_curso from USUARIOS_CURSOS where ID_usuario="&ID_usuario&" and ID_curso="&ID_curso
			set rs= conexao.execute(sql)
			
			if not rs.EOF then
				
				sql="update USUARIOS_CURSOS set inicio_usuario_curso='"&data_inicio&"', fim_usuario_curso='"&data_fim&"', status_usuario_curso="&verdade&""&_
					" where ID_usuario_curso="&rs("ID_usuario_curso")
				conexao.execute(sql)
				
			else
			
				sql="insert into USUARIOS_CURSOS (ID_usuario, ID_curso, inicio_usuario_curso, fim_usuario_curso, status_usuario_curso)"&_
					" values ("&ID_usuario&","&ID_curso&",'"&data_inicio&"','"&data_fim&"',"&verdade&")"
				conexao.execute(sql)
			
			end if
		
		rs_users.movenext:loop
		
		'' LOG
		call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "ETAPA 3.1: Liberou acesso � inscri��o: "&str_ID_inscricao  )
		
		'retorno_instudo = MATRICULAR_INSTUDO( str_ID_inscricao )
		
	end if
	
END SUB
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para mandar requisi��es para APIs ''
SUB SEND_API_CALL( url, body)

	Dim http: Set http  = Server.CreateObject("Microsoft.XMLHTTP")

	With http
	Call .Open("POST", url, False)
	Call .SetRequestHeader("Content-Type", "application/json")
	Call .SetRequestHeader("Accepts", "application/json")
	Call .Send(body)
	End With

END SUB

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para exibir detalhes das op��es e formas de pagamento da I-Pagare '''''
FUNCTION IPAGARE_RETORNO( tipo, condicao )

	select case tipo
		
		case "meio"
		
			select case condicao
				case "1"
					IPAGARE_RETORNO = "Cart�o de Cr�dito - Diners"
				case "2"
					IPAGARE_RETORNO = "Cart�o de Cr�dito - Mastercard"
				case "7"
					IPAGARE_RETORNO = "Cart�o de Cr�dito - Visa"
				case "11"
					IPAGARE_RETORNO = "Cart�o de Cr�dito - American Express"
				case "6"
					IPAGARE_RETORNO = "Boleto Banc�rio Bradesco"
				case "0"
					IPAGARE_RETORNO = "Cheques"
				case "13"
					IPAGARE_RETORNO = "Dep�sito Banc�rio Ita�"
				case "4"
					IPAGARE_RETORNO = "Dep�sito Banc�rio Bradesco"
				case "100"
					IPAGARE_RETORNO = "Dinheiro"
				case "110"
					IPAGARE_RETORNO = "D�bito"
			end select
		
		
		case "forma"
		
			IPAGARE_RETORNO = replace( replace( condicao, "A", "" ), "B", "" )
		
			if left( condicao, 1 ) = "A" then
				IPAGARE_RETORNO = IPAGARE_RETORNO & "x sem juros"
			elseif left( condicao, 1 ) = "B" then
				IPAGARE_RETORNO = IPAGARE_RETORNO & "x com juros"
			end if
			
		
		case "status"
		
			select case condicao
				case "1"
					IPAGARE_RETORNO = "Aguardando Pagamento"
				case "2"
					IPAGARE_RETORNO = "Aguardando confirma��o de pagamento"
				case "3"
					IPAGARE_RETORNO = "Pagamento confirmado"
				case "4"
					IPAGARE_RETORNO = "Cancelado"
				case "5"
					IPAGARE_RETORNO = "Pagamento expirado"
				case "6"
					IPAGARE_RETORNO = "Parcialmente Pago"	
			end select
		
	
	end select

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Formata��o para URLs amig�veis ''
FUNCTION FORMATAR_URL( var )
	dim pesquisa

	pesquisa = REPLACE(var,"�","a")
	pesquisa = REPLACE(pesquisa,"�","a")
	pesquisa = REPLACE(pesquisa,"�","a")
	pesquisa = REPLACE(pesquisa,"�","a")
	pesquisa = REPLACE(pesquisa,"�","a")
	pesquisa = REPLACE(pesquisa,"�","e")
	pesquisa = REPLACE(pesquisa,"�","e")
	pesquisa = REPLACE(pesquisa,"�","e")
	pesquisa = REPLACE(pesquisa,"�","e")
	pesquisa = REPLACE(pesquisa,"�","i")
	pesquisa = REPLACE(pesquisa,"�","i")
	pesquisa = REPLACE(pesquisa,"�","i")
	pesquisa = REPLACE(pesquisa,"�","i")
	pesquisa = REPLACE(pesquisa,"�","o")
	pesquisa = REPLACE(pesquisa,"�","o")
	pesquisa = REPLACE(pesquisa,"�","o")
	pesquisa = REPLACE(pesquisa,"�","o")
	pesquisa = REPLACE(pesquisa,"�","o")
	pesquisa = REPLACE(pesquisa,"�","u")
	pesquisa = REPLACE(pesquisa,"�","u")
	pesquisa = REPLACE(pesquisa,"�","u")
	pesquisa = REPLACE(pesquisa,"�","u")
	pesquisa = REPLACE(pesquisa,"�","c")
	pesquisa = REPLACE(pesquisa,"�","A")
	pesquisa = REPLACE(pesquisa,"�","A")
	pesquisa = REPLACE(pesquisa,"�","A")
	pesquisa = REPLACE(pesquisa,"�","A")
	pesquisa = REPLACE(pesquisa,"�","A")
	pesquisa = REPLACE(pesquisa,"�","E")
	pesquisa = REPLACE(pesquisa,"�","E")
	pesquisa = REPLACE(pesquisa,"�","E")
	pesquisa = REPLACE(pesquisa,"�","E")
	pesquisa = REPLACE(pesquisa,"�","I")
	pesquisa = REPLACE(pesquisa,"�","I")
	pesquisa = REPLACE(pesquisa,"�","I")
	pesquisa = REPLACE(pesquisa,"�","I")
	pesquisa = REPLACE(pesquisa,"�","O")
	pesquisa = REPLACE(pesquisa,"�","O")
	pesquisa = REPLACE(pesquisa,"�","O")
	pesquisa = REPLACE(pesquisa,"�","O")
	pesquisa = REPLACE(pesquisa,"�","O")
	pesquisa = REPLACE(pesquisa,"�","U")
	pesquisa = REPLACE(pesquisa,"�","U")
	pesquisa = REPLACE(pesquisa,"�","U")
	pesquisa = REPLACE(pesquisa,"�","U")
	pesquisa = REPLACE(pesquisa,"�","C")
	
	pesquisa = REPLACE(pesquisa,"�","o")
	pesquisa = REPLACE(pesquisa,"�","a")
	pesquisa = REPLACE(pesquisa,",","")
	pesquisa = REPLACE(pesquisa,".","")
	pesquisa = REPLACE(pesquisa," ","-")
	pesquisa = REPLACE(pesquisa,":","")
	pesquisa = REPLACE(pesquisa,"/","-")
	pesquisa = REPLACE(pesquisa,"\","-")
	pesquisa = REPLACE(pesquisa,"'","")
	pesquisa = REPLACE(pesquisa,"""","")
	pesquisa = REPLACE(pesquisa,"%","")
	
	pesquisa = lcase( left( pesquisa, 50 ) )
	
	'pesquisa = Server.URLEncode( pesquisa )

	FORMATAR_URL = pesquisa
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' ENVIAR CONTATO PARA KLICK MAIL ''
SUB CADASTRAR_KLICKMAIL(nome, email, tags)
	
'	Dim xmlhttp
'	Set xmlhttp = Server.CreateObject("MSXML2.XMLHTTP")
'	
'	xmlhttp.open "GET", ""&url_php&"klick/index.php?email="&email&"&nome="&nome&"&tags="&tags , false
'	xmlhttp.setRequestHeader "Content-Type", "text/html; charset=utf-8"
'	xmlhttp.Send
'	str_retorno = (xmlhttp.responseText)
'	set xmlhttp = nothing
	
END SUB
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' ENVIAR LEAD PARA A RD STATION ''
SUB CADASTRAR_RDSTATION( identificador, nome, email, perfil, interesse )
	
	Dim xmlhttp
	Set xmlhttp = Server.CreateObject("MSXML2.XMLHTTP")
	
	xmlhttp.open "GET", ""&url_php&"rdstation/index.php?identificador="&identificador&"&nome="&nome&"&email="&email&"&perfil="&perfil&"&interesse="&interesse&"" , false
	xmlhttp.setRequestHeader "Content-Type", "text/html; charset=utf-8"
	xmlhttp.Send
	str_retorno = (xmlhttp.responseText)
	set xmlhttp = nothing
	
END SUB
'''''''''''''''''''''''''''''''''''

'' ENVIAR LEAD PARA A RD STATION ''
SUB CONVERSAO_RDSTATION( email, ID_usuario, ID_inscricao, valor )
	
	Dim xmlhttp
	Set xmlhttp = Server.CreateObject("MSXML2.XMLHTTP")
	
	if valor = empty then
		valor = 0
	else
		valor = valor 'FORMATA_MOEDA( valor, true )
	end if

	nome = ""
	
	if isnull(email) and not ID_usuario=empty then
		sql = "select nome_usuario, email_usuario from USUARIOS where ID_usuario="&ID_usuario
		set rs_rd = ABRIR_RS(sql)
		
		if not rs_rd.EOF then
			email = rs_rd("email_usuario")
			nome = rs_rd("nome_usuario")
		end if
	
	elseif isnull(email) and not ID_inscricao=empty then
		sql = "select nome_usuario, email_usuario from CURSOS_INSCRICOES_USUARIOS inner join USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario=USUARIOS.ID_usuario where ID_inscricao="&ID_inscricao
		set rs_rd = ABRIR_RS(sql)
		
		if not rs_rd.EOF then
			email = rs_rd("email_usuario")
			nome = rs_rd("nome_usuario")
		end if

	else
		sql = "select nome_usuario from USUARIOS where email_usuario='"&email&"'"
		set rs_rd = ABRIR_RS(sql)

		if not rs_rd.EOF then
			nome = rs_rd("nome_usuario")
		end if
		
	end if

	if not email=empty and not ID_inscricao=empty then
		sql = "select titulo_curso from CURSOS_INSCRICOES inner join CURSOS on CURSOS_INSCRICOES.ID_curso=CURSOS.ID_curso where ID_inscricao="&ID_inscricao
		set rs_rd = ABRIR_RS(sql)

		if not rs_rd.EOF then
			titulo_curso = rs_rd("titulo_curso")
			identificador_curso = IDENTIFICA_CURSO( titulo_curso )
				
			call CADASTRAR_RDSTATION( "comprou_"&identificador_curso, nome, email, "", titulo_curso )
		end if
	end if
	
	if isnull(email)=false and not ""&email=empty then
	
		xmlhttp.open "GET", ""&url_php&"rdstation/index.php?acao=won&email="&email&"&valor="&valor&"" , false
		xmlhttp.setRequestHeader "Content-Type", "text/html; charset=utf-8"
		xmlhttp.Send
		str_retorno = (xmlhttp.responseText)
		set xmlhttp = nothing
	
	end if
	
END SUB
'''''''''''''''''''''''''''''''''''

'' Fun��o para gerar identificador do curso de acordo com t�tulo do curso
FUNCTION IDENTIFICA_CURSO( titulo_curso )

	identificador = lcase( titulo_curso )
	identificador = replace( identificador, "curso de ", "" )
	identificador = replace( identificador, " ", "_" )

	if ( InStr( identificador, "orat" ) > 0 ) then
		identificador = "oratoria"
	elseif ( InStr( identificador, "memoriza" ) > 0 ) then
		identificador = "leitura_memoria"
	elseif ( InStr( identificador, "dist" ) > 0 ) then
		identificador = "leitura_distancia"
	elseif ( InStr( identificador, "tempo" ) > 0 ) then
		identificador = "tempo"
	end if

	IDENTIFICA_CURSO = identificador

END FUNCTION
'''''''''''''''''''''''''''''''''''

'' ENVIAR CONTATO PARA LOCAWEB ''
SUB CADASTRAR_LOCAWEB(nome, email, ativar)
	
'	Dim xmlhttp
'	Set xmlhttp = Server.CreateObject("MSXML2.XMLHTTP")
'	
'	'' Mailchimp ''
'	if ativar = true then
'		xmlhttp.open "GET", "http://us5.api.mailchimp.com/1.3/?method=listSubscribe&apikey=069581ec854bae6e851b1a38a768691b-us5&id=46f14591b7&email_address="&email&"&update_existing=true&double_optin=false&OPTIN_IP="&request.ServerVariables("REMOTE_ADDR")&"&OPTIN_TIME="&FORMATA_DATA(now(),3)&" "&FORMATA_HORA(now(),4) , false
'		xmlhttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
'		xmlhttp.Send ""
'		str_mailchimp = (xmlhttp.responseText)
'	end if
'	
'	if ativar = true then
'		xmlhttp.open "GET", ""&url_php&"contatosImportar.php?nome="&nome&"&email="&email&"" , false
'	else
'		xmlhttp.open "GET", ""&url_php&"contatosDesativar.php?nome="&nome&"&email="&email&"" , false
'	end if
'	
'	xmlhttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
'	xmlhttp.Send ""
'	str_resultado = (xmlhttp.responseText)
'	set xmlhttp = nothing
	
END SUB
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' ENVIAR CAMPANHA PARA LOCAWEB ''
SUB ENVIAR_MSG_LOCAWEB(ID_maladireta)
	
	sql="select titulo_maladireta from MALA_DIRETA where ID_maladireta="&ID_maladireta
	set rs_md= conexao.execute(sql)
	
	if not rs_md.EOF then
		
		assunto = rs_md("titulo_maladireta")
		identificador = "(" & assunto & ") - enviado em "&FORMATA_DATA(now(),1)&""
	
		Dim xmlhttp
		Set xmlhttp = Server.CreateObject("MSXML2.XMLHTTP")
		xmlhttp.open "GET", ""&url_php&"mensagemCriar.php?id_campanha="&ID_maladireta&"&identificador="&identificador&"&assunto="&assunto&"" , false
		xmlhttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		xmlhttp.Send ""
		ID_maladireta_locaweb = (xmlhttp.responseText)
		'set xmlhttp = nothing
		
		'response.Write("TESTE:"&ID_maladiret_locaweb&"<BR />")
		
		if isnumeric(ID_maladireta_locaweb) then
			
			'response.Write(""&url_php&"mensagemAgendar.php?id_campanha="&ID_maladireta_locaweb&"&data_agendada="&FORMATA_DATA(dateadd("d",1,now()),3)&" "&FORMATA_HORA(dateadd("d",1,now()),4)&"")
			
			xmlhttp.open "GET", ""&url_php&"mensagemAgendar.php?id_campanha="&ID_maladireta_locaweb&"&data_agendada="&FORMATA_DATA(dateadd("d",1,now()),3)&" "&FORMATA_HORA(dateadd("d",1,now()),4)&"" , false
			xmlhttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
			xmlhttp.Send ""
			str_resultado = (xmlhttp.responseText)
			set xmlhttp = nothing
			
		end if
		
	end if
	
END SUB
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' ENVIAR CAMPANHA PARA LOCAWEB ''
SUB AGENDAR_LOCAWEB(ID_maladireta)
	
	sql="select titulo_maladireta from MALA_DIRETA where ID_maladireta="&ID_maladireta
	set rs_md= conexao.execute(sql)
	
	if not rs_md.EOF then
		
		assunto = rs_md("titulo_maladireta")
		identificador = "(" & assunto & ") - enviado em "&FORMATA_DATA(now(),1)&""
	
		response.Redirect(""&url_php&"criar_agendar.php?id_campanha="&ID_maladireta&"&identificador="&identificador&"&assunto="&assunto&"&data_agendada="&FORMATA_DATA(dateadd("n",5,now()),3)&" "&FORMATA_HORA(dateadd("n",5,now()),4)&"")
		
		'Dim xmlhttp
		'Set xmlhttp = Server.CreateObject("MSXML2.XMLHTTP")
		'xmlhttp.open "GET", ""&url_php&"criar_agendar.php?id_campanha="&ID_maladireta&"&identificador="&identificador&"&assunto="&assunto&"&data_agendada="&FORMATA_DATA(dateadd("d",1,now()),3)&" "&FORMATA_HORA(dateadd("d",1,now()),4)&"" , false
		'xmlhttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		'xmlhttp.Send ""
		'str_resultado = (xmlhttp.responseText)
		'set xmlhttp = nothing
	
	end if
	
END SUB
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' ABRIR UM RECORDSET NO BANCO DE DADOS 
FUNCTION ABRIR_RS( str_sql )
	
	'set ABRIR_RS = conexao.execute( str_sql )
	
	Set ABRIR_RS = Server.CreateObject("ADODB.Recordset")
	ABRIR_RS.CursorLocation = 3
	ABRIR_RS.CursorType = 0
	ABRIR_RS.LockType = 1
	ABRIR_RS.Open str_sql, conexao
	
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' EXIBE FORMULARIO DE UPLOAD COM PROGRESSAO '''''''''
SUB EXIBE_UPLOAD( objeto, pasta, extensoes )
	
	response.Write("<iframe id="""&objeto&"_frame"" src=""../_base/_upload/Upload-Progress.asp?upload_caminho="&pasta&"&upload_nome="&objeto&"&upload_extensoes="&extensoes&""" width=""300px"" frameborder=""0"" scrolling=""no"" height=""40px"" class=""bloco""></iframe>")
	response.Write("<div id="""&objeto&"_aviso"" style=""font-family:arial; font-size:12px; padding:15px 0px 15px 0px; visibility:hidden;"" class=""bloco""></div>")
	response.Write("<input type=""hidden"" name="""&objeto&""" id="""&objeto&"_arquivo"">")
	
END SUB
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' EXCLUI UM ARQUIVO CASO ELE EXISTA '''''''''''''''''
SUB EXCLUIR_ARQUIVO( arquivo )
	
	Set FSO = Server.CreateObject("Scripting.FileSystemObject")
	
	if ARQUIVO_EXISTE( arquivo ) then
		FSO.DeleteFile Server.Mappath( arquivo )
	end if
	
	Set FSO = nothing
	
END SUB
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' EXCLUI UMA PASTA CASO ELA EXISTA '''''''''''''''''
SUB EXCLUIR_PASTA( arquivo )
	
	Set FSO = Server.CreateObject("Scripting.FileSystemObject")
	
	if FSO.FolderExists( Server.MapPath(arquivo) ) then
		FSO.DeleteFolder Server.Mappath( arquivo )
	end if
	
	Set FSO = nothing
	
END SUB
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' EXCLUI UM ARQUIVO CASO ELE EXISTA '''''''''''''''''
FUNCTION ARQUIVO_EXISTE( arquivo )
	
	Set FSO = Server.CreateObject("Scripting.FileSystemObject")
	
	if FSO.FileExists( Server.MapPath(arquivo) ) then
		ARQUIVO_EXISTE = true
	else
		ARQUIVO_EXISTE = false
	end if
	
	Set FSO = nothing
	
END FUNCTION
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' FUN��O PARA FORMATAR IMAGEM ''''''''''''''''''''''''''''''''''''''''''
SUB FORMATA_IMAGEM( caminho, salvar, largura, altura, modo )
	
	if ARQUIVO_EXISTE( caminho ) = true then
	
		Set Jpeg = Server.CreateObject("Persits.Jpeg")
		Path = Server.Mappath(caminho)
		Jpeg.Open Path
	
		
		' esticar desproporcionalmente
		if modo = 1 then
			
			Jpeg.Width 	= largura
			Jpeg.Height = altura
		
		' esticar proporcionalmente
		elseif modo = 2 then
	
			if Jpeg.OriginalWidth > Jpeg.OriginalHeight then
				Jpeg.Width 	= largura
				Jpeg.Height = Jpeg.OriginalHeight * Jpeg.Width / Jpeg.OriginalWidth
			else
				Jpeg.Height = altura
				Jpeg.Width 	= Jpeg.OriginalWidth * Jpeg.Height / Jpeg.OriginalHeight
			end if
			
		' recortar
		elseif modo = 3 then
			
			if Jpeg.OriginalWidth > Jpeg.OriginalHeight then
				Jpeg.Height = altura
				largura_nova= Jpeg.OriginalWidth * Jpeg.Height / Jpeg.OriginalHeight
				Jpeg.Width 	= largura_nova
				
				cortar = ( largura_nova - largura ) / 2
				
				Jpeg.Crop cortar, 0, largura_nova-cortar, altura
			else
				Jpeg.Width 	= largura
				altura_nova = Jpeg.OriginalHeight * Jpeg.Width / Jpeg.OriginalWidth
				Jpeg.Height = altura_nova
				
				cortar = ( altura_nova - altura ) / 2
				
				Jpeg.Crop 0, cortar, largura, altura_nova-cortar
			end if
			
		end if
		
		'Esse m�todo � opcional, usado para melhorar o visual da imagem
		Jpeg.Sharpen 1, 101
		'Cria um thumbnail e o grava no caminho abaixo
		Jpeg.Save Server.Mappath(salvar)
		
	end if

END SUB
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' PADR�O GUID DO WINDOWS '''''''''''''''''''''''''''''''''''''''''''''''
Function CreateWindowsGUID()
	CreateWindowsGUID = CreateGUID(8) & "-" & _
						CreateGUID(4) & "-" & _
						CreateGUID(4) & "-" & _
						CreateGUID(4) & "-" & _
						CreateGUID(12)
End Function

Function CreateGUID(tmpLength)
	Randomize Timer
	Dim tmpCounter,tmpGUID
	Const strValid = "0123456789ABCDEF"
	For tmpCounter = 1 To tmpLength
		tmpGUID = tmpGUID & Mid(strValid, Int(Rnd(1) * Len(strValid)) + 1, 1)
	Next
	CreateGUID = tmpGUID
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para ler uma string com um XML ''
SUB LER_XML( str_xml )

	xmlDoc.async = false
	xmlDoc.loadXML(str_xml)
	
	If xmlDoc.parseError <> 0 Then
	  Response.Write "C�digo do erro: "& xmlDoc.parseError.ErrorCode &"<br />"
	  Response.Write "Posi��o no arquivo: "& xmlDoc.parseError.FilePos &"<br />"
	  Response.Write "Linha: "& xmlDoc.parseError.Line &"<br />"
	  Response.Write "Posi��o na linha: "& xmlDoc.parseError.LinePos &"<br />"
	  Response.Write "Descri��o: "& xmlDoc.parseError.Reason &"<br />"
	  Response.Write "Texto que causa o erro: "& xmlDoc.parseError.srcText &"<br />"
	  Response.Write "Arquivo com problemas: " & xmlDoc.parseError.URL
	end if

END SUB
''''''''''''''''''''''''''''''''''''''''''

'' FUN��O PARA LIBERAR ACESSO DO USU�RIO AO CURSO '''''''''''''''''''''''''''''''
SUB ENVIAR_CONTRATO( ID_pagamento, ID_inscricao )
	
	'' S� enviar contrato se nunca foi enviado
	if ( isnull(ID_pagamento) = false ) then
		sql = "select PAGAMENTOS.ID_inscricao, titulo_curso, ID_pagamento, data_inscricao from PAGAMENTOS inner join CURSOS_INSCRICOES on PAGAMENTOS.ID_inscricao=CURSOS_INSCRICOES.ID_inscricao inner join CURSOS on CURSOS_INSCRICOES.ID_curso=CURSOS.ID_curso where ID_pagamento="&ID_pagamento&" and contrato_inscricao is null"
	else
		sql = "select PAGAMENTOS.ID_inscricao, titulo_curso, ID_pagamento, data_inscricao from PAGAMENTOS inner join CURSOS_INSCRICOES on PAGAMENTOS.ID_inscricao=CURSOS_INSCRICOES.ID_inscricao inner join CURSOS on CURSOS_INSCRICOES.ID_curso=CURSOS.ID_curso where PAGAMENTOS.ID_inscricao="&ID_inscricao&" and contrato_inscricao is null"
	end if
	
	set rs_contrato = conexao.execute(sql)

	if not rs_contrato.EOF then

		if ( isnull(ID_pagamento) = true ) then
			ID_pagamento = rs_contrato("ID_pagamento")
		end if
		
		ID_inscricao = rs_contrato("ID_inscricao")
		titulo_curso = rs_contrato("titulo_curso")
		data_inscricao = rs_contrato("data_inscricao")

		'' S� enviar contratos para inscri��es feitas depois de abril de 2018
		if cdate(data_inscricao) >= cdate("2018-04-01 00:00:00") then
		
			sql = "select nome_usuario, email_usuario from CURSOS_INSCRICOES_USUARIOS inner join USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario=USUARIOS.ID_usuario where ID_inscricao="&ID_inscricao
			set rs_contrato = conexao.execute(sql)
			
			do while not rs_contrato.EOF
				
				mensagem = "<BR />Sr(a) "&rs_contrato("nome_usuario")&"<BR /><BR />Sua inscri��o no curso "&titulo_curso&" foi realizada com sucesso.<BR /><a href='"&website_01&"/../facix2/contrato/contrato.asp?ID="&ID_pagamento&"'>Clique aqui</a> para visualizar e imprimir seu contrato.<BR /><BR />Att,<BR />Methodus Cosultoria<BR /><a href='http://www.methodus.com.br'>www.methodus.com.br</a><BR /><a href='mailto:info@methodus.com.br'>info@methodus.com.br</a>"
				
				call ENVIAR_EMAIL( "info@methodus.com.br", rs_contrato("email_usuario"), "Contrato", MONTA_EMAIL( mensagem ) )
				call ENVIAR_EMAIL( "info@methodus.com.br", "info@methodus.com.br", "Contrato - Curso de "&titulo_curso&" Methodus Consultoria" , MONTA_EMAIL( mensagem ) )
				
			rs_contrato.movenext:loop
			
			sql = "update CURSOS_INSCRICOES set contrato_inscricao='"&DATA_BD(now())&"' where ID_inscricao="&ID_inscricao
			conexao.execute(sql)

		end if

	end if
	
END SUB
''''''''''''''''''''''''''''''''''''''''''

'' FUN��O PARA RETORNAR O SIGNO DE UMA DATA ''''''''''''''''
FUNCTION CALCULAR_SIGNO( data )
	
	if isdate(data) then
		
		data_signo 	= cdate( FORMATA_DATA( data, 1 ) )
		ano_signo	= year( data )
		
		if data_signo >= cdate("21/03/"&ano_signo&"") and data_signo <= cdate("20/04/"&ano_signo&"") then
			signo = "�ries / Fogo / Intui��o / Eu Sou"
			
			if data_signo >= cdate("21/03/"&ano_signo&"") and data_signo <= cdate("24/03/"&ano_signo&"") then
				signo = signo & " / Renascimento"
			elseif data_signo >= cdate("25/03/"&ano_signo&"") and data_signo <= cdate("02/04/"&ano_signo&"") then
				signo = signo & " / Crian�a"
			elseif data_signo >= cdate("3/4/"&ano_signo&"") and data_signo <= cdate("10/4/"&ano_signo&"") then
				signo = signo & " / Estrela"
			elseif data_signo >= cdate("11/4/"&ano_signo&"") and data_signo <= cdate("18/4/"&ano_signo&"") then
				signo = signo & " / Pioneiro"
			elseif data_signo >= cdate("19/4/"&ano_signo&"") and data_signo <= cdate("20/04/"&ano_signo&"") then
				signo = signo & " / Poder"
			end if
			
		elseif data_signo >= cdate("21/04/"&ano_signo&"") and data_signo <= cdate("20/05/"&ano_signo&"") then
			signo = "Touro / Terra / Sensa��o / Eu Tenho"
			
			if data_signo >= cdate("21/04/"&ano_signo&"") and data_signo <= cdate("24/04/"&ano_signo&"") then
				signo = signo & " / Poder"
			elseif data_signo >= cdate("25/04/"&ano_signo&"") and data_signo <= cdate("02/05/"&ano_signo&"") then
				signo = signo & " / Manifesta��o"
			elseif data_signo >= cdate("03/05/"&ano_signo&"") and data_signo <= cdate("10/05/"&ano_signo&"") then
				signo = signo & " / Professor"
			elseif data_signo >= cdate("11/05/"&ano_signo&"") and data_signo <= cdate("18/05/"&ano_signo&"") then
				signo = signo & " / Natural"
			elseif data_signo >= cdate("19/05/"&ano_signo&"") and data_signo <= cdate("20/05/"&ano_signo&"") then
				signo = signo & " / Energia"
			end if			
			
		elseif data_signo >= cdate("21/05/"&ano_signo&"") and data_signo <= cdate("20/06/"&ano_signo&"") then
			signo = "G�meos / Ar / Pensamento / Eu Comunico"
			
			if data_signo >= cdate("21/05/"&ano_signo&"") and data_signo <= cdate("24/05/"&ano_signo&"") then
				signo = signo & " / Energia"
			elseif data_signo >= cdate("25/05/"&ano_signo&"") and data_signo <= cdate("2/06/"&ano_signo&"") then
				signo = signo & " / Liberdade"
			elseif data_signo >= cdate("03/05/"&ano_signo&"") and data_signo <= cdate("10/06/"&ano_signo&"") then
				signo = signo & " / Nova Linguagem"
			elseif data_signo >= cdate("11/06/"&ano_signo&"") and data_signo <= cdate("18/06/"&ano_signo&"") then
				signo = signo & " / Buscador"
			elseif data_signo >= cdate("19/05/"&ano_signo&"") and data_signo <= cdate("21/06/"&ano_signo&"") then
				signo = signo & " / Magia"
			end if						
			
		elseif data_signo >= cdate("21/06/"&ano_signo&"") and data_signo <= cdate("21/07/"&ano_signo&"") then
			signo = "C�ncer / �gua / Sentimento / Eu Sinto"
			
			if data_signo >= cdate("22/06/"&ano_signo&"") and data_signo <= cdate("24/06/"&ano_signo&"") then
				signo = signo & " / Magia"
			elseif data_signo >= cdate("25/06/"&ano_signo&"") and data_signo <= cdate("02/07/"&ano_signo&"") then
				signo = signo & " / Emp�tico"
			elseif data_signo >= cdate("03/07/"&ano_signo&"") and data_signo <= cdate("10/07/"&ano_signo&"") then
				signo = signo & " / N�o-convencional"
			elseif data_signo >= cdate("11/07/"&ano_signo&"") and data_signo <= cdate("18/07/"&ano_signo&"") then
				signo = signo & " / Persuasivo"
			elseif data_signo >= cdate("19/07/"&ano_signo&"") and data_signo <= cdate("22/07/"&ano_signo&"") then
				signo = signo & " / Oscila��o"
			end if			
			
		elseif data_signo >= cdate("22/07/"&ano_signo&"") and data_signo <= cdate("22/08/"&ano_signo&"") then
			signo = "Le�o / Fogo / Intui��o / Eu Crio"
			
			if data_signo >= cdate("23/07/"&ano_signo&"") and data_signo <= cdate("25/07/"&ano_signo&"") then
				signo = signo & " / Oscila��o"
			elseif data_signo >= cdate("26/07/"&ano_signo&"") and data_signo <= cdate("02/08/"&ano_signo&"") then
				signo = signo & " / Autoridade"
			elseif data_signo >= cdate("03/08/"&ano_signo&"") and data_signo <= cdate("10/08/"&ano_signo&"") then
				signo = signo & " / For�a equilibrada"
			elseif data_signo >= cdate("11/08/"&ano_signo&"") and data_signo <= cdate("18/08/"&ano_signo&"") then
				signo = signo & " / Lideran�a"
			elseif data_signo >= cdate("19/08/"&ano_signo&"") and data_signo <= cdate("23/08/"&ano_signo&"") then
				signo = signo & " / Exposi��o"
			end if
			
		elseif data_signo >= cdate("23/08/"&ano_signo&"") and data_signo <= cdate("22/09/"&ano_signo&"") then
			signo = "Virgem / Terra / Sensa��o, Pensamento / Eu Sirvo"
			
			if data_signo >= cdate("24/08/"&ano_signo&"") and data_signo <= cdate("25/09/"&ano_signo&"") then
				signo = signo & " / Exposi��o"
			elseif data_signo >= cdate("26/08/"&ano_signo&"") and data_signo <= cdate("02/09/"&ano_signo&"") then
				signo = signo & " / Construtores de sistemas"
			elseif data_signo >= cdate("03/09/"&ano_signo&"") and data_signo <= cdate("10/09/"&ano_signo&"") then
				signo = signo & " / Enigma"
			elseif data_signo >= cdate("11/09/"&ano_signo&"") and data_signo <= cdate("18/09/"&ano_signo&"") then
				signo = signo & " / O literal"
			elseif data_signo >= cdate("19/09/"&ano_signo&"") and data_signo <= cdate("22/09/"&ano_signo&"") then
				signo = signo & " / Beleza"
			end if			
			
		elseif data_signo >= cdate("23/09/"&ano_signo&"") and data_signo <= cdate("22/10/"&ano_signo&"") then
			signo = "Libra / Ar / Pensamento, Sensa��o / Eu Peso"
			
			if data_signo >= cdate("23/09/"&ano_signo&"") and data_signo <= cdate("24/09/"&ano_signo&"") then
				signo = signo & " / Beleza"
			elseif data_signo >= cdate("25/09/"&ano_signo&"") and data_signo <= cdate("02/10/"&ano_signo&"") then
				signo = signo & " / Perfeccionismo"
			elseif data_signo >= cdate("03/10/"&ano_signo&"") and data_signo <= cdate("10/10/"&ano_signo&"") then
				signo = signo & " / Sociedade"
			elseif data_signo >= cdate("11/10/"&ano_signo&"") and data_signo <= cdate("18/10/"&ano_signo&"") then
				signo = signo & " / Teatro"
			elseif data_signo >= cdate("19/10/"&ano_signo&"") and data_signo <= cdate("22/10/"&ano_signo&"") then
				signo = signo & " / Dramaturgia e cr�tica"
			end if
			
		elseif data_signo >= cdate("23/10/"&ano_signo&"") and data_signo <= cdate("21/11/"&ano_signo&"") then
			signo = "Escorpi�o / �gua / Sentimento / Eu Controlo"
			
			if data_signo >= cdate("23/10/"&ano_signo&"") and data_signo <= cdate("25/10/"&ano_signo&"") then
				signo = signo & " / Drama e cr�tica"
			elseif data_signo >= cdate("26/10/"&ano_signo&"") and data_signo <= cdate("02/10/"&ano_signo&"") then
				signo = signo & " / Intensidade"
			elseif data_signo >= cdate("03/11/"&ano_signo&"") and data_signo <= cdate("11/11/"&ano_signo&"") then
				signo = signo & " / Profundidade"
			elseif data_signo >= cdate("12/11/"&ano_signo&"") and data_signo <= cdate("18/11/"&ano_signo&"") then
				signo = signo & " / Charme"
			elseif data_signo >= cdate("19/11/"&ano_signo&"") and data_signo <= cdate("21/11/"&ano_signo&"") then
				signo = signo & " / Revolu��o"
			end if
			
		elseif data_signo >= cdate("22/11/"&ano_signo&"") and data_signo <= cdate("21/12/"&ano_signo&"") then
			signo = "Sagit�rio / Fogo / Intui��o / Eu Filosofo"
			
			if data_signo >= cdate("22/11/"&ano_signo&"") and data_signo <= cdate("24/11/"&ano_signo&"") then
				signo = signo & " / Revolu��o"
			elseif data_signo >= cdate("25/11/"&ano_signo&"") and data_signo <= cdate("02/12/"&ano_signo&"") then
				signo = signo & " / Independ�ncia"
			elseif data_signo >= cdate("03/12/"&ano_signo&"") and data_signo <= cdate("10/12/"&ano_signo&"") then
				signo = signo & " / Origninador"
			elseif data_signo >= cdate("11/12/"&ano_signo&"") and data_signo <= cdate("18/12/"&ano_signo&"") then
				signo = signo & " / Tit�"
			elseif data_signo >= cdate("19/12/"&ano_signo&"") and data_signo <= cdate("21/12/"&ano_signo&"") then
				signo = signo & " / Profecia"
			end if
			
		elseif ( data_signo >= cdate("22/12/"&ano_signo&"") and data_signo <= cdate("31/12/"&ano_signo&"") ) or ( data_signo >= cdate("01/01/"&ano_signo&"") and data_signo <= cdate("20/01/"&ano_signo&"") ) then
			signo = "Capric�rnio / Terra / Sensa��o / Eu Domino"
			
			if data_signo >= cdate("22/12/"&ano_signo&"") and data_signo <= cdate("25/12/"&ano_signo&"") then
				signo = signo & " / Profecia"
			elseif data_signo >= cdate("26/12/"&ano_signo&"") and data_signo <= cdate("02/01/"&ano_signo&"") then
				signo = signo & " / Governante"
			elseif data_signo >= cdate("03/01/"&ano_signo&"") and data_signo <= cdate("09/01/"&ano_signo&"") then
				signo = signo & " / Determina��o"
			elseif data_signo >= cdate("10/01/"&ano_signo&"") and data_signo <= cdate("16/01/"&ano_signo&"") then
				signo = signo & " / Dom�nio"
			elseif data_signo >= cdate("17/01/"&ano_signo&"") and data_signo <= cdate("20/01/"&ano_signo&"") then
				signo = signo & " / Mist�rio e Imagina��o"
			end if
			
		elseif data_signo >= cdate("21/01/"&ano_signo&"") and data_signo <= cdate("19/02/"&ano_signo&"") then
			signo = "Aqu�rio / Ar / Pensamento / Eu Universalizo"
			
			if data_signo >= cdate("21/01/"&ano_signo&"") and data_signo <= cdate("22/01/"&ano_signo&"") then
				signo = signo & " / Mist�rio e Imagina��o"
			elseif data_signo >= cdate("23/01/"&ano_signo&"") and data_signo <= cdate("30/01/"&ano_signo&"") then
				signo = signo & " / G�nio"
			elseif data_signo >= cdate("31/01/"&ano_signo&"") and data_signo <= cdate("07/2/"&ano_signo&"") then
				signo = signo & " / Juventude e facilidade"
			elseif data_signo >= cdate("08/02/"&ano_signo&"") and data_signo <= cdate("15/02/"&ano_signo&"") then
				signo = signo & " / Aceita��o"
			elseif data_signo >= cdate("16/02/"&ano_signo&"") and data_signo <= cdate("19/02/"&ano_signo&"") then
				signo = signo & " / Sensibilidade"
			end if						
			
		elseif data_signo >= cdate("20/02/"&ano_signo&"") and data_signo <= cdate("20/03/"&ano_signo&"") then
			signo = "Peixes / �gua / Sentimento / Eu Acredito"
			
			if data_signo >= cdate("20/02/"&ano_signo&"") and data_signo <= cdate("22/02/"&ano_signo&"") then
				signo = signo & " / Sensibilidade"
			elseif data_signo >= cdate("23/02/"&ano_signo&"") and data_signo <= cdate("02/03/"&ano_signo&"") then
				signo = signo & " / Esp�rito"
			elseif data_signo >= cdate("03/03/"&ano_signo&"") and data_signo <= cdate("10/03/"&ano_signo&"") then
				signo = signo & " / Solid�o"
			elseif data_signo >= cdate("11/03/"&ano_signo&"") and data_signo <= cdate("18/03/"&ano_signo&"") then
				signo = signo & " / Dan�arinos e sonhadores"
			elseif data_signo >= cdate("19/03/"&ano_signo&"") and data_signo <= cdate("20/03/"&ano_signo&"") then
				signo = signo & " / Renascimento"
			end if
			
		end if
	
		CALCULAR_SIGNO = signo
	
	else
		CALCULAR_SIGNO = ""
	end if
	
END FUNCTION


'' UF8 ENCODE E DECODE ''''''''''''''''''''''''''''''''''''''''
function EncodeUTF8(s)
	dim i
	dim c
	
	i = 1
	do while i <= len(s)
		c = asc(mid(s,i,1))
		if c >= &H80 then
			s = left(s,i-1) + chr(&HC2 + ((c and &H40) / &H40)) + chr(c and &HBF) + mid(s,i+1)
			i = i + 1
		end if
		i = i + 1
	loop
	EncodeUTF8 = s 
end function

function DecodeUTF8(s)
	dim i
	dim c
	dim n
	
	i = 1
	do while i <= len(s)
		c = asc(mid(s,i,1))
		if c and &H80 then
			n = 1
			do while i + n < len(s)
				if (asc(mid(s,i+n,1)) and &HC0) <> &H80 then
					exit do
				end if
				n = n + 1
			loop
			if n = 2 and ((c and &HE0) = &HC0) then
				c = asc(mid(s,i+1,1)) + &H40 * (c and &H01)
			else
				c = 191 
			end if
			s = left(s,i-1) + chr(c) + mid(s,i+n)
		end if
		i = i + 1
	loop
	DecodeUTF8 = s 
end function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' FUN��O PARA TRANSFORMAR TAGS EM ENTIDADE HTML PARA TAG REAL ''
FUNCTION TRANSFORMA_TASG(texto)
	TRANSFORMA_TASG = replace( replace( texto&"", "&gt;", ">" ), "&lt;", "<" )
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' FUN��O PARA RESGATAR AS TURMAS DO INSTUDO E GRAVA-LAS EM UM XML ''
FUNCTION RESGATAR_TURMAS_INSTUDO()
	
	caminho_xml = "../_xml/turmas_instudo.xml"
	
	'' verificar se arquivo de cache existe e se � mais recente que 6 horas
	atualizar_xml = false
	if ARQUIVO_EXISTE( caminho_xml ) then
		'' verificar a data que o arquivo foi modificado ''
		Set FSO = Server.CreateObject("Scripting.FileSystemObject")
		Set xml_instudo = FSO.GetFile(Server.MapPath(caminho_xml))
		data_ultima_alteracao = DATA_BD(xml_instudo.DateLastModified)
		
		if isdate(data_ultima_alteracao) then
			if datediff("h",cdate(data_ultima_alteracao),cdate(now())) >= 6 then
				atualizar_xml = true
			else
				'' ultima atualiza��o do XML em menos de 6 horas
				atualizar_xml = false
			end if
		else
			atualizar_xml = true
		end if
		
	else
		atualizar_xml = true
	end if
	
	
	' se deve atualizar o XML
	if atualizar_xml = true then
		
		dim ws
		set ws = new webservice
		ws.url = url_ws_instudo
	
		ws.XML=	"<?xml version=""1.0"" encoding=""utf-8""?>"&_
				"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"&_
				"  <soap:Body>"&_
				"	<lista_turmas xmlns="""&url_ws_instudo&""">"&_
				"		<usuario>"&login_ws_instudo&"</usuario>"&_
				"		<senha>"&senha_ws_instudo&"</senha>"&_
				"	</lista_turmas>"&_
				"  </soap:Body>"&_
				"</soap:Envelope>"
		
		on error resume next
		ws.Invoke( url_ws_instudo&"listar_turmas" )
		
		str_xml = ws.response
		set ws = nothing
		
		on error goto 0
		
		if not str_xml = empty then
			
			'response.Write(str_xml)
			
			'' ler o xml de retorno
			call LER_XML( str_xml )
			set registros 			= xmlDoc.documentElement
			
			Set retorno_erro 		= xmlDoc.getElementsByTagName("ns1:erro")
			retorno_erro 			= retorno_erro(0).text
			
			Set retorno_msg 		= xmlDoc.getElementsByTagName("ns1:msg")
			retorno_msg 			= retorno_msg(0).text
			
			if retorno_erro = "0" then
				
				Set retorno_xml 		= xmlDoc.getElementsByTagName("ns1:xml")
				retorno_xml 			= retorno_xml(0).text
				
				xml_turmas = EncodeUTF8(retorno_xml)
				xml_turmas = TRANSFORMA_TASG( xml_turmas )
				
				
				''  GRAVAR EM XML ''
				Set FSO = Server.CreateObject("Scripting.FileSystemObject")
				caminho = Server.MapPath(caminho_xml)
				Set gravar = FSO.CreateTextFile(caminho,true)
				
				gravar.write (xml_turmas)
				gravar.close
				
				RESGATAR_TURMAS_INSTUDO = "1"
			
			else
				RESGATAR_TURMAS_INSTUDO = retorno_msg
			end if
		
		else
			RESGATAR_TURMAS_INSTUDO = "A conex�o com o Instudo falhou"
		end if
	
	else
		RESGATAR_TURMAS_INSTUDO = "1"
	end if
	

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' EXECUTA P�GINA E RETORNA RESULTADO DA TELA EM VARI�VEL '''''''''''''''''''''''
FUNCTION PROCESSA_PAGINA( pagina, metodo )
	
	Dim xmlhttp
	Set xmlhttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
	
	if metodo = "GET" then
		xmlhttp.Open metodo, pagina , False
		xmlhttp.setRequestHeader "Content-Type", "text/html; charset=iso-8859-1"	
		xmlhttp.Send
	else
		
		posicao_interrogacao = instr(pagina,"?")
		
		variaveis_post 	= right( pagina, (len(pagina) - posicao_interrogacao) )
		pagina 			= left( pagina, posicao_interrogacao-1 )
		
		xmlhttp.Open metodo, pagina , False
		
		xmlhttp.setRequestHeader "Content-type", "application/x-www-form-urlencoded; charset=iso-8859-1"
 		xmlhttp.setRequestHeader "Content-length", len(variaveis_post)
		
		xmlhttp.Send(variaveis_post)
		
	end if
	
	PROCESSA_PAGINA =  xmlhttp.responseText
	
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' FUN��O PARA FAZER UMA MATR�CULA DE UMA INSCRI��O NO INSTUDO ''
FUNCTION MATRICULAR_INSTUDO( ID_inscricao )
	
	if isnull(ID_inscricao)=false then
		
		dim ws
		
		sql = 	"select CURSOS_INSCRICOES.ID_inscricao, ID_inscricao_usuario, CURSOS_DATAS.ID_turma_instudo, CURSOS_INSCRICOES_USUARIOS.ID_usuario, nome_usuario, email_usuario, cpf_usuario, nascimento_usuario, ddd_tel_usuario, tel_usuario, "&_
				"ddd_cel_usuario, cel_usuario, cidade_usuario, estado_usuario, USUARIOS.ID_instudo as ID_usuario_instudo, CURSOS_INSCRICOES_USUARIOS.ID_instudo as ID_matricula_instudo "&_
				"from CURSOS_INSCRICOES inner join CURSOS_INSCRICOES_USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_inscricao = CURSOS_INSCRICOES.ID_inscricao inner join USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario = USUARIOS.ID_usuario "&_
				"inner join CURSOS_DATAS on CURSOS_INSCRICOES.ID_data = CURSOS_DATAS.ID_data where CURSOS_INSCRICOES_USUARIOS.ID_inscricao="&ID_inscricao&" and ID_turma_instudo is not null"
		set rs_instudo = ABRIR_RS(sql)
		
		if not rs_instudo.EOF then
			
			'' LOG
			call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "Etapa 3.2: Curso tem rela��o com Instudo, ID_inscricao: "&ID_inscricao  )
			
			do while not rs_instudo.EOF
			
				ID_inscricao			= rs_instudo("ID_inscricao")
				ID_inscricao_usuario	= rs_instudo("ID_inscricao_usuario")
				ID_turma_instudo		= rs_instudo("ID_turma_instudo")
				ID_usuario				= rs_instudo("ID_usuario")
				nome_usuario			= rs_instudo("nome_usuario")
				email_usuario 			= rs_instudo("email_usuario")
				cpf_usuario 			= rs_instudo("cpf_usuario")
				if isnull(rs_instudo("nascimento_usuario"))=false then
					nascimento_usuario 		= FORMATA_DATA( rs_instudo("nascimento_usuario"), 3 )
				end if
				ddd_tel_usuario 		= rs_instudo("ddd_tel_usuario")
				tel_usuario 			= left(rs_instudo("tel_usuario")&"",9)
				ddd_cel_usuario 		= rs_instudo("ddd_cel_usuario")
				cel_usuario	 			= left(rs_instudo("cel_usuario")&"",9)
				cidade_usuario 			= rs_instudo("cidade_usuario")
				estado_usuario 			= rs_instudo("estado_usuario")
				ID_usuario_instudo 		= rs_instudo("ID_usuario_instudo")
				ID_instudo_matricula 	= rs_instudo("ID_matricula_instudo")
				
				'' tratar vari�veis ''
				if (isnull(cel_usuario)=false and not cel_usuario=empty) and (isnull(ddd_cel_usuario)=false and not ddd_cel_usuario=empty) then
					ddd_tel_usuario 	= ddd_cel_usuario
					tel_usuario 		= cel_usuario
				end if
				''''''''''''''''''''''
				
				'' se aluno ainda n�o existe no Instudo, cria-lo
				if isnull(ID_usuario_instudo) then
					
					set ws = new webservice
					ws.url = url_ws_instudo
				
					str_xml="<?xml version=""1.0"" encoding=""utf-8""?>"&_
							"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"&_
							"  <soap:Body>"&_
							"	<cadastro_aluno xmlns="""&url_ws_instudo&""">"&_
							"		<usuario>"&login_ws_instudo&"</usuario>"&_
							"		<senha>"&senha_ws_instudo&"</senha>"&_
							"		<nome_aluno>"&nome_usuario&"</nome_aluno>"&_
							"		<email_aluno>"&email_usuario&"</email_aluno>"&_
							"		<senha_aluno>"&cpf_usuario&"</senha_aluno>"&_
							"		<nascimento>"&nascimento_usuario&"</nascimento>"&_
							"		<ddd_telefone>"&ddd_tel_usuario&"</ddd_telefone>"&_
							"		<telefone>"&tel_usuario&"</telefone>"&_
							"		<cidade>"&cidade_usuario&"</cidade>"&_
							"		<estado>"&estado_usuario&"</estado>"&_
							"	</cadastro_aluno>"&_
							"  </soap:Body>"&_
							"</soap:Envelope>"
					
					'' LOG
					'call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "Etapa 3.3: Cadastrar aluno: "&str_xml  )
					
					ws.XML = str_xml
					ws.Invoke( url_ws_instudo&"cadastrar_aluno" )
					
					str_xml = ws.response
					set ws = nothing
					
					'' ler o xml de retorno
					call LER_XML( str_xml )
					set registros 			= xmlDoc.documentElement
					
					Set retorno_erro 		= xmlDoc.getElementsByTagName("ns1:erro")
					retorno_erro 			= retorno_erro(0).text
					
					Set retorno_msg 		= xmlDoc.getElementsByTagName("ns1:msg")
					retorno_msg 			= retorno_msg(0).text
					
					if retorno_erro = "0" then
						
						Set retorno_ID_aluno 	= xmlDoc.getElementsByTagName("ns1:ID_aluno")
						ID_usuario_instudo 		= retorno_ID_aluno(0).text			
						
						'' atualizar cadastro do aluno com c�digo do instudo
						sql = "update USUARIOS set ID_instudo="&ID_usuario_instudo&" where ID_usuario="&ID_usuario
						conexao.execute(sql)
						
					end if
					
				end if
				
				
				
				'' se ainda n�o tem o ID do aluno no Instudo, deu erro ao cria-lo via webservice
				if isnull(ID_usuario_instudo) then
					MATRICULAR_INSTUDO = "Erro ao criar o aluno no Instudo (ERRO:"&retorno_erro&" -> "&retorno_msg&")"
					exit do
					
				elseif isnull(ID_instudo_matricula)=false then
					'MATRICULAR_INSTUDO = "Essa inscri��o j� tem uma matr�cula no Instudo atrelada (ID no Instudo:"&ID_instudo_matricula&")"
				else
				
					set ws = new webservice
					ws.url = url_ws_instudo
					
					str_xml	=	"<?xml version=""1.0"" encoding=""utf-8""?>"&_
								"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"&_
								"  <soap:Body>"&_
								"	<matricula_aluno xmlns="""&url_ws_instudo&""">"&_
								"		<usuario>"&login_ws_instudo&"</usuario>"&_
								"		<senha>"&senha_ws_instudo&"</senha>"&_
								"		<ID_aluno>"&ID_usuario_instudo&"</ID_aluno>"&_
								"		<ID_turma>"&ID_turma_instudo&"</ID_turma>"&_
								"		<ID_loja>"&ID_inscricao_usuario&"</ID_loja>"&_
								"	</matricula_aluno>"&_
								"  </soap:Body>"&_
								"</soap:Envelope>"
					
					'' LOG
					'call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "Etapa 3.4: Cadastrar matr�cula: "&str_xml  )
							
					ws.XML = str_xml
					ws.Invoke( url_ws_instudo&"matricular_aluno" )
					
					str_xml = ws.response
					set ws = nothing
					
					'' LOG
					'call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "Etapa 3.5: XML retornado: "&str_xml  )
								
					'' ler o xml de retorno
					call LER_XML( str_xml )
					set registros 			= xmlDoc.documentElement
					
					Set retorno_erro 		= xmlDoc.getElementsByTagName("ns1:erro")
					retorno_erro 			= retorno_erro(0).text
					
					Set retorno_msg 		= xmlDoc.getElementsByTagName("ns1:msg")
					retorno_msg 			= retorno_msg(0).text
					
					if retorno_erro = "0" then
						
						Set retorno_ID_matricula 	= xmlDoc.getElementsByTagName("ns1:ID_matricula")
						retorno_ID_matricula 		= retorno_ID_matricula(0).text			
						
						'' LOG
						call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "Etapa 3.6: Retorno da matr�cula: "&retorno_ID_matricula&" / ID_inscricao: "&ID_inscricao_usuario  )
						
						'' atualizar cadastro do aluno com c�digo do instudo
						sql = "update CURSOS_INSCRICOES_USUARIOS set ID_instudo="&retorno_ID_matricula&" where ID_inscricao_usuario="&ID_inscricao_usuario
						
						'' LOG
						call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "Etapa 3.7: UPDATE: "&sql  )
						
						conexao.execute(sql)
							
						MATRICULAR_INSTUDO = "1"
					
					else
						
						'' LOG
						call CRIAR_LOG( "log/LOG_ipagare_"&FORMATA_DATA(now(),3)&".txt", "Etapa 3.6: ERRO no Retorno da matr�cula: "&retorno_erro&" -> "&retorno_msg  )
						
						MATRICULAR_INSTUDO = "Erro ao criar matr�cula no Instudo (ERRO:"&retorno_erro&" -> "&retorno_msg&")"
						exit do
					end if		
				
				end if
				
				
			rs_instudo.movenext:loop
			
		else
			MATRICULAR_INSTUDO = "Essa inscri��o refere-se a um curso/data que n�o tem rela��es com o Instudo"
		end if
	
	end if
	
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' CRIAR LOG EM TXT '''''''''''''''''''''''''''''''''''''''''''''''''''''
SUB CRIAR_LOG( caminho, texto )
	
	Set log_FSO = Server.CreateObject("Scripting.FileSystemObject")
	
	on error resume next
	if ARQUIVO_EXISTE( caminho ) = false then
		caminho = Server.Mappath(caminho)
		Set GRAVAR = log_FSO.CreateTextFile(caminho,true)
		GRAVAR.write ("################################################################"&vbcrlf&vbcrlf)
		GRAVAR.close
	else
		caminho = Server.Mappath(caminho)
	end if
	
	strLOG = strLOG & "################################################################"&vbcrlf
	strLOG = strLOG & "## "&DATA_BD( now() )&" / IP: "&request.ServerVariables("REMOTE_ADDR")&vbcrlf
	strLOG = strLOG & texto &vbcrlf
	strLOG = strLOG & "################################################################"&vbcrlf&vbcrlf
	
	Set TXT = log_FSO.OpenTextFile(caminho)
	atual_LOG = TXT.readALL & strLOG
	TXT.close
	Set GRAVAR_LOG = log_FSO.CreateTextFile(caminho,true)
	GRAVAR_LOG.write (atual_LOG)
	GRAVAR_LOG.close
	on error goto 0
	
END SUB
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' CRIAR LOG EM TXT '''''''''''''''''''''''''''''''''''''''''''''''''''''
SUB LOG_UTILIZACAO( ID_usuario, texto )
	
	sql = "insert into LOG_UTILIZACAO (ID_usuario, data_log, descricao_log) values ("&ID_usuario&",'"&DATA_BD( now() )&"','"&texto&"')"
	conexao.execute(sql)
	
END SUB
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' ALOCAR CONTROLE DE QUIZ PARA ALUNO NA TURMA  '''''''''''''''''''''''''
FUNCTION ALOCAR_CONTROLE( ID_usuario, ID_data )
	
	'' verificar se j� existe controle alocado para ele
	sql = "select top 1 ID_controle from QUIZ_CONTROLES_RELACOES where ID_data="&ID_data&" and ID_usuario="&ID_usuario&""
	set rs_controle = ABRIR_RS(sql)
	
	if not rs_controle.EOF then
		ID_controle = rs_controle("ID_controle")
		
		ALOCAR_CONTROLE = ID_controle
	else
	
		sql = "select top 1 ID_controle from QUIZ_CONTROLES where not exists (select 1 from QUIZ_CONTROLES_RELACOES where ID_data="&ID_data&" and QUIZ_CONTROLES.ID_controle=QUIZ_CONTROLES_RELACOES.ID_controle) order by ID_controle asc"
		set rs_controle = ABRIR_RS(sql)
		
		if not rs_controle.EOF then
			
			ID_controle = rs_controle("ID_controle")
			
			sql = "insert into QUIZ_CONTROLES_RELACOES (ID_controle, ID_data, ID_usuario) values ("&ID_controle&","&ID_data&","&ID_usuario&")"
			conexao.execute(sql)
			
			ALOCAR_CONTROLE = ID_controle	
			
		else
			ALOCAR_CONTROLE = 0	
		end if
	
	end if
	
END FUNCTION

FUNCTION DESALOCAR_CONTROLE( ID_usuario, ID_data )
	
	sql = "delete from QUIZ_CONTROLES_RELACOES where ID_data="&ID_data&" and ID_usuario="&ID_usuario&""
	conexao.execute(sql)
	
	DESALOCAR_CONTROLE = 1	
	
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%>