//#########################################################//
//                     SCRIPTS GERAIS                      //
//#########################################################//


///////////// CONFIRMA��O DE A��O QUALQUER ////////////////
function confirmar( pagina, texto )
{
	if (texto == "0")
	{
	
		if (confirm("Tem certeza que deseja excluir esse Registro?\nEssa opera��o n�o pode ser revertida."))
			window.document.location=pagina;
		else
			return (false);
	}
	else
	{
		if (confirm(texto))
			window.document.location=pagina;
		else
			return (false);
	}
}
//////////////////////////////////////////////////////////////////

///////////// CONFIRMA��O DE CANCELAMENTO DE A��O ////////////////

function confirmar_cancelar()
{
		if (confirm("Tem certeza que deseja fechar essa janela?\nDados n�o salvos ser�o descartados."))
		{
			top.document.getElementById('i_atualiza').src = '../_base/layout_dialogo_carregar.asp';
			
			top.document.getElementById('div_fundo').style.visibility		= 'hidden';
			top.document.getElementById('div_atualiza').style.visibility	= 'hidden';
			//top.i_atualiza.location				= '../_base/layout_dialogo_carregar.asp';
			
			top.document.body.style.overflow	= '';
		}
		else
			return (false);
}
//////////////////////////////////////////////////////////////////

////////////////// ABRIR TELA DE ATUALIZA��O /////////////////////

function exibir_acao( pagina )
{
	document.getElementById('div_fundo').style.visibility		= 'visible';
	//parent.i_atualiza.location		= pagina;
	//parent.frames['i_atualiza']. location.href = pagina;
	document.getElementById('i_atualiza').src = pagina;
	document.getElementById('div_atualiza').style.visibility	= 'visible';
	
	document.body.style.overflow	= 'hidden';
	
	
	cookie_janela = LerCookie('facix_janela');
	//alert(cookie_janela);

	if ( cookie_janela == "max")
	{
		janela_condicao = 0;
		document.getElementById('div_atualiza').style.width = document.body.clientWidth;
		document.getElementById('div_atualiza').style.height = document.body.clientHeight;
	}else{
		janela_condicao = 1;
		document.getElementById('div_atualiza').style.width = 657;
		document.getElementById('div_atualiza').style.height = 428;
	}
		
}
//////////////////////////////////////////////////////////////////

//////////////////// ABRIR TELA DE PESQUISA //////////////////////

function abrir_pesquisa( condicao )
{
	if (condicao) // Para Abrir
	{
		document.getElementById('tr_menu').style.display='none';
		document.getElementById('tr_pesquisa01').style.display='block';
		document.getElementById('tr_pesquisa02').style.display='block';
	}
	else // Para Fechar
	{
		document.getElementById('tr_menu').style.display='block';
		document.getElementById('tr_pesquisa01').style.display='none';
		document.getElementById('tr_pesquisa02').style.display='none';
	}
}
//////////////////////////////////////////////////////////////////

////////////////////// ABRE JANELA QUALQUER //////////////////////
var win= null;
function janela(pagina,w,h,scroll,dimensao)
{
  var winl = ((screen.width-w)/2);
  var wint = ((screen.height-h)/2);
  var settings  ='height='+h+',';
      settings +='width='+w+',';
      settings +='top='+wint+',';
      settings +='left='+winl+',';
      settings +='scrollbars='+scroll+',';
      settings +='resizable='+dimensao+', status=no';
  win=window.open(pagina,"_blank",settings);
  if(parseInt(navigator.appVersion) >= 4){win.window.focus();}
}
//////////////////////////////////////////////////////////////////

//#########################################################//
//            SCRIPTS PARA VALIDA��O DE FORMS              //
//#########################################################//

////////////// FUN��O QUE CHECA VALORES NULOS ////////////////////
function checa_nulo(campo,n_campo)
{
	if (campo.value.length <= 0)
	{
		alerta= "O campo:\n";
		alerta+= "- "+n_campo+"\n";
		alerta+= "� de preenchimento obrigat�rio!";
		alert(alerta);
		if (campo.type != "hidden")
		{
			campo.focus();
		}
		return(false);
	}else{
		return(true);
	}	
}

///////////// FUN��O QUE CHECA TAMANHO DOS CAMPOS ///////////////
function checa_tamanho(campo,n_campo,tamanho)
{
	if (campo.value.length < tamanho)
	{
		alerta= "O campo:\n";
		alerta+= "- "+n_campo+"\n";
		alerta+= "deve ter no m�nimo "+tamanho+" caracteres!";
		alert(alerta);
		if (campo.type != "hidden")
		{
			campo.focus();
		}
		return(false);
	}else{
		return(true);
	}	
}

///////////// FUN��O QUE CHECA CARACTERES ///////////////
function checa_caracter(campo,n_campo,caracteres)
{
	var ver_numero = caracteres;
	var sk15 = campo.value;
	var invalido = true;
	for (i = 0;  i < sk15.length;  i++){
		ch = sk15.charAt(i);
		for (j = 0;  j < ver_numero.length;  j++)
		if (ch == ver_numero.charAt(j))
			break;
		if (j == ver_numero.length){
			invalido = false;
			break;
		}
	}
	if (!invalido){
		alerta= "O campo:\n";
		alerta+= "- "+n_campo+"\n";
		alerta+= "possui caracteres inv�lidos";
		alert(alerta);
		if (campo.type != "hidden")
		{
			campo.focus();
		}
		return(false);
	}else{
		return(true);
	}
}

///////////// FUN��O QUE CHECA VALIDADE DE E-MAIL ///////////////
function checa_email(campo,n_campo)
{
	//regexp_email = /^[a-z0-9\-](\.?\w)*(\-?\w)*@[a-z0-9\-]+(\.[a-z0-9]+)*(\.[a-z0-9]{2,4})$/i;
	
	regexp_email =  /^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/;
	
	if (!regexp_email.test(campo.value))
	{
        alerta= "O campo:\n";
		alerta+= "- "+n_campo+"\n";
		alerta+= "N�o foi digitado corretamente";
		alert(alerta);
		campo.focus();
		return(false);
	}else{
		return(true);
	}
}
/////////////////////////////////////////////////////////////

/////// FUN��O QUE CHECA PREENCHIMENTO DE CAMPOS RADIO E CHECKOX //////////
function checa_opcoes(campo,n_campo)
{
	var objeto 		= campo;
	var controle 	= 0;
	var qtd_itens 	= objeto.length;
	
	if (qtd_itens == undefined)
		qtd_itens = 1;
	
	if (qtd_itens == 1)
	{
		if (objeto.checked == true)
		{
			controle++;
		}
	}else{
		for (i=0; i < qtd_itens; i++)
		{
			if (objeto[i].checked == true)
			{
				controle++;
				break;
			}
		}
	}
	
	if (controle == 0)
	{
		alert("Escolha alguma op��o de "+n_campo+".");
		return(false);
	}else{
		return(true);
	}
}
/////////////////////////////////////////////////////////////

////// AUTO TAB - MUDA DE CAMPO DEPOIS DE DIGITAR ///////////
var isNN = (navigator.appName.indexOf("Netscape")!=-1);

function autoTab(input,len, e) {
	var keyCode = (isNN) ? e.which : e.keyCode; 
	var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
	if(input.value.length >= len && !containsElement(filter,keyCode)) {
		input.value = input.value.slice(0, len);
		input.form[(getIndex(input)+1) % input.form.length].focus();
	}
	function containsElement(arr, ele) {
		var found = false, index = 0;
		while(!found && index < arr.length)
			if(arr[index] == ele)
				found = true;
			else
				index++;
		return found;
	}
	function getIndex(input) {
		var index = -1, i = 0, found = false;
		while (i < input.form.length && index == -1)
			if (input.form[i] == input)index = i;
			else i++;
		return index;
	}
	return true;
}
/////////////////////////////////////////////////////////////

//////////////// ABRE CAIXAS DE DI�LOGO /////////////////////
function caixa_dialogo( pagina, altura, largura, rolagem, tipo )
{
	if (tipo == 0)
	{
		caixa = "window.showModalDialog('"+pagina+"', '', 'Resizable:no; DialogHeight:"+altura+"px; DialogWidth:"+largura+"px; Edge:raised; Help:no; Scroll:"+rolagem+"; Status:no; Center:yes;');";
	}
	else
	{
		caixa = "window.showModelessDialog('"+pagina+"', '', 'Resizable:no; DialogHeight:"+altura+"px; DialogWidth:"+largura+"px; Edge:raised; Help:no; Scroll:"+rolagem+"; Status:no; Center:yes;');";
	}
	
	eval(caixa);
}
/////////////////////////////////////////////////////////////

///////// POSICIONA LAYER DE ATUALIZA��O NO CENTRO //////////
function posicionaLayer()
{
	nome_div = "div_atualiza";
	with(document.getElementById(nome_div).style) 
	{
		/*vLeft = ((document.body.clientWidth - parseInt(width)) / 2);
		vTop = (( document.body.clientHeight - parseInt(height)) / 2);
		//vTop = ((document.body.clientHeight - parseInt(height)) / 2);
		position = 'absolute';
		left = vLeft;
		top = vTop;
		
		posLeft = document.body.scrollLeft + vLeft;
		posTop = document.body.scrollTop + vTop;*/
		
		position = 'absolute';
		
		cookie_janela = LerCookie('facix_janela');
	
		if ( cookie_janela == "max")
		{
			left = "0px";
			top = document.body.scrollTop+"px";
			
		}else{
			left = (((document.body.clientWidth) / 2)-328) + "px";
			top = (document.body.scrollTop+((window.innerHeight / 2 )-214))+"px";
		}
	}
	
	//nome_div = "div_fundo";
	//with(document.getElementById(nome_div).style) 
	//{
		/*width = document.body.clientWidth;
		height = document.body.clientHeight;
		position = 'absolute';
		
		posLeft = document.body.scrollLeft;
		posTop = document.body.scrollTop;*/
		
		//width = "100%"; //document.body.clientWidth;
		//height = document.getElementById("tabela_geral").clientHeight+"px";
		//position = 'absolute';
	//}
	
	setTimeout("posicionaLayer()",100);
}
/////////////////////////////////////////////////////////////

///////// MAXIMIZAR //////////
function maximizar()
{
	if (janela_condicao == 0)
	{
		document.getElementById('div_atualiza').style.width = document.body.clientWidth;
		document.getElementById('div_atualiza').style.height = document.body.clientHeight;
		GerarCookie('facix_janela', 'max', 1);
	}
	else
	{
		document.getElementById('div_atualiza').style.width = 657;
		document.getElementById('div_atualiza').style.height = 428;
		GerarCookie('facix_janela', 'min', 1);
	}
	setTimeout("maximizar()",100);
}
/////////////////////////////////////////////////////////////

//////////////////// VALIDA CPF /////////////////////////////
function checa_cpf( strcpf ) {
	
	numcpf=strcpf.value;
	
	numcpf= numcpf.replace(".", "");
	numcpf= numcpf.replace(".", "");
	numcpf= numcpf.replace("-", "");
	
	x = 0;
	soma = 0;
	dig1 = 0;
	dig2 = 0;
	texto = "";
	numcpf1="";
	len = numcpf.length; x = len -1;
	// var numcpf = "12345678909";
	for (var i=0; i <= len - 3; i++) {
		y = numcpf.substring(i,i+1);
		soma = soma + ( y * x);
		x = x - 1;
		texto = texto + y;
	}
	dig1 = 11 - (soma % 11);
	if (dig1 == 10) dig1=0 ;
	if (dig1 == 11) dig1=0 ;
	numcpf1 = numcpf.substring(0,len - 2) + dig1 ;
	x = 11; soma=0;
	for (var i=0; i <= len - 2; i++) {
		soma = soma + (numcpf1.substring(i,i+1) * x);
		x = x - 1;
	}
	dig2= 11 - (soma % 11);
	if (dig2 == 10) dig2=0;
	if (dig2 == 11) dig2=0;
	//alert ("Digito Verificador : " + dig1 + "" + dig2);
	if ((dig1 + "" + dig2) == numcpf.substring(len,len-2)) {
		return (true);
	}
	else
	{
		alert ("N�mero do CPF invalido!");
		strcpf.focus();
		return (false);
	}
			
}
/////////////////////////////////////////////////////////////

////////// MOSTRA UM LAYER NA POSI��O DESEJADA //////////////
function mostra_div(obj, camada) {

	var k = event.keyCode;
	var T = findPosY(obj); //top
	var L = findPosX(obj); //left
	var list = document.getElementById(camada);
	
	list.style.top=(T+obj.offsetHeight);
	list.style.left=L;
	list.style.display='block';

}
/////////////////////////////////////////////////////////////

////////// ESCONDE UM LAYER NA POSI��O DESEJADA /////////////
function fecha_div(obj, camada) {
	var list = document.getElementById(camada);
	list.style.display='none';
}
/////////////////////////////////////////////////////////////

////////////// ENCONTRA POSI��O X DE UM OBJETO //////////////
function findPosX(obj){
	var curleft=0;
	if(obj.offsetParent) {
	
		while(obj.offsetParent){
			curleft+=obj.offsetLeft
			obj=obj.offsetParent;
		}
	} else if(obj.x)
		curleft+=obj.x;
		return curleft;
}
/////////////////////////////////////////////////////////////

////////////// ENCONTRA POSI��O Y DE UM OBJETO //////////////
function findPosY(obj){
	var curtop=0;
	if(obj.offsetParent){
		while(obj.offsetParent){
			curtop+=obj.offsetTop
			obj=obj.offsetParent;
		}
	} else if(obj.y)
		curtop+=obj.y;
		return curtop;
}
/////////////////////////////////////////////////////////////

///////////////// MOUSE OVER - MUDA DE COR //////////////////
function mouse_over(src,clrOver) 
{
    //if (!src.contains(event.fromElement)) {
	//  src.bgColor = clrOver;
	//}
	src.bgColor = clrOver;
}
/////////////////////////////////////////////////////////////

///////////////// MOUSE OUT - MUDA DE COR ///////////////////
function mouse_out(src,clrIn) 
{
	//if (!src.contains(event.toElement)) {
	//  src.bgColor = clrIn;
	//}
	
	src.bgColor = clrIn;
}
/////////////////////////////////////////////////////////////


////////////////// CRIA CONTROLE DE V�DEO ///////////////////
function criar_controle_video(arquivo,largura,altura)
{
document.write('<OBJECT id="WinMedia" codeBase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701" type="application/x-oleobject" width="'+ largura +'" height="'+ altura +'" standby="Carregando..." classid="CLSID:6BF52A52-394A-11D3-B153-00C04F79FAA6">');
document.write('<PARAM NAME="URL" VALUE="'+ arquivo +'">');
document.write('<PARAM NAME="rate" VALUE="1">');
document.write('<PARAM NAME="balance" VALUE="0">');
document.write('<PARAM NAME="currentPosition" VALUE="0">');
document.write('<PARAM NAME="defaultFrame" VALUE="">');
document.write('<PARAM NAME="playCount" VALUE="1">');
document.write('<PARAM NAME="autoStart" VALUE="-1">');
document.write('<PARAM NAME="currentMarker" VALUE="0">');
document.write('<PARAM NAME="invokeURLs" VALUE="-1">');
document.write('<PARAM NAME="baseURL" VALUE="">');
document.write('<PARAM NAME="volume" VALUE="50">');
document.write('<PARAM NAME="mute" VALUE="0">');
document.write('<PARAM NAME="uiMode" VALUE="full">');
document.write('<PARAM NAME="stretchToFit" VALUE="0">');
document.write('<PARAM NAME="windowlessVideo" VALUE="0">');
document.write('<PARAM NAME="enabled" VALUE="-1">');
document.write('<PARAM NAME="enableContextMenu" VALUE="0">');
document.write('<PARAM NAME="fullScreen" VALUE="0">');
document.write('<PARAM NAME="SAMIStyle" VALUE="">');
document.write('<PARAM NAME="SAMILang" VALUE="">');
document.write('<PARAM NAME="SAMIFilename" VALUE="">');
document.write('<PARAM NAME="captioningID" VALUE="">');
document.write('<PARAM NAME="enableErrorDialogs" VALUE="0">');
document.write('<PARAM NAME="_cx" VALUE="8467">');
document.write('<PARAM NAME="_cy" VALUE="8149">');
document.write('<embed name="WinMedia" src="'+ arquivo +'" width="'+ largura +'" height="'+ altura +'" type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" Autostart="True" uiMode="Full" enableContextMenu="False"></embed>');
document.write('</object>');
}
/////////////////////////////////////////////////////////////

////////////////// CRIA CONTROLE DE FLASH ///////////////////
function criar_controle_flash(arquivo,largura,altura)
{
document.write('<OBJECT codeBase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="'+ largura +'" height="'+ altura +'" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">');
document.write('<PARAM NAME="_cx" VALUE="9181">');
document.write('<PARAM NAME="_cy" VALUE="7223">');
document.write('<PARAM NAME="FlashVars" VALUE="">');
document.write('<PARAM NAME="Movie" VALUE="'+ arquivo +'">');
document.write('<PARAM NAME="Src" VALUE="'+ arquivo +'">');
document.write('<PARAM NAME="WMode" VALUE="Window">');
document.write('<PARAM NAME="Play" VALUE="1">');
document.write('<PARAM NAME="Loop" VALUE="-1">');
document.write('<PARAM NAME="Quality" VALUE="High">');
document.write('<PARAM NAME="SAlign" VALUE="">');
document.write('<PARAM NAME="Menu" VALUE="-1">');
document.write('<PARAM NAME="Base" VALUE="">');
document.write('<PARAM NAME="AllowScriptAccess" VALUE="always">');
document.write('<PARAM NAME="Scale" VALUE="ShowAll">');
document.write('<PARAM NAME="DeviceFont" VALUE="0">');
document.write('<PARAM NAME="EmbedMovie" VALUE="0">');
document.write('<PARAM NAME="BGColor" VALUE="">');
document.write('<PARAM NAME="SWRemote" VALUE="">');
document.write('<PARAM NAME="MovieData" VALUE="">');
document.write('<PARAM NAME="SeamlessTabbing" VALUE="1">');
document.write('<PARAM NAME="Profile" VALUE="0">');
document.write('<PARAM NAME="ProfileAddress" VALUE="">');
document.write('<PARAM NAME="ProfilePort" VALUE="0">');
document.write('<embed type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="'+ arquivo +'" quality="high" width="'+ largura +'" height="'+ altura +'"></embed>');
document.write('</object>');
}
/////////////////////////////////////////////////////////////



////////////////// CRIA CONTROLE DE SHOCKWAVE ///////////////////
function criar_controle_shockwave(arquivo,largura,altura)
{
document.write('<OBJECT codeBase="http://download.macromedia.com/pub/shockwave/cabs/director/sw.cab#version=8,5,0,0" ID="exercicio_dcr" width="'+ largura +'" height="'+ altura +'" classid="clsid:166B1BCA-3F9C-11CF-8075-444553540000">');
document.write('<PARAM NAME="Src" VALUE="'+ arquivo +'">');
document.write('<param name=swStretchStyle value=fill>');
document.write('<PARAM NAME=bgColor VALUE=#000000> <PARAM NAME=progress VALUE=TRUE> <PARAM NAME=logo VALUE=TRUE> ');
document.write('<embed src="'+ arquivo +'" bgColor=#000000 progress=FALSE logo=FALSE  width="'+ largura +'" height="'+ altura +'" swStretchStyle=fill type="application/x-director" pluginspage="http://www.macromedia.com/shockwave/download/"></embed>');
document.write('</object>');
}
/////////////////////////////////////////////////////////////



///////// POSICIONA LAYER DE MENSAGENS NO CENTRO //////////
function posicionaLayer_msg()
{
	nome_div = "div_msg";
	with(document.getElementById(nome_div).style) 
	{
		/*vLeft = ((document.body.clientWidth - parseInt(width)) / 2);
		vTop = (( document.body.clientHeight - parseInt(height)) / 2);
		//vTop = ((document.body.clientHeight - parseInt(height)) / 2);
		position = 'absolute';
		left = vLeft;
		top = vTop;
		
		posLeft = document.body.scrollLeft + vLeft;
		posTop = document.body.scrollTop + vTop;*/
		
		
		position = 'absolute';

		left = (((document.body.clientWidth) / 2)-150) + "px";
		top = (document.body.scrollTop+((window.innerHeight / 2 )-90))+"px";
		
		
	}
	
	/*nome_div = "div_fundo";
	with(document.getElementById(nome_div).style) 
	{
		width = document.body.clientWidth;
		height = document.body.clientHeight;
		position = 'absolute';
		
		posLeft = document.body.scrollLeft;
		posTop = document.body.scrollTop;
	}*/
	
	setTimeout("posicionaLayer_msg()",100);
}
/////////////////////////////////////////////////////////////

//////////////// ABRE CAIXAS DE DI�LOGO /////////////////////
function caixa_dialogo_msg( pagina )
{
	document.getElementById("i_msg").src = pagina;
	document.getElementById("div_fundo").style.visibility = 'visible';
	document.getElementById("div_msg").style.visibility = 'visible';
	document.body.style.overflow = 'hidden';
}
/////////////////////////////////////////////////////////////


/*
    Exemplo:
    - Cria o cookie 'CookieTeste' com o valor 'HellowWorld!' que ir� expirar quando o browser for fechado.
    GerarCookie('CookieTeste', 'HellowWorld!', 0);
    - L� o conte�do armazenado no cookie.
    LerCookie('CookieTeste');
    - Excl�i o cookie.
    ExcluirCookie('CookieTeste');
*/

// Fun��o para criar o cookie.
// Para que o cookie seja destru�do quando o brawser for fechado, basta passar 0 no parametro lngDias.
function GerarCookie(strCookie, strValor, lngDias)
{
    var dtmData = new Date();

    if(lngDias)
    {
        dtmData.setTime(dtmData.getTime() + (lngDias * 24 * 60 * 60 * 1000));
        var strExpires = "; expires=" + dtmData.toGMTString();
    }
    else
    {
        var strExpires = "";
    }
    document.cookie = strCookie + "=" + strValor + strExpires + "; path=/";
}

// Fun��o para ler o cookie.
function LerCookie(strCookie)
{
    var strNomeIgual = strCookie + "=";
    var arrCookies = document.cookie.split(';');

    for(var i = 0; i < arrCookies.length; i++)
    {
        var strValorCookie = arrCookies[i];
        while(strValorCookie.charAt(0) == ' ')
        {
            strValorCookie = strValorCookie.substring(1, strValorCookie.length);
        }
        if(strValorCookie.indexOf(strNomeIgual) == 0)
        {
			//alert( strValorCookie.substring(strNomeIgual.length, strValorCookie.length) );
            return strValorCookie.substring(strNomeIgual.length, strValorCookie.length);
        }
    }
    //return null;
	return strValorCookie.substring(strNomeIgual.length, strValorCookie.length);
}

// Fun��o para excluir o cookie desejado.
function ExcluirCookie(strCookie)
{
    GerarCookie(strCookie, '', -1);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////

// Muda p�ginas de selects ///////////////////////////////////
function select_muda_pagina(id_select)
{
	var valor = document.getElementById('paginas').value;
	document.location.href = valor;
}
/////////////////////////////////////////////////////////////

///////// POSICIONA LAYER DE ATUALIZA��O NO CENTRO //////////
function bloqueio_inicial(bloqueio)
{
	if ( bloqueio == 1 )
	{
		
		nome_div = "div_fundo";
		with(document.getElementById(nome_div).style) 
		{
			width = document.body.clientWidth;
			height = document.body.clientHeight;
			//position = 'absolute';
			
			posLeft = document.body.scrollLeft;
			posTop = document.body.scrollTop;
		}
		
		document.body.style.overflow	= 'hidden';
		
	}else{
	
		top.document.getElementById('div_fundo').style.visibility		= 'hidden';
		
		document.body.style.overflow	= '';
	
	}
	
}
/////////////////////////////////////////////////////////////


////////////////////////////// AJAX /////////////////////////////////
function openAjax()
{ 
	var Ajax; 
	try
	{
		Ajax = new XMLHttpRequest(); // XMLHttpRequest para browsers mais populares, como: Firefox, Safari, dentre outros. 
	}
	catch(ee) 
	{ 
		try
		{
			Ajax = new ActiveXObject("Msxml2.XMLHTTP"); // Para o IE da MS 
		}
		catch(e)
		{ 
			try
			{
				Ajax = new ActiveXObject("Microsoft.XMLHTTP"); // Para o IE da MS 
			}
			catch(e)
			{
				Ajax = false; 
			} 
		} 
	} 
	return Ajax; 
} 

function carregaAjax(id, pagina, metodo)
{ 
	if(document.getElementById)  // Para os browsers complacentes com o DOM W3C.
	{ 
		var exibeResultado = document.getElementById(id); // div que exibir� o resultado. 
		var Ajax = openAjax(); // Inicia o Ajax. 
		
		Ajax.open(metodo, pagina, true); // fazendo a requisi��o 
		
		Ajax.onreadystatechange = function() 
		{ 
			if(Ajax.readyState == 1) // Quando estiver carregando, exibe: carregando...
			{ 
				exibeResultado.innerHTML = "<div style='width:80px; height:18px; background-color:#CC3300; padding-left:2px; padding-top:1px;'><font color='#FFFFFF'>Cadastrando...</font></div>"; 
			} 
			
			if(Ajax.readyState == 4) // Quando estiver tudo pronto. 
			{
				if(Ajax.status == 200)
				{ 
					var resultado = Ajax.responseText; // Coloca o retornado pelo Ajax nessa vari�vel 
					resultado = resultado.replace(/\+/g," "); // Resolve o problema dos acentos (saiba mais aqui: http://www.plugsites.net/leandro/?p=4) 
					resultado = unescape(resultado); // Resolve o problema dos acentos 
					exibeResultado.innerHTML = resultado; 
				}
				else
				{ 
					exibeResultado.innerHTML = "<div align='center'><strong>Erro: Problemas com a conex�o.</strong></div>"; 
				} 
			} 
		} 
	Ajax.send(null); // submete 
	} 
} 
/////////////////////////////////////////////////////////////////////


////////////////// VALIDA CNPJ //////////////////////////////
function checa_cnpj( CNPJ_obj ) 
{
	CNPJ= CNPJ_obj.value; 
	CNPJ= CNPJ.replace("-","");
	CNPJ= CNPJ.replace(".","");
	CNPJ= CNPJ.replace(".","");
	CNPJ= CNPJ.replace("/","");
	
	g=CNPJ.length-2;
		
	if(RealTestaCNPJ(CNPJ,g) == 1) 
	 { 
		g=CNPJ.length-1; 
		if(RealTestaCNPJ(CNPJ,g) == 1) 
		 { 
			 CNPJ_valido=1; 
		 } 
		 else 
		 { 
			 CNPJ_valido=0; 
		 } 
	 } 
	 else 
	 { 
		 CNPJ_valido=0; 
	 } 
	
	if (CNPJ_valido==0)
	{
		alert("N�mero do CNPJ inv�lido.");
		//CNPJ_obj.focus();
		return (false);
	}
}

function RealTestaCNPJ(CNPJ,g) 
{ 
	var VerCNPJ=0; 
	var ind=2; 
	var tam; 
	
	for(f=g;f>0;f--) 
	{ 
		VerCNPJ+=parseInt(CNPJ.charAt(f-1))*ind; 
		if(ind>8) 
		{ 
			ind=2; 
		} 
		else 
		{ 
			ind++; 
		} 
	} 
	
	VerCNPJ%=11; 
	if(VerCNPJ==0 || VerCNPJ==1) 
	{ 
		VerCNPJ=0; 
	} 
	else 
	{ 
		VerCNPJ=11-VerCNPJ; 
	} 
	if(VerCNPJ!=parseInt(CNPJ.charAt(g))) 
	{ 
		return(0); 
	} 
	else 
	{ 
		return(1); 
	} 
}
/////////////////////////////////////////////////////////////

// Browser detection /////////////////////////////////////////////
function detecta_navegador()
{
	// Internet Explorer
	var ie  = document.all != null;  //ie4 and above
	var ie5 = document.getElementById && document.all;
	var ie6 = document.getElementById && document.all&&(navigator.appVersion.indexOf("MSIE 6.")>=0); 
	
	// Netscape
	var ns4 = document.layers != null;
	var ns6 = document.getElementById && !document.all;
	var ns  = ns4 || ns6; 
	
	// Firefox
	var ff  = !document.layers && !document.all; 
	
	// Opera
	var op  = navigator.userAgent.indexOf("opera")>0;
	var op7 = op && operaVersion() <= 7;
	var op8 = op && operaVersion() >= 8; 

	if (ie){
		if (getInternetExplorerVersion() >= 10)
			return(false)
		else
			return(true)
	}else
		return(false)

}
	// Detects the Opera version
	function operaVersion() 
	{	
		agent = navigator.userAgent;	
		idx = agent.indexOf("opera");		
		if (idx>-1) 
		{		
			return parseInt(agent.subString(idx+6,idx+7));	
		}
	}
	
	// Detecta vers�o do IE
	function getInternetExplorerVersion() {
		
		var rv = -1;
		
		if (navigator.appName == 'Microsoft Internet Explorer'){
			var ua = navigator.userAgent;
		
			var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		
			if (re.exec(ua) != null)
				rv = parseFloat( RegExp.$1 );
		}
		
		return rv;
	}

/////////////////////////////////////////////////////////////////////

// PARA UPLOAD DE ARQUIVOS /////////////////////////////////////////
function arquivo(enviado,enviado_real,objeto)
{
	var enviado = enviado;
	document.getElementById(objeto+'_aviso').innerHTML='Arquivo &nbsp;<i><b>'+enviado+'</b></i>&nbsp; enviado.';
	document.getElementById(objeto+'_arquivo').value=enviado_real;
}
function verifica_carregado(arquivo,objeto)  
{  
	var enviando;
	enviando=arquivo;
	
	if (enviando=='2'){
		document.getElementById(objeto+'_frame').style.display="none";
		//document.getElementById(objeto+'_aviso').style.display="block";
		document.getElementById(objeto+'_aviso').style.visibility="visible";
		//document.envia_html.submit();
	}
	
} 
/////////////////////////////////////////////////////////////////////

// Substring de left ou right ///////////////////////////////////////
function left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}
function right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}
/////////////////////////////////////////////////////////////////////


// Contador de caracteres de textarea
function contaCaracteres( objeto, id, maximo, limitar ){

	valor_digitado = objeto.value;

	qtd_digitado = valor_digitado.length;	

	if ( qtd_digitado >= maximo && limitar ){
		valor_digitado = left( valor_digitado, maximo );
		objeto.value = valor_digitado;
	}

	qtd_digitado = valor_digitado.length;
	$('#'+id).html( qtd_digitado+' caracteres de '+maximo+' permitidos');

}
