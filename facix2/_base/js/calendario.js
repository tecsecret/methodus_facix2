//Global Variables
var day;
var mth;
var yrs;

//-----------------------------------------------------------------------------------------------------------
// FUNCTIONS
//-----------------------------------------------------------------------------------------------------------
function findPosX(obj)
{
	var curleft = 0;
	if (document.getElementById || document.all)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (document.layers)
		curleft += obj.x;
	return curleft;
}

//-----------------------------------------------------------------------------------------------------------
function findPosY(obj)
{
	var curtop = 0;
	if (document.getElementById || document.all)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (document.layers)
		curtop += obj.y;
	return curtop;
}

//-----------------------------------------------------------------------------------------------------------
function cbfIsLeapYear(int_year)
{
	var isleapyr;
	if (int_year%4==0)
	{	isleapyr = true;	}
	else 
	{	isleapyr = false;	}
	return isleapyr;
}

//-----------------------------------------------------------------------------------------------------------
function cbfGetMaxDays(dt_day, dt_mth, dt_yrs)
{
	var ls_max;
	if (dt_mth==2)
	{
		if (cbfIsLeapYear(dt_yrs))
		{	ls_max = 29;	}
		else
		{	ls_max = 28;	}
	}
	else if ((dt_mth==4) || (dt_mth==6) || (dt_mth==9) || (dt_mth==11))
	{	ls_max = 30;	}
	else
	{	ls_max = 31;	}
	return ls_max;
}

//-----------------------------------------------------------------------------------------------------------
function cbfGetMonthName(mth)
{
	switch(mth)
	{
		case 0 : return "Janeiro";
		case 1 : return "Fevereiro";
		case 2 : return "Mar�o";
		case 3 : return "Abril";
		case 4 : return "Maio";
		case 5 : return "Junho";
		case 6 : return "Julho";
		case 7 : return "Augosto";
		case 8 : return "Setembro";
		case 9 : return "Outubro";
		case 10 : return "Novembro";
		case 11 : return "Dezembro";
		default : return "Janeiro";
	}
}

//-----------------------------------------------------------------------------------------------------------
function cbfloadmefirst(formname, objname)
{
	var tempObj;
	var tempDate;
	tempObj = eval("document." + formname + "." + objname);
	tempDate = tempObj.value;
	
	if (tempDate.length<=3)
	{	
		tempDate = new Date();
		day = tempDate.getDate();
		mth = tempDate.getMonth()+1;
		yrs = tempDate.getFullYear();
	}
	else
	{
		day = tempDate.slice(tempDate.search("/")+1);
		mth = day.slice(0,day.search("/"));
		day = tempDate.slice(0,tempDate.search("/"));
		tempDate = tempDate.slice(3);
		yrs = tempDate.substr(tempDate.search("/")+1,4);
	}
	
	document.frames.CalFrame.document.fx.txtday.value=day;
	document.frames.CalFrame.document.fx.txtmth.value=mth;
	document.frames.CalFrame.document.fx.txtyrs.value=yrs;
	document.frames.CalFrame.document.fx.txtobj.value=objname;
	document.frames.CalFrame.document.fx.txtfrm.value=formname;
	document.frames.CalFrame.document.fx.txtcurmth.value=mth;
	document.frames.CalFrame.document.fx.txtcuryrs.value=yrs;
	
	cbfdrawcalendar(day,mth,yrs);
}

//-----------------------------------------------------------------------------------------------------------
function cbfshowcalendar(formname, objname, imgname)
{
	//The current size of the IFRAME is width=167 and height=200
	//var x=eval("document.all." + imgname + ".offsetLeft");
	//var y=eval("document.all." + imgname + ".offsetTop");
	var x=findPosX(eval("document.all." + imgname));
	var y=findPosY(eval("document.all." + imgname));
	var max_x = window.screen.width;
	var max_y = window.screen.height-100;
	
	cbfloadmefirst(formname, objname);
	if ((max_y-y<200) && (max_x-x<167))
	{
	document.all.CalFrame.style.top=y-200;
	document.all.CalFrame.style.left=x-167+eval("document.all." + imgname + ".offsetWidth");
	}
	else if (max_y-y<200) 
	{
	document.all.CalFrame.style.top=y-200;
	document.all.CalFrame.style.left=x;
	}
	else if (max_x-x<167)
	{
	document.all.CalFrame.style.top=y+eval("document.all." + imgname + ".offsetHeight");
	document.all.CalFrame.style.left=x-167+eval("document.all." + imgname + ".offsetWidth");
	}
	else
	{
	document.all.CalFrame.style.top=y+eval("document.all." + imgname + ".offsetHeight");
	document.all.CalFrame.style.left=x;
	}

	document.all.CalFrame.style.display="block";
}

//-----------------------------------------------------------------------------------------------------------
function cbfselectdate(objid)
{
	var tmpobj;
	var d=eval("document.all." + objid + ".value");
	var m=parent.document.frames.CalFrame.document.fx.txtmth.value;
	var y=parent.document.frames.CalFrame.document.fx.txtyrs.value;
	var objname=parent.document.frames.CalFrame.document.fx.txtobj.value;
	var frmname=parent.document.frames.CalFrame.document.fx.txtfrm.value;
	
	if (d!="")
	{
	cbfdrawcalendar(d,m,y);
	parent.document.frames.CalFrame.document.fx.txtday.value=d;
	
	tmpobj=eval("parent.document." + frmname + "." + objname);
	newdate=d+"/"+m+"/"+y;
	tmpobj.value=newdate;
	
	parent.document.all.CalFrame.style.display="none";
	}
}

//-----------------------------------------------------------------------------------------------------------
function cbfgotomonth(action, d, m, y)
{	
	if (action=="next")
	{	m++;
		if (m>12){m=1;y++;}
	}
	else if (action=="prev")
	{	m--;
		if (m<1){m=12;y--;}
	}
	else
	{	return false;	}
	parent.document.frames.CalFrame.document.fx.txtday.value=d;
	parent.document.frames.CalFrame.document.fx.txtmth.value=m;
	parent.document.frames.CalFrame.document.fx.txtyrs.value=y;
	cbfdrawcalendar(d,m,y);
}

//-----------------------------------------------------------------------------------------------------------
function cbfdrawcalendar(dd,mm,yy)
{
	var dayname;
	var mthname;
	var maxdays;
	var datenow = new Date();
	
	if ((dd!="") || (mm!="") || (yy!=""))
	{
		datenow.setDate(1);
		datenow.setMonth(mm-1);
		datenow.setYear(yy);
	}
	
	datenow.setDate(1);
	dayname = datenow.getDay()+1;
	mthname = cbfGetMonthName(datenow.getMonth());
	maxdays = cbfGetMaxDays("",datenow.getMonth()+1,datenow.getYear());
	
	if (!document.frames.CalFrame)
	{
		parent.document.frames.CalFrame.document.fx.txtmthyrs.value=mthname + " " + datenow.getYear();
		cbfclearcalendar();
		for (var x=1 ; x<=maxdays ; x++)
		{
			eval("parent.document.frames.CalFrame.document.all.d" + dayname + ".value=" + x);
			if ((dd==x) && (mm==parent.document.frames.CalFrame.document.fx.txtcurmth.value) && (yy==parent.document.frames.CalFrame.document.fx.txtcuryrs.value))
			{eval("parent.document.frames.CalFrame.document.all.d" + dayname + ".style.backgroundColor='pink'");}
			dayname++;
		}
	}
	else
	{
		document.frames.CalFrame.document.fx.txtmthyrs.value=mthname + " " + datenow.getYear();
		cbfclearcalendar();
		for (var x=1 ; x<=maxdays ; x++)
		{
			eval("document.frames.CalFrame.document.all.d" + dayname + ".value=" + x);
			if ((dd==x) && (mm==document.frames.CalFrame.document.fx.txtcurmth.value) && (yy==document.frames.CalFrame.document.fx.txtcuryrs.value))
			{eval("document.frames.CalFrame.document.all.d" + dayname + ".style.backgroundColor='pink'");}
			dayname++;
		}
	}
}

//-----------------------------------------------------------------------------------------------------------
function cbfclearcalendar()
{
	for (var x=1 ; x<=42 ; x++)
	{	
		if (!document.frames.CalFrame)
		{
		eval("parent.document.frames.CalFrame.document.all.d" + x + ".value=''");	
		eval("parent.document.frames.CalFrame.document.all.d" + x + ".style.backgroundColor='white'");
		}
		else
		{
		eval("document.frames.CalFrame.document.all.d" + x + ".value=''");	
		eval("document.frames.CalFrame.document.all.d" + x + ".style.backgroundColor='white'");
		}
	}
}

//-----------------------------------------------------------------------------------------------------------
/*----------------------------*/
/*		NO SHORTCUT KEY		  */
/*----------------------------*/
function disable_keycode()
{
	event.keyCode=0;
	event.returnValue=false;
}

function keydown()
{
//alert(event.keyCode);
	if ((event.altKey) && ((event.keyCode==37) || (event.keyCode==39))) disable_keycode();
	if ((event.ctrlKey) && ((event.keyCode==78) || (event.keyCode==82) || (event.keyCode==69) || (event.keyCode==87) || (event.keyCode==66) || (event.keyCode==72) || (event.keyCode==73) || (event.keyCode==76) || (event.keyCode==79) || (event.keyCode==68)  || (event.keyCode==83))) disable_keycode();
	if (event.keyCode==8) disable_keycode();
//	if (event.keyCode==116) disable_keycode();
	if (event.keyCode==122) disable_keycode();
	if (event.keyCode==93) {alert("No context menu"); disable_keycode();}
}

//-----------------------------------------------------------------------------------------------------------
/*----------------------------*/
/*		NO RIGHT CLICK		  */
/*----------------------------*/
function norightclick()
{
	if (window.event){
	if (event.button!=1)
	{	
		alert("No right click");
		event.cancelBubble=true;
		event.returnValue=false;
		return false;
	}}
}

//-----------------------------------------------------------------------------------------------------------
// MAIN PROCEDURE
//-----------------------------------------------------------------------------------------------------------
