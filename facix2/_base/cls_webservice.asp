<%
'***********************************************************************************
'
' Classe para acessar WebServices
' Author: Angelo Bestetti
' http://www.i-stream.com.br
' Purpose: Acessar webservices www.consultacpf.com
' Date: 2008/02/04
'***********************************************************************************
'Option Explicit

'**************************************************** 
' Classe para WebService
'****************************************************

Class WebService
  Public Url
  Public Method
  Public Response
  Public Parameters
  Public XML
 
  ' Funcao para Invokar o WebService
  Public Function Invoke( metodo )
    Dim xmlhttp
    Set xmlhttp = Server.CreateObject("MSXML2.XMLHTTP")
    xmlhttp.open "POST", Url , false
	xmlhttp.setRequestHeader "Host", "www.pagador.com.br" 
	xmlhttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"	
	xmlhttp.setRequestHeader "SOAPAction", """"&metodo&"""" 
    xmlhttp.Send XML
	'response.Write(Parameters.toString)
    response = xmlhttp.responseText
    set xmlhttp = nothing
  End Function
  
  
  
  Public Function Table(TableName)
	Dim oNodes, oNode, i
	Dim objDOMDoc
	Set objDOMDoc = Server.CreateObject("MSXML2.DOMDocument.4.0")

     objDOMDoc.async = true
     objDOMDoc.loadXML(response)

	If objDOMDoc.parseError.errorCode <> 0 Then
		Response.write("Error Code: ")
		Response.write(objDOMDoc.parseError.errorCode)
		Response.write("<br>Error Reason: ")
		Response.write(objDOMDoc.parseError.reason)
		Response.write("<br>Error Line: ")
		Response.write(objDOMDoc.parseError.line)
		Response.write("<br>Error Position: ")
		Response.write(objDOMDoc.parseError.linepos)
	     Response.Write("XML: " & objDOMDoc.xml & "<BR>") 
	Else
	     	'Response.write("Records: " & objDOMDoc.hasChildNodes & "<BR>")
	End If

	set oNodes = objDOMDoc.getElementsByTagName(TableName) 
   	Response.write("<strong>Tabela:</strong> " & TableName & "<br>")
   	Response.write("Qtd. Registros: " & oNodes.length  & "<br><br>")


	For each oNode in oNodes
    		Response.Write("Registro: " & oNode.Text & "<BR>" )
    		For i = 0 to oNode.childNodes.length-1
	    		Response.Write oNode.childNodes.Item(i).Nodename & ": " & oNode.childNodes.Item(i).Text & "<BR>" 
    		Next
    		Response.Write("<hr>")
 	Next 
    		Response.Write("<br>")
 End Function
   
  
  Public Function Field(FieldName)
	Dim oRoot, oNodes, oNode, i
	Dim objDOMDoc
	Set objDOMDoc = Server.CreateObject("MSXML2.DOMDocument.4.0")

     objDOMDoc.async = true
     objDOMDoc.loadXML(response)

	If objDOMDoc.parseError.errorCode <> 0 Then
		Response.write("Error Code: ")
		Response.write(objDOMDoc.parseError.errorCode)
		Response.write("<br>Error Reason: ")
		Response.write(objDOMDoc.parseError.reason)
		Response.write("<br>Error Line: ")
		Response.write(objDOMDoc.parseError.line)
		Response.write("<br>Error Position: ")
		Response.write(objDOMDoc.parseError.linepos)
	     Response.Write("XML: " & objDOMDoc.xml & "<BR>") 
	End If

	Set oNodes = objDOMDoc.documentElement.ChildNodes

	For Each oNode In oNodes 
	
	If oNode.NodeName = FieldName Then
	  Field = oNode.text 
	End if
	Next


 End Function

   
  Private Sub Class_Initialize()
    Set Parameters = New wsParameters
  End Sub
  
  Private Sub Class_Terminate()
    Set Parameters = Nothing
  End Sub
  
End class

'**************************************************** 
' Classe para wsParameters
'****************************************************
Class wsParameters
  Public mCol
  Public Function toString()
    Dim nItem
    Dim buffer
    buffer = ""
    For nItem = 1 to Count
      buffer = buffer & Item(nItem).toString & "&"
    Next
    If right(buffer,1)="&" then
      buffer = left(buffer,len(buffer)-1)
    End if
    toString = buffer 
  End Function
  
  Public Sub Clear
    set mcol = nothing 
    Set mCol = Server.CreateObject("Scripting.Dictionary") 
  End Sub
  
  Public Sub Add(pKey,pValue)
    Dim NewParameter
  
    Set NewParameter = New wsParameter
    NewParameter.Key = pKey
    NewParameter.Value = pValue
    mCol.Add mCol.count+1, NewParameter
  
    Set NewParameter = Nothing
  End Sub
  
  Public Function Item(nKey)
    Set Item=mCol.Item(nKey)
  End Function
  
  Public Function ExistsXKey(pKey)
    Dim nItem
  
    For nItem = 1 to mcol.count
      If mCol.Item(nItem).key = pKey Then
        ExistsXKeyword = True
        Exit For
      End if
    Next
  End Function
  
  Public Sub Remove(nKey)
    mCol.Remove(nKey)
  End sub
  
  Public Function Count()
    Count=mCol.count
  End Function
  
  Private Sub Class_Initialize()
    Set mCol = Server.CreateObject("Scripting.Dictionary")
  End Sub
  
  Private Sub Class_Terminate()
    Set mCol = Nothing
  End Sub
  
End class

'**************************************************** 
' Classe para wsParameter
'****************************************************
Class wsParameter
   Public Key
   Public Value
   Public Function toString()
     toString = Key & "=" & Value
   End Function
End Class
%>
