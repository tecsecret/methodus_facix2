<%
''''''''''''''''''''''''''''''' CONFIGURA��ES '''''''''''''''''''''''''''''''''
response.Buffer 	= true
%>
<!-- #include file="../_base/conexao.asp" -->
<%
'' Vari�veis ''
titulo_site			= "Facix v2.2 - Methodus"
email_01			= "info@methodus.com.br"
website_01			= "http://www.methodus.com.br/_ambiente_aula"

verdade				= "1" '"true"
falso				= "0" '"false"


'' PREVEN��O DE INJE��O DE SQL ''
FUNCTION sqlInjection()

	'Palavras que ser�o barradas caso encontradas nos request
	palavrasDoMal = array("insert", "drop ", " or ", "update", "cast(")
	erro_injecao = ""
	
	' Verificando o que � passado pelo request.queryString
	for each item in request.QueryString
		for j = lbound(palavrasDoMal) to ubound(palavrasDoMal)
			if instr(lcase(Request.QueryString(item)), lcase(palavrasDoMal(j))) > 0 then
				erro_injecao = erro_injecao & " | querystring: "&Request.QueryString(item)&""
			end if
		next
	next

	'Verificando o que � enviado por request.form
	for each item in request.form
		for j = lbound(palavrasDoMal) to ubound(palavrasDoMal)
			if instr(lcase(Request.form(item)), lcase(palavrasDoMal(j))) > 0 then
				erro_injecao = erro_injecao & " | form: "&Request.form(item)&""
			end if
		next
	next

	' Verifica o que est� sendo passado via cookies
	'for each item in request.Cookies
	'	for j = lbound(palavrasDoMal) to ubound(palavrasDoMal)
	'		if instr(lcase(Request.Cookies(item)), lcase(palavrasDoMal(j))) > 0 then
	'			erro_injecao = erro_injecao & " | cookie: "&Request.Cookies(item)&""
	'		end if
	'	next
	'next
	
	if not erro_injecao = "" then
		'call ENVIAR_EMAIL( "renan@pensadigital.com.br", "renan@pensadigital.com.br", website_01&" - Inje��o de c�digo", website_01&"<BR />"&erro_injecao )
		response.Clear()
		response.Redirect("../../")
	end if

END FUNCTION

sqlInjection()


FUNCTION LimpaLixo( input )

    dim lixo
    dim textoOK

    lixo = array ( "select" , "drop " ,  "update" , "--" , "insert" , "delete" ,  "xp_", "<script" )

    textoOK = input

     for j = 0 to uBound(lixo)
          textoOK = replace( textoOK ,  lixo(j) , "")
     next

     LimpaLixo = textoOK

END FUNCTION
'''''''''''''''''''''''''''''''''

'' Formata��o de INPUTS ''
FUNCTION FORMATAR( var, tipo, metodo )

	if metodo= 1 then ' Post
		formatar= request.form(var)
	elseif metodo= 2 then ' Querystring
		formatar= request.querystring(var)
	elseif metodo= 4 then ' componente de upload
		formatar= var
	else ' N�o especificado
		formatar= request(var)
	end if
	
	if tipo= 2 then ' Textarea
	
		formatar= replace(formatar,"'","''")
		formatar= replace(formatar,chr(34),"""")
		formatar= replace(formatar,"<","&lt;")
		formatar= replace(formatar,">","&gt;")
		formatar= replace(formatar,Chr(10),"<br />")
		formatar= replace(formatar,Chr(32) & Chr(32)," &nbsp;")
		
	elseif tipo= 3 then ' Textarea em HTML
		
		formatar= replace(formatar,"'","''")
		
	elseif tipo= 4 then ' Do banco para o textarea
		
		formatar= replace(formatar,"<br />",Chr(10))
		formatar= replace(formatar,"<BR />",Chr(10))
		formatar= replace(formatar,"<BR>",Chr(10))
		
	else ' Campo de texto normal ou variavel de entrada qualquer
		
		formatar= replace(formatar,"'","''")
		formatar= replace(formatar,chr(34),"""")
		formatar= replace(formatar,"<","&lt;")
		formatar= replace(formatar,">","&gt;")
		
	end if
	
	formatar= trim(formatar)
	
	formatar= LimpaLixo( formatar )
	
END FUNCTION
''''''''''''''''''''''''''

'' Formata datas ''
FUNCTION FORMATA_DATA( data, tipo )
	
	'' Tipos de datas
	'' 1- Brasileiro (21/12/2005)
	'' 2- Americano (12/21/2005)
	'' 3- Universal para BD (2005-12-21)
	
	if isdate(data)=false then
		Response.Clear()
		session("erro")="0006"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
		'response.redirect("../_erro/default.asp?erro=0006")
	end if
	
	if year(data)<1900 then
		Response.Clear()
		session("erro")="0006"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	select case tipo
		case 1
			FORMATA_DATA = FormatoData(data,"dd/mm/aaaa")
		case 2
			FORMATA_DATA = FormatoData(data,"mm/dd/aaaa")
		case else
			FORMATA_DATA = FormatoData(data,"aaaa-mm-dd")
	end select
	
END FUNCTION
'''''''''''''''''''

'' Formata hora ''
FUNCTION FORMATA_HORA( hora, tipo )
	
	'' Tipos de horas
	'' 1- 9h15m
	'' 2- 9:15h
	'' 3- 9:15
	'' 4- 9:15:05
	
	hora= timevalue(hora)
	
	select case tipo
		case 1
			FORMATA_HORA = FormatoHora(hora,"hhhmmm")
		case 2
			FORMATA_HORA = FormatoHora(hora,"hh:mmh")
		case 3
			FORMATA_HORA = FormatoHora(hora,"hh:mm")
		case else
			FORMATA_HORA = FormatoHora(hora,"hh:mm:ss")
	end select
	
END FUNCTION
'''''''''''''''''''

'' Fun��es para formata��o de data e hora ''
Function FormatoData(data,formato)

	if isdate(data)=false then
		Response.Clear()
		session("erro")="0006"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
		'response.redirect("../_erro/default.asp?erro=0006")
	end if

    Dia = Right("0" & Day(data), 2)
    Mes = Right("0" & Month(data), 2)    
    Ano = Year(data)

    DataFinal = Replace(formato, "NMdd", WeekDayName(WeekDay(Date)))
    DataFinal = Replace(DataFinal, "NMmm", MonthName(Month(Date)))
    DataFinal = Replace(DataFinal, "ABmm", MonthName(Month(Date), True))
    DataFinal = Replace(DataFinal, "dd", Dia)
    DataFinal = Replace(DataFinal, "mm", Mes)
    DataFinal = Replace(DataFinal, "aaaa", Ano)
    DataFinal = Replace(DataFinal, "aa", Mid(Ano, 3, 2))

    FormatoData = DataFinal

End Function

Function FormatoHora(hora_total,formato)
	
    Hora 		= Right("0" & Hour(hora_total), 2)
    Minuto 		= Right("0" & Minute(hora_total), 2)
    Segundo 	= Right("0" & Second(hora_total), 2)

    HoraFinal 	= Replace(formato, "hh", Hora)
    HoraFinal 	= Replace(HoraFinal, "mm", Minuto)
    HoraFinal 	= Replace(HoraFinal, "ss", Segundo)

    FormatoHora = HoraFinal

End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Formata��o para pesquisas ''
FUNCTION FORMATAR_PESQUISA( var, condicao )
	pesquisa = REPLACE( var, "*", "%" )
	pesquisa = REPLACE( pesquisa,"a","[a,�,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"e","[e,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"i","[i,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"o","[o,�,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"u","[u,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"c","[c,�]")
	pesquisa = REPLACE( pesquisa,"A","[A,�,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"E","[E,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"I","[I,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"O","[O,�,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"U","[U,�,�,�,�]")
	pesquisa = REPLACE( pesquisa,"C","[C,�]")
	
	if condicao then
		pesquisa = REPLACE( pesquisa, " ", "%" )
	end if
	
	FORMATAR_PESQUISA = pesquisa
END FUNCTION
''''''''''''''''''''''''''''''''

'' Formatar Moeda para banco de dados ''
FUNCTION FORMATA_MOEDA( valor, parametro )

	if parametro = true then
	
		valor = replace( valor, ".", "")
		valor = replace( valor, ",", ".")
		
	else
	
		valor = formatnumber( valor, 2 )
		
	end if
	
	FORMATA_MOEDA = valor

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''

'' Formata��o para dinheiro ''
FUNCTION FORMATAR_DINHEIRO( var )
	FORMATAR_DINHEIRO = REPLACE( var, ".", "" )
	FORMATAR_DINHEIRO = REPLACE( FORMATAR_DINHEIRO, ",", "." )
END FUNCTION
''''''''''''''''''''''''''''''''

'' Testa se um componente existe instalado no servidor ''
Function TestObject( comIdentity )
	On Error Resume Next
	TestObject = False
	Err.Clear
	Set xTestObj = Server.CreateObject( comIdentity )
	If Err = 0 Then TestObject = True
	Set xTestObj = Nothing
	Err.clear
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Tira c�digo HTML de qualquer string ''
FUNCTION tira_codigo( var,quantidade )

      Do While True
            ini = InStr(1,var,"<")

            If ini = 0 Then
                  Exit Do
            end if

            fim = InStr(ini,var,">")

            If fim = 0 Then
                  Exit Do
            end if

            parcial = Mid(var,ini,(fim-ini)+1)
            var = Replace(var,parcial,"")
      Loop
	  
      tira_codigo = left(var,quantidade) 
	  
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''

'' CRIPTOGRAFIA SEGURA (DESTRUTIVA) ''
FUNCTION criptografar(senha)

	var = senha
	
	var= lcase(var)
	i = 1
	total= 0
	do while i<= len(var)
		numero= asc(mid(var,i,1))
		total= ((total+numero)*11.7)
		i= i+1
	loop
	var= total
	var= (var/71)
	var= (var*var)
	var= (var*1529439)
	var= fix(var)
	var= left(var,16)
	var= cstr(var)
	var= replace(var,",","")
	
	criptografar= var
	
END FUNCTION
''''''''''''''''''''''''''''''''''''''

'' SETA PASTAS PERMITIDAS PARA O USU�RIO ''
FUNCTION permissoes_usuario()

	dim sql
	
	usuario_departamentos = ""
	sql="select * from PRESTADOR_DEPARTAMENTOS where ID_prestador="&session("admin")
	set rs_permissao= conexao.execute(sql)
	do while not rs_permissao.EOF
		usuario_departamentos = usuario_departamentos & rs_permissao("ID_departamento")

		rs_permissao.movenext
		if not rs_permissao.EOF then
			usuario_departamentos = usuario_departamentos & ","
		end if
	loop
	
	permissoes_usuario = usuario_departamentos

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''

'' CHECA SE EST� LOGADO E QUAL � A PERMISS�O ''
SUB checalogado()
	
	ID_admin= session("admin")
	
	if ID_admin=empty then
		checa_logado = false
	else

		areas= Request.ServerVariables("PATH_INFO")
		area_a= split(areas,"/")
		area= area_a(ubound(area_a)-1)
		
		sql="select ID_pasta from PASTAS where arquivo_pasta='"&area&"'"
		set rs_permissao= conexao.execute(sql)
		
		if rs_permissao.EOF then
			checa_logado = true
		else
		
			ID_pasta= rs_permissao("ID_pasta")
			
			parte = Split(permissoes_usuario(), ",", -1, 1)
			qtd = UBound (parte)
			i=0
			sql= "select * from PERMISSOES where (ID_departamento=0"
			do while i <= qtd
				sql= sql+" or ID_departamento="&trim(parte(i))
				i= i+1
			loop
			sql= sql+") and ID_pasta="&ID_pasta
			set rs_permissao= conexao.execute(sql)
			
			
			if not rs_permissao.EOF then
				checa_logado = true
			else
				checa_logado = false
			end if
		
		end if
		
	end if
	
	set rs_permissao= nothing
	
	if checa_logado = false then
		Response.Clear()
		session("erro") = "0003"
		response.redirect(session("voltar"))
	end if 
	'response.Write("<BR /><BR />"&area&"<BR />"&usuario_departamentos&"<BR />"&ID_pasta&"<BR />"&checa_logado)
	
END SUB
''''''''''''''''''''''''''''''''''''''''''''''

'' CRIA��O DO MENU MIGALGA DE P�O ''
FUNCTION cria_titulo(var)
	
	cria_titulo= "<table border=""0"" cellpadding=""5"" cellspacing=""0"" width=""100%""><TR><TD bgcolor=""#EEEEEE"">"
	
	parte = Split(var, ";", -1, 1)
	qtd = UBound (parte)
	i=0
	do while i <= qtd
	
		parte2 = Split(parte(i), "@", -1, 1)
		if parte2(1)="" then
			cria_titulo= cria_titulo&" <b>"&parte2(0)&"</b>"
		else
			cria_titulo= cria_titulo&" <a href="""&parte2(1)&""" class=""link_01"">"&parte2(0)&"</a>"
		end if
		
		i= i+1
		
		if i <= qtd then
			cria_titulo= cria_titulo&" &gt;&gt; "
		end if
	loop
	
	cria_titulo= cria_titulo&"</TD></TR></table><BR />"
	
END FUNCTION
''''''''''''''''''''''''''''''''''''''

'' fun��es para montar a session com url completa ''
FUNCTION SESSION_VOLTAR()

		SESSION_VOLTAR = "http://"&Request.ServerVariables("SERVER_NAME")&Request.ServerVariables("URL")&"?"&Request.ServerVariables("QUERY_STRING")

END FUNCTION
''''''''''''''''''''''''''''''''''''''''''''''''''''

'' fun��o para determinar qual ser� a vari�vel do link voltar ''
FUNCTION PAGINA_VOLTAR( tipo )
	
	select case tipo
	case "0"
		PAGINA_VOLTAR = ""
	case "1"
		PAGINA_VOLTAR = "javascript: history.back(1);"
	case "2"
		PAGINA_VOLTAR = session("voltar")
	case else
		PAGINA_VOLTAR = tipo
	end select
	
END FUNCTION
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para formatar uma quantidade total de segundos em HH:MM:SS ''
FUNCTION CALCULA_TEMPO(total)
					
	hora 			= fix(total/3600)
	minuto 			= fix((total - (hora*3600))/60)
	segundo 		= total - ((hora*3600)+(minuto*60))
						
	CALCULA_TEMPO 	= hora&":"&right("0"&minuto,2)&":"&right("0"&segundo,2)
						
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para formatar uma hora em HH:MM:SS em segundos''
FUNCTION CALCULA_SEGUNDOS(tempo)

	parte 	= Split(tempo, ":", -1, 1)
					
	hora 	= parte(0)*3600
	minuto 	= parte(1)*60
	segundo = parte(2)
						
	CALCULA_SEGUNDOS = hora + minuto + segundo
						
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para abrir conex�o com um XML ''
set xmlDoc=server.createObject("microsoft.xmldom")
SUB CONEXAO_XML( caminho )

	xmlDoc.async = false
	caminho = Server.MapPath(caminho)
	xmlDoc.load(caminho)

END SUB
''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para montar e-mail ''
FUNCTION MONTA_EMAIL(conteudo)	
	
	'strCabecalho 	= "<html><head><title>Methodus</title><style type=""text/css"">body,td,th {font-family: Arial, Helvetica, sans-serif;font-size: 11px;color: #000000;}body {background-color: #96c1c8;background-image: url(http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_fundo.jpg);background-repeat: repeat-x;margin-left: 10px;margin-top: 10px;margin-right: 10px;margin-bottom: 10px;}.menu_util{font-size:8px;color:#666666;text-decoration:none;}.menu_util:hover{text-decoration:underline;}</style></head><body><div style=""width:100%; position:relative; cursor:default;"" align=""center""><div style=""width:600px; position:relative; text-align:left;""><table border=""0"" width=""600"" cellpadding=""0"" cellspacing=""0"" align=""center""><TR><TD width=""18""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_borda_01.gif"" width=""18"" height=""108"" /></TD><TD width=""564"" bgcolor=""#FFFFFF""><div style=""position:relative; left:0px; background:url(http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_topo.gif); width:513px; height:108px;""><BR /><BR /><table border=""0"" width=""100%"" cellpadding=""0"" cellspacing=""0""><TR><TD align=""center""><a href=""http://www.methodus.com.br""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_logo.gif"" width=""222"" height=""40"" border=""0"" /></a><BR /><font color=""#BBBBBB"" style=""font-size:12px;"">Aprimorando Talentos</font></TD><TD><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/bullet_01.gif"" width=""35"" height=""63"" border=""0"" align=""left"" hspace=""10"" /></TD><TD><div><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/b.gif"" width=""1"" height=""5"" /></div><font style=""color:#0a477e""><b style=""font-size:16px"">11 3288.2777</b><BR />Av. Paulista 2202  Cj 134<BR />S�o Paulo, SP<BR />Metr� Consola��o<BR /></font></TD><TD align=""right""><a href=""http://www.methodus.com.br"" class=""menu_util""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/menu_util_home.gif"" width=""11"" height=""11"" border=""0"" align=""absmiddle"" /> &nbsp; WEB SITE</a> </TD></TR></table></div></div>            	</TD><TD width=""18""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_borda_02.gif"" width=""18"" height=""108"" /></TD></TR></table></div><div style=""width:600px; position:relative; text-align:left; background-color:#FFFFFF;""><table border=""0"" width=""569"" cellpadding=""0"" cellspacing=""0"" align=""center""><TR><TD valign=""top"">"
	
	'strRodape 		= "</TD></TR></table></div><div style=""width:600px; position:relative; text-align:left; background-color:#FFFFFF; height:30px;""><div><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/b.gif"" width=""1"" height=""5"" /></div><table border=""0"" width=""95%"" cellpadding=""3"" cellspacing=""5"" align=""center""><TR><TD align=""center"" bgcolor=""#336699""><font style=""color:#FFFFFF; font-size:9px;"">Copyright 2008 | Todos os direitos reservados Methodus.</font></TD></TR></table></div><div style=""width:600px; position:relative; text-align:left;""><img src=""http://www.methodus.com.br/_ambiente_aula/methodus/_img/layout_rodape_borda.gif"" width=""600"" height=""17"" /></div></div></body></html>"
	
	strCabecalho 	= "<html><head><title>Methodus</title><style type=""text/css"">td, div, body { font-family:Arial, Helvetica, sans-serif; font-size:12px; }</style></head><body bgcolor=""#96c1c8""><div style=""background-color:#FFFFFF; width:600px; position:relative; text-align:left;"" align=""center""><div style=""position:relative; height:60px;""><div style=""position:relative; top:5px; left:10px; float:left; height:60px; width:250px; text-align:center;""><font style=""color:#203682; font-family:'Times New Roman'; font-size:34px; font-weight:bold; line-height:32px;"">METHODUS</font><BR /><font style=""color:#CCCCCC; font-family:'Arial'; font-size:13px; font-weight:bold; line-height:12px;"">Aprimorando Talentos</font><BR /><a href=""http://www.methodus.com.br""><font style=""color:#203682; font-family:Arial; font-size:10px; line-height:11px;"">www.methodus.com.br</font></a></div><div style=""position:relative; float:right; top:15px; right:20px; height:60px;""><font style=""color:#203682; font-family:Arial; font-size:12px;"">Av. Paulista 2202 Cj 134<BR />S�o Paulo, SP - Metr� Consola��o<BR /><b>11 3288 2777 / 3285 1052</b></font></div></div><div style=""position:relative; width:100%;""><div style=""padding:15px;"">"
	
	strRodape 		= "</div></div></div></body></html>"
	
	MONTA_EMAIL = strCabecalho & conteudo & strRodape

END FUNCTION
'''''''''''''''''''''''''''''''

'' Fun��es para enviar e-mail com CDONTS ou CDOSYS ''
SUB ENVIAR_EMAIL( de, para, assunto, mensagem )


	Set CDOMail = Server.CreateObject("CDO.Message")
	Set CDOCon = Server.CreateObject ("CDO.Configuration")
	
	'Host
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp.methodus.com.br"
	'Porta
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	'Porta do CDO
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	'SMTP Autenticado = 1
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	'Usu�rio
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = "info@methodus.com.br"
	'Senha
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "met123"
	'Timeout (segundos)
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
	CDOCon.Fields.update
	Set CDOMail.Configuration = CDOCon
	
	CDOMail.From 		= "Methodus <info@methodus.com.br>"
	CDOMail.To 			= para
	CDOMail.Subject 	= assunto
	CDOMail.HtmlBody 	= mensagem
	
	'Enviando
	CDOMail.Send
	
	'Finalizando
	Set CDOMail = Nothing
	Set CDOCon = Nothing




	'If TestObject("CDONTS.NewMail") Then
		'CDONTS
	'	Set EnviarMail = Server.CreateObject("CDONTS.NewMail")
	'	EnviarMail.Body = mensagem
	'	EnviarMail.Importance = 1
	'	EnviarMail.BodyFormat = 0
	'	EnviarMail.MailFormat = 0
	'else
		'CDOSYS
	'	Set EnviarMail = CreateObject("CDO.Message")
	'	EnviarMail.HtmlBody = mensagem
	'end if
	
	'EnviarMail.From = de
	'EnviarMail.To = para
	'EnviarMail.Subject = assunto
	
	'EnviarMail.Send
	'Set EnviarMail = Nothing
	
		'Set EnviarMail = Server.CreateObject("Persits.MailSender")
		'EnviarMail.Host = "mail.methodus.com.br"
		'EnviarMail.Username = "info@methodus.com.br"
		'EnviarMail.Password = "met123"
		'EnviarMail.From = "info@methodus.com.br"
		'EnviarMail.FromName = "Methodus"
		'EnviarMail.AddAddress para, para
		'EnviarMail.Subject = assunto
		'EnviarMail.IsHTML = True
		'EnviarMail.Body = mensagem
		'EnviarMail.Timestamp 	= Now()
		'On Error Resume Next
		'EnviarMail.Send
		
		'Set EnviarMail = nothing	
						
END SUB

Function TestObject( comIdentity )
	On Error Resume Next
	TestObject = False
	Err.Clear
	Set xTestObj = Server.CreateObject( comIdentity )
	If Err = 0 Then TestObject = True
	Set xTestObj = Nothing
	Err.clear
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para enviar e-mail com ASP MAIL ''
SUB ENVIAR_EMAIL_DEDICADO( para, assunto, mensagem )

	Set CDOMail = Server.CreateObject("CDO.Message")
	Set CDOCon = Server.CreateObject ("CDO.Configuration")
	
	'Host
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = dedicado_smtp '"66.135.61.51" '"smtp.cursoleituradinamica.com"
	'Porta
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	'Porta do CDO
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	'SMTP Autenticado = 1
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	'Usu�rio
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = dedicado_email '"info@cursoleituradinamica.com" '"postmaster@cursoleituradinamica.com"
	'Senha
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = dedicado_senha '"met1234"
	'Timeout (segundos)
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
	CDOCon.Fields.update
	Set CDOMail.Configuration = CDOCon
	
	CDOMail.From 		= "Methodus <"&dedicado_email&">"
	CDOMail.To 			= para
	CDOMail.Subject 	= assunto
	CDOMail.HtmlBody 	= mensagem
	
	'Enviando
	On Error Resume Next
	CDOMail.Send
	
	'Finalizando
	Set CDOMail = Nothing
	Set CDOCon = Nothing

	'Dim EnviarMail2
	
	'Set EnviarMail2 = Server.CreateObject("Persits.MailSender")
	'EnviarMail2.Host = "mail.methodus.com.br"
	'EnviarMail2.Username = "info@methodus.com.br"
	'EnviarMail2.Password = "met123"
	'EnviarMail2.From = "info@methodus.com.br"
	'EnviarMail2.FromName = "Methodus"
	'EnviarMail2.AddAddress para, "Aluno"
	'EnviarMail2.Subject = assunto
	'EnviarMail2.IsHTML = True
	'EnviarMail2.Body = mensagem
	'EnviarMail2.Timestamp 	= Now()
	'On Error Resume Next
	'EnviarMail2.Send
	
	'Set EnviarMail2 = nothing
						
END SUB
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para enviar e-mail com ASP MAIL ''
SUB ENVIAR_EMAIL_ANEXO( para, assunto, mensagem, anexo )


	Set CDOMail = Server.CreateObject("CDO.Message")
	Set CDOCon = Server.CreateObject ("CDO.Configuration")
	
	'Host
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp.methodus.com.br"
	'Porta
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	'Porta do CDO
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	'SMTP Autenticado = 1
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	'Usu�rio
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = "info@methodus.com.br"
	'Senha
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "met123"
	'Timeout (segundos)
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
	CDOCon.Fields.update
	Set CDOMail.Configuration = CDOCon
	
	CDOMail.From 		= "Methodus <info@methodus.com.br>"
	CDOMail.To 			= para
	if not anexo = empty then
		CDOMail.AddAttachment(anexo) 'anexa o arquivo
	end if
	CDOMail.Subject 	= assunto
	CDOMail.HtmlBody 	= mensagem
	
	'Enviando
	CDOMail.Send
	
	'Finalizando
	Set CDOMail = Nothing
	Set CDOCon = Nothing

	'Dim EnviarMail2
	
	'Set EnviarMail2 = Server.CreateObject("Persits.MailSender")
	'EnviarMail2.Host = "mail.methodus.com.br"
	'EnviarMail2.Username = "info@methodus.com.br"
	'EnviarMail2.Password = "met123"
	'EnviarMail2.From = "info@methodus.com.br"
	'EnviarMail2.FromName = "Methodus"
	'EnviarMail2.AddAddress para, "Aluno"
	'EnviarMail2.Subject = assunto
	
	'if not anexo = empty then
	'	EnviarMail2.AddAttachment anexo
	'end if
	
	'EnviarMail2.IsHTML = True
	'EnviarMail2.Body = mensagem
	'EnviarMail2.Timestamp 	= Now()
	'On Error Resume Next
	'EnviarMail2.Send
	
	'Set EnviarMail2 = nothing
						
END SUB
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��es para enviar e-mail com ASP MAIL ''
SUB ENVIAR_EMAIL_DEDICADO_ANEXO( para, assunto, mensagem, anexo )


	Set CDOMail = Server.CreateObject("CDO.Message")
	Set CDOCon = Server.CreateObject ("CDO.Configuration")
	
	'Host
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = dedicado_smtp '"66.135.61.51" '"smtp.cursoleituradinamica.com"
	'Porta
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	'Porta do CDO
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	'SMTP Autenticado = 1
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	'Usu�rio
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = dedicado_email '"info@cursoleituradinamica.com" '"postmaster@cursoleituradinamica.com"
	'Senha
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = dedicado_senha '"met1234"
	'Timeout (segundos)
	CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
	CDOCon.Fields.update
	Set CDOMail.Configuration = CDOCon
	
	CDOMail.From 		= "Methodus <"&dedicado_email&">"
	CDOMail.To 			= para

	if not anexo = empty then
		CDOMail.AddAttachment(anexo) 'anexa o arquivo
	end if
	CDOMail.Subject 	= assunto
	CDOMail.HtmlBody 	= mensagem
	
	'Enviando
	CDOMail.Send
	
	'Finalizando
	Set CDOMail = Nothing
	Set CDOCon = Nothing
						
END SUB
''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para formatar CNPJ ''
FUNCTION FORMATA_CNPJ(cnpj, condicao)

	if condicao = true then
	
		cnpj_novo = replace( replace( replace( cnpj, ".", ""), "-", ""), "/", "")
		
		FORMATA_CNPJ = cnpj_novo
	
	else
	
		cnpj = right( "0000000000000000"&cnpj, 15)
		
		cnpj_novo = mid(cnpj, 1, 3) & "." & mid(cnpj, 4, 3) & "." & mid(cnpj, 7, 3) & "/" & mid(cnpj, 10, 4) & "-" & mid(cnpj, 14, 2)
		
		FORMATA_CNPJ = cnpj_novo
		
	end if

END FUNCTION
'''''''''''''''''''''''''''''''

'' FUN��O PARA MONTAR A MALA-DIRETA DO CURSO ''
FUNCTION MAILING_CURSO( ID_curso )

	sql = 	"SELECT titulo_curso, ID_curso, resumo_curso, distancia_curso, mailing_curso, cor_curso FROM CURSOS where ID_curso="&ID_curso&""
	set rs_curso = conexao.execute(sql)
	
	if not rs_curso.EOF then
	
	str_mailing = rs_curso("mailing_curso")
	
	'' Se � curso a dist�ncia ''
	if rs_curso("distancia_curso") = true then
		
		'' montar os valores do curso ''
		sql = "select top 1 * from CURSOS_PLANOS where ID_curso="&ID_curso&" and status_plano="&verdade&" and empresarial_plano="&falso&" order by ID_plano"
		set rs_plano = conexao.execute(sql)
		
		str_mailing_valor = ""
		if not rs_plano.EOF then
		
			'str_mailing_valor = str_mailing_valor & "<div align=""center"">"
            'str_mailing_valor = str_mailing_valor & "    <strong style=""FONT-SIZE: 26px; COLOR: "&rs_curso("cor_curso")&""">R$ "&FORMATA_MOEDA( rs_plano("valor_plano"), false )&"</strong> "
            'str_mailing_valor = str_mailing_valor & "</div>"
        	
			str_mailing_valor = str_mailing_valor & ""
			
			
			str_mailing_valor = str_mailing_valor & "<div>"
            str_mailing_valor = str_mailing_valor & "<b>Investimento:</b> <font style=""FONT-SIZE: 18px; COLOR: "&rs_curso("cor_curso")&";""><b>R$ "&FORMATA_MOEDA( rs_plano("valor_plano"), false )&"</b></font><BR /><a href=""http://www.methodus.com.br/_ambiente_aula/methodus/investimento/cursos.asp?curso="&ID_curso&""">(fa�a seu pedido)</a><BR /><font style=""font-size:4px; color:#FFFFFF"">.</font><BR />"
            
			if isnull(rs_plano("desconto1_plano"))=false then
            	str_mailing_valor = str_mailing_valor & "� vista "&rs_plano("desconto1_plano")&"% de desconto: <b>R$ "&FORMATA_MOEDA( (rs_plano("valor_plano")-( (rs_plano("valor_plano")*rs_plano("desconto1_plano"))/100 )), false )&"</b>"
            end if
                
            str_mailing_valor = str_mailing_valor & "<BR />a prazo, at� "&rs_plano("parcelas_plano")&" vezes sem juros<BR /><font color="""&rs_curso("cor_curso")&""" style=""font-size:10px;"">"
            
            if rs_plano("parcelas_plano") > 1 then

            	for i=2 to rs_plano("parcelas_plano")

                	str_mailing_valor = str_mailing_valor & ""&i&" x "&FORMATA_MOEDA( ( rs_plano("valor_plano") / i ), false )&" / "
                    
                next
                
            end if

            str_mailing_valor = str_mailing_valor & "</font></div>"
			
		end if
		''''''''''''''''''''''''''''''''
		
		str_mailing = replace( str_mailing, "[VALOR]", str_mailing_valor )
		
	
	'' se � curso presencial ''	
	else
		
		'' montar as datas do curso ''
		sql = "select top 5 * from CURSOS_DATAS where ID_curso="&rs_curso("ID_curso")&" and inicio_data>='"&DATA_BD(dateadd("d",-1,now()))&"' order by inicio_data"
		set rs_data= conexao.execute(sql)
		
		str_mailing_data = ""
		do while not rs_data.EOF
			
        	str_mailing_data = str_mailing_data & "<font color="""&rs_curso("cor_curso")&"""><b>&gt;&gt;</b></font> "&rs_data("horario_data")&"<BR /><div style=""padding-left:18px;""><b>"&FORMATA_DATA( rs_data("inicio_data"), 1 )&"</b> � <b>"&FORMATA_DATA( rs_data("fim_data"), 1 )&"</b></div>"
        	
		rs_data.movenext:loop
		''''''''''''''''''''''''''''''
		
		'' montar os valores do curso ''
		sql = "select top 1 * from CURSOS_PLANOS where ID_curso="&ID_curso&" and status_plano="&verdade&" and empresarial_plano="&falso&" order by ID_plano"
		set rs_plano = conexao.execute(sql)
		
		str_mailing_valor = ""
		if not rs_plano.EOF then
		
       		str_mailing_valor = str_mailing_valor & ""
			
			
			str_mailing_valor = str_mailing_valor & "<div>"
            str_mailing_valor = str_mailing_valor & "<b>Investimento:</b> <font style=""FONT-SIZE: 18px; COLOR: "&rs_curso("cor_curso")&";""><b>R$ "&FORMATA_MOEDA( rs_plano("valor_plano"), false )&"</b></font> <a href=""http://www.methodus.com.br/_ambiente_aula/methodus/investimento/cursos.asp?curso="&ID_curso&""">(fa�a sua inscri��o)</a><BR /><font style=""font-size:4px; color:#FFFFFF"">.</font><BR />"
            
			if isnull(rs_plano("desconto1_plano"))=false then
            	str_mailing_valor = str_mailing_valor & "� vista "&rs_plano("desconto1_plano")&"% de desconto: <b>R$ "&FORMATA_MOEDA( (rs_plano("valor_plano")-( (rs_plano("valor_plano")*rs_plano("desconto1_plano"))/100 )), false )&"</b>"
            end if
                
            str_mailing_valor = str_mailing_valor & "<BR />a prazo, at� "&rs_plano("parcelas_plano")&" vezes sem juros<BR /><font color="""&rs_curso("cor_curso")&""" style=""font-size:10px;"">"
            
            if rs_plano("parcelas_plano") > 1 then

            	for i=2 to rs_plano("parcelas_plano")

                	str_mailing_valor = str_mailing_valor & ""&i&" x "&FORMATA_MOEDA( ( rs_plano("valor_plano") / i ), false )&" / "
                    
                next
                
            end if

            str_mailing_valor = str_mailing_valor & "</font></div>"
            
            if isnull(rs_plano("desconto2_plano"))=false then
                str_mailing_valor = str_mailing_valor & "<div align=""center""><font style=""font-size:10px;"">Obs.: Para mais de uma pessoa, "&rs_plano("desconto2_plano")&"% de desconto � vista ou "&rs_plano("desconto3_plano")&"% de desconto a prazo</font></div>"
            end if
        
		end if
		''''''''''''''''''''''''''''''''
		
		str_mailing = replace( str_mailing, "[DATAS]", str_mailing_data )
		str_mailing = replace( str_mailing, "[VALOR]", str_mailing_valor )
		
	end if
	
	'' para pegar �ltimos artigos ''
	if instr( 1, str_mailing, "[ARTIGOS:", 1 ) > 0 then
	
		'' Leitura dinamica
		if instr( 1, str_mailing, "[ARTIGOS:1]", 1 ) > 0 then
			sql = "select top 3 ID_artigo, titulo_artigo from ARTIGOS where ID_tema=1 and status_artigo="&verdade&" and data_artigo<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_artigo desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_artigo = ""
			do while not rsp.EOF
				str_mailing_artigo = str_mailing_artigo & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/_ambiente_aula/methodus/artigos/detalhes.asp?ID="&rsp("ID_artigo")&""">"&rsp("titulo_artigo")&"</a><br />"
			rsp.movenext:loop
		
			str_mailing = replace( str_mailing, "[ARTIGOS:1]", str_mailing_artigo )
		end if
		
		
		'' Comunica��es
		if instr( 1, str_mailing, "[ARTIGOS:2]", 1 ) > 0 then
			sql = "select top 3 ID_artigo, titulo_artigo from ARTIGOS where ID_tema=2 and status_artigo="&verdade&" and data_artigo<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_artigo desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_artigo = ""
			do while not rsp.EOF
				str_mailing_artigo = str_mailing_artigo & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/_ambiente_aula/methodus/artigos/detalhes.asp?ID="&rsp("ID_artigo")&""">"&rsp("titulo_artigo")&"</a><br />"
			rsp.movenext:loop
		
			str_mailing = replace( str_mailing, "[ARTIGOS:2]", str_mailing_artigo )
		end if
		
		'' Gerenciamento
		if instr( 1, str_mailing, "[ARTIGOS:3]", 1 ) > 0 then
			sql = "select top 3 ID_artigo, titulo_artigo from ARTIGOS where ID_tema=3 and status_artigo="&verdade&" and data_artigo<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_artigo desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_artigo = ""
			do while not rsp.EOF
				str_mailing_artigo = str_mailing_artigo & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/_ambiente_aula/methodus/artigos/detalhes.asp?ID="&rsp("ID_artigo")&""">"&rsp("titulo_artigo")&"</a><br />"
			rsp.movenext:loop
		
			str_mailing = replace( str_mailing, "[ARTIGOS:3]", str_mailing_artigo )
		end if
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimas noticias ''
	if instr( 1, str_mailing, "[NOTICIAS]", 1 ) > 0 then
		
		sql = "select top 3 ID_noticia, titulo_noticia from NOTICIAS where status_noticia="&verdade&" and data_noticia<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_noticia desc"
		set rsp = conexao.execute(sql)
		
		do while not rsp.EOF
			str_mailing_noticia = str_mailing_noticia & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/_ambiente_aula/methodus/noticias/detalhes.asp?ID="&rsp("ID_noticia")&""">"&rsp("titulo_noticia")&"</a><br />"
		rsp.movenext:loop
		
		str_mailing = replace( str_mailing, "[NOTICIAS]", str_mailing_noticia )
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimas noticias ''
	if instr( 1, str_mailing, "[ARTIGOS_CARREIRAS]", 1 ) > 0 then
		
		sql = "select top 3 ID_artigo, titulo_artigo from ARTIGOS_CARREIRAS where status_artigo="&verdade&" and data_artigo<='"&DATA_BD(now())&"' and mm="&verdade&" order by data_artigo desc"
		set rsp = conexao.execute(sql)
		
		do while not rsp.EOF
			str_mailing_artigoc = str_mailing_artigoc & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/_ambiente_aula/methodus/artigos_carreiras/detalhes.asp?ID="&rsp("ID_artigo")&""">"&rsp("titulo_artigo")&"</a><br />"
		rsp.movenext:loop
		
		str_mailing = replace( str_mailing, "[ARTIGOS_CARREIRAS]", str_mailing_artigoc )
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimas noticias ''
	if instr( 1, str_mailing, "[LINKS]", 1 ) > 0 then
		
		sql = "select top 3 ID_link, url_link, titulo_link from LINKS where status_link="&verdade&" and mm="&verdade&" order by data_link desc"
		set rsp = conexao.execute(sql)
		
		do while not rsp.EOF
			str_mailing_link = str_mailing_link & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href="""&rsp("url_link")&""">"&rsp("titulo_link")&"</a><br />"
		rsp.movenext:loop
		
		str_mailing = replace( str_mailing, "[LINKS]", str_mailing_link )
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimos livros ''
	if instr( 1, str_mailing, "[LIVROS:", 1 ) > 0 then
	
		'' Leitura dinamica
		if instr( 1, str_mailing, "[LIVROS:1]", 1 ) > 0 then
			sql = "select top 3 ID_livro, titulo_livro, corpo_livro from LIVROS where ID_tema=1 and status_livro="&verdade&" and mm="&verdade&" order by data_livro desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <b style=""font-size:9px;"">"&rsp("titulo_livro")&"</b><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[LIVROS:1]", str_mailing_livro )
		end if
			
		'' Comunica��es
		if instr( 1, str_mailing, "[LIVROS:2]", 1 ) > 0 then
			sql = "select top 3 ID_livro, titulo_livro, corpo_livro from LIVROS where ID_tema=2 and status_livro="&verdade&" and mm="&verdade&" order by data_livro desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <b style=""font-size:9px;"">"&rsp("titulo_livro")&"</b><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[LIVROS:2]", str_mailing_livro )
		end if
		
		'' Gerenciamento
		if instr( 1, str_mailing, "[LIVROS:3]", 1 ) > 0 then
			sql = "select top 3 ID_livro, titulo_livro, corpo_livro from LIVROS where ID_tema=3 and status_livro="&verdade&" and mm="&verdade&" order by data_livro desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <b style=""font-size:9px;"">"&rsp("titulo_livro")&"</b><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[LIVROS:3]", str_mailing_livro )
		end if
		
	end if
	'''''''''''''''''''''''''''''''
	
	'' para pegar �ltimos Videos ''
	if instr( 1, str_mailing, "[VIDEOS:", 1 ) > 0 then
	
		'' Leitura dinamica
		if instr( 1, str_mailing, "[VIDEOS:1]", 1 ) > 0 then
			sql = "select top 3 ID_video, titulo_video from VIDEOS where ID_tema=1 and status_video="&verdade&" and chamada_video<>'' order by data_video desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/_ambiente_aula/methodus/videos/detalhes.asp?ID="&rsp("ID_video")&""">"&rsp("titulo_video")&"</a><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[VIDEOS:1]", str_mailing_livro )
		end if
			
		'' Comunica��es
		if instr( 1, str_mailing, "[VIDEOS:2]", 1 ) > 0 then
			sql = "select top 3 ID_video, titulo_video from VIDEOS where ID_tema=2 and status_video="&verdade&" and chamada_video<>'' order by data_video desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/_ambiente_aula/methodus/videos/detalhes.asp?ID="&rsp("ID_video")&""">"&rsp("titulo_video")&"</a><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[VIDEOS:2]", str_mailing_livro )
		end if
		
		'' Gerenciamento
		if instr( 1, str_mailing, "[VIDEOS:3]", 1 ) > 0 then
			sql = "select top 3 ID_video, titulo_video from VIDEOS where ID_tema=3 and status_video="&verdade&" and chamada_video<>'' order by data_video desc"
			set rsp = conexao.execute(sql)
			
			str_mailing_livro = ""
			do while not rsp.EOF
				str_mailing_livro = str_mailing_livro & "<font color=""#ffc000"">&gt;</font> <a style=""font-size:9px;"" href=""http://www.methodus.com.br/_ambiente_aula/methodus/videos/detalhes.asp?ID="&rsp("ID_video")&""">"&rsp("titulo_video")&"</a><BR />"
			rsp.movenext:loop
			
			str_mailing = replace( str_mailing, "[VIDEOS:3]", str_mailing_livro )
		end if
		
	end if
	'''''''''''''''''''''''''''''''
	
	
	
	MAILING_CURSO = str_mailing
	
	end if

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''


'' FUN��O PARA ENVIAR A MALA-DIRETA DO CURSO PARA ALUNO EM HOR�RIO DETERMINADO ''
FUNCTION ENVIA_MAILING_CURSO( ID_curso, nome, email )

	tempo_atraso = 20

	ID_curso = split( ID_curso, "," )
	
	for i=0 to ubound(ID_curso)

		sql = 	"SELECT titulo_curso FROM CURSOS where ID_curso="&ID_curso(i)&""
		set rs_curso = conexao.execute(sql)
		
		if not rs_curso.EOF then
			
			titulo_curso 	= rs_curso("titulo_curso")
			assunto			= titulo_curso
			mensagem 		= ( MONTA_EMAIL( "<b>"&nome&"</b>, seguem informa��es relevantes<BR />sobre o treinamento de <b>"&titulo_curso&"</b><BR /><BR />"& MAILING_CURSO( ID_curso(i) ) ) )
		
			'call ENVIAR_EMAIL( email_01, email, assunto, mensagem )
			
			'Set Mail = Server.CreateObject("Persits.MailSender")
			'Mail.Host = "mail.methodus.com.br"
			'Mail.Username = "info@methodus.com.br"
			'Mail.Password = "met123"
			'Mail.From = "info@methodus.com.br"
			'Mail.FromName = "Methodus"
			'Mail.AddAddress email, nome
			'Mail.Subject = assunto
			'Mail.IsHTML = True
			'Mail.Body = mensagem
			'Mail.Queue = True
					'Mail.Timestamp 	= Now() + (1/24)
					'Mail.Timestamp 	= Now()
			'Mail.Timestamp 	= DateAdd( "n", (tempo_atraso+i), Now() )
			'On Error Resume Next
			'Mail.Send
			
			'Set Mail = nothing	
			
			
			Set CDOMail = Server.CreateObject("CDO.Message")
			Set CDOCon = Server.CreateObject ("CDO.Configuration")
			
			'Host
			CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp.methodus.com.br"
			'Porta
			CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			'Porta do CDO
			CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			'SMTP Autenticado = 1
			CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
			'Usu�rio
			CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = "info@methodus.com.br"
			'Senha
			CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "met123"
			'Timeout (segundos)
			CDOCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
			CDOCon.Fields.update
			Set CDOMail.Configuration = CDOCon
			
			CDOMail.From 		= "Methodus <info@methodus.com.br>"
			CDOMail.To 			= nome &"<"& email &">"
			CDOMail.Subject 	= assunto
			CDOMail.HtmlBody 	= mensagem
			
			'Enviando
			CDOMail.Send
			
			'Finalizando
			Set CDOMail = Nothing
			Set CDOCon = Nothing
			
		
		end if
	
	next
	
END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' FUN��O PARA LIBERAR ACESSO DO USU�RIO AO CURSO '''''''''''''''''''''''''''''''
SUB LIBERAR_ACESSO( ID_inscricao )

	sql="select * from CURSOS_INSCRICOES where ID_inscricao="&ID_inscricao
	set rs = conexao.execute(sql)
	
	if not rs.EOF then
	
		'' descobrir qual � o �ltimo n�mero de inscri��o ''
		if isnull(rs("numero_inscricao")) then
			sql = "select top 1 * from CURSOS_INSCRICOES where numero_inscricao is not null order by numero_inscricao desc"
			set rs_ultimo = conexao.execute(sql)
			if not rs_ultimo.EOF then
				numero_inscricao = rs_ultimo("numero_inscricao")+1
			else
				numero_inscricao = 1
			end if
			
			sql="update CURSOS_INSCRICOES set numero_inscricao="&numero_inscricao&" where ID_inscricao="&rs("ID_inscricao")
			conexao.execute(sql)
		end if
	
		'' Atualizar a inscri��o ''
		sql="update CURSOS_INSCRICOES set pago_inscricao="&verdade&" where ID_inscricao="&rs("ID_inscricao")
		conexao.execute(sql)
		'''''''''''''''''''''''''''
	
		ID_curso 	= rs("ID_curso")
		data_inicio	= DATA_BD( now() )
		data_fim	= DATA_BD( dateadd( "m", 12, now() ) )
		
		sql = "SELECT USUARIOS.ID_usuario FROM CURSOS_INSCRICOES INNER JOIN CURSOS_INSCRICOES_USUARIOS ON "&_
			  "CURSOS_INSCRICOES.ID_inscricao = CURSOS_INSCRICOES_USUARIOS.ID_inscricao INNER JOIN "&_
			  "USUARIOS ON CURSOS_INSCRICOES_USUARIOS.ID_usuario = USUARIOS.ID_usuario "&_
			  "where CURSOS_INSCRICOES_USUARIOS.ID_inscricao="&rs("ID_inscricao")
		set rs_users = conexao.execute(sql)
		
		do while not rs_users.EOF
		
			ID_usuario = rs_users("ID_usuario")
		
			sql="select ID_usuario_curso from USUARIOS_CURSOS where ID_usuario="&ID_usuario&" and ID_curso="&ID_curso
			set rs= conexao.execute(sql)
			
			if not rs.EOF then
				
				sql="update USUARIOS_CURSOS set inicio_usuario_curso='"&data_inicio&"', fim_usuario_curso='"&data_fim&"', status_usuario_curso="&verdade&""&_
					" where ID_usuario_curso="&rs("ID_usuario_curso")
				conexao.execute(sql)
				
			else
			
				sql="insert into USUARIOS_CURSOS (ID_usuario, ID_curso, inicio_usuario_curso, fim_usuario_curso, status_usuario_curso)"&_
					" values ("&ID_usuario&","&ID_curso&",'"&data_inicio&"','"&data_fim&"',"&verdade&")"
				conexao.execute(sql)
			
			end if
		
		rs_users.movenext:loop
		
	end if
	
END SUB
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'' Fun��o para exibir detalhes das op��es e formas de pagamento da I-Pagare '''''
FUNCTION IPAGARE_RETORNO( tipo, condicao )

	select case tipo
		
		case "meio"
		
			select case condicao
				case "1"
					IPAGARE_RETORNO = "Cart�o de Cr�dito - Diners"
				case "2"
					IPAGARE_RETORNO = "Cart�o de Cr�dito - Mastercard"
				case "7"
					IPAGARE_RETORNO = "Cart�o de Cr�dito - Visa"
				case "11"
					IPAGARE_RETORNO = "Cart�o de Cr�dito - American Express"
				case "6"
					IPAGARE_RETORNO = "Boleto Banc�rio Bradesco"
				case "0"
					IPAGARE_RETORNO = "Cheques"
				case "13"
					IPAGARE_RETORNO = "Dep�sito Banc�rio Ita�"
				case "4"
					IPAGARE_RETORNO = "Dep�sito Banc�rio Bradesco"
				case "100"
					IPAGARE_RETORNO = "Dinheiro"
			end select
		
		
		case "forma"
		
			IPAGARE_RETORNO = replace( replace( condicao, "A", "" ), "B", "" )
		
			if left( condicao, 1 ) = "A" then
				IPAGARE_RETORNO = IPAGARE_RETORNO & "x sem juros"
			elseif left( condicao, 1 ) = "B" then
				IPAGARE_RETORNO = IPAGARE_RETORNO & "x com juros"
			end if
			
		
		case "status"
		
			select case condicao
				case "1"
					IPAGARE_RETORNO = "Aguardando Pagamento"
				case "2"
					IPAGARE_RETORNO = "Aguardando confirma��o de pagamento"
				case "3"
					IPAGARE_RETORNO = "Pagamento confirmado"
				case "4"
					IPAGARE_RETORNO = "Cancelado"
				case "5"
					IPAGARE_RETORNO = "Pagamento expirado"
				case "6"
					IPAGARE_RETORNO = "Parcialmente Pago"	
			end select
		
	
	end select

END FUNCTION
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%>