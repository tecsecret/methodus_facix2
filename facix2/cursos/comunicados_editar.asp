<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar Comunicado@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>
	<!-- #INCLUDE file="../_FCKeditor/fckeditor.asp" -->


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

function checa(formulario)
{
	campo=[
			formulario.titulo,
			formulario.dia1,
			formulario.mes1,
			formulario.ano1,
			formulario.dia2,
			formulario.mes2,
			formulario.ano2
		  ];
	n_campo=[
			"T�tulo",
			"Data de in�cio da exibi��o",
			"Data de t�rmino da exibi��o"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
		
	if (checa_nulo(campo[1],n_campo[1])==false)
		return(false);
	if (checa_caracter(campo[1],n_campo[1],"1234567890")==false)
		return(false);
	if (checa_nulo(campo[2],n_campo[1])==false)
		return(false);
	if (checa_caracter(campo[2],n_campo[1],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[3],n_campo[1],4)==false)
		return(false);
	if (checa_caracter(campo[3],n_campo[1],"1234567890")==false)
		return(false);
		
	if (checa_nulo(campo[4],n_campo[2])==false)
		return(false);
	if (checa_caracter(campo[4],n_campo[2],"1234567890")==false)
		return(false);
	if (checa_nulo(campo[5],n_campo[2])==false)
		return(false);
	if (checa_caracter(campo[5],n_campo[2],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[6],n_campo[2],4)==false)
		return(false);
	if (checa_caracter(campo[6],n_campo[2],"1234567890")==false)
		return(false);
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID	=	formatar("ID",1,2)

sql = 	"select * from COMUNICADOS where ID_comunicado="&ID
set rs = conexao.execute(sql)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_editar" method="post" action="comunicados_acoes.asp?acao=editar" onSubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
	<input type="hidden" name="ID_curso" value="<%=rs("ID_curso")%>" />
	<tr>
		<td class="titulo_02" width="50%"><b>T�tulo</b></td>
		<td class="titulo_02" width="50%"><b>Status</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="titulo" style="width:100%" value="<%=rs("titulo_comunicado")%>" />
		</td>
		<td>
			<input type="radio" name="ativo" value="<%=verdade%>"<%if rs("status_comunicado")=true then%> checked<%end if%> /> Ativo 
			<input type="radio" name="ativo" value="<%=falso%>"<%if rs("status_comunicado")=false then%> checked<%end if%> /> Inativo
		</td>
	</tr>
	<TR>
		<TD width="50%"><b>Data de in�cio da exibi��o</b></TD>
		<TD width="50%"><b>Data de t�rmino da exibi��o</b></TD>
	</TR>
	<TR>
		<TD>
			<input type="text" name="dia1" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=day( rs("inicio_comunicado") )%>" /> / 
			<input type="text" name="mes1" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=month( rs("inicio_comunicado") )%>" /> / 
			<input type="text" name="ano1" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=year( rs("inicio_comunicado") )%>" />
		</TD>
		<TD>
			<input type="text" name="dia2" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=day( rs("fim_comunicado") )%>" /> / 
			<input type="text" name="mes2" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=month( rs("fim_comunicado") )%>" /> / 
			<input type="text" name="ano2" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=year( rs("fim_comunicado") )%>" />
		</TD>
	</TR>
	<tr>
		<td class="titulo_02" colspan="2"><b>Conte�do</b></td>
	</tr>
	<tr>
		<td colspan="2">
			<%
			Dim oFCKeditor
			Set oFCKeditor = New FCKeditor
			oFCKeditor.BasePath = "../_FCKeditor/"
			oFCKeditor.ToolbarSet = "Padrao"
			oFCKeditor.Height = "240"
			oFCKeditor.Value = rs("corpo_comunicado")
			
			oFCKeditor.Create "corpo"
			%>	
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Editar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->