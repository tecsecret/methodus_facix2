<!-- #include file="../_base/config.asp" -->
<!-- #INCLUDE FILE = "../_base/componente_upload.asp" -->
<%
call checalogado()


'' Vari�veis do componente de Upload ''
Dim oFO, oProps, oFile, i, item, oMyName
Set oFO = New FileUpload

Set oProps = oFO.GetUploadSettings
with oProps

	.Extensions = Array("swf", "dcr", "dir")
	'.UploadDirectory = Server.Mappath("../downloads/arquivos/")
	.AllowOverWrite = true
	.MaximumFileSize = 500000000  '500 Mb
	.MininumFileSize = 1     '1 bytes
	.UploadDisabled = false
	
End with

oFO.ProcessUpload
'''''''''''''''''''''''''''''''''''''''



acao 			= 	formatar("acao",1,2)
condicao		=	formatar("condicao",1,2)

titulo 			=	formatar(oFO.Form("titulo"),1,4)
ID_nivel		=	formatar(oFO.Form("ID_nivel"),1,4)
corpo			=	formatar(oFO.Form("corpo"),3,4)
tempo			=	formatar(oFO.Form("tempo"),1,4)
ativo 			=	formatar(oFO.Form("ativo"),1,4)
del_imagem 		=	formatar(oFO.Form("del_imagem"),1,4)


ID 				= 	formatar("ID",1,2)
if ID=empty then
	ID 			= 	formatar(oFO.Form("ID"),1,4)
end if


'INCLUS�O
if acao="incluir" then

	if titulo=empty or ativo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	
	'' para descobrir a posi��o ''
	sql = "select top 1 ordem_exercicio from LEITURA_EXERCICIOS where ID_nivel = "&ID_nivel&" order by ordem_exercicio desc"
	set rs = conexao.execute(sql)
	
	if not rs.EOF then
		ordem = rs("ordem_exercicio") + 1
	else
		ordem = 1
	end if
	''''''''''''''''''''''''''''''
	
	sql="insert into LEITURA_EXERCICIOS (ID_nivel,titulo_exercicio,conteudo_exercicio,ordem_exercicio,tempo_exercicio,status_exercicio)"&_
		" values ("&ID_nivel&",'"&titulo&"','"&corpo&"',"&ordem&","&tempo&","&ativo&") select @@identity as NovoId"
	set rs = conexao.execute(sql).nextrecordset
	
	ID_exercicio = rs("NovoId")
	
	'' Cria uma pasta com nome do ID do Produto para gravar arquivos relacionados ''
	Set FSO = Server.CreateObject("Scripting.FileSystemObject")
	FSO.CreateFolder Server.Mappath("..\..\asa\_arquivos_exercicios\"&ID_exercicio)
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	'' Para fazer upload da imagem ''
	set oFile 				= oFO.File("arquivo")
	'oFile.FileName			= titulo&"."&oFile.FileExtension
	nome_arquivo 			= oFile.FileName
	oProps.UploadDirectory 	= Server.Mappath("../../asa/_arquivos_exercicios/"&ID_exercicio&"/")
	
	if lcase(oFile.FileExtension) <> "dcr" and lcase(oFile.FileExtension) <> "swf" and lcase(oFile.FileExtension) <> "dir" then
		'erro de extens�o
	else
		oFile.SaveAsFile
		
		sql="update LEITURA_EXERCICIOS set arquivo_exercicio='"&nome_arquivo&"' where ID_exercicio="&ID_exercicio
		conexao.execute(sql)
		
	end if
	'''''''''''''''''''''''''''''''''

	session("sucesso") = "0001"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	
'EXCLUIR
elseif acao="excluir" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if
	
	
	sql = "select ID_nivel from LEITURA_EXERCICIOS where ID_exercicio="&ID
	set rs = conexao.execute(sql)
	ID_nivel = rs("ID_nivel")
	
	
	'' deletar ''
	Set FSO = Server.CreateObject("Scripting.FileSystemObject")
	FSO.DeleteFolder Server.Mappath("../../asa/_arquivos_exercicios/"&ID), false
	
	sql="delete from LEITURA_EXERCICIOS where ID_exercicio="&ID
	conexao.execute(sql)
	'''''''''''''
	
	'' reorganizar a ordem ''
	sql = "select ID_exercicio from LEITURA_EXERCICIOS where ID_nivel="&ID_nivel&" order by ordem_exercicio"
	set rs = conexao.execute(sql)
	
	i = 1
	do while not rs.EOF
	
		sql = "update LEITURA_EXERCICIOS set ordem_exercicio="&i&" where ID_exercicio="&rs("ID_exercicio")
		conexao.execute(sql)
		
		i = i + 1
	
	rs.movenext:loop
	'''''''''''''''''''''''''
	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))


'EDITAR
elseif acao="editar" then

	if ID=empty or titulo=empty or ativo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	
	sql="update LEITURA_EXERCICIOS set titulo_exercicio='"&titulo&"',conteudo_exercicio='"&corpo&"',tempo_exercicio="&tempo&",status_exercicio="&ativo&""&_
		" where ID_exercicio="&ID
	conexao.execute(sql)
	
	ID_exercicio = ID
	
	Set FSO = Server.CreateObject("Scripting.FileSystemObject")
	
	
		if del_imagem = "1" then
			sql="select arquivo_exercicio from LEITURA_EXERCICIOS where ID_exercicio="&ID_exercicio
			set rs= conexao.execute(sql)
			
			if not rs("arquivo_exercicio")=empty then
				FSO.DeleteFile Server.Mappath("../../asa/_arquivos_exercicios/"&ID_exercicio&"/"&rs("arquivo_exercicio"))
			
				sql="update LEITURA_EXERCICIOS set arquivo_exercicio=null where ID_exercicio="&ID_exercicio
				conexao.execute(sql)
			end if
		end if
	
	
	'' Para fazer upload da imagem ''
	set oFile 				= oFO.File("arquivo")
	'oFile.FileName			= titulo&"."&oFile.FileExtension
	nome_arquivo 			= oFile.FileName
	oProps.UploadDirectory 	= Server.Mappath("../../asa/_arquivos_exercicios/"&ID_exercicio&"/")
	
	if not nome_arquivo=empty then
	
	if lcase(oFile.FileExtension) <> "swf" and lcase(oFile.FileExtension) <> "dcr" and lcase(oFile.FileExtension) <> "dir" then
		'erro de extens�o
	else
		sql="select arquivo_exercicio from LEITURA_EXERCICIOS where ID_exercicio="&ID_exercicio
		set rs= conexao.execute(sql)
		
		if not rs("arquivo_exercicio")=empty then
			FSO.DeleteFile Server.Mappath("../../asa/_arquivos_exercicios/"&ID_exercicio&"/"&rs("arquivo_exercicio"))
		end if
	
		oFile.SaveAsFile

		sql="update LEITURA_EXERCICIOS set arquivo_exercicio='"&nome_arquivo&"' where ID_exercicio="&ID_exercicio
		conexao.execute(sql)
	end if	
	
	end if
	'''''''''''''''''''''''''''''''''
	
	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	
	
'MOVER
elseif acao="mover" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql="select * from LEITURA_EXERCICIOS where ID_exercicio="&ID
	set rs= conexao.execute(sql)
	
	posicao_atual 		= rs("ordem_exercicio")
	
	if condicao="subir" then
		nova_posicao	= posicao_atual-1
	else
		nova_posicao	= posicao_atual+1
	end if
	
	sql="select ID_exercicio from LEITURA_EXERCICIOS where ID_nivel="&rs("ID_nivel")&" and ordem_exercicio="&nova_posicao
	set rs= conexao.execute(sql)
	
	if not rs.EOF then
	
		sql="update LEITURA_EXERCICIOS set ordem_exercicio="&nova_posicao&" where ID_exercicio="&ID
		conexao.execute(sql)
	
		sql="update LEITURA_EXERCICIOS set ordem_exercicio="&posicao_atual&" where ID_exercicio="&rs("ID_exercicio")
		conexao.execute(sql)
		
	end if

	'session("sucesso") = "0002"
	response.Redirect(session("voltar"))
	

'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>