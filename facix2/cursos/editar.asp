<%@ codepage="1252" language="VBScript" %>
<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar Curso@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>
	<script src="../_ckeditor/ckeditor.js"></script>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE CURSOS
function checa(formulario)
{
	campo=[
			formulario.titulo
		  ];
	n_campo=[
			"T�tulo"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
		
	formulario.botao_enviar.disabled = true;
	formulario.botao_enviar.value = "Processando";
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>


<link rel="stylesheet" type="text/css" href="../_base/css/colorpicker.css" />
<script type="text/javascript" src="../_base/js/colorpicker.js"></script>

<script type="text/javascript">

window.onload = function() {
	var cp = new ColorPicker(findPosX(document.getElementById("cor")), (findPosY(document.getElementById("cor"))-100) );
	//cp.setVisible(true);
	cp.onChange = function() {
		a_incluir.cor.value = cp.getColor();
		document.getElementById("div_cor").style.background = cp.getColor();
		//document.getElementsByTagName("cor")[0].style.background = cp.getColor();
		//window.alert("Cor escolhida: "+cp.getColor());
	}
	document.getElementById("abre").onmousedown = function() {
		cp.setVisible(true);
	}
}

</script>




<%
ID 	= 	formatar("ID",1,2)

sql = 	"select * from CURSOS where ID_curso="&ID
set rs = conexao.execute(sql)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form ENCTYPE="multipart/form-data" name="a_incluir" method="post" action="acoes.asp?acao=editar" onSubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
	<tr>
		<td><b>T�tulo</b></td>
        <td class="titulo_02" width="50%"><b>Tema</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="titulo" style="width:100%" value="<%=Server.HTMLEncode(rs("titulo_curso"))%>" />
		</td>
        <td>
			<%
			sql="select titulo_tema, ID_tema from TEMAS where status_tema="&verdade&""
			set rs_tema= conexao.execute(sql)
			%>
			<select name="tema" style="width:100%">
				<option value="null">Sem Categoria</option>
				<%do while not rs_tema.EOF%>
					<option value="<%=rs_tema("ID_tema")%>"<%if rs_tema("ID_tema")=rs("ID_tema") then%> selected="selected"<%end if%>><%=rs_tema("titulo_tema")%></option>
				<%rs_tema.movenext:loop%>
			</select>
		</td>
	</tr>
    <tr>
		<td class="titulo_02" colspan="2"><b>Texto da campanha atual</b></td>
	</tr>
	<tr>
		<td colspan="2">
            <textarea id="editor1" name="resumo"><%=rs("resumo_curso")%></textarea>
			<script>
                CKEDITOR.replace( 'editor1', {
                    height: '140px',
                    toolbar: 'Basico'			
                });
            </script>
		</td>
	</tr>
	<tr>
		<td class="titulo_02" colspan="2"><b>Descri��o</b></td>
	</tr>
	<tr>
		<td colspan="2">
            <textarea id="editor2" name="corpo"><%=rs("conteudo_curso")%></textarea>
			<script>
                CKEDITOR.replace( 'editor2', {
                    height: '240px',
                    toolbar: 'Padrao'			
                });
            </script>
		</td>
	</tr>
    <tr>
		<td class="titulo_02" colspan="2"><b>Mailing</b></td>
	</tr>
	<tr>
		<td colspan="2">
            <textarea id="editor3" name="mailing"><%=rs("mailing_curso")%></textarea>
			<script>
                CKEDITOR.replace( 'editor3', {
                    height: '240px',
                    toolbar: 'Padrao'			
                });
            </script>
		</td>
	</tr>
	<tr>
		<td class="titulo_02" width="50%"><b>Tipo</b></td>
		<td class="titulo_02"><b>Status</b></td>
        
	</tr>
	<tr>
		<td>
			<select name="tipo" style="width:100%;">
			<%
			call CONEXAO_XML("../_xml/tipos_cursos.xml")
			set tipos = xmlDoc.documentElement
			
			if tipos.hasChildNodes() then
			for each registro in tipos.childNodes
				set codigo = registro.selectSingleNode("codigo")
 				set titulo = registro.selectSingleNode("titulo")
			%>
				<option value="<%=codigo.text%>"<%if cstr( codigo.text ) = cstr( rs("tipo_curso") ) then%> selected="selected"<%end if%>><%=Server.HTMLEncode( titulo.text )%></option>
			<%
			next
			end if
			%>
			</select>
		</td>
		<!--<input type="hidden" name="tipo" value="<%=rs("tipo_curso")%>" />-->
		<td>
			<input type="radio" name="ativo" value="<%=verdade%>"<%if rs("status_curso")=true then%> checked<%end if%> /> Ativo 
			<input type="radio" name="ativo" value="<%=falso%>"<%if rs("status_curso")=false then%> checked<%end if%> /> Inativo
		</td>
        
	</tr>
    <tr>
        <td><b>Cor</b></td>
        <td><b>Curso a Dist�ncia?</b></td>
	</tr>
	<tr>
        <td>
			<input type="hidden" name="cor" style="width:50px;" value="<%=rs("cor_curso")%>" />
            <div id="div_cor" style="width:25px; height:15px; background-color:<%=rs("cor_curso")%>; border:solid  #000000; border-width:1px; float:left;"></div> <a id="abre" href="javascript:void(0);">Selecionar Cor</a>
		</td>
        <td>
			<input type="radio" name="distancia" value="<%=verdade%>"<%if rs("distancia_curso")=true then%> checked<%end if%> /> Sim 
			<input type="radio" name="distancia" value="<%=falso%>"<%if rs("distancia_curso")=false or isnull(rs("distancia_curso")) then%> checked<%end if%> /> N�o
		</td>
	</tr>
    <tr>
		<td class="titulo_02" colspan="2"><b>Imagem do Banner</b></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="file" name="arquivo1" style="width:100%" />
		</td>
	</tr>
    <%if not rs("banner_curso")=empty then%>
    <TR>
		<td colspan="2">
			Arquivo atual: <a href="../../methodus/_arquivos_banner/<%=rs("banner_curso")%>" target="_blank"><%=rs("banner_curso")%></a>
		</td>
    </TR>
	<%end if%>
    <tr>
		<td class="titulo_02" colspan="2"><b>Meta-Tags</b> (apenas para administra��o)</td>
	</tr>
	<tr>
		<td colspan="2">
			<%
            metatags = ""
			if isnull(rs("metatags"))=false then
				metatags = Server.HTMLEncode(rs("metatags"))
			end if
            %>
			<textarea name="metatags" style="width:100%; height:60px;"><%=metatags%></textarea>	
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" name="botao_enviar" value="Editar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->