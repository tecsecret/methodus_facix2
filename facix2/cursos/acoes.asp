<!-- #include file="../_base/config.asp" -->
<!-- #INCLUDE FILE = "../_base/componente_upload.asp" -->
<%
call checalogado()


'' Vari�veis do componente de Upload ''
Dim oFO, oProps, oFile, i, item, oMyName
Set oFO = New FileUpload

Set oProps = oFO.GetUploadSettings
with oProps

	.Extensions = Array("jpg")
	'.UploadDirectory = Server.Mappath("../downloads/arquivos/")
	.AllowOverWrite = true
	.MaximumFileSize = 50000000  '50 Mb
	.MininumFileSize = 1     '1 bytes
	.UploadDisabled = false
	
End with

oFO.ProcessUpload
'''''''''''''''''''''''''''''''''''''''


acao 		= 	formatar("acao",1,2)
condicao 	= 	formatar("condicao",1,2)

ID			=	formatar(oFO.Form("ID"),1,4)
titulo 		=	formatar(oFO.Form("titulo"),1,4)
resumo 		=	formatar(oFO.Form("resumo"),3,4)
corpo 		=	formatar(oFO.Form("corpo"),3,4)
mailing		=	formatar(oFO.Form("mailing"),3,4)
tipo 		=	formatar(oFO.Form("tipo"),1,4)
ativo 		=	formatar(oFO.Form("ativo"),1,4)
distancia	=	formatar(oFO.Form("distancia"),1,4)
cor			=	formatar(oFO.Form("cor"),1,4)
metatags	=	formatar(oFO.Form("metatags"),3,4)
tema 		=	formatar(oFO.Form("tema"),1,4)

if ID=empty then
	ID 		= 	formatar("ID",1,2)
end if

if tema=empty then
	tema 		=	"null"
end if


'INCLUS�O
if acao="incluir" then

	if titulo=empty or ativo=empty or tipo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	
	'' Para fazer upload do arquivo ''
	set oFile 				= oFO.File("arquivo1")
	'oFile.FileName			= ID&"."&oFile.FileExtension
	nome_arquivo 			= FORMATA_HORA( now(), 1 ) &"_"& oFile.FileName
	oFile.FileName			= nome_arquivo
	oProps.UploadDirectory 	= Server.Mappath( "../../methodus/_arquivos_banner" )
	
	if not oFile.FileName=empty then
	
		if lcase(oFile.FileExtension) <> "jpg" then
			'erro de extens�o
		else
		
			oFile.SaveAsFile
	
		end if

	else
		nome_arquivo = ""
	end if
	'''''''''''''''''''''''''''''''''
	
	
	sql="select top 1 ordem_curso from CURSOS order by ordem_curso desc"
	set rs= conexao.execute(sql)
	
	if not rs.EOF then
		posicao = rs("ordem_curso")+1
	else
		posicao = 1
	end if
	
	
	
	sql="insert into CURSOS (titulo_curso, conteudo_curso, tipo_curso, status_curso, distancia_curso, cor_curso, banner_curso, ordem_curso, resumo_curso, mailing_curso, metatags, ID_tema)"&_
		" values ('"&titulo&"','"&corpo&"',"&tipo&","&ativo&","&distancia&",'"&cor&"','"&nome_arquivo&"',"&posicao&",'"&resumo&"','"&mailing&"','"&metatags&"',"&tema&")"
	conexao.execute(sql)
	
	
	'' remover cache do site
	call EXCLUIR_ARQUIVO( "../../api/xml/variados/cursos.xml" )

	session("sucesso") = "0001"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	
	
'EXCLUIR
elseif acao="excluir" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if
	
	
	sql="delete from USUARIOS_CURSOS where ID_curso="&ID
	conexao.execute(sql)
	
	sql="delete from COMUNICADOS where ID_curso="&ID
	conexao.execute(sql)
	
	
	sql="select banner_curso from CURSOS where ID_curso="&ID
	set rs= conexao.execute(sql)
			
	if not rs("banner_curso")="" then
		Set FSO = Server.CreateObject("Scripting.FileSystemObject")
		FSO.DeleteFile Server.Mappath( "../../methodus/_arquivos_banner/"& rs("banner_curso") )
	end if
	
	sql="delete from CURSOS where ID_curso="&ID
	conexao.execute(sql)
	
	'' Para corrigir ordem ''
		sql="select ID_curso from CURSOS order by ordem_curso"
		set rs= conexao.execute(sql)
		
		i=1
		do while not rs.EOF
			sql="update CURSOS set ordem_curso="&i&" where ID_curso="&rs("ID_curso")
			conexao.execute(sql)
			i = i + 1
		rs.movenext:loop
	'''''''''''''''''''''''''
	
	'' remover cache do site
	call EXCLUIR_ARQUIVO( "../../api/xml/variados/cursos.xml" )
	call EXCLUIR_ARQUIVO( "../../api/xml/cursos/curso_"&ID&".xml" )
	
	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))

'EDITAR
elseif acao="editar" then

	if ID=empty or titulo=empty or ativo=empty or tipo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	
	
	'' Para fazer upload do arquivo ''
	set oFile 				= oFO.File("arquivo1")
	'oFile.FileName			= ID&"."&oFile.FileExtension
	nome_arquivo 			= FORMATA_HORA( now(), 1 ) &"_"& oFile.FileName
	oFile.FileName			= nome_arquivo
	oProps.UploadDirectory 	= Server.Mappath( "../../methodus/_arquivos_banner" )
	
	if not oFile.FileName=empty then
	
		if lcase(oFile.FileExtension) <> "jpg" then
			'erro de extens�o
		else
		
			sql="select banner_curso from CURSOS where ID_curso="&ID
			set rs= conexao.execute(sql)
			
			if not rs("banner_curso")="" then
				Set FSO = Server.CreateObject("Scripting.FileSystemObject")
				FSO.DeleteFile Server.Mappath( "../../methodus/_arquivos_banner/"& rs("banner_curso") )
			end if
		
			oFile.SaveAsFile
			
			sql="update CURSOS set banner_curso='"&nome_arquivo&"'"&_
				" where ID_curso="&ID
			conexao.execute(sql)
	
		end if

	end if
	'''''''''''''''''''''''''''''''''
	
	
	sql="update CURSOS set titulo_curso='"&titulo&"', conteudo_curso='"&corpo&"', tipo_curso="&tipo&", status_curso="&ativo&", distancia_curso="&distancia&",cor_curso='"&cor&"',resumo_curso='"&resumo&"',mailing_curso='"&mailing&"',metatags='"&metatags&"', ID_tema="&tema&""&_
		" where ID_curso="&ID
	conexao.execute(sql)
	
	
	'' Para corrigir ordem ''
		sql="select ID_curso from CURSOS order by ordem_curso"
		set rs= conexao.execute(sql)
		
		i=1
		do while not rs.EOF
			sql="update CURSOS set ordem_curso="&i&" where ID_curso="&rs("ID_curso")
			conexao.execute(sql)
			i = i + 1
		rs.movenext:loop
	'''''''''''''''''''''''''
	
	'' remover cache do site
	call EXCLUIR_ARQUIVO( "../../api/xml/variados/cursos.xml" )
	call EXCLUIR_ARQUIVO( "../../api/xml/cursos/curso_"&ID&".xml" )
	

	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	
	
'MOVER POSI��O
elseif acao="mover" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql="select ordem_curso from CURSOS where ID_curso="&ID
	set rs= conexao.execute(sql)
	
	posicao_atual 		= rs("ordem_curso")
	
	if condicao="subir" then
		nova_posicao	= posicao_atual-1
	else
		nova_posicao	= posicao_atual+1
	end if
	
	sql="select ID_curso from CURSOS where ordem_curso="&nova_posicao
	set rs= conexao.execute(sql)
	
	
	sql="update CURSOS set ordem_curso="&nova_posicao&" where ID_curso="&ID
	conexao.execute(sql)
	
	if not rs.EOF then
	
		sql="update CURSOS set ordem_curso="&posicao_atual&" where ID_curso="&rs("ID_curso")
		conexao.execute(sql)
		
	end if
	
	'' remover cache do site
	call EXCLUIR_ARQUIVO( "../../api/xml/variados/cursos.xml" )

	'session("sucesso") = "0002"
	response.Redirect(session("voltar"))
	
	

'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>