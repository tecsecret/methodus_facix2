<%@ codepage="1252" language="VBScript" %>
<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Enviar Mala-direta@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>
	<script src="../_ckeditor/ckeditor.js"></script>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE CURSOS
function checa(formulario)
{
	campo=[
			formulario.nome,
			formulario.email
		  ];
	n_campo=[
			"Nome",
			"E-mail"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
	
	if (checa_nulo(campo[1],n_campo[1])==false)
		return(false);
	if (checa_email(campo[1],n_campo[1])==false)
		return(false);
		
	formulario.botao_enviar.disabled = true;
	formulario.botao_enviar.value = "Processando";
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>




<%
ID 			= 	formatar("ID",1,2)
ID_usuario 	= 	formatar("ID_usuario",1,2)

if not ID_usuario=empty then
	sql = "select nome_usuario, email_usuario from USUARIOS where ID_usuario="&ID_usuario&""
	set rs = conexao.execute(sql)
	if not rs.EOF then
		nome_usuario 	= rs("nome_usuario")
		email_usuario	= rs("email_usuario")
	end if
end if


sql = 	"select * from CURSOS where ID_curso="&ID
set rs = conexao.execute(sql)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_incluir" method="post" action="email_acoes.asp?acao=enviar" onSubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
	<tr>
		<td colspan="2">Enviando mala direta do curso <b><%=Server.HTMLEncode(rs("titulo_curso"))%></b></td>
	</tr>
    <tr>
		<td width="50%"><b>Nome</b></td>
        <td width="50%"><b>E-mail</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="nome" style="width:100%" value="<%=Server.HTMLEncode(nome_usuario)%>" />
		</td>
        <td>
			<input type="text" name="email" style="width:100%" value="<%=Server.HTMLEncode(email_usuario)%>" />
		</td>
	</tr>
    <tr>
		<td colspan="2"><b>Campanha</b></td>
	</tr>
	<tr>
		<td colspan="2">  
            <select name="marketing" style="width:100%;">
                <%
				call CONEXAO_XML("../_xml/textos_automaticos.xml")
				set textos = xmlDoc.documentElement
			
				set curso = textos.selectsinglenode("curso[@ID_curso='"&ID&"']")
				
				for each campanha in curso.childnodes
				
					imagem = campanha.selectsinglenode("@imagem").text
					assunto = campanha.selectsinglenode("@assunto").text
					tempo = campanha.selectsinglenode("@tempo").text
					
					if not imagem = empty then
					%>
                	<option value="<%=tempo%>"><%=assunto%></option>
                	<%
					end if
				next
				%>
            </select>
		</td>
	</tr>
    <tr>
		<td colspan="2"><b>Anexo</b></td>
	</tr>
	<tr>
		<td colspan="2">
        
        
        
			<%
            'Configura��es do script:
            
            'Parametro passado pela URL quando se clica em uma determinada pasta ou arquivo:
            ParametroPasta = "anexos"
            
            'A session abaixo deve ser alterada para o caminho f�sico onde estar�o os arquivos e subdiret�rios a serem acessados:
            Session("Path") = Server.Mappath("../ftp/Uploads/anexos")
            Path = Session("Path")
            'A vari�vel abaixo deve ser alterada para o caminho virtual das pastas no servidor:
            PathVirtualArquivos = ("../ftp/Uploads")
            'A vari�vel abaixo deve ser alterada para o caminho virtual deste script:
            PathScript = ("../ftp/default.asp")
            
            'Criando o objeto Filesystem Object:
            SET FSO = Server.CreateObject("Scripting.FileSystemObject")
            'Setando o caminho que ser� indicado como pasta raiz:
            Set Pasta = FSO.GetFolder(""&Path&"")
            'Solicitando a lista de arquivos para a pasta raiz:
            set arquivos = pasta.files
            'Setando a pasta raiz:
            set Raiz = Pasta
            'Verificando as subpastas da pasta raiz:
            Set Pastas = Raiz.SubFolders
            
            'Verificando o Nome da pasta Raiz:
            Nome = pasta.name
            
            'Verificando se h� subpastas:
            
            contador = 0
            for each subpastas in pastas
            contador = contador+1
            next
            %>        
        
			<!--<input type="text" name="anexo" style="width:100%" /> -->
            <select name="anexo" style="width:100%;">
            	<option value="">-- Nenhum --</option>
                <%for each arquivo in arquivos%>
                	<%
					caminho = PathVirtualArquivos&"/"&ParametroPasta
					if ParametroPasta <> "" then
						caminho = caminho &"/"
					end if
					caminho = caminho & arquivo.name
					%>
                	<option value="<%=Server.HTMLEncode(Server.MapPath(caminho))%>"><%=Server.HTMLEncode(arquivo.name)%></option>
                <%next%>
            </select>
		</td>
	</tr>
    <tr>
		<td colspan="2"><b>Coment�rios</b></td>
	</tr>
	<tr>
		<td colspan="2">            
            <textarea id="editor1" name="corpo"><b>[NOME]</b>, seguem informa&ccedil;&otilde;es solicitadas sobre o <b><%=rs("titulo_curso")%><BR /><BR />Prof. Alcides Schotten - diretor executivo</b></textarea>
			<script>
                CKEDITOR.replace( 'editor1', {
                    height: '140px',
                    toolbar: 'Basico'			
                });
            </script>
		</td>
	</tr>
    <tr>
		<td colspan="2"><input type="checkbox" name="html" value="1" /> Apenas salvar o HTML</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" name="botao_enviar" value="Enviar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->