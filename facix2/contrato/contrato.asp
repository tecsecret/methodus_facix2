<%Response.Expires = 0%>
<% 
Response.Buffer=true 
Response.AddHeader "cache-control", "private" 
Response.AddHeader "pragma", "no-cache" 
Response.AddHeader "Cache-Control", "must-revalidate" 
Response.AddHeader "Cache-Control", "no-cache" 
Response.Addheader "Last-Modified:", Now() 
%>
<!-- #include file="../_base/config.asp" -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Methodus</title>
</head>

<body>
    
<style type="text/css">
.folha {
	page-break-before:always;
}

td {
	font-family:"Times New Roman", Times, serif;
	font-size:13px;
}
</style>
<style media="print">
.noprint     { display: none }
</style>


<div id="Tela" class="noprint">

<table border="0" cellpadding="5" cellspacing="0" width="100%">
	<TR>
		<TD align="center">
            <input type="button" value="Imprimir" onClick="javascript: print();" />
		</TD>
	</TR>
</table>

</div>




<%
ID	=	formatar("ID",1,2)

sql= "select * from PAGAMENTOS where ID_pagamento="&ID
set rs= conexao.execute(sql)

sql = 	"select CURSOS.titulo_curso, CURSOS.ID_curso from "&_
		"CURSOS_INSCRICOES INNER JOIN CURSOS ON CURSOS_INSCRICOES.ID_curso = CURSOS.ID_curso "&_
		"where ID_inscricao="&rs("ID_inscricao")
set rs_curso = conexao.execute(sql)

sql = "select * from CURSOS_INSCRICOES where ID_inscricao="&rs("ID_inscricao")&""
set rsi = conexao.execute(sql)

if not rsi.EOF then
	numero_inscricao = rsi("numero_inscricao")
	
	sql = "select * from CURSOS_DATAS where ID_data="&rsi("ID_data")&""
	set rs_data = conexao.execute(sql)
end if

ID_curso = rs_curso("ID_curso")
select case ID_curso
	case 4
		tipo = "leitura"
	case 5
		tipo = "oratoria"
	case 6
		tipo = "tempo"
	case 7
		tipo = "distancia"
	case else
		tipo = "leitura"
end select

sql = "select USUARIOS.* from CURSOS_INSCRICOES_USUARIOS inner join USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario=USUARIOS.ID_usuario where ID_inscricao="&rs("ID_inscricao")&""
set rs_usuario = conexao.execute(sql)

i= 0
do while not rs_usuario.EOF
	i = i + 1
%>

<%if i=1 then%>
	<div style="height:800px;"><!--900px;-->
<%else%>
	<div class="folha" style="height:800px;">
<%end if%>

    <table border="0" cellpadding="3" cellspacing="1" width="100%"><TR><TD>
    
        <center><font style="font-size:13.5pt; font-weight:bold; color:#06C;">
        
        M E T H O D U S  &nbsp;   C O N S U L T O R I A<BR />
                                      em Aprendizagem e Treinamento Ltda.<BR />
                                             C.N.P.J.  02.519.301/0001 - 51
</font></center>
        <BR /><BR />
        
        <%if tipo = "leitura" then%>
        <table border="0" cellpadding="0" cellspacing="0">
        <TR>
        	<TD><b>Curso:</b> Alta Performance em Aprendizagem</TD>
        </TR>
        <TR>
        	<TD><b>T�cnicas abordadas no curso:</b> Leitura Din�mica, T�cnicas de Memoriza��o, T�cnicas de Mind Maps, T�cnicas de Concentra��o</TD>
        </TR>
        </table>
        <%elseif tipo = "oratoria" then%>
        <table border="0" cellpadding="0" cellspacing="0">
        <TR>
        	<TD><b>Curso:</b> Comunica��es Verbais e N�o Verbais </TD>
        </TR>
        <TR>
        	<TD><b>T�cnicas abordadas no curso:</b> T�cnicas de Orat�ria / Ret�rica, T�cnicas de Apresenta��o, Intelig�ncia Interpessoal, Reuni�es Eficazes</TD>
        </TR>
        </table>
        <%elseif tipo = "tempo" then%>
        <table border="0" cellpadding="0" cellspacing="0">
        <TR>
        	<TD><b>Curso:</b> Auto Gerenciamento</TD>
        </TR>
        <TR>
        	<TD><b>T�cnicas abordadas no curso:</b> Administra��o do Tempo, AutoMotiva��o, Administra��o do Stress</TD>
        </TR>
        </table>
        <%elseif tipo = "distancia" then%>
        <table border="0" cellpadding="0" cellspacing="0">
        <TR>
        	<TD><b>Curso:</b> Leitura Din�mica � Dist�ncia</TD>
        </TR>
        <TR>
        	<TD><b>T�cnicas abordadas no curso:</b> Nivel B�sico, N�vel Intemedi�rio, N�vel Avan�ado</TD>
        </TR>
        </table>
        <%end if%>
        
        <BR />
        <table border="0" cellpadding="0" cellspacing="0" width="100%"><TR>
        <TD valign="top">
            
            <%if tipo <> "distancia" then%>
            
                <b>Sobre data de in�cio e t�rmino, hor�rio e dias das aulas: </b><BR />
                <%=rs_data("horario_data")%> de <%=FORMATA_DATA( rs_data("inicio_data"), 1 )%> � <%=FORMATA_DATA( rs_data("fim_data"), 1 )%><BR />
                <%=replace(rs_data("descricao_data")&"","<br />"," | ")%>
                
            <%else%>
            	<b>N�:</b> <%=numero_inscricao%><BR />
            <%end if%>
             
        </TD>
        </TR></table>
        <BR />
        
        <%
		nome_usuario 		= rs_usuario("nome_usuario")
		email_usuario 		= rs_usuario("email_usuario")
		endereco_usuario 	= rs_usuario("endereco_usuario")
		cidade_usuario 		= rs_usuario("cidade_usuario")
		estado_usuario 		= rs_usuario("estado_usuario")
		cep_usuario 		= rs_usuario("cep_usuario")
		rg_usuario 			= rs_usuario("rg_usuario")
		cpf_usuario 		= rs_usuario("cpf_usuario")
		nascimento_usuario 	= rs_usuario("nascimento_usuario")
		ddd_tel_usuario 	= rs_usuario("ddd_tel_usuario")
		tel_usuario 		= rs_usuario("tel_usuario")
		cargo_usuario 		= rs_usuario("cargo_usuario")
		profissao_usuario 	= rs_usuario("profissao_usuario")
		
		if nome_usuario = empty then
			nome_usuario = "______________________________________"
		end if
		if email_usuario = empty then
			email_usuario = "___________________________________"
		end if
		if endereco_usuario = empty then
			endereco_usuario = "____________________________________"
		end if
		if cidade_usuario = empty then
			cidade_usuario = "___________________________"
		end if
		if estado_usuario = empty then
			estado_usuario = "_____"
		end if
		if cep_usuario = empty then
			cep_usuario = "______________"
		end if
		if rg_usuario = empty or isnull(rg_usuario) or rg_usuario = " " then
			rg_usuario = "_____________________"
		end if
		if cpf_usuario = empty then
			cpf_usuario = "_____________________"
		end if
		if isdate(nascimento_usuario)=false then
			nascimento_usuario = "_____________________"
		else
			nascimento_usuario = FORMATA_DATA( nascimento_usuario, 1 )
		end if
		if ddd_tel_usuario = empty then
			ddd_tel_usuario = "_____"
		end if
		if tel_usuario = empty then
			tel_usuario = "_____________________"
		end if
		if cargo_usuario = empty then
			cargo_usuario = "_____________________"
		end if
		if profissao_usuario = empty then
			profissao_usuario = "_____________________"
		end if
		%>
        
        <b>Participante:</b> <%=nome_usuario%> &nbsp; <b>E-mail:</b> <%=email_usuario%> <BR />
        <b>Endere�o:</b> <%=endereco_usuario%> &nbsp; <b>Cidade:</b> <%=cidade_usuario%> / <%=estado_usuario%> &nbsp; <b>CEP:</b> <%=cep_usuario%><BR /> 
        <!--<b>RG:</b> <%=rg_usuario%> &nbsp; --><b>CPF:</b> <%=cpf_usuario%> &nbsp; <b>Nasc.:</b> <%=nascimento_usuario%> &nbsp; <b>WhatsApp:</b> (<%=ddd_tel_usuario%>) <%=tel_usuario%><BR />
        <b>Forma��o intelectual:</b> <%=cargo_usuario%> &nbsp; <b>Empresa:</b> <%=profissao_usuario%><BR />        
        <BR />
        
        <%if tipo = "leitura" then%>
        
        <table border="0" cellpadding="0" cellspacing="0" width="100%"><TR><TD>
        <b>Estou de acordo com os itens abaixo relacionados:</b><BR />
        . o curso � composto de 10 sess�es com dura��o de 3 horas (30 horas/aula);<BR />
. o material did�tico � constitu�do de uma apostila(Manual), acesso exclusivo � �rea do aluno no site da Methodus para fazer o download do CD-Rom e textos complementares enviados por e-mail;<BR />
. o certificado s� ser� entregue mediante participa��o m�nima de 90% das sess�es e  assimila��o de 80%
  do seu conte�do program�tico;<BR />
. a inscri��o ser� efetivada � vista ou parcelada, mediante cheques pr�-datados � empresa, ou atrav�s de 
  cart�es de d�bito ou cr�dito parcelados at� 12 vezes. <BR />

        </TD></TR></table>
        
        <%elseif tipo = "oratoria" then%>
        
        <table border="0" cellpadding="0" cellspacing="0" width="100%"><TR><TD>
        <b>Estou de acordo com os itens abaixo relacionados:</b><BR />
        . o curso � composto de 10 sess�es com dura��o de 3 horas (30 horas/aula);<BR />
. o material did�tico � constitu�do de apostila e acesso exclusivo ao site para visualiza��o e download das grava��es das apresenta��es do aluno;<BR />
. o certificado s� ser� entregue mediante participa��o m�nima de 90% das sess�es e  assimila��o de 80%
  do seu conte�do program�tico;<BR />
. a inscri��o ser� efetivada � vista ou parcelada, mediante cheques pr�-datados � empresa, ou atrav�s de 
  cart�es de d�bito ou cr�dito parcelados at� 12 vezes. <BR />

        </TD></TR></table>
        
        <%elseif tipo = "tempo" then%>
        
        <table border="0" cellpadding="0" cellspacing="0" width="100%"><TR><TD>
        <b>Estou de acordo com os itens abaixo relacionados:</b><BR />
        . o curso � composto de 4 sess�es com dura��o de 3 horas (12 horas/aula);<BR />
. o material did�tico � constitu�do de uma apostila(Manual) e textos complementares enviados por e-mail;<BR />
. o certificado s� ser� entregue mediante participa��o m�nima de 90% das sess�es e  assimila��o de 80%
  do seu conte�do program�tico;<BR />
. a inscri��o ser� efetivada � vista ou parcelada, mediante cheques pr�-datados � empresa, ou atrav�s de 
  cart�es de d�bito ou cr�dito parcelados at� 6 vezes. <BR />

        </TD></TR></table>
        
        <%elseif tipo = "distancia" then%>
        
        <table border="0" cellpadding="0" cellspacing="0" width="100%"><TR><TD>
        <b>Estou de acordo com os itens abaixo relacionados:</b><BR />
        . o material did�tico � constitu�do de um Manual e um CD-ROM com exerc�cios e testes;<BR />
. a inscri��o ser� efetivada � vista ou parcelada, mediante cheques pr�-datados � empresa, ou atrav�s de 
  cart�es de d�bito ou cr�dito parcelados at� 6 vezes. <BR />

        </TD></TR></table>
        
        <%end if%>
        
        <BR />
        
        <!--<%
		call CONEXAO_XML("../_xml/pagamentos_meio.xml")
		set raiz = xmlDoc.documentElement
		
		for each registro in raiz.childNodes
		%>
			<%if rs("meio_pagamento")=registro.selectSingleNode("@codigo").text then%><b>Pagamento:</b> <%=registro.text%><%end if%>
		<%next%>-->
        
       		<table border="0" cellpadding="1" cellspacing="0" width="100%">
                
                <script type="text/javascript">
					function parcelas(qtd)
					{
						document.getElementById("tr_<%=rs_usuario("ID_usuario")%>_parcelas").style.display = 'block';
					
						//alert(qtd);
						qtd = qtd.replace( "A0","" );
						qtd = qtd.replace( "A","" );
						qtd = qtd.replace( "B0","" );
						qtd = qtd.replace( "B","" );
					
						for (i=1; i<=qtd; i++)
						{
							document.getElementById("div_parcela_<%=rs_usuario("ID_usuario")%>_"+i).style.display = 'block';
						}
						qtd = i;
						for (i=qtd; i<=12; i++)
						{
							document.getElementById("div_parcela_<%=rs_usuario("ID_usuario")%>_"+i).style.display = 'none';
						}
					}
				</script>
                
                <TR id="tr_<%=rs_usuario("ID_usuario")%>_parcelas" style="display:block;">
					<TD colspan="2">
						
                        <%
						numero_parcelas = cint( replace( replace( rs("forma_pagamento"), "A", "" ), "B", "") )
						%>
                        <b>Valor do Investimento::</b>  R$ <%=FORMATA_MOEDA(rs("valor_pagamento"),false)%>&nbsp;<%if numero_parcelas = 1 then%>� vista<%else%>em <%=numero_parcelas%> vezes<%end if%><BR />
                        
                        <b>Parcelas:</b><BR />
                        
                        <%
						obs_parcelas = rs("parcelas_obs_pagamento")
						if obs_parcelas = empty or isnull(obs_parcelas) then
							obs_parcelas = ";p1[:][x];p2[:][x];p3[:][x];p4[:][x];p5[:][x];p6[:][x];p7[:][x];p8[:][x];p9[:][x];p10[:][x];p11[:][x];p12[:][x]"
						end if
						obs_parcelas = split( obs_parcelas, ";" )
						
						str_valores = rs("str_valores_pagamento")
						if str_valores = empty or isnull(str_valores) then
							str_valores = ";p1[:][x];p2[:][x];p3[:][x];p4[:][x];p5[:][x];p6[:][x];p7[:][x];p8[:][x];p9[:][x];p10[:][x];p11[:][x];p12[:][x]"
						end if
						str_valores = split( str_valores, ";" )
						
						str_datas = rs("str_datas_pagamento")
						if str_datas = empty or isnull(str_datas) then
							str_datas = ";p1[:][x];p2[:][x];p3[:][x];p4[:][x];p5[:][x];p6[:][x];p7[:][x];p8[:][x];p9[:][x];p10[:][x];p11[:][x];p12[:][x]"
						end if
						str_datas = split( str_datas, ";" )
						%>
                        
                        <%
						'' para preencher caso os campos estejam vazios ''
						valor_temp 	= FORMATA_MOEDA( rs("valor_pagamento") / replace( replace( rs("forma_pagamento"), "A", "" ), "B", ""), false )
						data_temp	= rs("data_pagamento")
						%>
                        
                        <%for i = 1 to 12%>
                        
                        	<%
							obs_temp 			= split( obs_parcelas(i), "[:]" )
							str_valores_temp 	= split( str_valores(i), "[:]" )
							str_datas_temp 		= split( str_datas(i), "[:]" )
							%>
                        
                            <div id="div_parcela_<%=rs_usuario("ID_usuario")%>_<%=i%>" style="display:<%if i=1 then%>block<%else%>none<%end if%>;">
                            	
                                <table border="0" cellpadding="2" cellspacing="0"><TR>
                                <TD width="500">
                                	Parcela <%=i%>/<%=cint(replace(replace(rs("forma_pagamento"),"A",""),"B",""))%> &nbsp; <%=replace( obs_temp(1), "[x]", "" )%> &nbsp; 
                                </TD>
                                <TD width="150">
                                	Valor: R$ <%if replace( str_valores_temp(1), "[x]", "" )="" then%><%=valor_temp%><%else%><%=replace( str_valores_temp(1), "[x]", "" )%><%end if%> &nbsp;
                                </TD>
                                <TD width="150">
                                	Data: <%if str_datas_temp(1)<>"[x]" then%><%=FORMATA_DATA( str_datas_temp(1), 1 )%><%else%><%=FORMATA_DATA( dateadd( "m", (i-1), data_temp ), 1 )%><%end if%>
                           		</TD>
                                </TR></table>
                           
                            </div>
                            
                        <%next%>
                        
					</TD>
				</TR>
                
                <%if rs("forma_pagamento")<>"A01" then%>
                <script type="text/javascript">
					
					parcelas('<%=rs("forma_pagamento")%>');
					
				</script>
                <%end if%>
                
			</table>
            
            <BR /><BR />
        	<table border="0" cellpadding="0" cellspacing="0" width="100%"><TR>
            <TD valign="top">
                <b>Localiza��o do Curso:</b> Av. Paulista, 2202 cj 134 � 13� andar � S�o Paulo � SP.<BR />
                Metr� Consola��o(na mesma quadra). No pr�dio existe o Estacionamento Estapar(sem conv�nio).<BR />
                <BR />
                <b>Canais para solucionar d�vidas:</b> <BR />
                - E-mail: info@methodus.com.br<BR />
                - Telefone: (11) 3288 2777<BR />
                - Whattsapp: (11) 97148 6385 <BR />
                - Skype: info@methodus.com.br 
        
            </TD>
            </TR></table>
            
            
            
            
    
    </TD></TR></table>
	
    
    </div>
    
    
    <table border="0" cellpadding="0" cellspacing="0" width="100%"><TR>
    <TD valign="top" align="left">
        <img src="../_img/assinatura_alcides.jpg" width="230" />
    </TD>
    </TR></table>
    
    
    <BR />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#06C;"><TR>
    <TD valign="top" align="left">
        www.methodus.com.br<BR />
        info@methodus.com.br
    </TD>
    <TD valign="top" align="center">
        Av. Paulista, 2202 cj 134 <BR />
        01310-300 � S�o Paulo � SP
    </TD>
    <TD valign="top" align="right">
        Tel. (11) 3288 2777<BR />
        Whattsapp (11) 97148 6385
    </TD>
    </TR></table>
    
    
<%rs_usuario.movenext:loop%>


		
			

</body>
</html>