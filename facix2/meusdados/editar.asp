<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar Meus Dados@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE PRESTADORES
function checa(formulario)
{
	campo=[formulario.nome,formulario.usuario,formulario.senha,formulario.ddd_telcom,formulario.telcom,formulario.ddd_telres,formulario.telres,formulario.ddd_telcel,formulario.telcel,formulario.nasc_dia,formulario.nasc_mes,formulario.nasc_ano,formulario.ddd_tel,formulario.tel,formulario.email];
	n_campo=["Nome do Prestador","Usu�rio","Senha","DDD do telefone comercial","Telefone comercial","DDD do telefone residencial","Telefone residencial","DDD do telefone celular","Telefone celular","Dia de nascimento","M�s de nascimento","Ano de nascimento","DDD do Telefone","Telefone","E-Mail"];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
		
	if (checa_nulo(campo[9],n_campo[9])==false)
		return(false);
	if (checa_caracter(campo[9],n_campo[9],"1234567890")==false)
		return(false);
	if (checa_nulo(campo[10],n_campo[10])==false)
		return(false);
	if (checa_caracter(campo[10],n_campo[10],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[11],n_campo[11],4)==false)
		return(false);
	if (checa_caracter(campo[11],n_campo[11],"1234567890")==false)
		return(false);
		
	if (checa_tamanho(campo[1],n_campo[1],4)==false)
		return(false);
	if (checa_caracter(campo[1],n_campo[1],"qwertyuiopasdfghjkl�zxcvbnm1234567890QWERTYUIOPASDFGHJKL�ZXCVBNM.-_")==false)
		return(false);
		
	if (campo[2].value.length > 0)
	{
	if (checa_tamanho(campo[2],n_campo[2],4)==false)
		return(false);
	if (checa_caracter(campo[2],n_campo[2],"qwertyuiopasdfghjkl�zxcvbnm1234567890QWERTYUIOPASDFGHJKL�ZXCVBNM.-_")==false)
		return(false);
	}
	
	if (checa_email(campo[14],n_campo[14])==false)
		return(false);
		
	if (campo[3].value.length > 0 || campo[4].value.length > 0)
	{
		if (checa_tamanho(campo[3],n_campo[3],2)==false)
			return(false);
		if (checa_caracter(campo[3],n_campo[3],"1234567890")==false)
			return(false);
		if (checa_tamanho(campo[4],n_campo[4],7)==false)
			return(false);
		if (checa_caracter(campo[4],n_campo[4],"1234567890-. ")==false)
			return(false);
	}
		
	if (checa_tamanho(campo[5],n_campo[5],2)==false)
		return(false);
	if (checa_caracter(campo[5],n_campo[5],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[6],n_campo[6],7)==false)
		return(false);
	if (checa_caracter(campo[6],n_campo[6],"1234567890-. ")==false)
		return(false);
		
	if (checa_tamanho(campo[7],n_campo[7],2)==false)
		return(false);
	if (checa_caracter(campo[7],n_campo[7],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[8],n_campo[8],8)==false)
		return(false);
	if (checa_caracter(campo[8],n_campo[8],"1234567890-. ")==false)
		return(false);
	
	if (campo[12].value.length > 0 || campo[13].value.length > 0)
	{
		if (checa_tamanho(campo[12],n_campo[12],2)==false)
			return(false);
		if (checa_caracter(campo[12],n_campo[12],"1234567890")==false)
			return(false);
		if (checa_tamanho(campo[13],n_campo[13],7)==false)
			return(false);
		if (checa_caracter(campo[13],n_campo[13],"1234567890-. ")==false)
			return(false);
	}
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID	=	session("admin")

sql= "select * from PRESTADORES where ID_prestador="&ID
set rs= conexao.execute(sql)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<tr>
	<form name="a_editar_prestador" method="post" action="acoes.asp?acao=editar" onsubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
		<td>
		
			<table border="0" cellpadding="1" cellspacing="0" width="100%">
				<TR>
					<TD width="50%"><b>Nome:</b></TD>
					<TD width="50%"><b>Data de Nascimento:</b></TD>
				</TR>
				<TR>
					<TD>
						<input type="text" name="nome" style="width:100%" maxlength="200" value="<%=rs("nome_prestador")%>" />
					</TD>
					<TD>
						<input type="text" name="nasc_dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=right("0"&day(rs("nascimento_prestador")),2)%>" /> / 
						<input type="text" name="nasc_mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=right("0"&month(rs("nascimento_prestador")),2)%>" /> / 
						<input type="text" name="nasc_ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=year(rs("nascimento_prestador"))%>" />
					</TD>
				</TR>
				<TR>
					<TD width="50%"><b>Usu�rio:</b></TD>
					<TD width="50%"><b>Senha:</b></TD>
				</TR>
				<TR>
					<TD><input type="text" name="usuario" style="width:100%" maxlength="15" value="<%=rs("usuario_prestador")%>" /></TD>
					<TD><input type="text" name="senha" style="width:100%" maxlength="15" onMouseover="mostra_dica('N�o preencha esse campo se n�o quiser alterar a senha')" onMouseout="esconde_dica()" /></TD>
				</TR>
				<TR>
					<TD width="50%"><b>E-Mail:</b></TD>
					<TD width="50%">&nbsp;</TD>
				</TR>
				<TR>
					<TD>
						<input type="text" name="email" style="width:100%" maxlength="200" value="<%=rs("email_prestador")%>" />
					</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD width="50%"><b>Tel. Comercial:</b></TD>
					<TD width="50%"><b>Tel. Residencial:</b></TD>
				</TR>
				<TR>
					<TD><input type="text" name="ddd_telcom" style="width:17%" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=rs("ddd_tel_com_prestador")%>" />&nbsp;<input type="text" name="telcom" style="width:80%" maxlength="9" value="<%=rs("tel_com_prestador")%>" /></TD>
					<TD><input type="text" name="ddd_telres" style="width:17%" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=rs("ddd_tel_res_prestador")%>" />&nbsp;<input type="text" name="telres" style="width:80%" maxlength="9" value="<%=rs("tel_res_prestador")%>" /></TD>
				</TR>
				<TR>
					<TD width="50%"><b>Tel. Celular:</b></TD>
					<TD width="50%"><b>Outro Telefone:</b></TD>
				</TR>
				<TR>
					<TD><input type="text" name="ddd_telcel" style="width:17%" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=rs("ddd_tel_cel_prestador")%>" />&nbsp;<input type="text" name="telcel" style="width:80%" maxlength="9" value="<%=rs("tel_cel_prestador")%>" /></TD>
					<TD><input type="text" name="ddd_tel" style="width:17%" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=rs("ddd_tel_outro_prestador")%>" />&nbsp;<input type="text" name="tel" style="width:80%" maxlength="9" value="<%=rs("tel_outro_prestador")%>" /></TD>
				</TR>
			</table>
			
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Editar" style="width:150px;" />
		</td>
	</form>
	</tr>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->