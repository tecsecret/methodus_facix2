<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar Noticia@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>
	<script src="../_ckeditor/ckeditor.js"></script>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// EDI��O DE NOT�CIAS
function checa(formulario)
{
	campo=[
			formulario.titulo
		  ];
	n_campo=[
			"T�tulo"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID 	= 	formatar("ID",1,2)
sql="select * from NOTICIAS where ID_noticia="&ID
set rs= conexao.execute(sql)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_editar_noticia" method="post" action="acoes.asp?acao=editar" onSubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
	<tr>
		<td class="titulo_02" width="50%"><b>T�tulo</b></td>
		<td class="titulo_02" width="50%"><b>Status</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="titulo" style="width:100%" value="<%=Server.HTMLEncode(rs("titulo_noticia"))%>" />
		</td>
		<td>
			<input type="radio" name="ativo" value="<%=verdade%>"<%if rs("status_noticia")=true then%> checked<%end if%> /> Ativo 
			<input type="radio" name="ativo" value="<%=falso%>"<%if rs("status_noticia")=false then%> checked<%end if%> /> Inativo
		</td>
	</tr>
	<tr>
		<td class="titulo_02" colspan="2"><b>Conte�do</b></td>
	</tr>
	<tr>
		<td colspan="2">	
            <textarea id="editor1" name="corpo"><%=rs("corpo_noticia")%></textarea>
			<script>
                CKEDITOR.replace( 'editor1', {
                    height: '240px',
                    toolbar: 'Padrao'			
                });
            </script>	
		</td>
	</tr>
    <tr>
		<td class="titulo_02" colspan="2"><b>Palavras-chave</b> (para pesquisas)</td>
	</tr>
	<tr>
		<td colspan="2">
			<%
            palavras = ""
			if isnull(rs("palavras"))=false then
				palavras = Server.HTMLEncode(rs("palavras"))
			end if
            %>
			<textarea name="palavras" style="width:100%; height:60px;"><%=palavras%></textarea>	
		</td>
	</tr>
    <TR>
        <TD><b>Data para Publica��o:</b></TD>
        <TD><b>Aparece em maladireta?</b></TD>
    </TR>
    <TR>
        <TD>
            <input type="text" name="pub_dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=day(rs("data_noticia"))%>" /> / 
            <input type="text" name="pub_mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=month(rs("data_noticia"))%>" /> / 
            <input type="text" name="pub_ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=year(rs("data_noticia"))%>" />
            �s
            <input type="text" name="pub_hora" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=hour(rs("data_noticia"))%>" />h
            <input type="text" name="pub_min" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=minute(rs("data_noticia"))%>" />m
        </TD>
        <td>
			<input type="radio" name="mm" value="<%=verdade%>"<%if rs("mm")=true then%> checked<%end if%> /> Sim 
			<input type="radio" name="mm" value="<%=falso%>"<%if rs("mm")=false or isnull(rs("mm")) then%> checked<%end if%> /> N�o
		</td>
    </TR>
    <tr>
		<td><b>Imagem para home</b></td>
        <td valign="bottom"><b>Chamada para home</b></td>
	</tr>
    <tr>
		<td valign="top">
			<%call EXIBE_UPLOAD( "foto", "../../../methodus/_arquivos_home/", "jpg,gif,png" )%>
            <%if not rs("foto")=empty then%>
				Arquivo atual: <a href="../../methodus/_arquivos_home/<%=rs("foto")%>" target="_blank"><%=rs("foto")%></a>
			<%end if%>
		</td>
        <td valign="top">
        	<%
            palavras = ""
			if isnull(rs("chamada"))=false then
				palavras = Server.HTMLEncode(rs("chamada"))
			end if
            %>
			<textarea name="chamada" style="width:100%; height:50px;"><%=palavras%></textarea>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Editar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->