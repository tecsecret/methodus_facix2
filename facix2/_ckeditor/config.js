﻿/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	//config.language = 'fr';
	//config.uiColor = '#F8F8F8';
	//config.skin = 'kama';
	
	config.skin = 'v2';
	config.height = '400px';
	config.width = '100%';
	config.entities = false;
	config.enterMode = CKEDITOR.ENTER_BR,
	config.shiftEnterMode = CKEDITOR.ENTER_P;			
	config.filebrowserBrowseUrl = '../_ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = '../_ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = '../_ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = '../_ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = '../_ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = '../_ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Flash';
	config.filebrowserWindowWidth = '700';
 	config.filebrowserWindowHeight = '500';
	
	config.toolbar_Tudo =
	[
		{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
			'HiddenField' ] },
		'/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
		'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
	];
	
	config.toolbar_Padrao =
	[
		
		{ name: 'document', items : [ 'Source','-','Preview','Print' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		'/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
		'/',
		{ name: 'styles', items : [ 'Font','FontSize' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
	];
	
	config.toolbar_Padrao2 = [
		['Source','Preview','-','Templates'],
		['Cut','Copy','Paste','PasteText','PasteWord','-','Print','SpellChecker'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
		['OrderedList','UnorderedList','-','Outdent','Indent'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
		['Link','Unlink','Anchor'],
		['Image','Table','Rule'],
		['TextColor','BGColor'],
		['Font','FontSize']
	] ;
	
	config.toolbar_Basico = [
		['Source','Bold','Italic','Underline','-','OrderedList','UnorderedList','-','Link','Unlink']
	] ;
	
	config.toolbar_Basico_fonte = [
		['Source','Bold','Italic','Underline','-','OrderedList','UnorderedList','-','Link','Unlink','FontSize']
	] ;
	
};
