<!--#include file="bibliotecas/cabecalho.asp"-->

<%
ID_aulas	= formatar("ID_aulas",1,2)

sql = "select titulo_aula from QUIZ_AULAS where ID_aula in ( "&ID_aulas&" )"
set rs = ABRIR_RS(sql)
%>

<div class="titulo_cheio">
	<%
	str_titulos = ""
	do while not rs.EOF
		if str_titulos = empty then
			str_titulos = rs("titulo_aula")
		else
			str_titulos = str_titulos &" + "& rs("titulo_aula")
		end if
	rs.movenext:loop
	
	response.Write(str_titulos)
	%>
</div>



<%
'sql = "select * from CURSOS_INSCRICOES where ID_data="&ID_data&" and ID_curso="&ID&" and pago_inscricao="&verdade&" order by data_inscricao"
sql = 	"select DISTINCT nome_usuario, USUARIOS.ID_usuario, codigo_controle from CURSOS_INSCRICOES "&_
		"INNER JOIN CURSOS_INSCRICOES_USUARIOS ON CURSOS_INSCRICOES.ID_inscricao = CURSOS_INSCRICOES_USUARIOS.ID_inscricao "&_
		"inner join USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario=USUARIOS.ID_usuario "&_
		"inner join QUIZ_CONTROLES_RELACOES on QUIZ_CONTROLES_RELACOES.ID_usuario = USUARIOS.ID_usuario and QUIZ_CONTROLES_RELACOES.ID_data = CURSOS_INSCRICOES.ID_data "&_
		"inner join QUIZ_CONTROLES on QUIZ_CONTROLES_RELACOES.ID_controle = QUIZ_CONTROLES.ID_controle "&_
		"where CURSOS_INSCRICOES.ID_data="&ID_data&" and pago_inscricao="&verdade&" order by nome_usuario"
'response.Write(sql)
set rs = ABRIR_RS(sql)
%>
<div class="tabela" id="tabela_aula">
	<div class="linha">
    	<div class="td titulo" lang="25">&nbsp;</div>
        <%
		quantidade_colunas = 2
		tamanho_cada_coluna = fix( 75 / quantidade_colunas )
		%>
        <div class="td titulo" lang="<%=tamanho_cada_coluna%>">GT</div>
        <div class="td titulo" lang="<%=tamanho_cada_coluna%>">&nbsp;</div>
    </div>
    
    <div class="linha">
    	<div class="td titulo" lang="25"><a href="javascript:" onclick="reOrdenar('nome_aluno_',0,1)">Nome do participante</a></div>
        <div class="td titulo" lang="<%=tamanho_cada_coluna%>">
            <div class="caixa_dividida"><a href="javascript:" onclick="reOrdenar('td_gt_pc_',1)">PCR</a></div>
            <div class="caixa_dividida com_borda"><a href="javascript:" onclick="reOrdenar('td_gt_pcm_',1)">PCM</a></div>
        </div>
        <div class="td titulo" lang="<%=tamanho_cada_coluna%>"><a href="javascript:" onclick="reOrdenar('td_mi_',1)">Quantas vezes melhor T</a></div>
    </div>


	<%
	ordem_aluno = 0
	
    do while not rs.EOF
		MA_pc = 0
		MA_pcm = 0
		
		ordem_aluno = ordem_aluno + 1
        %>
        <div class="linha linha_aluno" id="aluno_<%=rs("ID_usuario")%>">
            <div class="td" lang="25"><div id="nome_aluno_<%=rs("ID_usuario")%>" lang="<%=ordem_aluno%>"><span id="posicao_<%=rs("ID_usuario")%>"></span><%=server.HTMLEncode(rs("nome_usuario"))%></div></div>
            
            <%
            temp = split( ID_aulas, "," )
			quantidade_aulas = ubound( temp ) + 1
			
			temp_ma_pc 	= 0
			temp_ma_pcm	= 0
			
			m_inicio	= 0
			pcm_aula1	= 0
			
			sql = "select * from QUIZ_NOTAS where ID_usuario="&rs("ID_usuario")&" and ID_data="&ID_data&" and tempo_nota is not null and pcm_nota is not null"
			set rs_m_inicio = ABRIR_RS(sql)
			
			if not rs_m_inicio.EOF then
				if rs_m_inicio("pcm_nota") > 0 then
					pcm_aula1	= rs_m_inicio("pcm_nota")
				end if
			end if
				
			for i=0 to ubound( temp )
			
				aluno_ma_pc = 0
				aluno_ma_pcm = 0
				qtd_testes_respondidos = 0
				
				
				sql = "select * from QUIZ_TESTES where status_teste="&verdade&" and ID_aula ="&temp(i)&""
				set rs_testes = ABRIR_RS(sql)
				
				quantidade_testes = rs_testes.recordcount
						
				do while not rs_testes.EOF
					
					sql = "select * from QUIZ_NOTAS where ID_usuario="&rs("ID_usuario")&" and ID_teste="&rs_testes("ID_teste")&" and ID_data="&ID_data&""
					set rs_nota = ABRIR_RS(sql)
					
					if not rs_nota.EOF then
						if isnull(rs_nota("pcm_nota"))=false and isnull(rs_nota("valor_nota"))=false then
							aluno_pc 	= rs_nota("valor_nota")
							aluno_pcm 	= formatnumber(rs_nota("pcm_nota"),2)
							
							aluno_ma_pc = aluno_ma_pc + aluno_pc
							aluno_ma_pcm = aluno_ma_pcm + aluno_pcm
							qtd_testes_respondidos = qtd_testes_respondidos + 1
						end if
					end if	
					
				rs_testes.movenext:loop
				
				temp_ma_pc 	= temp_ma_pc + (aluno_ma_pc / quantidade_testes)
				temp_ma_pcm	= temp_ma_pcm + (aluno_ma_pcm / quantidade_testes)
				
				if aluno_ma_pcm > 0 and pcm_aula1 > 0 then
					m_inicio 	= m_inicio + ( round(aluno_ma_pcm / quantidade_testes,2) / round(pcm_aula1,2) )
				end if
				
'				if rs("ID_usuario")=167772 then
'					response.Write("<BR />M Inicio: "&( round(aluno_ma_pcm / quantidade_testes,2) / round(pcm_aula1,2) )&"<BR />")
'					response.Write("<BR />SOMA: "&m_inicio&"<BR />")
'				end if
			
			next
			
			
			
			if temp_ma_pc > 0 then
				MA_pc 	= FORMATA_MOEDA( ( temp_ma_pc ) / quantidade_aulas, false ) 'qtd_testes_respondidos
			end if
			
			if temp_ma_pcm > 0 then
				MA_pcm 	= FORMATA_MOEDA( ( temp_ma_pcm ) / quantidade_aulas, false ) 'qtd_testes_respondidos
			end if
					
'			m_inicio	= -1
'			
'			sql = "select * from QUIZ_NOTAS where ID_usuario="&rs("ID_usuario")&" and ID_data="&ID_data&" and tempo_nota is not null and pcm_nota is not null"
'			set rs_m_inicio = ABRIR_RS(sql)
'			
'			if not rs_m_inicio.EOF then
'				if MA_pcm > 0 and rs_m_inicio("pcm_nota") > 0 then
'					m_inicio 	= MA_pcm / rs_m_inicio("pcm_nota")
'					m_inicio 	= formatnumber( (m_inicio/quantidade_aulas), 2 )
'				end if
'			end if

			if m_inicio > 0 then
				m_inicio 	= formatnumber( (m_inicio/quantidade_aulas), 2 )
			end if
			%>
			
            <div class="td center" lang="<%=tamanho_cada_coluna%>">
                <div class="caixa_dividida GT_pc esconder" id="td_gt_pc_<%=rs("ID_usuario")%>" lang="<%=round(MA_pc,0)%>" onclick="deixaVisivel('td_gt_pc_')">
					<%=round(MA_pc,0)%>
                </div>
                <div class="caixa_dividida com_borda GT_pcm esconder" id="td_gt_pcm_<%=rs("ID_usuario")%>" lang="<%=round(MA_pcm,0)%>" onclick="deixaVisivel('td_gt_pcm_')">
					<%=round(MA_pcm,0)%>
                </div>
            </div>
            <div class="td center" lang="<%=tamanho_cada_coluna%>">
            	<div id="td_mi_<%=rs("ID_usuario")%>" lang="<%=m_inicio%>" class="esconder" onclick="deixaVisivel('td_mi_')">
					<%if m_inicio >= 0 then%><%=m_inicio%><%else%>&nbsp;<%end if%>
                </div>
            </div>
		</div>
		<%
    rs.movenext:loop
    %>
    
</div>


<!--#include file="bibliotecas/rodape.asp"-->