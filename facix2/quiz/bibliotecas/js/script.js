$(document).ready(function() {
	$('.td').each(function(i, obj) {
		if ($(obj).prop('lang'))
			$(obj).css('width', $(obj).prop('lang')+'%' );
	});
});


function carregaUpload(id){
	
	formulario = document.getElementById("form_"+id);
	
	enviar = 0;
	if ( formulario.anexo ){
		if ( formulario.anexo.value.length > 0 ){
			if ( right( formulario.anexo.value, 5 ) == ".xlsx" || right( formulario.anexo.value, 4 ) == ".xls" ){
				enviar = 1;
			}
		}
	}
	
	if ( enviar ){
		$("#upload_"+id).removeClass('icone_upload');
		$("#upload_"+id).addClass('upload_carregando');
		formulario.submit();
	}else{
		alert("Arquivo inválido. O sistema aceita apenas arquivos XLS e XLSX.");
		formulario.anexo.value = "";	
	}
		
}


function SortByName(a, b){
  var aName = a[0].toLowerCase();
  var bName = b[0].toLowerCase(); 
  return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}


function reOrdenar( id, desc, sem_rank ){
	
	var alunos = [];
	
	$('div[id^="'+id+'"]').each(function(index, value) { 
		
		ID_aluno 		= parseFloat( $(this).attr('id').replace(id, "") );
		valor_campo 	= parseFloat( $(this).attr('lang').replace(",", ".") );
		
		//console.log("ID_aluno: "+ID_aluno+" / valor: "+valor_campo);
		
		var temp = [valor_campo,ID_aluno];
		
		alunos.push(temp);
		
	});
	
	//console.log("TESTE DA ARRAY 0: "+alunos);
	

	nova_array = alunos.sort(function(a, b)
	{
		if (a[0] == b[0]) { return 0; }
		if (a[0] > b[0])
		{
			if (desc)
				return -1;
			else
				return 1;
		}
		else
		{
			if (desc)
				return 1;
			else
				return -1;
		}
	});
	
	//console.log("TESTE DA ARRAY 1: "+nova_array);
	
	novo_html = "";
	
	for (i = 0; i < nova_array.length; i++) { 
		ID_aluno_temp = nova_array[i][1];
		
		if (sem_rank){
			$('#posicao_'+ID_aluno_temp).html('');
		}else{
			$('#posicao_'+ID_aluno_temp).html((i+1)+'º - ');
		}
		
		if (ID_aluno_temp == 0)
			novo_html += '<div class="linha linha_aluno" id="aluno_'+ID_aluno_temp+'" style="color:#F00;">' + $('#aluno_'+ID_aluno_temp).html() + '</div>';
		else
			novo_html += '<div class="linha linha_aluno" id="aluno_'+ID_aluno_temp+'">' + $('#aluno_'+ID_aluno_temp).html() + '</div>';
	}
	
	$( ".linha_aluno" ).remove();
	$('#tabela_aula').append(novo_html);
	
	//console.log(novo_html);
		
}

function mudaTempo( ID_usuario, ID_tempo, ID_aula, ID_data ){
	valor_hora = $('#tempo_'+ID_usuario);
	
	if (valor_hora.val().length >= 2){
				
		$.ajax({url: "acoes.asp?acao=tempo&ID_usuario="+ID_usuario+"&ID_teste="+ID_tempo+"&ID_aula="+ID_aula+"&ID_data="+ID_data+"&tempo="+valor_hora.val(), success: function(result){
			console.log("Tempo do usuário "+ID_usuario+" gravado!");
		}});
			
	}
	
}

function deixaVisivel( id ){
	
	$('div[id^="'+id+'"]').each(function(index, value) { 
		
		$(this).removeClass('esconder');
		$(this).attr('onclick','');
		
	});
	
	reOrdenar('nome_aluno_',0,1);
	
}

function refreshDeixaVisivel( ids ){
	
	//console.log('DEBUG: '+ids);
	var temp = ids.split(";");
	//console.log('Tamanho: '+temp.length);

	for (conta_coluna=0; conta_coluna<temp.length; conta_coluna++){

		//console.log('item '+conta_coluna+': '+temp[conta_coluna]);
		deixaVisivel('td_pc_'+temp[conta_coluna]+'_');
		deixaVisivel('td_pcm_'+temp[conta_coluna]+'_');
	}

}



//############################################################
//# Título            : Mácasra_para_campos_só_números       #
//# Criado por        : Dinho Ferrari                        #
//# Data da Criação   : 09/04/2007                           #                                                          
//# E-mail            : gleydy1@hotmail.com                  #  
//############################################################

//############################################################
//#                                                          #
//#      Por favor mantenha os créditos!!!!                  #
//# Caso tenha gostado desse script me mande um e-mail!      #
//# Bugs e comentários - gleydy1@hotmail.com                 #
//# * Se alterar este script para melhor me avise!           #
//#                                                          #
//############################################################
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

function leech(v){
    v=v.replace(/o/gi,"0")
    v=v.replace(/i/gi,"1")
    v=v.replace(/z/gi,"2")
    v=v.replace(/e/gi,"3")
    v=v.replace(/a/gi,"4")
    v=v.replace(/s/gi,"5")
    v=v.replace(/t/gi,"7")
    return v
}

function horario(v){
    v=v.replace(/\D/g,"")                    
    v=v.replace(/(\d{2})(\d)/,"$1:$2")       
    return v
}

///// ############################################ ///////
///// ############################################ ///////

