<!-- #include file="../_base/config.asp" -->
<%
call checalogado()

acao 			= 	formatar("acao",1,2)

uid 			=	formatar("uid",1,1)
data_dia 		=	formatar("data_dia",1,1)
data_mes 		=	formatar("data_mes",1,1)
data_ano 		=	formatar("data_ano",1,1)
valor 			=	FORMATA_MOEDA( formatar("valor",1,1), true )
estatus			=	formatar("estatus",1,1)
meio	 		=	formatar("meio",1,1)
forma 			=	formatar("forma",1,1)
observacoes		=	formatar("observacoes",2,1)
ID_usuario		= 	formatar("ID_usuario",1,1)

titulo 			=	formatar("titulo",1,1)
descricao		=	formatar("descricao",2,1)

ID 				= 	formatar("ID",1,3)

'' formatar vari�ves que chegam separadas ''
if isdate(""&data_dia&"/"&data_mes&"/"&data_ano&"") then
	data		=	DATA_BD( ""&data_dia&"/"&data_mes&"/"&data_ano&"" )
end if
''''''''''''''''''''''''''''''''''''''''''''

'' formatar variavel de parcelas ''
for i=1 to 12

	'' string da parcela
	parcela_temp = formatar( "parcela_"&i ,1,1 )
	if parcela_temp = empty then
		parcela_temp = 0
	end if
	
	parcelas = parcelas & ";p"&i&":"&parcela_temp
	
	'' string das observa��es
	obs_parcela_temp = formatar( "obs_parcela_"&i ,1,1 )
	if obs_parcela_temp = empty then
		obs_parcela_temp = "[x]"
	end if
	
	obs_parcelas = obs_parcelas & ";p"&i&"[:]"&obs_parcela_temp
	
	'' string das valores
	str_valores_temp = formatar( "str_valores_"&i ,1,1 )
	if str_valores_temp = empty then
		str_valores_temp = "[x]"
	end if
	
	str_valores = str_valores & ";p"&i&"[:]"&str_valores_temp
	
	'' string das datas
	str_datas_temp = formatar( "str_datas_"&i ,1,1 )
	if str_datas_temp = empty then
		str_datas_temp = "[x]"
	else
		str_datas_temp = FORMATA_DATA( str_datas_temp, 3 )
	end if
	
	str_datas = str_datas & ";p"&i&"[:]"&str_datas_temp
	
next
'''''''''''''''''''''''''''''''''''


'INCLUS�O
if acao="incluir" then

	if ID_usuario=empty or UID=empty or data=empty or estatus=empty or meio=empty or forma=empty or valor=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
		'' incluir pagamento avulso ''
		sql = 	"insert into PAGAMENTOS_AVULSOS (data_avulso, ID_usuario, titulo_avulso, descricao_avulso) values ('"&DATA_BD( now() )&"', "&ID_usuario&", '"&titulo&"', '"&descricao&"')"&_
				" select @@identity as NovoId"
		set rs = conexao.execute(sql).nextrecordset
		
		ID_avulso = rs("NovoId")
		''''''''''''''''''''''''''''''
		
		'' Criar um novo registro de pagamento para o pedido ''
		sql =	"insert into PAGAMENTOS (ID_avulso, identificacao_pagamento, cod_status_pagamento, data_pagamento, "&_
				"forma_pagamento, meio_pagamento, valor_pagamento, obs_pagamento, parcelas_pagamento, parcelas_obs_pagamento, str_valores_pagamento, str_datas_pagamento) values "&_
				"("&ID_avulso&",'"&UID&"','"&estatus&"','"&data&"', "&_
				"'"&forma&"','"&meio&"',"&valor&",'"&observacoes&"','"&parcelas&"','"&obs_parcelas&"','"&str_valores&"','"&str_datas&"')"
		conexao.execute(sql)
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
	session("sucesso") = "0001"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	
	
'EXCLUIR
elseif acao="excluir" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if
	
	sql = "select ID_avulso from PAGAMENTOS where ID_pagamento="&ID
	set rs = conexao.execute(sql)
	
	if not rs.EOF then
		sql = "delete from PAGAMENTOS_AVULSOS where ID_avulso="&rs("ID_avulso")
		conexao.execute(sql)
	end if
	
	sql="delete from PAGAMENTOS where ID_pagamento="&ID
	conexao.execute(sql)
	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))
	
	

'EDITAR
elseif acao="editar" then

	if ID=empty or UID=empty or data=empty or estatus=empty or meio=empty or forma=empty or valor=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	
	sql = "select ID_avulso from PAGAMENTOS where ID_pagamento="&ID
	set rs = conexao.execute(sql)
	
	if not rs.EOF then
		sql = "update PAGAMENTOS_AVULSOS set titulo_avulso='"&titulo&"', descricao_avulso='"&descricao&"' where ID_avulso="&rs("ID_avulso")
		conexao.execute(sql)
	end if
	
	
	sql =	"update PAGAMENTOS set identificacao_pagamento='"&uid&"', data_pagamento='"&data&"', valor_pagamento="&valor&", cod_status_pagamento='"&estatus&"'"&_
			", meio_pagamento='"&meio&"', forma_pagamento='"&forma&"', obs_pagamento='"&observacoes&"', parcelas_pagamento='"&parcelas&"', parcelas_obs_pagamento='"&obs_parcelas&"', "&_
			"str_valores_pagamento='"&str_valores&"', str_datas_pagamento='"&str_datas&"' where ID_pagamento="&ID
	conexao.execute(sql)
	

	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	

'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>