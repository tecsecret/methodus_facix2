<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Incluir Pagamento Avulso@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE PRESTADORES
function checa(formulario)
{
	campo=	[
				formulario.uid,
				formulario.data_dia,
				formulario.data_mes,
				formulario.data_ano,
				formulario.valor
			];
			
	n_campo=[
				"UID",
				"Dia da Data de Pagamento",
				"M�s da Data de Pagamento",
				"Ano da Data de Pagamento",
				"Valor"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
		
		
	if (checa_tamanho(campo[1],n_campo[1],1)==false)
		return(false);
	if (checa_caracter(campo[1],n_campo[1],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[2],n_campo[2],1)==false)
		return(false);
	if (checa_caracter(campo[2],n_campo[2],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[3],n_campo[3],4)==false)
		return(false);
	if (checa_caracter(campo[3],n_campo[3],"1234567890")==false)
		return(false);
		
	if (checa_nulo(campo[4],n_campo[4])==false)
		return(false);
	if (checa_caracter(campo[4],n_campo[4],"1234567890.,")==false)
		return(false);

	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>


<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_editar" method="post" action="avulso_acoes.asp?acao=incluir" onsubmit="return checa(this);">
	<input type="hidden" name="ID_usuario" value="<%=formatar("ID_usuario",1,2)%>" />
	<tr>
		<td>
		
			<table border="0" cellpadding="1" cellspacing="0" width="100%">
            	<tr>
                    <td colspan="2"><b>T�tulo do Pagamento Avulso</b></td>
                </tr>
                <tr>
                	<TD colspan="2">
						<input type="text" name="titulo" style="width:100%" maxlength="240" />
					</TD>
                </tr>
                <TR>
					<TD colspan="2">
						<b>Descri��o do Pagamento Avulso:</b><BR>
						<textarea name="descricao" style="width:100%; height:100px;"></textarea>
					</TD>
				</TR>
				<TR>
					<TD width="50%"><b>UID:</b></TD>
					<TD width="50%"><b>Data do Pagamento:</b></TD>
				</TR>
				<TR>
					<TD>
						<input type="text" name="uid" style="width:100%" maxlength="200" />
					</TD>
					<TD>
						<input type="text" name="data_dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=right("0"&day(now()),2)%>" /> / 
						<input type="text" name="data_mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=right("0"&month(now()),2)%>" /> / 
						<input type="text" name="data_ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=year(now())%>" />
					</TD>
				</TR>
				<TR>
					<TD width="50%"><b>Valor:</b></TD>
					<TD width="50%"><b>Estatus:</b></TD>
				</TR>
				<TR>
					<TD>R$ <input type="text" name="valor" style="width:90%" maxlength="15" /></TD>
					<TD>
                    	
                    	<select name="estatus" style="width:100%;">
							<%
                            call CONEXAO_XML("../_xml/pagamentos_cod_status.xml")
                            set raiz = xmlDoc.documentElement
                            
                            for each registro in raiz.childNodes
                            %>
							<option value="<%=registro.selectSingleNode("@codigo").text%>"><%=registro.text%></option>
						<%next%>
						</select>
                    	
                    </TD>
				</TR>
                <TR>
					<TD width="50%"><b>Meio:</b></TD>
					<TD width="50%"><b>Forma:</b></TD>
				</TR>
				<TR>
					<TD>
                    
                    	<select name="meio" style="width:100%;">
							<%
                            call CONEXAO_XML("../_xml/pagamentos_meio.xml")
                            set raiz = xmlDoc.documentElement
                            
                            for each registro in raiz.childNodes
                            %>
							<option value="<%=registro.selectSingleNode("@codigo").text%>"><%=registro.text%></option>
						<%next%>
						</select>
                    
                    </TD>
					<TD>
                    	
                    	<select name="forma" style="width:100%;" onchange="javascript: return parcelas(a_editar.forma.value);">
							<%
                            call CONEXAO_XML("../_xml/pagamentos_forma.xml")
                            set raiz = xmlDoc.documentElement
                            
                            for each registro in raiz.childNodes
                            %>
							<option value="<%=registro.selectSingleNode("@codigo").text%>"><%=registro.text%></option>
						<%next%>
						</select>
                    	
                    </TD>
				</TR>
                <TR>
					<TD colspan="2">
						<b>Observa��es:</b><BR>
						<textarea name="observacoes" style="width:100%; height:100px;"></textarea>
					</TD>
				</TR>
                
                <script type="text/javascript">
					function parcelas(qtd)
					{
						document.getElementById("tr_parcelas").style.display = 'block';
					
						//alert(qtd);
						qtd = qtd.replace( "A0","" );
						qtd = qtd.replace( "A","" );
						qtd = qtd.replace( "B0","" );
						qtd = qtd.replace( "B","" );
					
						for (i=1; i<=qtd; i++)
						{
							document.getElementById("div_parcela_"+i).style.display = 'block';
						}
						qtd = i;
						for (i=qtd; i<=12; i++)
						{
							document.getElementById("div_parcela_"+i).style.display = 'none';
						}
					}
				</script>
                
                <TR id="tr_parcelas" style="display:block;">
					<TD colspan="2">
						
                        <b>Parcelas:</b><BR />
                        
                        <%for i = 1 to 12%>
                        <div id="div_parcela_<%=i%>" style="display:<%if i=1 then%>block<%else%>none<%end if%>;">
                        	<input type="checkbox" name="parcela_<%=i%>" id="check_<%=i%>" value="1" /> 
                            <label for="check_<%=i%>">Parcela # <%=i%> 
                            	<input type="text" style="width:200px;" name="obs_parcela_<%=i%>" maxlength="100" />
                                Valor: R$ <input type="text" style="width:60px;" name="str_valores_<%=i%>" maxlength="20" /> 
                                Data: <input type="text" style="width:80px;" name="str_datas_<%=i%>" maxlength="10" />
                            </label>
                        </div>
                        <%next%>
                        
					</TD>
				</TR>
                
			</table>
			
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Incluir" style="width:150px;" />
		</td>
	</tr>
    </form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->