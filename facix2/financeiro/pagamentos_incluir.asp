<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Incluir Pagamento@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE PRESTADORES
function checa(formulario)
{
	campo=	[
				formulario.uid,
				formulario.data_dia,
				formulario.data_mes,
				formulario.data_ano,
				formulario.valor
			];
			
	n_campo=[
				"UID",
				"Dia da Data de Pagamento",
				"M�s da Data de Pagamento",
				"Ano da Data de Pagamento",
				"Valor"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
		
		
	if (checa_tamanho(campo[1],n_campo[1],1)==false)
		return(false);
	if (checa_caracter(campo[1],n_campo[1],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[2],n_campo[2],1)==false)
		return(false);
	if (checa_caracter(campo[2],n_campo[2],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[3],n_campo[3],4)==false)
		return(false);
	if (checa_caracter(campo[3],n_campo[3],"1234567890")==false)
		return(false);
		
	if (checa_nulo(campo[4],n_campo[4])==false)
		return(false);
	if (checa_caracter(campo[4],n_campo[4],"1234567890.,")==false)
		return(false);

	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>


<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_editar" method="post" action="pagamentos_acoes.asp?acao=incluir" onsubmit="return checa(this);">
	<input type="hidden" name="ID_usuario" value="<%=formatar("ID_usuario",1,2)%>" />
	<tr>
		<td>
		
			<table border="0" cellpadding="1" cellspacing="0" width="100%">
            	<tr>
                    <td colspan="2"><b>Curso</b></td>
                </tr>
                <tr>
                    <td colspan="2">
                        
                        <%
                        sql = "select top 100 CURSOS_DATAS.*, CURSOS.titulo_curso from CURSOS_DATAS inner join CURSOS on CURSOS_DATAS.ID_curso=CURSOS.ID_curso where (inicio_data >= '"& DATA_BD(dateadd("d",-180,now())) &"' or (fim_data>='"&DATA_BD(now())&"')) order by titulo_curso, inicio_data desc"
						set rs_data = conexao.execute(sql)
						
						if not rs_data.EOF then
						%>

						<select name="ID_curso" style="width:100%;">
							<%
							ID_curso = 0
							do while not rs_data.EOF
							%>
                            
                            	<%if cstr(rs_data("ID_curso")) <> cstr(ID_curso) then%>
                                    <optgroup label="<%=rs_data("titulo_curso")%>"></optgroup>
                                    <%
                                    ID_curso = rs_data("ID_curso")
                                    %>
                                <%end if%>
									
                            	<option value="<%=rs_data("ID_curso")%>:<%=rs_data("ID_data")%>"><%=rs_data("horario_data")%> / de <%=FORMATA_DATA( rs_data("inicio_data"), 1 )%> � <%=FORMATA_DATA( rs_data("fim_data"), 1 )%></option>
								
							<%rs_data.movenext:loop%>
                            
                            <%
							'' Ver cursos � dist�ncia ''
							sql = "select ID_curso, titulo_curso from CURSOS where distancia_curso="&verdade&""
							set rs_data = conexao.execute(sql)
							
							if not rs_data.EOF then
							%>
                            	<optgroup label="Cursos a dist�ncia">
                                
                            		<option value="<%=rs_data("ID_curso")%>:0"><%=rs_data("titulo_curso")%></option>
                                
                                </optgroup>
                            <%end if%>
                            
                            
						</select>
						
						<%end if%> 
                        
                    </td>
                </tr>
				<TR>
					<TD width="50%"><b>UID:</b></TD>
					<TD width="50%"><b>Data do Pagamento:</b></TD>
				</TR>
				<TR>
					<TD>
						<input type="text" name="uid" style="width:100%" maxlength="200" />
					</TD>
					<TD>
						<input type="text" name="data_dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=right("0"&day(now()),2)%>" /> / 
						<input type="text" name="data_mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=right("0"&month(now()),2)%>" /> / 
						<input type="text" name="data_ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=year(now())%>" />
					</TD>
				</TR>
                <TR>
					<TD width="50%"><b>N� Inscri��o:</b></TD>
					<TD width="50%"><b>Modalidade:</b></TD>
				</TR>
				<TR>
					<TD>
                    	<%
						sql = "select top 1 numero_inscricao from CURSOS_INSCRICOES order by numero_inscricao desc"
						set rsi = conexao.execute(sql)
						
						if not rsi.EOF then
							numero_inscricao = rsi("numero_inscricao") + 1
						end if
						%>
						<input type="text" name="numero_inscricao" style="width:100%" maxlength="200" value="<%=numero_inscricao%>" />
					</TD>
					<TD>
                    	<select name="modalidade" style="width:98%;">
							<option value="0" selected="selected">Presencial</option>
                            <option value="1">Online</option>
						</select>
                    </TD>
				</TR>
				<TR>
					<TD width="50%"><b>Valor:</b></TD>
					<TD width="50%"><b>Estatus:</b></TD>
				</TR>
				<TR>
					<TD>R$ <input type="text" name="valor" style="width:90%" maxlength="15" /></TD>
					<TD>
                    	
                    	<select name="estatus" style="width:100%;">
							<%
                            call CONEXAO_XML("../_xml/pagamentos_cod_status.xml")
                            set raiz = xmlDoc.documentElement
                            
                            for each registro in raiz.childNodes
                            %>
							<option value="<%=registro.selectSingleNode("@codigo").text%>"><%=registro.text%></option>
						<%next%>
						</select>
                    	
                    </TD>
				</TR>
                <TR>
					<TD width="50%"><b>Meio:</b></TD>
					<TD width="50%"><b>Forma:</b></TD>
				</TR>
				<TR>
					<TD>
                    
                    	<select name="meio" style="width:100%;">
							<%
                            call CONEXAO_XML("../_xml/pagamentos_meio.xml")
                            set raiz = xmlDoc.documentElement
                            
                            for each registro in raiz.childNodes
                            %>
							<option value="<%=registro.selectSingleNode("@codigo").text%>"><%=registro.text%></option>
						<%next%>
						</select>
                    
                    </TD>
					<TD>
                    	
                    	<select name="forma" style="width:100%;" onchange="javascript: return parcelas(a_editar.forma.value);">
							<%
                            call CONEXAO_XML("../_xml/pagamentos_forma.xml")
                            set raiz = xmlDoc.documentElement
                            
                            for each registro in raiz.childNodes
                            %>
							<option value="<%=registro.selectSingleNode("@codigo").text%>"><%=registro.text%></option>
						<%next%>
						</select>
                    	
                    </TD>
				</TR>
                <TR>
					<TD colspan="2">
						<b>Observa��es:</b><BR>
						<textarea name="observacoes" style="width:100%; height:100px;"></textarea>
					</TD>
				</TR>
                
                <script type="text/javascript">
					function parcelas(qtd)
					{
						document.getElementById("tr_parcelas").style.display = 'block';
					
						//alert(qtd);
						qtd = qtd.replace( "A0","" );
						qtd = qtd.replace( "A","" );
						qtd = qtd.replace( "B0","" );
						qtd = qtd.replace( "B","" );
					
						for (i=1; i<=qtd; i++)
						{
							document.getElementById("div_parcela_"+i).style.display = 'block';
						}
						qtd = i;
						for (i=qtd; i<=12; i++)
						{
							document.getElementById("div_parcela_"+i).style.display = 'none';
						}
					}
				</script>
                
                <TR id="tr_parcelas" style="display:block;">
					<TD colspan="2">
						
                        <b>Parcelas:</b><BR />
                        
                        <%for i = 1 to 12%>
                        <div id="div_parcela_<%=i%>" style="display:<%if i=1 then%>block<%else%>none<%end if%>;">
                        	<input type="checkbox" name="parcela_<%=i%>" id="check_<%=i%>" value="1" /> 
                            <label for="check_<%=i%>">Parcela # <%=i%> 
                            	<input type="text" style="width:200px;" name="obs_parcela_<%=i%>" maxlength="100" />
                            	Valor: R$ <input type="text" style="width:60px;" name="str_valores_<%=i%>" id="str_valores_<%=i%>" maxlength="20" /> 
                                Data: <input type="text" style="width:80px;" name="str_datas_<%=i%>" maxlength="10" /> 
                            </label>
                        </div>
                        <%next%>
                        
                        <div style="clear:both; display:block; padding-left:270px; padding-top:5px; padding-bottom:15px; font-size:14px; font-weight:bold;">TOTAL: R$ <label id="total_calculado">0,00</label></div>
                        
                        <script type="text/javascript">
							function calcula_total()
							{
								valor_total_calculado = 0;
								for (i=1; i<=12; i++)
								{
									objeto = document.getElementById("div_parcela_"+i);
									
									if (objeto.style.display == 'block')
									{
										if ( document.getElementById("str_valores_"+i).value.length > 0 )
										{
											valor_temp = document.getElementById("str_valores_"+i).value.replace(".","");
											valor_temp = valor_temp.replace(",",".");
											valor_total_calculado = valor_total_calculado + parseFloat( valor_temp );
										}
									}
									
								}
								
								valor_total_calculado = valor_total_calculado.toFixed(2);
								
								document.getElementById("total_calculado").innerHTML = valor_total_calculado.replace(".",",");
								
							}
							setInterval("calcula_total();",1000);
						</script>
                        
					</TD>
				</TR>
                
			</table>
			
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Incluir" style="width:150px;" />
		</td>
	</tr>
    </form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->