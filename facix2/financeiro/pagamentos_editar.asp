<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar Pagamento@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>
    
<script src="../_base/codebase/dhtmlxcommon.js"></script>
<script src="../_base/codebase/dhtmlxcombo.js"></script>
<link rel="STYLESHEET" type="text/css" href="../_base/codebase/dhtmlxcombo.css">
<script>
	window.dhx_globalImgPath="../_base/codebase/imgs/";
</script>
<script src="../_base/codebase/ext/dhtmlxcombo_whp.js"></script>

<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE PRESTADORES
function checa(formulario)
{
	campo=	[
				formulario.uid,
				formulario.data_dia,
				formulario.data_mes,
				formulario.data_ano,
				formulario.valor
			];
			
	n_campo=[
				"UID",
				"Dia da Data de Pagamento",
				"M�s da Data de Pagamento",
				"Ano da Data de Pagamento",
				"Valor"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
		
		
	if (checa_tamanho(campo[1],n_campo[1],1)==false)
		return(false);
	if (checa_caracter(campo[1],n_campo[1],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[2],n_campo[2],1)==false)
		return(false);
	if (checa_caracter(campo[2],n_campo[2],"1234567890")==false)
		return(false);
	if (checa_tamanho(campo[3],n_campo[3],4)==false)
		return(false);
	if (checa_caracter(campo[3],n_campo[3],"1234567890")==false)
		return(false);
		
	if (checa_nulo(campo[4],n_campo[4])==false)
		return(false);
	if (checa_caracter(campo[4],n_campo[4],"1234567890.,")==false)
		return(false);

	formulario.botao_submit.value = 'Editando, aguarde...';
	formulario.botao_submit.disabled = true;

	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID	=	formatar("ID",1,2)

if not ID=empty then
	sql= "select * from PAGAMENTOS where ID_pagamento="&ID
	set rs= conexao.execute(sql)
	
	if not rs.EOF then
		identificacao_pagamento = rs("identificacao_pagamento")
		ID_inscricao 			= rs("ID_inscricao")
		data_pagamento 			= rs("data_pagamento")
		valor_pagamento 		= rs("valor_pagamento")
		cod_status_pagamento 	= rs("cod_status_pagamento")
		meio_pagamento 			= rs("meio_pagamento")
		forma_pagamento 		= rs("forma_pagamento")
		obs_pagamento 			= rs("obs_pagamento")
		parcelas_obs_pagamento 	= rs("parcelas_obs_pagamento")
		str_valores_pagamento 	= rs("str_valores_pagamento")
		str_datas_pagamento 	= rs("str_datas_pagamento")
		parcelas_pagamento 		= rs("parcelas_pagamento")
	end if
else
	ID_inscricao = formatar("ID_inscricao",1,2)
	
	sql = "select * from CURSOS_PLANOS inner join CURSOS_INSCRICOES on CURSOS_INSCRICOES.ID_plano = CURSOS_PLANOS.ID_plano where ID_inscricao="&ID_inscricao&""
	set rs_plano = conexao.execute(sql)
	
	data_pagamento = now()
	valor_pagamento = rs_plano("valor_plano")
	forma_pagamento = "A01"

	sql = "select * from CURSOS_INSCRICOES where ID_inscricao="&ID_inscricao&""
	set rs2 = conexao.execute(sql)

	forma_pagamento = "A"&right( ("0"&rs2("pagto_forma")), 2 )
	meio_pagamento = rs2("pagto_meio")
	
end if


sql = "select * from CURSOS_INSCRICOES where ID_inscricao="&ID_inscricao&""
set rs2 = conexao.execute(sql)

if not rs2.EOF then
	ID_curso 	= rs2("ID_curso")
	ID_plano 	= rs2("ID_plano")
	ID_data 	= rs2("ID_data")
	modalidade	= rs2("online_inscricao")
	
	if isnull(modalidade) then
		modalidade = false
	end if
	
	sql = "select * from CURSOS_PLANOS where ID_plano="&ID_plano&""
	set rs2 = conexao.execute(sql)
	
	if not rs2.EOF then
		empresarial = rs2("empresarial_plano")
	end if
end if
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_editar" method="post" action="pagamentos_acoes.asp?acao=editar" onsubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
    <input type="hidden" name="ID_inscricao" value="<%=ID_inscricao%>" />
    <tr>
    	<TD>
        	<input type="hidden" name="mudar_curso" value="0" id="id_mudar_curso" />
        	<div id="div_01"><a href="javascript:" onclick="document.getElementById('div_01').style.display='none';document.getElementById('div_02').style.display='block';document.getElementById('id_mudar_curso').value='1';">Mudar Curso e data deste pagamento</a></div>
        
        	<div id="div_02" style="display:none;">
            	
                <%
				sql = "select top 100 CURSOS_DATAS.*, CURSOS.titulo_curso from CURSOS_DATAS inner join CURSOS on CURSOS_DATAS.ID_curso=CURSOS.ID_curso where (inicio_data >= '"& DATA_BD(dateadd("d",-180,now())) &"' or (fim_data>='"&DATA_BD(now())&"')) order by titulo_curso, inicio_data desc"
				set rs_data = conexao.execute(sql)
				
				if not rs_data.EOF then
				%>

				<select name="ID_curso" id="zona_ID_curso" style="width:98%; height:200px;" multiple="multiple">
					<%
					ID_curso = 0
					do while not rs_data.EOF
					%>
					
						<%if cstr(rs_data("ID_curso")) <> cstr(ID_curso) then%>
							<optgroup label="<%=rs_data("titulo_curso")%>"></optgroup>
							<%
							ID_curso = rs_data("ID_curso")
							%>
						<%end if%>
							
						<option value="<%=rs_data("ID_curso")%>:<%=rs_data("ID_data")%>"><%=rs_data("horario_data")%> / de <%=FORMATA_DATA( rs_data("inicio_data"), 1 )%> � <%=FORMATA_DATA( rs_data("fim_data"), 1 )%></option>
						
					<%rs_data.movenext:loop%>
					
					<%
					'' Ver cursos � dist�ncia ''
					sql = "select ID_curso, titulo_curso from CURSOS where distancia_curso="&verdade&""
					set rs_data = conexao.execute(sql)
					
					if not rs_data.EOF then
					%>
						<optgroup label="Cursos a dist�ncia">
						
							<option value="<%=rs_data("ID_curso")%>:0"><%=rs_data("titulo_curso")%></option>
						
						</optgroup>
					<%end if%>
					
					
				</select>
                <script>
					/*if (detecta_navegador())
					{
						var x = dhtmlXComboFromSelect("zona_ID_curso");
						x.readonly("enable");
						x.setOptionHeight(120);
						x.enableOptionAutoPositioning(false);
					}*/
				</script>
				
				<%end if%> 
                
                <BR />
                <a href="javascript:" onclick="document.getElementById('div_01').style.display='block';document.getElementById('div_02').style.display='none';document.getElementById('id_mudar_curso').value='0';">N�o trocar de curso</a></div>
            </div>
        </TD>
    </tr>
	<tr>
		<td>
		
			<table border="0" cellpadding="1" cellspacing="0" width="100%">
				<TR>
					<TD width="50%"><b>UID:</b></TD>
					<TD width="50%"><b>Data do Pagamento:</b></TD>
				</TR>
				<TR>
					<TD>
						<input type="text" name="uid" style="width:100%" maxlength="200" value="<%=identificacao_pagamento%>" />
					</TD>
					<TD>
						<input type="text" name="data_dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=right("0"&day(data_pagamento),2)%>" /> / 
						<input type="text" name="data_mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=right("0"&month(data_pagamento),2)%>" /> / 
						<input type="text" name="data_ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=year(data_pagamento)%>" />
					</TD>
				</TR>
                <TR>
					<TD width="50%"><b>N� Inscri��o:</b></TD>
					<TD width="50%"><b>Modalidade:</b></TD>
				</TR>
				<TR>
					<TD>
                    	<%
						sql = "select numero_inscricao from CURSOS_INSCRICOES where ID_inscricao="&ID_inscricao&""
						set rsi = conexao.execute(sql)
						
						if not rsi.EOF then
							numero_inscricao = rsi("numero_inscricao")
						end if
						%>
						<input type="text" name="numero_inscricao" style="width:100%" maxlength="200" value="<%=numero_inscricao%>" />
					</TD>
					<TD>
                    	<select name="modalidade" style="width:98%;">
							<option value="0"<%if modalidade=false then%> selected="selected"<%end if%>>Presencial</option>
                            <option value="1"<%if modalidade=true then%> selected="selected"<%end if%>>Online</option>
						</select>
                    </TD>
				</TR>
				<TR>
					<TD width="50%"><b>Valor:</b></TD>
					<TD width="50%"><b>Estatus:</b></TD>
				</TR>
				<TR>
					<TD>R$ <input type="text" name="valor" style="width:90%" maxlength="15" value="<%=FORMATA_MOEDA( valor_pagamento, false )%>" /></TD>
					<TD>
                    	
						<input type="hidden" name="estatus_antigo" value="<%=cod_status_pagamento%>" />
                    	<select name="estatus" id="zona_estatus" style="width:98%;">
							<%
                            call CONEXAO_XML("../_xml/pagamentos_cod_status.xml")
                            set raiz = xmlDoc.documentElement
                            
                            for each registro in raiz.childNodes
                            %>
							<option value="<%=registro.selectSingleNode("@codigo").text%>"<%if cod_status_pagamento=registro.selectSingleNode("@codigo").text then%> selected="selected"<%end if%>><%=registro.text%></option>
						<%next%>
						</select>
                        <script>
							if (detecta_navegador())
							{
								var x = dhtmlXComboFromSelect("zona_estatus");
								x.readonly("enable");
								x.setOptionHeight(120);
								x.enableOptionAutoPositioning(false);
							}
						</script>
                    	
                    </TD>
				</TR>
                <TR>
					<TD width="50%"><b>Meio:</b></TD>
					<TD width="50%"><b>Forma:</b></TD>
				</TR>
				<TR>
					<TD>
                    
                    	<select name="meio" id="zona_meio" style="width:98%;" onchange="javascript: return parcelas(a_editar.forma.value);">
							<%
                            call CONEXAO_XML("../_xml/pagamentos_meio.xml")
                            set raiz = xmlDoc.documentElement
                            
                            for each registro in raiz.childNodes
								if meio_pagamento=registro.selectSingleNode("@codigo").text then
									str_titulo_meio_pagto = registro.text
								end if
                            %>
								<option value="<%=registro.selectSingleNode("@codigo").text%>"<%if meio_pagamento=registro.selectSingleNode("@codigo").text then%> selected="selected"<%end if%>><%=registro.text%></option>
							<%next%>
						</select>
                        <script>
							if (detecta_navegador())
							{
								var x = dhtmlXComboFromSelect("zona_meio");
								x.readonly("enable");
								x.setOptionHeight(120);
								x.enableOptionAutoPositioning(false);
							}
						</script>
                    
                    </TD>
					<TD>
                    	
                    	<select name="forma" id="zona_forma" style="width:98%;" onchange="javascript: return parcelas(a_editar.forma.value);">
							<%
                            call CONEXAO_XML("../_xml/pagamentos_forma.xml")
                            set raiz = xmlDoc.documentElement
                            
                            for each registro in raiz.childNodes
                            %>
								<option value="<%=registro.selectSingleNode("@codigo").text%>"<%if forma_pagamento=registro.selectSingleNode("@codigo").text then%> selected="selected"<%end if%>><%=registro.text%></option>
							<%next%>
						</select>
                        <script>
							/*if (detecta_navegador())
							{
								var x = dhtmlXComboFromSelect("zona_forma");
								x.readonly("enable");
								x.setOptionHeight(120);
								x.enableOptionAutoPositioning(false);
							}*/
						</script>
                    	
                    </TD>
				</TR>
                <TR>
					<TD colspan="2">
						<b>Observa��es:</b><BR>
						<textarea name="observacoes" style="width:100%; height:100px;"><%if not obs_pagamento=empty then%><%=formatar(obs_pagamento&"",4,4)%><%end if%></textarea>
					</TD>
				</TR>
                
                <script type="text/javascript">
					function parcelas(qtd)
					{
						//document.getElementById("tr_parcelas").style.display = 'block';

						// Se for pagamento via boleto exibir links
						if ( $('#zona_meio').val() == 6 ){
							$('.link_boleto').show();
						}else{
							$('.link_boleto').hide();
						}
					
						//alert(qtd);
						qtd = qtd.replace( "A0","" );
						qtd = qtd.replace( "A","" );
						qtd = qtd.replace( "B0","" );
						qtd = qtd.replace( "B","" );
					
						for (i=1; i<=qtd; i++)
						{
							document.getElementById("div_parcela_"+i).style.display = 'block';
						}
						qtd = i;
						for (i=qtd; i<=12; i++)
						{
							document.getElementById("div_parcela_"+i).style.display = 'none';
						}
					}
				</script>
                
                <TR id="tr_parcelas">
					<TD colspan="2" width="100%">
						
                        <b>Parcelas:</b><BR />
                        
                        <%
						obs_parcelas = parcelas_obs_pagamento
						if obs_parcelas = empty or isnull(obs_parcelas) then
							obs_parcelas = ";p1[:][x];p2[:][x];p3[:][x];p4[:][x];p5[:][x];p6[:][x];p7[:][x];p8[:][x];p9[:][x];p10[:][x];p11[:][x];p12[:][x]"
							
							'' se � pagamento com cart�o, colocar o texto automaticamente em cada parcela
							if ( meio_pagamento = 2 or meio_pagamento = 11 or meio_pagamento = 1 or meio_pagamento = 7 ) then
								str_titulo_meio_pagto = str_titulo_meio_pagto&" Cr�dito"
								obs_parcelas = ""
								for conta=1 to 12
									obs_parcelas = obs_parcelas & ";p"&conta&"[:]"&str_titulo_meio_pagto&""
								next
							end if
							
						end if
						obs_parcelas = split( obs_parcelas, ";" )
						
						str_valores = str_valores_pagamento
						if str_valores = empty or isnull(str_valores) then
							str_valores = ";p1[:][x];p2[:][x];p3[:][x];p4[:][x];p5[:][x];p6[:][x];p7[:][x];p8[:][x];p9[:][x];p10[:][x];p11[:][x];p12[:][x]"
						end if
						str_valores = split( str_valores, ";" )
						
						str_datas = str_datas_pagamento
						if str_datas = empty or isnull(str_datas) then
							str_datas = ";p1[:][x];p2[:][x];p3[:][x];p4[:][x];p5[:][x];p6[:][x];p7[:][x];p8[:][x];p9[:][x];p10[:][x];p11[:][x];p12[:][x]"
							
							'' se � pagamento com cart�o, colocar o texto automaticamente em cada parcela
							if ( meio_pagamento = 2 or meio_pagamento = 11 or meio_pagamento = 1 or meio_pagamento = 7 ) then
								str_datas 	= ""
								data_temp	= data_pagamento
								for conta=1 to 12
									str_datas = str_datas & ";p"&conta&"[:]"&FORMATA_DATA( dateadd( "m", conta, data_temp ), 1 )&""
								next
							end if
						end if
						str_datas = split( str_datas, ";" )
						%>
                        
                        <%
						'' para preencher caso os campos estejam vazios ''
						valor_temp 	= FORMATA_MOEDA( valor_pagamento / replace( replace( forma_pagamento, "A", "" ), "B", ""), false )
						data_temp	= data_pagamento
						%>
                        
                        <%for i = 1 to 12%>
                        
                        	<%
							obs_temp 			= split( obs_parcelas(i), "[:]" )
							str_valores_temp 	= split( str_valores(i), "[:]" )
							str_datas_temp 		= split( str_datas(i), "[:]" )
							%>
                        
                            <div id="div_parcela_<%=i%>" style="display:<%if i=1 then%>block<%else%>none<%end if%>;">
                                <input type="checkbox" name="parcela_<%=i%>" id="check_<%=i%>" value="1"<%if instr(parcelas_pagamento,";p"&i&":1") > 0 then%> checked="checked"<%end if%> /> 
                                <label for="check_<%=i%>">Parcela # <%=i%> 
                                	<input type="text" style="width:180px;" name="obs_parcela_<%=i%>" maxlength="100" value="<%=replace( obs_temp(1), "[x]", "" )%>" />
                                    Valor: R$ <input type="text" style="width:60px;" name="str_valores_<%=i%>" id="str_valores_<%=i%>" maxlength="20" value="<%if replace( str_valores_temp(1), "[x]", "" )="" then%><%=valor_temp%><%else%><%=replace( str_valores_temp(1), "[x]", "" )%><%end if%>" /> 
                                	Data: <input type="text" style="width:80px;" name="str_datas_<%=i%>" maxlength="10" value="<%if str_datas_temp(1)<>"[x]" then%><%=FORMATA_DATA( str_datas_temp(1), 1 )%><%else%><%=FORMATA_DATA( dateadd( "m", (i-1), data_temp ), 1 )%><%end if%>" /> 
                                </label>
								<a href="http://www.methodus.com.br/site/boleto/?ID=<%=ID_inscricao%>&referencia=1&parcela=<%=i%>" target="_blank" class="link_boleto" style="display:none;">Imprimir</a>
                            </div>
                            
                        <%next%>
                        
                        <div style="clear:both; display:block; padding-left:270px; padding-top:5px; padding-bottom:15px; font-size:14px; font-weight:bold;">TOTAL: R$ <label id="total_calculado">0,00</label></div>
                        
                        <script type="text/javascript">
							function calcula_total()
							{
								valor_total_calculado = 0;
								for (i=1; i<=12; i++)
								{
									objeto = document.getElementById("div_parcela_"+i);
									
									if (objeto.style.display == 'block')
									{
										if ( document.getElementById("str_valores_"+i).value.length > 0 )
										{
											valor_temp = document.getElementById("str_valores_"+i).value.replace(".","");
											valor_temp = valor_temp.replace(",",".");
											valor_total_calculado = valor_total_calculado + parseFloat( valor_temp );
										}
									}
									
								}
								
								valor_total_calculado = valor_total_calculado.toFixed(2);
								
								document.getElementById("total_calculado").innerHTML = valor_total_calculado.replace(".",",");
								
							}
							setInterval("calcula_total();",1000);
						</script>
                        
					</TD>
				</TR>
                
                <%if forma_pagamento<>"A01" then%>
                <script type="text/javascript">
					
					parcelas('<%=forma_pagamento%>');
					
				</script>
                <%end if%>
                
			</table>
			
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Editar" style="width:150px;" id="botao_submit" />
		</td>
	</tr>
    </form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->