<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/cls_webservice.asp" -->
<%
call checalogado()

acao 			= 	formatar("acao",1,2)

uid 			=	formatar("uid",1,1)
data_dia 		=	formatar("data_dia",1,1)
data_mes 		=	formatar("data_mes",1,1)
data_ano 		=	formatar("data_ano",1,1)
valor 			=	FORMATA_MOEDA( formatar("valor",1,1), true )
estatus			=	formatar("estatus",1,1)
estatus_antigo	=	formatar("estatus_antigo",1,1)
meio	 		=	formatar("meio",1,1)
forma 			=	formatar("forma",1,1)
observacoes		=	formatar("observacoes",2,1)
ID_curso		= 	formatar("ID_curso",1,1)
ID_usuario		= 	formatar("ID_usuario",1,1)
inscricao 		= 	formatar("numero_inscricao",1,1)
modalidade 		= 	formatar("modalidade",1,1)

mudar_curso		= 	formatar("mudar_curso",1,1)

ID 				= 	formatar("ID",1,3)
ID_inscricao 	= 	formatar("ID_inscricao",1,1)

'' formatar vari�ves que chegam separadas ''
if isdate(""&data_dia&"/"&data_mes&"/"&data_ano&"") then
	data		=	DATA_BD( ""&data_dia&"/"&data_mes&"/"&data_ano&"" )
end if
''''''''''''''''''''''''''''''''''''''''''''

'' formatar variavel de parcelas ''
for i=1 to 12

	'' string da parcela
	parcela_temp = formatar( "parcela_"&i ,1,1 )
	if parcela_temp = empty then
		parcela_temp = 0
	end if
	
	parcelas = parcelas & ";p"&i&":"&parcela_temp
	
	'' string das observa��es
	obs_parcela_temp = formatar( "obs_parcela_"&i ,1,1 )
	if obs_parcela_temp = empty then
		obs_parcela_temp = "[x]"
	end if
	
	obs_parcelas = obs_parcelas & ";p"&i&"[:]"&obs_parcela_temp
	
	'' string das valores
	str_valores_temp = formatar( "str_valores_"&i ,1,1 )
	if str_valores_temp = empty then
		str_valores_temp = "[x]"
	end if
	
	str_valores = str_valores & ";p"&i&"[:]"&str_valores_temp
	
	'' string das datas
	str_datas_temp = formatar( "str_datas_"&i ,1,1 )
	if str_datas_temp = empty then
		str_datas_temp = "[x]"
	else
		str_datas_temp = FORMATA_DATA( str_datas_temp, 3 )
	end if
	
	str_datas = str_datas & ";p"&i&"[:]"&str_datas_temp
	
next
'''''''''''''''''''''''''''''''''''


'INCLUS�O
if acao="incluir" then

	if ID_usuario=empty or ID_curso=empty or UID=empty or data=empty or estatus=empty or meio=empty or forma=empty or valor=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	'' separar variavel de curso e data ''
	partes 		= split( ID_curso, ":" )
	ID_curso 	= partes(0)
	ID_data 	= partes(1)
	''''''''''''''''''''''''''''''''''''''
	
		'' procurar plano adequado ''
		sql = "select ID_plano from CURSOS_PLANOS where empresarial_plano="&falso&" and ID_curso="&ID_curso
		set rs_plano = conexao.execute(sql)
		
		ID_plano = rs_plano("ID_plano")
		'''''''''''''''''''''''''''''
	
		'' incluir inscri��o ''
		sql = 	"insert into CURSOS_INSCRICOES (data_inscricao, ID_curso, ID_plano, ID_data, pago_inscricao, online_inscricao) values ('"&DATA_BD( now() )&"', "&ID_curso&", "&ID_plano&", "&ID_data&", "&falso&", "&modalidade&")"&_
				" select @@identity as NovoId"
		set rs = conexao.execute(sql).nextrecordset
		
		ID_inscricao = rs("NovoId")
		'''''''''''''''''''''''
		
		if isnumeric(inscricao) then
		
			sql = "UPDATE CURSOS_INSCRICOES set numero_inscricao="&inscricao&" where ID_inscricao="&ID_inscricao&""
			conexao.execute(sql)
			
		end if
		
		
		'' incluir usu�rio na inscri��o ''
		sql = "insert into CURSOS_INSCRICOES_USUARIOS (ID_inscricao, ID_usuario) values ("&ID_inscricao&","&ID_usuario&")"
		conexao.execute(sql)
		''''''''''''''''''''''''''''''''''
		
		'' Criar um novo registro de pagamento para o pedido ''
		sql =	"insert into PAGAMENTOS (ID_inscricao, identificacao_pagamento, cod_status_pagamento, data_pagamento, "&_
				"forma_pagamento, meio_pagamento, valor_pagamento, obs_pagamento, parcelas_pagamento, parcelas_obs_pagamento, str_valores_pagamento, str_datas_pagamento) values "&_
				"("&ID_inscricao&",'"&UID&"','"&estatus&"','"&data&"', "&_
				"'"&forma&"','"&meio&"',"&valor&",'"&observacoes&"','"&parcelas&"','"&obs_parcelas&"','"&str_valores&"','"&str_datas&"')"
		conexao.execute(sql)
		
		if estatus=3 or estatus="3" or estatus=6 or estatus="6" or estatus=11 or estatus="11" or estatus=2 or estatus="2" then
			call LIBERAR_ACESSO( ID_inscricao )

			if estatus<>2 and estatus<>"2" and estatus<>6 and estatus<>"6" then
				call ENVIAR_CONTRATO( null, ID_inscricao )
			end if
		end if
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
	session("sucesso") = "0001"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	
	
'EXCLUIR
elseif acao="excluir" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if
	
	'' deletar registros relacionados ''
	sql = "select ID_inscricao from PAGAMENTOS where ID_pagamento="&ID
	set rs = conexao.execute(sql)
	
	if not rs.EOF then
		
		sql = "delete from CURSOS_INSCRICOES_EMPRESAS where ID_inscricao="&rs("ID_inscricao")
		conexao.execute(sql)
		
		sql = "delete from CURSOS_INSCRICOES_USUARIOS where ID_inscricao="&rs("ID_inscricao")
		conexao.execute(sql)
		
		sql = "delete from CURSOS_INSCRICOES where ID_inscricao="&rs("ID_inscricao")
		conexao.execute(sql)
		
	end if
	
	
	sql="delete from PAGAMENTOS where ID_pagamento="&ID
	conexao.execute(sql)
	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))
	
	

'EDITAR
elseif acao="editar" then

	if (ID=empty and ID_inscricao=empty) or UID=empty or data=empty or estatus=empty or meio=empty or forma=empty or valor=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	if not ID=empty then
	
		sql =	"update PAGAMENTOS set identificacao_pagamento='"&uid&"', data_pagamento='"&data&"', valor_pagamento="&valor&", cod_status_pagamento='"&estatus&"'"&_
				", meio_pagamento='"&meio&"', forma_pagamento='"&forma&"', obs_pagamento='"&observacoes&"', parcelas_pagamento='"&parcelas&"', parcelas_obs_pagamento='"&obs_parcelas&"', "&_
				"str_valores_pagamento='"&str_valores&"', str_datas_pagamento='"&str_datas&"' where ID_pagamento="&ID
		conexao.execute(sql)
		
	else
	
		sql =	"insert into PAGAMENTOS (ID_inscricao, identificacao_pagamento, cod_status_pagamento, data_pagamento, "&_
				"forma_pagamento, meio_pagamento, valor_pagamento, obs_pagamento, parcelas_pagamento, parcelas_obs_pagamento, str_valores_pagamento, str_datas_pagamento) values "&_
				"("&ID_inscricao&",'"&UID&"','"&estatus&"','"&data&"', "&_
				"'"&forma&"','"&meio&"',"&valor&",'"&observacoes&"','"&parcelas&"','"&obs_parcelas&"','"&str_valores&"','"&str_datas&"')"&_
				" select @@identity as NovoId"
		set rs = conexao.execute(sql).nextrecordset
		
		ID = rs("NovoId")
		
	end if
	
	sql= "select ID_inscricao from PAGAMENTOS where ID_pagamento="&ID
	set rs= conexao.execute(sql)
	
	sql = "UPDATE CURSOS_INSCRICOES set online_inscricao="&modalidade&" where ID_inscricao="&rs("ID_inscricao")&""
	conexao.execute(sql)
	
	if isnumeric(inscricao) then
	
		sql = "UPDATE CURSOS_INSCRICOES set numero_inscricao="&inscricao&" where ID_inscricao="&rs("ID_inscricao")&""
		conexao.execute(sql)
		
	end if
	
	if mudar_curso = "1" then
		
		'' separar variavel de curso e data ''
		partes 		= split( ID_curso, ":" )
		ID_curso 	= partes(0)
		ID_data 	= partes(1)
		''''''''''''''''''''''''''''''''''''''
		
		sql = "UPDATE CURSOS_INSCRICOES set ID_curso="&ID_curso&", ID_data="&ID_data&" where ID_inscricao="&rs("ID_inscricao")&""
		conexao.execute(sql)
		
	end if
	
	'' Liberar Acesso caso pagamento confirmado ''
	if estatus=3 or estatus="3" or estatus=6 or estatus="6" or estatus=11 or estatus="11" or estatus=2 or estatus="2" then
		
		if not rs.EOF then
			ID_inscricao = rs("ID_inscricao")
			call LIBERAR_ACESSO( ID_inscricao )
			
			if estatus<>2 and estatus<>"2" and estatus<>6 and estatus<>"6" then
				if ( cstr(""&estatus) <> cstr(""&estatus_antigo) ) then
					call ENVIAR_CONTRATO( ID, ID_inscricao )
					call CONVERSAO_RDSTATION( null, null, ID_inscricao, valor )
				end if
			end if
		end if
		
	end if
	''''''''''''''''''''''''''''''''''''''''''''''

	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	

'CONTRATO
elseif acao="contrato" then
	
	call ENVIAR_CONTRATO( ID, null )
	
	session("sucesso") = "0002"
	response.Redirect(session("voltar"))


'INSTUDO
elseif acao="instudo" then
	
	retorno_instudo = MATRICULAR_INSTUDO( ID )
	
	if retorno_instudo = "1" then
		session("sucesso") = "0002"
	else
		'session("erro") = "0001"
		response.Write("ERRO: "&retorno_instudo)
		response.End()
	end if
	
	response.Redirect(session("voltar"))
	

'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>