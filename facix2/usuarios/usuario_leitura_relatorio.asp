<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_principal_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

<%
cor				= 	"#FFFFFF"
ID				= 	formatar("ID",1,2)

sql = "select * from USUARIOS_CURSOS where ID_usuario_curso="&ID
set rs = conexao.execute(sql)

ID_curso 		=	rs("ID_curso")
ID_usuario 		=	rs("ID_usuario")

sql = "select titulo_curso from CURSOS where ID_curso="&ID_curso
set rs = conexao.execute(sql)
%>

	<%
	' Definir a vari�vel do bot�o voltar ' - ' 0: sem bot�o; 1:Voltar no hist�rico; 2: session(voltar) '
	voltar	=	PAGINA_VOLTAR( "usuario_default.asp?ID="&ID_usuario )
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("P�gina Principal@../_principal/;Usu�rios@default.asp;Detalhes do usu�rio@usuario_default.asp?ID="&ID_usuario&";Relat�rio de "&rs("titulo_curso")&"@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	
	session("voltar") = SESSION_VOLTAR()
	%>
    
<script src="../../asa/_base/js/script_01.js" type="text/javascript"></script>
<script type="text/javascript">	
	function mostrar_frequencia(id, gatilho)
	{
		mostra_div(gatilho,'detalhes_exercicio');
		carregaAjax('ajax_frequencia', 'usuario_leitura_relatorio_ajax_frequencia.asp?ID='+id+'&ID_usuario_curso=<%=ID%>', 'GET');
	}
	
	function mostrar_desempenho(id, gatilho)
	{
		mostra_div(gatilho,'detalhes_exercicio');
		carregaAjax('ajax_frequencia', 'usuario_leitura_relatorio_ajax_desempenho.asp?ID='+id+'&ID_usuario_curso=<%=ID%>', 'GET');
	}
</script>

<!-- CAMADA COM DETALHES DA FREQUENCIA DE EXERCICIOS -->
<div id="detalhes_exercicio" style="position:absolute; display:none; overflow: auto; z-index:1;" class="caixa_selecao" onMouseOver="return mostra_div('detalhes_exercicio','detalhes_exercicio');" onMouseOut="return fecha_div(this,'detalhes_exercicio');">
	<div id="ajax_frequencia"></div>
</div>


<BR />
<table border="0" cellpadding="4" cellspacing="0" align="center">
	<!-- NIVEIS -->
	<%
	sql = "select * from LEITURA_NIVEIS where ID_curso="&ID_curso&" and status_nivel="&verdade&" order by ordem_nivel"
	set rs = Server.CreateObject("ADODB.RecordSet")
	rs.open sql,conexao,2,2,1
	
	nivel_completo = false
	
	do while not rs.EOF
	%>
	<TR>
		<TD colspan="3">
			<font style="font-size:14px;"><b>N�vel <%=rs("ordem_nivel")%>:</b> <%=rs("titulo_nivel")%></font>
		</TD>
	</TR>
	
		<%
		if rs("ordem_nivel") = 1 or nivel_completo = true then
			nivel_completo = true
		%>
		
		<!-- EXERCICIOS -->
		<%
		sql = "select * from LEITURA_EXERCICIOS where ID_nivel="&rs("ID_nivel")&" and status_exercicio="&verdade&" order by ordem_exercicio"
		set rs_cap = Server.CreateObject("ADODB.RecordSet")
		rs_cap.open sql,conexao,2,2,1
		
			do while not rs_cap.EOF
			%>
				
				<TR bgcolor="<%=cor%>" onMouseOut="mouse_out(this,'<%=cor%>');" onMouseOver="mouse_over(this,'#FFCC00');" style="cursor:pointer;">
					<TD width="50">&nbsp;</TD>
					<TD colspan="2" onClick="return mostrar_frequencia('<%=rs_cap("ID_exercicio")%>', this);" onMouseOut="return fecha_div(this,'detalhes_exercicio');">
						                       
						<%
						''Ver se aluno j� fez o exerc�cio
						sql = "select ID_log from LOG_EXERCICIOS where ID_usuario_curso="&ID&" and ID_exercicio="&rs_cap("ID_exercicio")&""
						set rs_log = conexao.execute(sql)
						
						if not rs_log.EOF then
						%>
							<img src="../_img/icone_check_on.gif" width="13" height="13" align="absmiddle" />
						<%
						else
							nivel_completo = false
						%>
							<img src="../_img/icone_check_off.gif" width="13" height="13" align="absmiddle" />
						<%end if%>
						
						<b>Exerc�cio <%=rs_cap("ordem_exercicio")%>:</b> <%=rs_cap("titulo_exercicio")%>
                        					
					</TD>
				</TR>
			<%
			rs_cap.movenext:loop
			%>
			
		<!-- TESTES -->
		<%
		sql = "select * from LEITURA_TESTES where ID_nivel="&rs("ID_nivel")&" and status_teste="&verdade&" order by ordem_teste"
		set rs_cap = Server.CreateObject("ADODB.RecordSet")
		rs_cap.open sql,conexao,2,2,1
		
			qtd_testes 	= 0
			total_PCs	= 0
			
			testes_completos = true
		
			do while not rs_cap.EOF
			%>
				
				<TR bgcolor="<%=cor%>" onMouseOut="mouse_out(this,'<%=cor%>');" onMouseOver="mouse_over(this,'#FFCC00');" style="cursor:pointer;">
					<TD width="50">&nbsp;</TD>
					<TD colspan="2" onClick="return mostrar_desempenho('<%=rs_cap("ID_teste")%>', this);" onMouseOut="return fecha_div(this,'detalhes_exercicio');">
						
						<%
						''Ver se aluno j� fez o teste e verificar m�dia
						sql = "select top 1 ID_log, acertos_log from LOG_TESTES where ID_usuario_curso="&ID&" and ID_teste="&rs_cap("ID_teste")&" order by acertos_log desc"
						set rs_log = conexao.execute(sql)
						
						if not rs_log.EOF then
						
							porcentagem_teste 	= rs_log("acertos_log")'(cdbl( rs_log("acertos_log") )*100)/cdbl( rs_cap("PLM_teste") )
							total_PCs			= cdbl( total_PCs ) + cdbl( rs_log("acertos_log") )
						
							if cdbl( porcentagem_teste ) >= cdbl( rs("media_nivel") ) then
							%>
								<img src="../_img/icone_check_green.gif" width="13" height="13" align="absmiddle" />
							<%else%>
								<img src="../_img/icone_check_red.gif" width="13" height="13" align="absmiddle" />
							<%
							end if
						else
							testes_completos = false
						%>
							<img src="../_img/icone_check_off.gif" width="13" height="13" align="absmiddle" />
						<%end if%>
						
						<b>Teste <%=rs_cap("ordem_teste")%>:</b> <%=rs_cap("titulo_teste")%>			
					</TD>
				</TR>
			<%
				qtd_testes = qtd_testes + 1
			rs_cap.movenext:loop
			
			if qtd_testes > 0 then
			
				media_total_testes = cdbl(total_PCs) / cdbl(qtd_testes)
				
				if cdbl( media_total_testes ) < cdbl( rs("media_nivel") ) then
					nivel_completo = false
				end if
				
			else
				testes_completos = false
				nivel_completo = true
			end if
			
			%>
            
            <%if nivel_completo = true and testes_completos = true then%>
            	<TR>
                    <TD colspan="3">
                        <font style="color:#009900; font-size:10px;">
                            O PCM (Palavra Compreendidas por Minuto) foi de <%=formatnumber( media_total_testes, 0 )%>%.<BR />
                            O aluno est� apto a fazer o pr�ximo n�vel.
                        </font>
                    </TD>
                </TR>
            <%elseif nivel_completo = false and testes_completos = true then%>
                <TR>
                    <TD colspan="3">
                        <font style="color:#FF0000; font-size:10px;">
                            O PCM (Palavras Compreendidas por Minuto) dos <%=qtd_testes%> testes foi de <%=formatnumber( media_total_testes, 0 )%>%.<BR />
                            Para passar para o N�vel Seguinte faz-se necess�rio atingir o PCM de <%=rs("media_nivel")%>%.
                        </font>
                    </TD>
                </TR>
            <%end if%>
            
        <%else%>
        
        	<TR>
                <TD colspan="3">
                    <font style="color:#999999; font-size:10px;">
                        O n�vel acima precisa ser completo para liberar o conte�do deste n�vel.
                    </font>
                </TD>
            </TR>
				
		<%end if%>	
			
	<%			
	rs.movenext:loop
	%>
	
	
	
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_principal_02.asp" -->