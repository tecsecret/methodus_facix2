<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_principal_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Definir a vari�vel do bot�o voltar ' - ' 0: sem bot�o; 1:Voltar no hist�rico; 2: session(voltar) '
	voltar	=	PAGINA_VOLTAR( "default.asp" )
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("P�gina Principal@../_principal/;Usu�rios@default.asp;Detalhes do Usu�rio@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	
	session("voltar") = SESSION_VOLTAR()
	%>


<%
ID			=	formatar("ID",1,2)

sql="select USUARIOS.*, titulo_perfil from USUARIOS left join PERFIL on USUARIOS.ID_perfil=PERFIL.ID_perfil where ID_usuario="&ID
set rs = conexao.execute(sql)

if rs.EOF then
	response.Clear()
	response.Redirect("default.asp")
	response.End()
end if
'''''''''''''''''''''''''''''''''''''''''''''
%>


<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">	
	<tr bgcolor="#CCCCCC">
		<td align="center" bgcolor="#FFFFFF" valign="top">
        
        	<%if isnull(rs("foto_usuario")) then%>
                <img id="img_foto" src="../_img/semfoto.gif" width="201" height="151" />
            <%else%>
                <img id="img_foto" src="../../asa/_arquivos_usuarios/<%=rs("foto_usuario")%>" width="201" height="151" />
            <%end if%>
            <BR />
            <a href="javascript:" onClick="exibir_acao( 'editar.asp?ID=<%=rs("ID_usuario")%>' );"><img src="../_img/icone_editar.gif" width="15" height="16" border="0" align="absmiddle" alt="Editar" /> Editar dados do usu�rio</a>
            <BR />
            <a href="javascript:" onClick="exibir_acao( 'foto_editar.asp?ID=<%=rs("ID_usuario")%>' );"><img src="../_img/icone_camera.gif" width="16" height="16" border="0" align="absmiddle" alt="Editar" /> Editar Foto</a>
            <BR />
            <a href="javascript:" onClick="exibir_acao( '../cursos/email_escolha.asp?ID=<%=rs("ID_usuario")%>' );"><img src="../_img/icone_email.gif" width="27" height="16" border="0" align="absmiddle" alt="Enviar Mala-Direta" /> Enviar Mala-Direta</a>
            
            <!--<BR />
            <a href="javascript:" onClick="return confirmar('tutor_acoes.asp?ID=<%=rs("ID_usuario")%>&acao=enviar', 'Quer mesmo enviar o aviso e tutorial sobre o site para este aluno?' );"><img src="../_img/icone_resposta.gif" width="16" height="16" border="0" align="absmiddle" alt="Enviar Tutorial" /> Enviar Aviso Inicial</a>
            -->
        </td>
        <td bgcolor="#FFFFFF">
		
			<table border="0" cellpadding="3" cellspacing="1" width="100%">
				<tr>
					<td>
					
						<table border="0" cellpadding="1" cellspacing="0" width="100%">
							<TR>
								<TD align="right"><b>Nome:</b> &nbsp; </TD>
								<TD><%=rs("nome_usuario")%></TD>
								<TD align="right"><b>Data de Nascimento:</b> &nbsp; </TD>
								<TD><%if isnull(rs("nascimento_usuario"))=false then%><%=FORMATA_DATA( rs("nascimento_usuario"), 1)%><%end if%></TD>
							</TR>
							<TR>
								<TD align="right"><b>E-mail:</b> &nbsp; </TD>
								<TD><a href="mailto:<%=rs("email_usuario")%>"><%=rs("email_usuario")%></a></TD>
								<TD align="right"><b>Data de Cadastro:</b> &nbsp; </TD>
								<TD><%=FORMATA_DATA( rs("data_usuario"), 1)%></TD>
							</TR>
                            <TR>
								<TD align="left" colspan="4"><b>Data de Atualiza��o:</b> &nbsp; <%=FORMATA_DATA( rs("data_atualizacao_usuario"), 1)%></TD>
							</TR>
							<TR>
								<TD align="right"><b>Telefone:</b> &nbsp; </TD>
								<TD>(<%=rs("ddd_tel_usuario")%>) <%=rs("tel_usuario")%></TD>
								<TD align="right"><b>Tel. Celular:</b> &nbsp; </TD>
								<TD>(<%=rs("ddd_cel_usuario")%>) <%=rs("cel_usuario")%></TD>
							</TR>
                            <TR>
								<TD align="right"><b>Telefone 2:</b> &nbsp; </TD>
								<TD>(<%=rs("ddd_tel2_usuario")%>) <%=rs("tel2_usuario")%></TD>
								<TD align="right"><b>Tipo de Pessoa:</b> &nbsp; </TD>
								<TD><%=rs("tipo_usuario")%></TD>
							</TR>
							<TR>
								<TD align="right"><b>RG:</b> &nbsp; </TD>
								<TD><%=rs("rg_usuario")%></TD>
								<TD align="right"><b>CPF:</b> &nbsp; </TD>
								<TD><%=rs("cpf_usuario")%></TD>
							</TR>
							<TR>
								<TD align="right"><b>Endere�o:</b> &nbsp; </TD>
								<TD><%=rs("endereco_usuario")%></TD>
								<TD align="right"><b>Complemento:</b> &nbsp; </TD>
								<TD><%=rs("bairro_usuario")%></TD>
							</TR>
							<TR>
								<TD align="right"><b>Cidade:</b> &nbsp; </TD>
								<TD><%=rs("cidade_usuario")%></TD>
								<TD align="right"><b>CEP:</b> &nbsp; </TD>
								<TD><%=rs("cep_usuario")%></TD>
							</TR>
							<TR>
								<TD align="right"><b>Estado:</b> &nbsp; </TD>
								<TD><%=rs("estado_usuario")%></TD>
								<TD align="right"><b>Deseja receber mala-direta:</b> &nbsp; </TD>
								<TD><%if rs("newsletter_usuario")=true then%>sim<%else%>n�o<%end if%></TD>
							</TR>
                            <TR>
								<TD align="right"><b>Forma��o Intelectual:</b> &nbsp; </TD>
								<TD><%=rs("cargo_usuario")%></TD>
								<TD align="right"><b>Empresa:</b> &nbsp; </TD>
								<TD><%=rs("profissao_usuario")%></TD>
							</TR>
                            <TR>
								<TD align="right" valign="top"><b>Como conheceu:</b> &nbsp; </TD>
								<TD colspan="3">
									<%=rs("conheceu_usuario")%>
									<%
									'' verificar se esse aluno algum dia foi indicado por algu�m
									if instr(rs("conheceu_usuario"),"amigo") then
									
										sql = "select top 1 nome_usuario, email_usuario, ID_usuario from RECOMENDACOES inner join USUARIOS on RECOMENDACOES.ID_usuario1=USUARIOS.ID_usuario where ID_usuario2="&ID&" order by data_recomendacao desc"
										set rs_amigo = conexao.execute(sql)
										
										if not rs_amigo.EOF then
											response.Write( ": "& rs_amigo("nome_usuario") & "("&rs_amigo("email_usuario")&")" )
										else
											response.Write( ": Nenhum amigo cadastrado" )
										end if
									
									end if
									%>
								</TD>
							</TR>
                            <TR>
								<TD align="right" valign="top"><b>Observa��es:</b> &nbsp; </TD>
								<TD colspan="3"><%=rs("observacoes_usuario")%></TD>
							</TR>
                            <!--<TR>
								<TD align="right" valign="top"><b>Prospec��o:</b> &nbsp; </TD>
								<TD colspan="3"><%=rs("prospeccao_usuario")%></TD>
							</TR>-->
                            <TR>
								<TD align="right" valign="top"><b>Cursos &nbsp;&nbsp; <BR />de Interesse:</b> &nbsp; </TD>
								<TD colspan="3">
								
									<%
									if isnull(rs("interesses_usuario"))=false then
									
										partes = split( rs("interesses_usuario"), ";" )
										
										for i=0 to (ubound(partes)-1)
											
											sql = "select titulo_curso from CURSOS where ID_curso="&replace( partes(i), "id", "" )&""
											set rs_interesse = conexao.execute(sql)
											
											if not rs_interesse.EOF then
												response.Write( rs_interesse("titulo_curso") & "<BR />" )
											end if
											
										next
										
									end if
									%>
                                
                                </TD>
							</TR>
                            <TR>
								<TD align="right" valign="top"><b>Comportamento:</b> </TD>
								<TD colspan="3">
								
									<%
									if isnull(rs("entrou_usuario"))=false then
									
										partes = split( rs("entrou_usuario"), ";" )
										
										for i=0 to (ubound(partes)-1)
										
											response.Write( partes(i) & "<BR />" )
											
										next
										
									end if
									%>
                                
                                </TD>
							</TR>
                            <TR>
								<TD align="right" valign="top"><b>Perfil:</b> </TD>
								<TD colspan="3"><%=rs("titulo_perfil")%></TD>
							</TR>
						</table>
						
					</td>
				</tr>
			</table>
		
		</td>
	</tr>
</table>



<BR />
<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	<tr id="tr_menu" bgcolor="#EEEEEE">
		<TD colspan="6" align="center" height="30">
			<!--<a href="javascript:" onClick="exibir_acao( 'usuario_incluir.asp?ID=<%=ID%>' );" class="link_02"><img src="../_img/icone_incluir.gif" width="32" height="32" align="absmiddle" border="0" /> Incluir usu�rio em um Curso</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
            
            <a href="javascript:" onClick="exibir_acao( '../financeiro/pagamentos_incluir.asp?ID_usuario=<%=ID%>' );" class="link_02"><img src="../_img/icone_incluir.gif" width="32" height="32" align="absmiddle" border="0" /> Incluir em um curso</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <a href="javascript:" onClick="exibir_acao( '../financeiro/avulso_incluir.asp?ID_usuario=<%=ID%>' );" class="link_02"><img src="../_img/icone_incluir.gif" width="32" height="32" align="absmiddle" border="0" /> Incluir Pagto. Avulso</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
			<a href="#" onClick="document.location='comunicados_default.asp?ID=<%=ID%>';" class="link_02"><img src="../_img/icone_pasta.gif" width="32" height="32" align="absmiddle" border="0" /> Gerenciar comunicados</a>
        </TD>
	</tr>
    
    
    <%
	sql = 	"(select DISTINCT PAGAMENTOS.ID_pagamento, PAGAMENTOS.ID_inscricao, PAGAMENTOS.ID_avulso, PAGAMENTOS.identificacao_pagamento, PAGAMENTOS.cod_status_pagamento,"&_
			" PAGAMENTOS.data_pagamento, PAGAMENTOS.forma_pagamento, PAGAMENTOS.meio_pagamento, PAGAMENTOS.valor_pagamento,"&_
			" PAGAMENTOS.parcelas_pagamento, CURSOS_INSCRICOES.contrato_inscricao from PAGAMENTOS INNER JOIN CURSOS_INSCRICOES ON PAGAMENTOS.ID_inscricao = CURSOS_INSCRICOES.ID_inscricao"&_
			" inner join CURSOS_INSCRICOES_USUARIOS ON CURSOS_INSCRICOES.ID_inscricao = CURSOS_INSCRICOES_USUARIOS.ID_inscricao inner join"&_
			" USUARIOS on CURSOS_INSCRICOES_USUARIOS.ID_usuario = USUARIOS.ID_usuario where CURSOS_INSCRICOES_USUARIOS.ID_usuario="&ID&")"
			
	sql = sql & " UNION ALL "
			
	sql = 	sql & "(select DISTINCT PAGAMENTOS.ID_pagamento, PAGAMENTOS.ID_inscricao, PAGAMENTOS.ID_avulso, PAGAMENTOS.identificacao_pagamento, PAGAMENTOS.cod_status_pagamento,"&_
			" PAGAMENTOS.data_pagamento, PAGAMENTOS.forma_pagamento, PAGAMENTOS.meio_pagamento, PAGAMENTOS.valor_pagamento,"&_
			" PAGAMENTOS.parcelas_pagamento, null from PAGAMENTOS inner join PAGAMENTOS_AVULSOS on PAGAMENTOS.ID_avulso=PAGAMENTOS_AVULSOS.ID_avulso"&_
			" where PAGAMENTOS_AVULSOS.ID_usuario="&ID&")"
			
	sql = sql & " order by data_pagamento desc"
	set rs = conexao.execute(sql)	
	%>
    
    
	<tr bgcolor="#CCCCCC">
		<td align="center"><b>Turma</b></td>
        <td align="center"><b>Pagamento</b></td>
        <td align="center"><b>Caracter�sticas</b></td>
		<td align="center" width="100"><b>A��es</b></td>
	</tr>
	<%
	'' XML de meios de pagamento
	call CONEXAO_XML("../_xml/pagamentos_meio.xml")
	set registros_meios = xmlDoc.documentElement
	
	'' XML de status de pagamento
	call CONEXAO_XML("../_xml/pagamentos_cod_status.xml")
    set registros_status = xmlDoc.documentElement
	
	
	do while not rs.EOF
	
		descricao = "<b>UID:</b> "& rs("identificacao_pagamento") &"<BR />"
					
		select case rs("cod_status_pagamento")
			case "2"
				cor = "#eaf9f9"
			case "3","11"
				cor = "#CCFF99"
			case "4","5"
				cor = "#FFCCCC"
			case "6"
				cor = "#FFFF99"
			case else
				cor = "#FFFFFF"
		end select
	%>
	<tr bgcolor="<%=cor%>">
        
        <td>
        	<%
			tem_turma_instudo = 0
			
			'' verificar se e pagamento de curso ou avulso ''
			if isnull(rs("ID_inscricao")) then
				
				sql = "select * from PAGAMENTOS_AVULSOS where ID_avulso="&rs("ID_avulso")&""
				set rs_obs = conexao.execute(sql)
				
				if rs_obs("titulo_avulso")=empty or isnull(rs_obs("titulo_avulso")) then
					response.Write("Pagamento Avulso.")
				else
				%>
					<%=rs_obs("titulo_avulso")%>
				<%end if
				
			else
				
				sql = 	"select CURSOS.titulo_curso, CURSOS.ID_curso, CURSOS.tipo_curso, CURSOS_DATAS.ID_data, CURSOS_DATAS.inicio_data, CURSOS_DATAS.fim_data, CURSOS_DATAS.horario_data, CURSOS_INSCRICOES.numero_inscricao, ID_turma_instudo from "&_
						"CURSOS_INSCRICOES INNER JOIN CURSOS ON CURSOS_INSCRICOES.ID_curso = CURSOS.ID_curso INNER JOIN "&_
						"CURSOS_DATAS ON CURSOS_INSCRICOES.ID_data = CURSOS_DATAS.ID_data where ID_inscricao="&rs("ID_inscricao")
				set rs_curso = conexao.execute(sql)
				
				if not rs_curso.EOF then
					
					if isnull(rs_curso("ID_turma_instudo"))=false then
						tem_turma_instudo = rs_curso("ID_turma_instudo")
					end if
					%>
				
					<b><%=rs_curso("titulo_curso")%></b><BR /> 
					de <%=FORMATA_DATA( rs_curso("inicio_data"), 1 )%> a <%=FORMATA_DATA( rs_curso("fim_data"), 1 )%><BR />
					<%=rs_curso("horario_data")%>
                    
                    <%if isnull(rs_curso("numero_inscricao")) then%>
                        <!--<BR /><BR />C�digo do Pedido: <b><%=rs("ID_inscricao")%></b> -->
                        <BR /><BR />Sem n�mero de inscri��o
                    <%else%>
                        <BR /><BR />N�mero inscri��o: <b><%=rs_curso("numero_inscricao")%></b>
                    <%end if%>
					
					<BR />
					<a href="../consultas/turma_default.asp?ID=<%=rs_curso("ID_curso")%>&ID_data=<%=rs_curso("ID_data")%>">Ver Turma</a>
				
				<%else%>
				
					<%
					sql = 	"select CURSOS.titulo_curso, CURSOS.ID_curso, CURSOS.tipo_curso, CURSOS_INSCRICOES.numero_inscricao from "&_
							"CURSOS_INSCRICOES INNER JOIN CURSOS ON CURSOS_INSCRICOES.ID_curso = CURSOS.ID_curso "&_
							"where ID_inscricao="&rs("ID_inscricao")
					set rs_curso = conexao.execute(sql)
					
					if not rs_curso.EOF then
					%>
					
						<b><%=rs_curso("titulo_curso")%></b>
						
                        <%if isnull(rs_curso("numero_inscricao")) then%>
                            <!--<BR /><BR />C�digo do Pedido: <b><%=rs("ID_inscricao")%></b> -->
                            <BR /><BR />Sem n�mero de inscri��o
                        <%else%>
                            <BR /><BR />N�mero inscri��o: <b><%=rs_curso("numero_inscricao")%></b>
                        <%end if%>
					
					<%end if%>
				
				<%end if%>
                
            <%end if%>
            
        </td>
        
        <!--<td align="center">
			<%=FORMATA_DATA( rs("data_pagamento"), 1 )%><BR />
        	<%=FORMATA_HORA( rs("data_pagamento"), 1 )%>
        </td> -->
        
        <td align="center">
        
        	<%
			if isnull(rs("ID_inscricao"))=false then
			
        	'' Mostrar participantes relacionados � inscri��o ''
			sql = "SELECT USUARIOS.nome_usuario, USUARIOS.ID_usuario, USUARIOS.email_usuario FROM CURSOS_INSCRICOES INNER JOIN CURSOS_INSCRICOES_USUARIOS ON "&_
				  "CURSOS_INSCRICOES.ID_inscricao = CURSOS_INSCRICOES_USUARIOS.ID_inscricao INNER JOIN "&_
				  "USUARIOS ON CURSOS_INSCRICOES_USUARIOS.ID_usuario = USUARIOS.ID_usuario "&_
				  "where CURSOS_INSCRICOES_USUARIOS.ID_inscricao="&rs("ID_inscricao")&" and USUARIOS.ID_usuario<>"&ID&""
			set rs_parceiros = conexao.execute(sql)
			
			if not rs_parceiros.EOF then
			%>
				Este pagamento foi feito em conjunto com:<BR /> 
				<%
				do while not rs_parceiros.EOF
				%>
					<b><a href="usuario_default.asp?ID=<%=rs_parceiros("ID_usuario")%>"><%=rs_parceiros("nome_usuario")%></a></b><BR />
				<%
				rs_parceiros.movenext:loop
				%>
                <BR />
            <%    
			end if
			
			end if
        	%>
		
			<%
			'' meio de pagamento
			set registro = registros_meios.selectsinglenode("registro[@codigo='"&rs("meio_pagamento")&"']")
			response.Write(registro.text&"<BR />")
			
			'' forma de pagamento
			response.Write( IPAGARE_RETORNO( "forma", rs("forma_pagamento") ) &"<BR />" )
			
			'' status do pagamento
			set registro = registros_status.selectsinglenode("registro[@codigo='"&rs("cod_status_pagamento")&"']")
			response.Write("<b>"&registro.text&"</b>")
			%>
            
            <BR /><BR />
            
            <%if rs("meio_pagamento")="6" then%>
            	N� do boleto: <%=rs("ID_inscricao")%>
            	<BR /><BR />
            <%end if%>
            
            R$&nbsp;<%=FORMATA_MOEDA( rs("valor_pagamento"), false )%>
            
            <BR /><BR />
            
            <a href="javascript:" onClick="exibir_acao( '../financeiro/<%if isnull(rs("ID_inscricao")) then%>avulso<%else%>pagamentos<%end if%>_editar.asp?ID=<%=rs("ID_pagamento")%>' );">Ver investimento</a>
            
            <%
			'' Somar Total
			valor_total = valor_total + rs("valor_pagamento")
			%>
        
        </td>
        
        <td> 
        
        	<%
			ID_matricula_instudo = 0
			
			'' verificar se e pagamento de curso ou avulso ''
			if isnull(rs("ID_inscricao")) then
			
				sql = "select * from PAGAMENTOS_AVULSOS where ID_avulso="&rs("ID_avulso")&""
				set rs_obs = conexao.execute(sql)
				
				if rs_obs("descricao_avulso")=empty or isnull(rs_obs("descricao_avulso")) then
					response.Write("Nenhuma informa��o.")
				else
				%>
					<%=rs_obs("descricao_avulso")%>
				<%end if%>
            	    
            <%
			else
				
				'' verificar se existe observa��es desse usu�rio com essa inscri��o ''
				sql = "select * from CURSOS_INSCRICOES_USUARIOS where ID_usuario="&ID&" and ID_inscricao="&rs("ID_inscricao")&""
				set rs_obs = conexao.execute(sql)
				
				if not rs_obs.EOF then
					
					if isnull(rs_obs("ID_instudo"))=false then
						ID_matricula_instudo = rs_obs("ID_instudo")
					end if
					
					if rs_obs("obs_inscricao_usuario")=empty or isnull(rs_obs("obs_inscricao_usuario")) then
						response.Write("Nenhuma informa��o.")
					else
					%>
						<%=rs_obs("obs_inscricao_usuario")%>
					<%end if%>
					
					<BR />
					<a href="javascript:" onClick="exibir_acao( 'obs_editar.asp?ID=<%=rs_obs("ID_inscricao_usuario")%>' );">Editar Caracter�sticas</a>
					
				<%else%>
					Nenhuma informa��o.
				<%end if%>
        	
            <%end if%>
            
        </td>
		
		<td align="center">
        
        	<%
			'' verificar se e pagamento de curso ou avulso ''
			if isnull(rs("ID_inscricao")) then
			%>
            	<a href="javascript:" onClick="exibir_acao( '../financeiro/avulso_editar.asp?ID=<%=rs("ID_pagamento")%>' );"><img src="../_img/icone_editar.gif" width="15" height="16" border="0" align="absmiddle" alt="Editar" /></a>
				 |  
				<a href="#" onClick="return confirmar('../financeiro/avulso_acoes.asp?ID=<%=rs("ID_pagamento")%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>
            <%
			else
				
				if not rs_curso.EOF then
				
					'response.Write("TESTE"&rs_curso("ID_curso"))
					sql="select ID_usuario_curso from USUARIOS_CURSOS where ID_usuario="&ID&" and ID_curso="&rs_curso("ID_curso")
					set rs_cu = conexao.execute(sql)
					
					if not rs_cu.EOF then
						if rs_curso("tipo_curso")=1 then
							link = "usuario_leitura_relatorio.asp?ID="&rs_cu("ID_usuario_curso")&""
						else
							link = "../cursos/oratoria_lista.asp?ID="&rs_cu("ID_usuario_curso")&""
						end if
					end if
					
					if not rs_cu.EOF then
					%>
					<a href="<%=link%>"><img src="../_img/icone_engrenagem.gif" width="13" height="13" border="0" align="absmiddle" alt="Editar participa��o no curso" /></a>
					 | 	
					<%end if%>
					
					<!--<a href="javascript:" onClick="exibir_acao( '../financeiro/pagamentos_editar.asp?ID=<%=rs("ID_pagamento")%>' );"><img src="../_img/icone_editar.gif" width="15" height="16" border="0" align="absmiddle" alt="Editar" /></a>
					 |  -->
					<a href="#" onClick="return confirmar('../financeiro/pagamentos_acoes.asp?ID=<%=rs("ID_pagamento")%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>
                            
                    <BR />
                    
                    <a href="../contrato/contrato.asp?ID=<%=rs("ID_pagamento")%>" target="_blank">Ver contrato</a>
                    <%if isnull(rs("contrato_inscricao")) then%>
                        | <a href="#" onClick="return confirmar('../financeiro/pagamentos_acoes.asp?ID=<%=rs("ID_pagamento")%>&acao=contrato','Tem certeza que deseja enviar o contrato?')"><img src="../_img/icone_email.gif" width="16" height="10" border="0" align="absmiddle" alt="Enviar Contrato" /></a>
                    <%else%>
                        | Contrato enviado em <%=FORMATA_DATA( rs("contrato_inscricao"), 1 )%>
                    <%end if%>    
                    
                    <%if tem_turma_instudo > 0 and ID_matricula_instudo = 0 then%>
                        <BR />
                        <a href="../financeiro/pagamentos_acoes.asp?acao=instudo&ID=<%=rs("ID_inscricao")%>">Matricular no Instudo</a> 
                    <%else%>
                    	<!--<BR />
                        DEBUG: <%=tem_turma_instudo%> / <%=ID_matricula_instudo%>   -->    
                    <%end if%>
                    
        		<%
				end if
			end if
			%>
            

        
        </td>
	</tr>
	<%
	if cor="#FFFFFF" then
		cor= "#EEEEEE"
	else
		cor= "#FFFFFF"
	end if
	
	rs.movenext:loop
	%>
    
    <tr bgcolor="<%=cor%>">
    	<TD colspan="4">&nbsp;</TD>
    </tr>
    <tr bgcolor="<%=cor%>">
    	<TD><font style="font-size:16px; font-weight:bold;">TOTAL:</font></TD>
		<td colspan="2" align="right">
        	<font style="font-size:16px;">
            R$&nbsp;<%=FORMATA_MOEDA( valor_total, false )%>
            </font>
        </td>
        <TD>&nbsp;</TD>
    </tr>    
</table>    




   
   
   
<%
cor		= 	"#FFFFFF"

sql = 	"select CURSOS_INSCRICOES.*, CURSOS.titulo_curso, CURSOS_DATAS.inicio_data, CURSOS_DATAS.fim_data, CURSOS_DATAS.horario_data from CURSOS_INSCRICOES "&_
		"inner join CURSOS_INSCRICOES_USUARIOS on CURSOS_INSCRICOES.ID_inscricao=CURSOS_INSCRICOES_USUARIOS.ID_inscricao inner join CURSOS on CURSOS_INSCRICOES.ID_curso=CURSOS.ID_curso "&_
		"inner join CURSOS_DATAS on CURSOS_INSCRICOES.ID_data=CURSOS_DATAS.ID_data where CURSOS_INSCRICOES_USUARIOS.ID_usuario="&ID&" "&_
		"and not exists (select top 1 1 from PAGAMENTOS where PAGAMENTOS.ID_inscricao=CURSOS_INSCRICOES.ID_inscricao) order by data_inscricao"
set rs = conexao.execute(sql)

if not rs.EOF then
%>
<BR />
<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	
    <tr bgcolor="#000000">
		<td align="center" colspan="4" style="color:#FFFFFF;"><b>Inscri��es pendentes</b></td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td align="center" class="titulo_02" width="400"><b>Inscri��o</b></td>
		<td align="center" class="titulo_02"><b>Valor</b></td>
        <td align="center" class="titulo_02"><b>Pago</b></td>
		<td align="center" class="titulo_02"><b>A��es</b></td>
	</tr>
	<%
	i = 0
	j = 0
	do while not rs.EOF
	%>
	<tr bgcolor="<%=cor%>" onMouseOut="mouse_out(this,'<%=cor%>');" onMouseOver="mouse_over(this,'#FFFFEE');">
		<td>
		
			N�mero do pedido: <b><%=right( "000000"&rs("ID_inscricao"), 6 )%></b> - 
            
            <%
			sql = "select * from CURSOS_PLANOS where ID_plano="&rs("ID_plano")&" and ID_curso="&rs("ID_curso")
			set rs_plano = conexao.execute(sql)

			if rs_plano("empresarial_plano") = true then
				response.Write("<b>""Empresarial""</b>")
			else
				response.Write("<b>""Pessoa F�sica""</b>")
			end if
			%>
            
            <BR />
            
            <%=rs("titulo_curso")%> - de <%=FORMATA_DATA( rs("inicio_data"), 1 )%> a <%=FORMATA_DATA(rs("fim_data"), 1 )%><BR />(<%=rs("horario_data")%>)<BR />
            
            <%
			'' Mostrar empresas relacionadas � inscri��o ''
			sql = "SELECT EMPRESAS.razao_empresa FROM CURSOS_INSCRICOES INNER JOIN CURSOS_INSCRICOES_EMPRESAS ON "&_
                  "CURSOS_INSCRICOES.ID_inscricao = CURSOS_INSCRICOES_EMPRESAS.ID_inscricao INNER JOIN "&_
                  "EMPRESAS ON CURSOS_INSCRICOES_EMPRESAS.ID_empresa = EMPRESAS.ID_empresa "&_
				  "where CURSOS_INSCRICOES_EMPRESAS.ID_inscricao="&rs("ID_inscricao")
			set rs_empresa = conexao.execute(sql)
			
			do while not rs_empresa.EOF
			%>
            	<b>Empresa:</b> <%=rs_empresa("razao_empresa")%><BR />
            <%
			rs_empresa.movenext:loop
			'''''''''''''''''''''''''''''''''''''''''''''''
			%>
            
            <%
			'' Mostrar participantes relacionados � inscri��o ''
			sql = "SELECT USUARIOS.nome_usuario FROM CURSOS_INSCRICOES INNER JOIN CURSOS_INSCRICOES_USUARIOS ON "&_
                  "CURSOS_INSCRICOES.ID_inscricao = CURSOS_INSCRICOES_USUARIOS.ID_inscricao INNER JOIN "&_
                  "USUARIOS ON CURSOS_INSCRICOES_USUARIOS.ID_usuario = USUARIOS.ID_usuario "&_
				  "where CURSOS_INSCRICOES_USUARIOS.ID_inscricao="&rs("ID_inscricao")
			set rs_empresa = conexao.execute(sql)
			
			k = 1
			do while not rs_empresa.EOF
			%>
            	<b>Participante <%=k%>:</b> <%=rs_empresa("nome_usuario")%><BR />
            <%
				k = k + 1
			rs_empresa.movenext:loop
			'''''''''''''''''''''''''''''''''''''''''''''''
			%>
        
        </td>
		<td align="center">
        	<%
			sql = "select top 1 valor_pagamento from PAGAMENTOS where ID_inscricao="&rs("ID_inscricao")&" order by ID_pagamento desc"
			set rs_pagto = conexao.execute(sql)
			
			if not rs_pagto.EOF then
				valor 	= rs_pagto("valor_pagamento")
			else
				valor 	= rs_plano("valor_plano") * (k-1)
			end if
			
			valor_total = valor_total + valor
			%>
        	R$ <%=FORMATA_MOEDA( valor, false )%>
        </td>
        <td align="center">
        	<%
			if rs("pago_inscricao") = true then
				i = i + 1
				ativo = 0
				valor_total_pago = cdbl(valor_total_pago) + cdbl(valor)
			%>
        		<img src="../_img/icone_check.gif" width="17" height="15" />
            <%
			else
				ativo = 1
			%>
            	&nbsp;
            <%end if%>
        </td>
		<td align="center">
			<a href="javascript:" onClick="exibir_acao( '../financeiro/pagamentos_editar.asp?ID_inscricao=<%=rs("ID_inscricao")%>' );"><img src="../_img/icone_check2.gif" width="16" height="16" border="0" align="absmiddle" alt="Adicionar status do pagamento" /></a>
			
            <!-- | 	
			<a href="#" onClick="return confirmar('../agenda/turma_acoes.asp?ID=<%=rs("ID_inscricao")%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>-->
		</td>
	</tr>
	<%
		j = j + 1
	if cor="#FFFFFF" then
		cor= "#EEEEEE"
	else
		cor= "#FFFFFF"
	end if
	
	rs.movenext:loop
	%>
	
</table>
<%end if%>
    
    
    
    
    
    
<!--<BR />
<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	<tr id="tr_menu" bgcolor="#EEEEEE">
		<TD colspan="4" align="center" height="30">
			<a href="javascript:" onClick="exibir_acao( 'usuario_incluir.asp?ID=<%=ID%>' );" class="link_02"><img src="../_img/icone_incluir.gif" width="32" height="32" align="absmiddle" border="0" /> Incluir usu�rio em um Curso</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="#" onClick="document.location='comunicados_default.asp?ID=<%=ID%>';" class="link_02"><img src="../_img/icone_pasta.gif" width="32" height="32" align="absmiddle" border="0" /> Gerenciar comunicados</a>
		</TD>
	</tr>   
	
    <tr bgcolor="#CCCCCC">
		<td align="center"><b>CURSOS EM ANDAMENTO:</b></td>
		<td align="center" width="100"><b>In�cio</b></td>
		<td align="center" width="100"><b>Fim</b></td>
		<td align="center" width="80"><b>A��es</b></td>
	</tr>
	<%
'	sql = "select USUARIOS_CURSOS.*, CURSOS.titulo_curso, CURSOS.tipo_curso from USUARIOS_CURSOS inner join CURSOS on USUARIOS_CURSOS.ID_curso=CURSOS.ID_curso where ID_usuario="&ID&" order by inicio_usuario_curso desc"
'	set rs = conexao.execute(sql)
'	
'	cor = "#FFFFFF"
'	
'	if not rs.EOF then
'	do while not rs.EOF
'		if rs("status_usuario_curso")=false then
'			cor = "FFEEEE"
'		end if
	%>
	<tr bgcolor="<%=cor%>" onMouseOut="mouse_out(this,'<%=cor%>');" onMouseOver="mouse_over(this,'#FFFFEE');">
	
		<%
'		if rs("tipo_curso")=1 then
'			link = "usuario_leitura_relatorio.asp?ID="&rs("ID_usuario_curso")&""
'		else
'			link = "../cursos/oratoria_lista.asp?ID="&rs("ID_usuario_curso")&""
'		end if
		%>
	
		<td style="cursor:pointer;" onClick="document.location='<%'=link%>'">- <%'=rs("titulo_curso")%></td>
		<td style="cursor:pointer;" onClick="document.location='<%'=link%>'" align="center"><%'=FORMATA_DATA( rs("inicio_usuario_curso"), 1 )%></td>
		<td style="cursor:pointer;" onClick="document.location='<%'=link%>'" align="center"><%'=FORMATA_DATA( rs("fim_usuario_curso"), 1 )%></td>
		<td align="center">
			<a href="javascript:" onClick="exibir_acao( 'usuario_editar.asp?ID=<%'=rs("ID_usuario_curso")%>' );"><img src="../_img/icone_editar.gif" width="15" height="16" border="0" align="absmiddle" alt="Editar" /></a>
			 | 	
			<a href="#" onClick="return confirmar('usuario_acoes.asp?ID=<%'=rs("ID_usuario_curso")%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>
		</td>
	</tr>
	<%
'		if cor="#FFFFFF" then
'			cor= "#EEEEEE"
'		else
'			cor= "#FFFFFF"
'		end if
'	rs.movenext:loop
'	else
	%>
	<tr bgcolor="#FFFFFF">
		<td align="center" colspan="4">
			N�o existe registro		
		</td>
	</tr>
	<%'end if%>
     
     
</table> -->



<%
sql = "select top 30 * from LOG_UTILIZACAO where ID_usuario="&ID&" order by data_log desc"
set rs_log = conexao.execute(sql)

if not rs_log.EOF then
%>
    <BR />
    <table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
        <tr bgcolor="#EEEEEE">
            <td align="center" colspan="2"><b>ESTAT�STICAS DE UTILIZA��O: (�ltimos 30)</b></td>
        </tr>
        <tr bgcolor="#CCCCCC">
            <td align="center"><b>A��o</b></td>
            <td align="center"><b>Data</b></td>
        </tr>
        <%
        do while not rs_log.EOF
        %>
        <tr bgcolor="#FFFFFF">
            <td>
                <%=rs_log("descricao_log")%>
            </td>
            <td align="center">
                <%=FORMATA_DATA( rs_log("data_log"), 1 )%> �s <%=FORMATA_HORA( rs_log("data_log"), 1 )%>
            </td>
        </tr>
        <%
        rs_log.movenext:loop	
        %>
    </table>
<%end if%>

<BR />
<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	<tr bgcolor="#EEEEEE">
		<td align="center" colspan="3"><b>ESTAT�STICAS DE ACESSO: (�ltimos 10 acessos)</b></td>
	</tr>
    <tr bgcolor="#CCCCCC">
		<td align="center"><b>In�cio do acesso</b></td>
        <td align="center"><b>Fim do acesso</b></td>
        <td align="center"><b>Dura��o do acesso</b></td>
	</tr>
    <%
	sql = "select top 10 * from LOG_ACESSOS where ID_usuario="&ID&" order by inicio_acesso desc"
	set rs_log = conexao.execute(sql)
	
	if not rs_log.EOF then
		do while not rs_log.EOF
		%>
		<tr bgcolor="#FFFFFF">
			<td align="center">
				<%=FORMATA_DATA( rs_log("inicio_acesso"), 1 )%> �s <%=FORMATA_HORA( rs_log("inicio_acesso"), 1 )%>
			</td>
            <td align="center">
				<%=FORMATA_DATA( rs_log("fim_acesso"), 1 )%> �s <%=FORMATA_HORA( rs_log("fim_acesso"), 1 )%>
			</td>
            <td align="center">
            
            	<%
				duracao_horas 	= 0
				duracao_minutos = 0
				
            	duracao = DateDiff("n", rs_log("inicio_acesso"), rs_log("fim_acesso"))
				
				if duracao >= 60 then
					duracao_horas 	= fix( (duracao/60) )
					duracao_minutos = (duracao mod 60)
				end if
				
				if duracao_horas > 0 then
                %>
                	<%=duracao_horas%> hora<%if duracao_horas > 1 then%>s<%end if%>
                    <%if duracao_minutos > 0 then%>
                    	 e <%=duracao_minutos%> minuto<%if duracao_minutos > 1 then%>s<%end if%>
                    <%end if%>
                <%else%>
            		<%=DateDiff("n", rs_log("inicio_acesso"), rs_log("fim_acesso"))%> minutos
                <%end if%>
                
			</td>
		</tr>
		<%
		rs_log.movenext:loop	
	else
	%>
    <tr bgcolor="#FFFFFF">
    	<td align="center" colspan="3">
        	Esse aluno nunca se conectou
        </td>
    </tr>
    <%end if%>
</table>


<%
sql = "select top 100 INDICACOES_LOG.*, INDICACOES.titulo_indicacao from INDICACOES_LOG inner join INDICACOES on INDICACOES_LOG.ID_indicacao=INDICACOES.ID_indicacao where ID_usuario="&ID&" order by data_log desc"
set rs_log = conexao.execute(sql)

if not rs_log.EOF then
%>
<BR />
<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	<tr bgcolor="#EEEEEE">
		<td align="center" colspan="3"><b>ESTAT�STICAS DE ACESSO AOS TEXTOS DE INDICA��O: (�ltimos 100)</b></td>
	</tr>
    <tr bgcolor="#CCCCCC">
		<td align="center"><b>Texto indicado</b></td>
        <td align="center"><b>Data da Indica��o</b></td>
        <td align="center"><b>Data que leu</b></td>
	</tr>
    <%
	do while not rs_log.EOF
	%>
	<tr bgcolor="<%if rs_log("lido_log")=true then%>#CCFF99<%else%>#FFFFFF<%end if%>">
		<td>
			- <%=rs_log("titulo_indicacao")%>
		</td>
        <td align="center">
			<%=FORMATA_DATA( rs_log("data_log"), 1 )%> �s <%=FORMATA_HORA( rs_log("data_log"), 1 )%>
		</td>
		<td align="center">
        	<%if rs_log("lido_log")=true and isdate(rs_log("data_lido_log")) then%>
				<%=FORMATA_DATA( rs_log("data_lido_log"), 1 )%> �s <%=FORMATA_HORA( rs_log("data_lido_log"), 1 )%>
            <%end if%>
		</td>
	</tr>
	<%
	rs_log.movenext:loop	
	%>
</table>
<%end if%>

<%
sql = "select top 50 * from LOG_ENVIOS where ID_usuario="&ID&" order by data_log desc"
set rs_log = conexao.execute(sql)

if not rs_log.EOF then
%>
<BR />
<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	<tr bgcolor="#EEEEEE">
		<td align="center" colspan="3"><b>ESTAT�STICAS DE ENVIOS DE E-MAILS: (�ltimos 50)</b></td>
	</tr>
    <tr bgcolor="#CCCCCC">
		<td align="center"><b>Texto</b></td>
        <td align="center"><b>Data de envio</b></td>
	</tr>
    <%
	do while not rs_log.EOF
	%>
	<tr bgcolor="#FFFFFF">
		<td>
			- <%=rs_log("texto_log")%>
		</td>
        <td align="center">
			<%=FORMATA_DATA( rs_log("data_log"), 1 )%> �s <%=FORMATA_HORA( rs_log("data_log"), 1 )%>
		</td>
	</tr>
	<%
	rs_log.movenext:loop	
	%>
</table>
<%end if%>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_principal_02.asp" -->