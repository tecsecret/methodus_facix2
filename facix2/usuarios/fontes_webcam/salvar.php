<?php
error_reporting(E_ALL);
//fun��o respons�vel pela exporta��o do MC...
function exporta()
{
	global $_REQUEST;
	//verificando se o GD est� instalado...
	if(!function_exists("imagecreate")) die("Para exportar o MC, vc necessitar� da biblioteca GD instalada!");	
	//resgatando vari�veis do flash e tb verificamos se as mesmas foram preenchidas...
	$w = (isset($_REQUEST['width']) && strlen($_REQUEST['width']) > 0) ? intval($_REQUEST['width']) : 3;
	$h = (isset($_REQUEST['height']) && strlen($_REQUEST['height']) > 0) ? intval($_REQUEST['height']) : 3;	
	$s = (isset($_REQUEST['dataString']) && strlen($_REQUEST['dataString']) > 0) ? $_REQUEST['dataString'] : "2550000,2550000\n2550000,2550000";
	$ext = (isset($_REQUEST['format']) && strlen($_REQUEST['format']) > 0) ? $_REQUEST['format'] : "png";
	$nome = (isset($_REQUEST['filename']) && strlen($_REQUEST['filename']) > 0) ? $_REQUEST['filename'] : "imagem";	
	//criando image do GD...
	$image = (function_exists("imagecreatetruecolor")) ? imagecreatetruecolor($w, $h) : imagecreate($w, $h);
	//preenchendo o fundo da imagem de branco...os pixels brancos nao vem do flash por uma economia...
	imagefill($image, 0, 0, 0xFFFFFF);	
	$color = explode(chr(13), $s);
	for($i = 0; $i < $h; $i++){
		$linha = explode(",", $color[$i]);
		for($j = 0; $j < $w; $j++){
			$hex = $linha[$j];
			if(strlen($hex) > 0){
				//convertendo HEX para DECIMAL...
				while(strlen($hex) < 6) $hex = "0" . $hex;
				$r = hexdec(substr($hex, 0, 2));
				$g = hexdec(substr($hex, 2, 2));
				$b = hexdec(substr($hex, 4, 2));
				//alocando o rgb a imagem...
				$tmp = imagecolorallocate($image, $r, $g, $b);
				//inserindo pixel na nova imagem...
				imagesetpixel($image, $j, $i, $tmp);
			}
		}
	}
	//salvando o arquivo de imagem, de acordo com o tipo especificado...
	if($ext == "jpg")
		imagejpeg($image, $nome, 100);
	
	if($ext == "png")
		imagepng($image, $nome, 100);

	//limpando mem�ria...
	imagedestroy($image);
	//resposta para o Flash...
	echo "salvo=true&file=" . $nome . "&";
}
//executando fun��o export...
exporta();
?>