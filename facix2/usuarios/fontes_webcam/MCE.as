﻿/*
Class MCE - MovieClip Exporter
Classe para facilitação da exportação pixel a pixel de um mc, para salvar como uma Imagem
Autor: Lucas Ferreira - http://www.lucasferreira.com/
Bugs/reports: contato@lucasferreira.com
*/

dynamic class MCE {
	
	private var events:Object;
	private var firstTimer:Date;
	private var lastTimer:Date;
	private var dataString:String;
	private var mcAlvo:MovieClip;
	private var bdMC:flash.display.BitmapData;
	private var _ext:String;
	private var w:Number = 100;
	private var h:Number = 100;
	private var row:Number = 0;
	private var loopCopy:Number;
	private var lvTransport:LoadVars;
	private var cancelar:Boolean = false;
	
	function MCE(){
		this.lvTransport = new LoadVars();
		this.removeListener();
		this._ext = "jpg";
	}
	
	/* funções de dispacho de eventos by Lucas Ferreira */
	private function callEvent(evt:String, args:Array):Void
	{
		if(typeof this.events == "object" && typeof this.events[evt] == "function"){
			this.events[evt].apply(this, args);
		}
	}
	
	public function addListener(evt_listerner:Object):Void
	{
		this.removeListener();
		this.events = evt_listerner;
	}
	
	public function removeListener(Void):Void
	{
		delete this.events;
		this.events = new Object();
	}
	
	/*getter`s and setter`s*/
	public function set format(f:String):Void
	{
		this._ext = f;
	}
	
	public function get format():String
	{
		return this._ext;
	}
	
	public function set target(t:MovieClip):Void
	{
		this.mcAlvo = t;
		this.refreshBD();		
	}
	
	public function get target():MovieClip
	{
		return this.mcAlvo;	
	}
	
	/*funções para salvar o MC*/
	private function refreshBD():Void
	{
		this.w = this.mcAlvo._width;
		this.h = this.mcAlvo._height;
		this.lvTransport.width = this.w;
		this.lvTransport.height = this.h;		
		this.bdMC = new flash.display.BitmapData(this.w, this.h, true);
		this.bdMC.draw(this.mcAlvo);
	}
	private function copyLinha():Void
	{
		var dataRow:Array = new Array();
		for (var i = 0; i < this.w; i++) {
			var rgb:Array = new Array();
			var px = this.bdMC.getPixel(i, this.row);
			var strpx = px.toString(16);
			if(px == 0xFFFFFF) strpx = "";
			dataRow.push(strpx);
		}
		this.dataString += dataRow.join(",") + chr(13);
		this.row++;
		this.callEvent("onCaptureProgress", [this.mcAlvo, int(new Number(this.row/this.h) * 100)]);			
		if(this.row >= this.h && !this.cancelar){
			clearInterval(this.loopCopy);
			this.lastTimer = new Date();
			this.lvTransport.dataString = this.dataString;			
			this.callEvent("onCaptureEnd", [this.mcAlvo, this.lvTransport, this.firstTimer, this.lastTimer]);			
		}
	}
	public function export(filename:String):Void
	{
		this.refreshBD();
		this.lvTransport.format = this.format;
		this.lvTransport.filename = (filename == undefined) ? "imagem." + this.format : filename;		
		this.cancelar = false;		
		this.firstTimer = new Date();
		this.dataString = "";
		this.row = 0;
		this.callEvent("onCaptureStart", [this.mcAlvo, this.firstTimer]);		
		this.loopCopy = setInterval(mx.utils.Delegate.create(this, this.copyLinha), 3);
	}
	public function cancel():Void
	{
		this.cancelar = true;
		clearInterval(this.loopCopy);
		this.lastTimer = new Date();
		this.callEvent("onCaptureCancel", [this.mcAlvo, this.firstTimer, this.lastTimer]);			
	}
	
}