<%@ codepage="1252" language="VBScript" %>
<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_principal_01.asp" -->
<!-- INÍCIO DO CONTEÚDO -->

	<%
	' Definir a variável do botão voltar ' - ' 0: sem botão; 1:Voltar no histórico; 2: session(voltar) '
	voltar	=	PAGINA_VOLTAR( "../_principal/" )
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Cria Menu migalha de pão da página '
	response.Write( cria_titulo("Página Principal@../_principal/;Fornecedores@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	
	session("voltar") = SESSION_VOLTAR()
	%>


<%
cor			= 	"#FFFFFF"

titulo		=	formatar("titulo",1,2)
contato		=	formatar("contato",1,2)
produto		=	formatar("produto",1,2)
pesquisar	=	formatar("pesquisar",1,2)

'' Se foi feita alguma pesquisa, carregar a página já com a janela de pesquisa aberta ''
if pesquisar = "1" then
	status_01 = "none"
	status_02 = "block"
else
	status_01 = "block"
	status_02 = "none"
end if
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


sql= "select id_fornecedor, titulo_fornecedor, contato_fornecedor, comentarios_fornecedor, produto_fornecedor,status_fornecedor, data_cadastro from fornecedores where id_fornecedor>0"

if not titulo=empty then
	sql=sql+" and titulo_fornecedor like '%"&FORMATAR_PESQUISA(titulo,true)&"%'"
end if

if not contato=empty then
	sql=sql+" and titulo_fornecedor like '%"&FORMATAR_PESQUISA(contato,true)&"%'"
end if

if not produto=empty then
	sql=sql+" and titulo_fornecedor like '%"&FORMATAR_PESQUISA(produto,true)&"%'"
end if


sql= sql+" order by titulo_fornecedor"


set rs= conexao.execute(sql)
%>
<style>
.detalhes{cursor:pointer;}
</style>

<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	<tr id="tr_menu" bgcolor="#EEEEEE" style="display:<%=status_01%>;">
		<TD colspan="4" align="center" height="30">
			<a href="javascript:" onClick="exibir_acao( 'incluir.asp' );" class="link_02"><img src="../_img/icone_incluir.gif" width="32" height="32" align="absmiddle" border="0" /> Incluir novo Fornecedor</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="javascript:" onClick="abrir_pesquisa(true);" class="link_02"><img src="../_img/icone_pesquisar.gif" width="38" height="32" align="absmiddle" border="0" /> Pesquisar Fornecedores</a>
		</TD>
	</tr>
	
	<!-- INÍCIO DA PESQUISA -->
	<tr id="tr_pesquisa01" style="display:<%=status_02%>;">
		<TD colspan="4" height="5" bgcolor="#EEEEEE" align="center"><a href="javascript:" onClick="abrir_pesquisa(false);" class="link_04">(X) Fechar Pesquisa</a></TD>
	</tr>
	<tr id="tr_pesquisa02" style="display:<%=status_02%>;">
		<TD colspan="4" bgcolor="#FFFFDD" align="center">
		
			
			<table border="0" cellpadding="3" cellspacing="0">
			<form name="pesquisa" method="get" action="default.asp">
				<input type="hidden" name="pesquisar" value="1" />
				<TR>
					<TD align="right"><b>Nome fornecedor:</b> </TD>
					<TD><input type="text" name="titulo" style="width:180px;" value="<%=titulo%>" /></TD>
                   
					<TD align="right"><b>Nome contato:</b> </TD>
					<TD><input type="text" name="contato" style="width:180px;" value="<%=contato%>" /></TD>
    			</TR>
                <TR>
					<TD align="right"><b>Produto:</b> </TD>
					<TD><input type="text" name="produto" style="width:180px;" value="<%=produto%>" /></TD>
					
					<TD>&nbsp;</TD>
                    <TD><input type="submit" value="Pesquisar" style="width:90px;" /></TD>
				</TR>
			</form>
			</table>
		
		</TD>
	</tr>
	<!-- FIM DA PESQUISA -->
	
	<tr bgcolor="#CCCCCC">
		<td width="496" align="center" class="titulo_02" ><b>Nome Fornecedor</b></td>
		<td width="647" align="center" class="titulo_02"><b>Contato Fornecedor</b></td>
		<td width="435" align="center" class="titulo_02"><b>Produto</b></td>
		
        <td width="283"></td>
	</tr>
	<%do while not rs.EOF
	
	if rs("status_fornecedor")=false then
		cor="#FFEEEE"
	end if
	%>
	<tr bgcolor="<%=cor%>">
		<td class="detalhes" onClick="abrir_pesquisa(false); exibir_acao( 'detalhes_fornecedor.asp?ID=<%=rs("id_fornecedor")%>' );"><%=rs("titulo_fornecedor")%></td>
		<td class="detalhes" onClick="abrir_pesquisa(false); exibir_acao( 'detalhes_fornecedor.asp?ID=<%=rs("id_fornecedor")%>' );"><%=rs("contato_fornecedor")%></td>
		<td class="detalhes" onClick="abrir_pesquisa(false); exibir_acao( 'detalhes_fornecedor.asp?ID=<%=rs("id_fornecedor")%>' );"><%=rs("produto_fornecedor")%></td>
        
		<td align="center">
			<a href="javascript:" onClick="abrir_pesquisa(false); exibir_acao( 'editar.asp?ID=<%=rs("id_fornecedor")%>' );"><img src="../_img/icone_editar.gif" width="15" height="16" border="0" align="absmiddle" alt="Editar" /></a>
			
			 | 	
			
				<a href="#" onclick="return confirmar('acoes.asp?ID=<%=rs("id_fornecedor")%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>	
			
		</td>
	</tr>
	<%
	if cor="#FFFFFF" then
		cor= "#EEEEEE"
	else
		cor= "#FFFFFF"
	end if
	
	rs.movenext:loop
	%>
</table>


<!-- FIM DO CONTEÚDO -->
<!-- #include file="../_base/layout_principal_02.asp" -->