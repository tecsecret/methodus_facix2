<?php
include '../../../api/biblioteca/funcoes.php';
include '../../../api/biblioteca/classes.php';
$conexao = conexaoBD();

$retorno = file_get_contents('php://input');

//teste
/*$retorno = '{"leads":
			  [{"id":"1",
				"email":"suporte@resultadosdigitais.com.br",
				"name":"Bruno Ghisi",
				"company":"Resultados Digitais"
			  }] 
			}';*/
			
/*$retorno = '{"leads":[{"id":"139873478","email":"renan_pensa@hotmail.com","name":"Renan Teste do Webhook","company":null,"job_title":null,"bio":null,"public_url":"http://rdstation.com.br/leads/public/91fd0754-a351-4054-a7c3-7298b48edee2","created_at":"2017-02-23T14:23:42.000-03:00","opportunity":"false","number_conversions":"10","user":null,"first_conversion":{"content":{"nome":"Fulano","perfil":"","interesse":"Curso de Oratória","identificador":"alunos","created_at":"2017-02-23 17:23:42 UTC","email_lead":"renan_pensa@hotmail.com"},"created_at":"2017-02-23T14:23:42.000-03:00","cumulative_sum":"1","source":"alunos","conversion_origin":{"source":"unknown","medium":"unknown","value":null,"campaign":"unknown","channel":"Unknown"}},"last_conversion":{"content":{"identificador":"guia-medo-de-falar-em-publico","traffic_source":"encoded_eyJmaXJzdF9zZXNzaW9uIjp7InZhbHVlIjoiMTIxNjkwMTM1LjE0NjM2NzUwNzguNjcuNy51dG1jc3I9bWV0aG9kdXMuY29tLmJyfHV0bWNjbj0ocmVmZXJyYWwpfHV0bWNtZD1yZWZlcnJhbHx1dG1jY3Q9L3NpdGUvaG9tZS8ifSwiY3VycmVudF9zZXNzaW9uIjp7InZhbHVlIjoiKG5vbmUpIiwiZXh0cmFfcGFyYW1zIjp7fX0sImNyZWF0ZWRfYXQiOjE0OTMxNDMxMTc2MjV9","created_at":"2017-04-25 18:07:38 UTC","email_lead":"renan_pensa@hotmail.com","nome":"Renan Teste do Webhook","Quem é Você - Oratória":"Sou um profissional de vendas"},"created_at":"2017-04-25T15:07:38.000-03:00","cumulative_sum":"10","source":"guia-medo-de-falar-em-publico","conversion_origin":{"source":"(direct)","medium":"(none)","value":null,"campaign":"(direct)","channel":"Direct"}},"custom_fields":{"Quem é Você - Oratória":"Sou um profissional de vendas","perfil":"Sou um profissional e preciso fazer apresentações"},"website":null,"personal_phone":null,"mobile_phone":null,"city":null,"state":null,"tags":null,"lead_stage":"Cliente","last_marked_opportunity_date":null,"fit_score":"d","interest":15}]}';
*/
			
criarLog( $retorno, 'log/webhook_' );

// validate read-only stream for read raw data from the request body
if(empty($retorno)) {
    // Não foi enviado nada
	header("HTTP/1.0 400 Erro");
	echo "ERRO: Nenhum parâmetro enviado.\n";
	die();
}else{   
	
	$retorno = json_decode($retorno, true);
	
	//var_dump($retorno);
	//echo '<hr />';
	
	// Para cada lead enviado
	foreach ($retorno as $registro){
		$lead 			= $registro[0];
		$lead_email		= $lead['email'];
		$lead_nome		= $lead['name'];
		$lead_ID_curso	= '4';
		$lead_ID_perfil	= '';
		$lead_origem	= 'landingpage';
		$observacao		= '';
		
		criarLog( $lead_email, 'log/webhook_' );
		
		echo 'Nome: '.$lead_nome.'<br />';
		echo 'Email: '.$lead_email.'<br />';
		
		$campos_extras = $lead['custom_fields'];
		
		foreach ($campos_extras as $campo=>$valor){
			if ( strpos($campo,'Quem é') === false ){
				$observacao .= ' / '.$campo.': '.$valor.'';	
			}else{
				
				$perfil = $valor;
				$observacao .= ' / Perfil: '.$valor.'';	
				
				// verificar dados do curso escolhido, para Mailing e LOG
				$xml_perfis = executaPagina( 'api/', array( 'a'=>'perfis', 'metodo'=>'listar' ) );
				$perfis 	= lerXML( $xml_perfis );
				
				if ( $perfis->erro == 0 ){
					
					$perfis				= $perfis->perfis;
					$perfil_escolhido	= $perfis->xpath("perfil[.='".$perfil."']");
					
					if ( count( $perfil_escolhido ) >= 1 ){
						$perfil_escolhido	= $perfil_escolhido[0];
						
						$lead_ID_curso		= $perfil_escolhido['ID_curso'].'';
						$lead_ID_perfil		= $perfil_escolhido['codigo'].'';
						
						echo 'ID_curso: '.$lead_ID_curso.'<br />';
						echo 'ID_perfil: '.$lead_ID_perfil.'<br />';
						
						criarLog( 'ID_curso: '.$lead_ID_curso, 'log/webhook_' );
						
					}
						
				}
				
			}
		}
		
		// Efetuar cadastro no Facix
		$xml_cadastro 	= executaPagina( 'api/', array( 'a'=>'aluno', 'metodo'=>'cadastro', 'nome'=>$lead_nome, 'email'=>$lead_email, 'curso'=>$lead_ID_curso, 'perfil'=>$lead_ID_perfil, 'origem'=>$lead_origem, 'formato'=>'xml', 'webhook'=>'1' ), 'post' );
		$cadastro	 	= lerXML( $xml_cadastro );	
		
		if ( $cadastro->erro == 0 ){
			echo 'Cadastro efetuado com sucesso<br />';
		}else{
			echo 'Cadastro NÃO efetuado<br />';	
		}
		
		echo 'Observações: '.$observacao.'<br />';
		
		criarLog( 'Observações: '.$observacao, 'log/webhook_' );
		
	}
	       
}

?>