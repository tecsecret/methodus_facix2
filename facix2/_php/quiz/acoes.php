<?php

$ID_teste 		= @$_POST['ID_teste'];
$ID_aula 		= @$_POST['ID_aula'];
$ID_data		= @$_POST['ID_data'];
$tipo			= @$_POST['tipo'];
$deixar_visivel	= @$_POST['deixar_visivel'];

$target_dir = "arquivos/";
$target_file = $target_dir . basename($_FILES["anexo"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

// Check if file already exists
/*if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}*/
// Check file size
if ($_FILES["anexo"]["size"] > 900000) {
    exit ("Sorry, your file is too large.");
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "xls" && $imageFileType != "xlsx" ) {
    exit ( "Sorry, files are allowed." );
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    exit ( "Sorry, your file was not uploaded." );
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["anexo"]["tmp_name"], $target_file)) {
        //echo "The file ". basename( $_FILES["anexo"]["name"]). " has been uploaded.";
    } else {
        exit ( "Sorry, there was an error uploading your file." );
    }
}



/*
 * PHP Excel - Read a simple 2007 XLSX Excel file
 */

/** Set default timezone (will throw a notice otherwise) */
date_default_timezone_set('America/Los_Angeles');

include 'Classes/PHPExcel/IOFactory.php';

//$inputFileName = 'Teste 2 texto 2 para Renan_04-05-2015_15;16.xlsx';
$inputFileName = $target_file; //@$_POST['arquivo'];


//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

//  Loop through each row of the worksheet in turn
for ($row = 1; $row <= $highestRow; $row++) {
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	
	if( $rowData[0][0] == "ID do Aluno" ){
		$inicio_notas = ($row+1);
		break;
	}
	
    //foreach($rowData[0] as $k=>$v){
        //echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
	//}
}

$array_notas = '';
for ($row = $inicio_notas; $row <= $highestRow; $row++) {
	$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	
	$ID_dispositivo = $rowData[0][3];
	$nota_aluno		= $rowData[0][4];
	
	//echo $ID_dispositivo.' = '.$nota_aluno.'%<BR />';	
	$array_notas .= $ID_dispositivo.':'.$nota_aluno.';';	
}

//echo "http://www.methodus.com.br/facix2/quiz/acoes.asp?acao=enviar&ID_teste=$ID_teste&ID_aula=$ID_aula&ID_data=$ID_data&tipo=$tipo&array_notas=$array_notas";
header("Location: http://www.methodus.com.br/facix2/quiz/acoes.asp?acao=enviar&ID_teste=$ID_teste&ID_aula=$ID_aula&ID_data=$ID_data&tipo=$tipo&array_notas=$array_notas&deixar_visivel=$deixar_visivel");
//header("Location: http://localhost/OneDrive/Methodus/web/facix2/quiz/acoes.asp?acao=enviar&ID_teste=$ID_teste&ID_aula=$ID_aula&ID_data=$ID_data&tipo=$tipo&array_notas=$array_notas&deixar_visivel=$deixar_visivel");
die();
?>