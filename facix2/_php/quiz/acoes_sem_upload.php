<?php

/*
 * PHP Excel - Read a simple 2007 XLSX Excel file
 */

/** Set default timezone (will throw a notice otherwise) */
date_default_timezone_set('America/Los_Angeles');

include 'Classes/PHPExcel/IOFactory.php';

//$inputFileName = 'Teste 2 texto 2 para Renan_04-05-2015_15;16.xlsx';
$inputFileName = @$_POST['arquivo'];


//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
    . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

//  Loop through each row of the worksheet in turn
for ($row = 1; $row <= $highestRow; $row++) {
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	
	if( $rowData[0][0] == "ID do Aluno" ){
		$inicio_notas = ($row+1);
		break;
	}
	
    //foreach($rowData[0] as $k=>$v){
        //echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
	//}
}

for ($row = $inicio_notas; $row <= $highestRow; $row++) {
	$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	
	$ID_dispositivo = $rowData[0][3];
	$nota_aluno		= $rowData[0][5];
	
	//echo $ID_dispositivo.' = '.$nota_aluno.'%<BR />';	
	echo $ID_dispositivo.':'.$nota_aluno.';';	
}
?>