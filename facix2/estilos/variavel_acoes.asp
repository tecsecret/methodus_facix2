<!-- #include file="../_base/config.asp" -->
<!-- #INCLUDE FILE = "../_base/componente_upload.asp" -->
<%
call checalogado()

'' Vari�veis do componente de Upload ''
Dim oFO, oProps, oFile, i, item, oMyName
Set oFO = New FileUpload

Set oProps = oFO.GetUploadSettings
with oProps

	.Extensions = Array("jpg","gif","png","pdf","doc")
	.UploadDirectory = Server.Mappath("../../site/_arquivos_estilos/")
	.AllowOverWrite = true
	.MaximumFileSize = 5000000  '5 Mb
	.MininumFileSize = 1     '1 bytes
	.UploadDisabled = false
	
End with

oFO.ProcessUpload
'''''''''''''''''''''''''''''''''''''''


acao 		= 	formatar("acao",1,2)

titulo 		=	formatar(oFO.Form("titulo"),1,4)
valor 		=	formatar(oFO.Form("valor"),3,4)
tipo 		=	formatar(oFO.Form("tipo"),1,4)
ID_tema 	=	formatar(oFO.Form("ID_tema"),1,4)

ID 			= 	formatar(oFO.Form("ID"),1,4)
if ID=empty then
	ID 		= 	formatar("ID",1,2)
end if


'INCLUS�O
if acao="incluir" then

	if titulo=empty or tipo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql="insert into TEMAS_VARIAVEIS (ID_tema, titulo_variavel, valor_variavel, tipo_variavel)"&_
		" values ("&ID_tema&",'"&titulo&"','"&valor&"','"&tipo&"')"&_
		" select @@identity as NovoId"
	set rs = conexao.execute(sql).nextrecordset
	
	ID_variavel = rs("NovoId")
	
	if tipo = "file" then
	
		'' Para fazer upload do arquivo ''
		set oFile 				= oFO.File("arquivo")
		'oFile.FileName			= ID&"."&oFile.FileExtension
		nome_arquivo 			= FORMATA_HORA( now(), 1 ) &"_"& oFile.FileName
		oFile.FileName			= nome_arquivo
		
		if not oFile.FileExtension=empty then
			oFile.SaveAsFile
			
			sql = "update TEMAS_VARIAVEIS set valor_variavel='"&nome_arquivo&"' where ID_variavel="&ID_variavel&""
			conexao.execute(sql)
			
		else
			nome_arquivo = ""
		end if
		'''''''''''''''''''''''''''''''''
	
	end if
	
	

	session("sucesso") = "0001"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	
	
'EXCLUIR
elseif acao="excluir" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if
	
	sql="delete from TEMAS_VARIAVEIS where ID_variavel="&ID
	conexao.execute(sql)
	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))
	
	

'EDITAR
elseif acao="editar" then

	if ID=empty or titulo=empty or tipo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	if tipo = "file" then
		
		sql="update TEMAS_VARIAVEIS set titulo_variavel='"&titulo&"', tipo_variavel='"&tipo&"'"&_
			" where ID_variavel="&ID
		conexao.execute(sql)
		
		'' Para fazer upload do arquivo ''
		set oFile 				= oFO.File("arquivo")
		'oFile.FileName			= ID&"."&oFile.FileExtension
		nome_arquivo 			= FORMATA_HORA( now(), 1 ) &"_"& oFile.FileName
		oFile.FileName			= nome_arquivo
		
		if not oFile.FileExtension=empty then
			oFile.SaveAsFile
			
			sql = "update TEMAS_VARIAVEIS set valor_variavel='"&nome_arquivo&"' where ID_variavel="&ID&""
			conexao.execute(sql)
				
		else
			nome_arquivo = ""
		end if
		'''''''''''''''''''''''''''''''''
	
	else
	
		sql="update TEMAS_VARIAVEIS set titulo_variavel='"&titulo&"', valor_variavel='"&valor&"', tipo_variavel='"&tipo&"'"&_
			" where ID_variavel="&ID
		conexao.execute(sql)

	
	end if
	
	
	
	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	

'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>