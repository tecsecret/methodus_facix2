<%Response.Expires = 0%>
<% 
Response.Buffer=true 
Response.AddHeader "cache-control", "private" 
Response.AddHeader "pragma", "no-cache" 
Response.AddHeader "Cache-Control", "must-revalidate" 
Response.AddHeader "Cache-Control", "no-cache" 
Response.Addheader "Last-Modified:", Now() 
%>
<!-- #include file="../../_base/config.asp" -->
<%
'' atualizar pr�xima passagem da regra ''
SUB PROXIMA_PASSAGEM_REGRA(ID_regra_sub)
	
	sql = "select intervalo_regra from REGRAS where ID_regra="&ID_regra_sub&""
	set rs_pox = ABRIR_RS(sql)
	
	if not rs_pox.EOF then
		if isnumeric(rs_pox("intervalo_regra")) and rs_pox("intervalo_regra")>0 then
			proxima_passagem = DATA_BD(dateadd( "n", rs_pox("intervalo_regra"), now() ))
			
			sql2 = "update REGRAS set ultima_regra='"&proxima_passagem&"' where ID_regra="&ID_regra_sub
			conexao.execute(sql2)
		end if
	end if
	
	
END SUB
'''''''''''''''''''''''''''''''''''''''''
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Monitor</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Courier New, Courier, monospace;
	font-size: 14px;
	color: #FFFFFF;
}
body {
	background-color: #000000;
	margin-left: 25px;
	margin-top: 25px;
	margin-right: 25px;
	margin-bottom: 25px;
}
-->
</style>
<script type="text/javascript">
	setTimeout("window.location.reload()",3000);
</script>
</head>

<body>

<b>MONITOR DE REGRAS - Methodus:</b>
<BR /><BR />

<%
	'' VERIFICAR REGRAS ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	sql = "select * from REGRAS where status_regra="&verdade&" and (ultima_regra is null or ultima_regra <= '"&DATA_BD(now())&"') order by ordem_regra asc, ID_regra asc"
	set rs = ABRIR_RS(sql)
	
	gerou = false
	do while not rs.EOF and gerou = false
	
		sql = rs("sql_regra")
		
		'' formatar query ''
		sql = replace( sql, "[REGRA]", cstr( rs("ID_regra") ) )		
		sql = replace( sql, "[DIA]", cstr(  day( now() ) ) ) 
		sql = replace( sql, "[MES]", cstr(  month( now() ) ) ) 
		sql = replace( sql, "[ANO]", cstr(  year( now() ) ) ) 
		''''''''''''''''''''
		'response.Write(sql&"<BR><BR>")
		set rs_regra = ABRIR_RS(sql)
		
		'response.Write("Tentou passar a regra '"&rs("titulo_regra")&"'<BR />")
		
		if not rs_regra.EOF then
			
			ID = rs_regra(0)
			
			sql = 	"insert into REGRAS_EMAILS (ID_regra,ID_generico,data_email,status_email) values ("&rs("ID_regra")&","&ID&",'"&DATA_BD(now())&"',"&verdade&")"&_
					" select @@identity as NovoId"
			set rs_email = conexao.execute(sql).nextrecordset
			
			ID_email = rs_email("NovoId")
			
			response.Write("C�digo: "&ID_email&" - Regra: "&rs("titulo_regra")&" - ID: "&ID&"<BR />")
			
			'' executar p�gina ASP externa para gerar os emails ''
			session("ID_email") = ID_email
			server.execute(rs("arquivo_regra"))
			
			gerou = true
			
		else
			'' atualizar pr�xima passagem da regra ''
			call PROXIMA_PASSAGEM_REGRA(rs("ID_regra"))
			'''''''''''''''''''''''''''''''''''''''''
			gerou = false
		end if
	
	rs.movenext:loop
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	'' ENVIAR E-MAILS ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	sql = "select top 1 * from REGRAS_EMAILS where status_email="&falso&" order by data_email"
	set rs = ABRIR_RS(sql)
	
	if not rs.EOF then
		
		'' verificar se no e-mail tem @
		if instr( rs("para_email"), "@" ) <= 2 then
			bloqueio = ", assunto_email=assunto_email+' (N�O ENVIADO / E-MAIL INV�LIDO)'"
		else
			
			'' verificar se existe bloqueio para envio de e-mails ''
			sql="select top 1 1 from MALA_DIRETA_BLOQUEIOS where email_bloqueio='"& replace( rs("para_email"), "'", "" ) &"'"
			set rs_bloqueio = ABRIR_RS(sql)
			
			if not rs_bloqueio.EOF then
				bloqueio = ", assunto_email=assunto_email+' (N�O ENVIADO / E-MAIL NA LISTA DE BLOQUEIOS)'"
			end if
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		end if
		
		sql = "update REGRAS_EMAILS set status_email="&verdade&", envio_email='"&DATA_BD(now())&"'"&bloqueio&" where ID_email="&rs("ID_email")&""
		conexao.execute(sql)
		
		if bloqueio = empty then
			call ENVIAR_EMAIL( rs("de_email"), rs("para_email"), rs("assunto_email"), rs("mensagem_email") )
		
			response.Write("<BR /><BR /><b>ENVIANDO MENSAGEM PARA:</b> "&rs("para_email")&"")
		else
			response.Write("<BR /><BR /><b>MENSAGEM PARA:</b> "&rs("para_email")&" BLOQUEADA, E-MAIL ESTA NA LISTA DE BLOQUEIOS")
		end if
	
	end if
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



'' Criar arquivo XML com data da �ltima passagem ''
str_xml = 	"<?xml version=""1.0"" encoding=""iso-8859-1""?>"&_
			"<registros>"&_
			"<data>"&DATA_BD(now())&"</data>"&_
			"<ip>"&Request.ServerVariables("REMOTE_ADDR")&"</ip>"&_
			"</registros>"

''  GRAVAR EM TXT ''
Set FSO = Server.CreateObject("Scripting.FileSystemObject")
caminho = Server.MapPath("ultima_data.xml")
Set gravar = FSO.CreateTextFile(caminho,true)
'Foi criado o objeto e logo ap�s busca o txt em caminho para gravar, se n�o achar, vai cria-lo (por causa da marca��o TRUE)

gravar.write (str_xml)
gravar.close
'''''''''''''''''''''''''''''''''''''''''''''''''''
%>

</body>
</html>