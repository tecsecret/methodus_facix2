<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_principal_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Definir a vari�vel do bot�o voltar ' - ' 0: sem bot�o; 1:Voltar no hist�rico; 2: session(voltar) '
	voltar	=	PAGINA_VOLTAR( "../_principal/" )
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("P�gina Principal@../_principal/;Prestadores@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	
	session("voltar") = SESSION_VOLTAR()
	%>


<%
cor			= 	"#FFFFFF"

pesquisa	=	formatar("pesquisa",1,2)
nome		=	formatar("nome",1,2)
p_status	=	formatar("p_status",1,2)
cargo		=	formatar("cargo",1,2)


'' Se foi feita alguma pesquisa, carregar a p�gina j� com a janela de pesquisa aberta ''
if pesquisa = "1" then
	status_01 = "none"
	status_02 = "block"
else
	status_01 = "block"
	status_02 = "none"
end if
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


sql= "select ID_prestador, nome_prestador, exclusao_prestador from PRESTADORES where ID_prestador>0"

if not nome=empty then
	sql=sql+" and nome_prestador like '%"&FORMATAR_PESQUISA(nome,true)&"%'"
end if

if p_status=empty then
	sql=sql+" and exclusao_prestador is null"
elseif p_status="1" then
	sql=sql+" and exclusao_prestador is not null"
end if

if not cargo=empty then
	sql=sql+" and exists (select * from PRESTADOR_CARGOS where ID_prestador=PRESTADORES.ID_prestador and ID_cargo="&cargo&")"
end if

sql= sql+" order by nome_prestador"

set rs= conexao.execute(sql)
%>


<table border="0" cellpadding="3" cellspacing="1" width="100%" bgcolor="#CCCCCC">
	<tr id="tr_menu" bgcolor="#EEEEEE" style="display:<%=status_01%>;">
		<TD colspan="4" align="center" height="30">
			<a href="javascript:" onClick="exibir_acao( 'incluir.asp' );" class="link_02"><img src="../_img/icone_incluir.gif" width="32" height="32" align="absmiddle" border="0" /> Incluir novo Prestador</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="javascript:" onClick="abrir_pesquisa(true);" class="link_02"><img src="../_img/icone_pesquisar.gif" width="38" height="32" align="absmiddle" border="0" /> Pesquisar Prestadores</a>
		</TD>
	</tr>
	
	<!-- IN�CIO DA PESQUISA -->
	<tr id="tr_pesquisa01" style="display:<%=status_02%>;">
		<TD colspan="4" height="5" bgcolor="#EEEEEE" align="center"><a href="javascript:" onClick="abrir_pesquisa(false);" class="link_04">(X) Fechar Pesquisa</a></TD>
	</tr>
	<tr id="tr_pesquisa02" style="display:<%=status_02%>;">
		<TD colspan="4" bgcolor="#FFFFDD" align="center">
		
			
			<table border="0" cellpadding="3" cellspacing="0">
			<form name="pesquisa" method="get" action="default.asp">
				<input type="hidden" name="pesquisa" value="1" />
				<TR>
					<TD align="right"><b>Cargo:</b> </TD>
					<TD>
						<select name="cargo">
							<option value="">Todos os Cargos</option>
							<%
							sql= "select * from CARGOS order by nome_cargo"
							set rs_c= conexao.execute(sql)
						
							do while not rs_c.EOF
							%>
								<option value="<%=rs_c("ID_cargo")%>"<%if cstr(rs_c("ID_cargo"))=cargo then%> selected<%end if%>><%=rs_c("nome_cargo")%></option>
							<%rs_c.movenext:loop%>
						</select>
					</TD>
					<TD align="right"><b>Status:</b> </TD>
					<TD>
						<select name="P_status">
							<option value="0"<%if p_status="0" then%> selected<%end if%>>Todos</option>
							<option value=""<%if p_status="" then%> selected<%end if%>>Somente ativos</option>
							<option value="1"<%if p_status="1" then%> selected<%end if%>>Somente desativados</option>
						</select>
					</TD>
					<TD align="right"><b>Nome:</b> </TD>
					<TD><input type="text" name="nome" style="width:180px;" value="<%=nome%>" /></TD>
					
					<TD><input type="submit" value="Pesquisar" style="width:90px;" /></TD>
				</TR>
			</form>
			</table>
		
		</TD>
	</tr>
	<!-- FIM DA PESQUISA -->
	
	<tr bgcolor="#CCCCCC">
		<td align="center" class="titulo_02" width="200"><b>Prestador</b></td>
		<td align="center" class="titulo_02"><b>Cargo(s)</b></td>
		<td align="center" class="titulo_02"><b>Departamento(s)</b></td>
		<td align="center" class="titulo_02" width="60"><b>A��es</b></td>
	</tr>
	<%do while not rs.EOF%>
	<tr bgcolor="<%=cor%>">
		<td><%=rs("nome_prestador")%></td>
		<td>
			<%
			sql="select PRESTADOR_CARGOS.*, CARGOS.nome_cargo from PRESTADOR_CARGOS inner join CARGOS on PRESTADOR_CARGOS.ID_cargo=CARGOS.ID_cargo where ID_prestador="&rs("ID_prestador")
			set rs_car= conexao.execute(sql)
			
			do while not rs_car.EOF
			%>
				<%=rs_car("nome_cargo")%>
				<%
				rs_car.movenext
				if not rs_car.EOF then
					response.Write(" / ")
				end if
			loop%>
		</td>
		<td>
			<%
			sql="select PRESTADOR_DEPARTAMENTOS.*, DEPARTAMENTOS.nome_departamento from PRESTADOR_DEPARTAMENTOS inner join DEPARTAMENTOS on PRESTADOR_DEPARTAMENTOS.ID_departamento=DEPARTAMENTOS.ID_departamento where ID_prestador="&rs("ID_prestador")
			set rs_dep= conexao.execute(sql)
			
			do while not rs_dep.EOF
			%>
				<%=rs_dep("nome_departamento")%>
				<%
				rs_dep.movenext
				if not rs_dep.EOF then
					response.Write(" / ")
				end if
			loop%>
		</td>
		<td align="center">
			<a href="javascript:" onClick="abrir_pesquisa(false); exibir_acao( 'editar.asp?ID=<%=rs("ID_prestador")%>' );"><img src="../_img/icone_editar.gif" width="15" height="16" border="0" align="absmiddle" alt="Editar" /></a>
			<%if not rs("ID_prestador")=1 then%>
			 | 	
			<%if isdate(rs("exclusao_prestador"))=false then%>	
				<a href="#" onclick="return confirmar('acoes.asp?ID=<%=rs("ID_prestador")%>&acao=excluir','0')"><img src="../_img/icone_excluir.gif" width="16" height="16" border="0" align="absmiddle" alt="Excluir" /></a>	
			<%else%>
				<a href="acoes.asp?ID=<%=rs("ID_prestador")%>&acao=restaurar"><img src="../_img/icone_check.gif" width="17" height="15" border="0" align="absmiddle" alt="Restaurar" /></a>	
			<%end if
			end if%>
		</td>
	</tr>
	<%
	if cor="#FFFFFF" then
		cor= "#EEEEEE"
	else
		cor= "#FFFFFF"
	end if
	
	rs.movenext:loop
	%>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_principal_02.asp" -->