<!-- #include file="../_base/config.asp" -->
<!-- #INCLUDE FILE = "../_base/componente_upload.asp" -->
<%
call checalogado()

'' Vari�veis do componente de Upload ''
Dim oFO, oProps, oFile, i, item, oMyName
Set oFO = New FileUpload

Set oProps = oFO.GetUploadSettings
with oProps

	.Extensions = Array("jpg","gif")
	.UploadDirectory = Server.Mappath("../../methodus/_arquivos_home/")
	.AllowOverWrite = true
	.MaximumFileSize = 5000000  '5 Mb
	.MininumFileSize = 1     '1 bytes
	.UploadDisabled = false
	
End with

oFO.ProcessUpload
'''''''''''''''''''''''''''''''''''''''



acao 		= 	formatar("acao",1,2)

titulo 		=	formatar(oFO.Form("titulo"),1,4)
corpo 		=	formatar(oFO.Form("corpo"),3,4)
ativo 		=	formatar(oFO.Form("ativo"),1,4)
tema 		=	formatar(oFO.Form("tema"),1,4)
mm 			=	formatar(oFO.Form("mm"),1,4)
chamada		=	formatar(oFO.Form("chamada"),1,4)
editora		=	formatar(oFO.Form("editora"),1,4)

ID 			= 	formatar(oFO.Form("ID"),1,4)
if ID=empty then
	ID 		= 	formatar("ID",1,2)
end if


'INCLUS�O
if acao="incluir" then

	if titulo=empty or corpo=empty or ativo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	
	'' Para fazer upload do arquivo ''
	set oFile 				= oFO.File("foto")
	'oFile.FileName			= ID&"."&oFile.FileExtension
	nome_arquivo 			= FORMATA_HORA( now(), 1 ) &"_"& oFile.FileName
	oFile.FileName			= nome_arquivo
	
	if not oFile.FileExtension=empty then
		if lcase(oFile.FileExtension) <> "jpg" and lcase(oFile.FileExtension) <> "gif" then
			'erro de extens�o
		else
			oFile.SaveAsFile
			
			'' #####################################################
			'' Para formatar a imagem '''''''''''''''
			largura = 118
			altura = 150
			'Criando o thumbnail:
			Set Jpeg = Server.CreateObject("Persits.Jpeg")
			'Caminho da Imagem
			'Path = Server.Mappath(caminho & nome_arquivo)
			Path = Server.Mappath("../../methodus/_arquivos_home/"& nome_arquivo)
			'Busca a Imagem
			Jpeg.Open Path
			'Especifica o tamanho da imagem
			Jpeg.Height = altura
			Jpeg.Width = largura
			'Esse m�todo � opcional, usado para melhorar o visual da imagem
			Jpeg.Sharpen 1, 101
			'Cria um thumbnail e o grava no caminho abaixo
			Jpeg.Save Server.Mappath("../../methodus/_arquivos_home/"& nome_arquivo)
			'''''''''''''''''''''''''''''''''''''''
			'' #####################################################
			
		end if
	else
		nome_arquivo = ""
	end if
	'''''''''''''''''''''''''''''''''
	
	
	sql="insert into LIVROS (data_livro, titulo_livro, corpo_livro, status_livro,ID_tema,mm,foto,chamada,editora)"&_
		" values ('"& DATA_BD( now() ) &"','"&titulo&"','"&corpo&"',"&ativo&","&tema&","&mm&",'"&nome_arquivo&"','"&chamada&"','"&editora&"')"
	conexao.execute(sql)

	session("sucesso") = "0001"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
'EXCLUIR
elseif acao="excluir" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if
	
	sql="delete from LIVROS where ID_livro="&ID
	conexao.execute(sql)
	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))

'EDITAR
elseif acao="editar" then

	if ID=empty or titulo=empty or corpo=empty or ativo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	'' Para fazer upload do arquivo ''
	set oFile 				= oFO.File("foto")
	'oFile.FileName			= ID&"."&oFile.FileExtension
	nome_arquivo 			= FORMATA_HORA( now(), 1 ) &"_"& oFile.FileName
	oFile.FileName			= nome_arquivo
	
	if not oFile.FileExtension=empty then
		if lcase(oFile.FileExtension) <> "jpg" and lcase(oFile.FileExtension) <> "gif" then
			'erro de extens�o
		else
			oFile.SaveAsFile
			
			'' #####################################################
			'' Para formatar a imagem '''''''''''''''
			largura = 118
			altura = 150
			'Criando o thumbnail:
			Set Jpeg = Server.CreateObject("Persits.Jpeg")
			'Caminho da Imagem
			'Path = Server.Mappath(caminho & nome_arquivo)
			Path = Server.Mappath("../../methodus/_arquivos_home/"& nome_arquivo)
			'Busca a Imagem
			Jpeg.Open Path
			'Especifica o tamanho da imagem
			Jpeg.Height = altura
			Jpeg.Width = largura
			'Esse m�todo � opcional, usado para melhorar o visual da imagem
			Jpeg.Sharpen 1, 101
			'Cria um thumbnail e o grava no caminho abaixo
			Jpeg.Save Server.Mappath("../../methodus/_arquivos_home/"& nome_arquivo)
			'''''''''''''''''''''''''''''''''''''''
			'' #####################################################
			
			sql="update LIVROS set foto='"&nome_arquivo&"'"&_
				" where ID_livro="&ID
			conexao.execute(sql)
			
		end if
	else
		nome_arquivo = ""
	end if
	'''''''''''''''''''''''''''''''''
	
	sql="update LIVROS set titulo_livro='"&titulo&"', corpo_livro='"&corpo&"', status_livro="&ativo&", ID_tema="&tema&",mm="&mm&",chamada='"&chamada&"',editora='"&editora&"'"&_
		" where ID_livro="&ID
	conexao.execute(sql)

	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")

'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>