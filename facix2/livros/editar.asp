<%@ codepage="1252" language="VBScript" %>
<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Editar Livro@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>
	<!-- #INCLUDE file="../_FCKeditor/fckeditor.asp" -->


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// EDI��O DE ARTIGO
function checa(formulario)
{
	campo=[
			formulario.titulo
		  ];
	n_campo=[
			"T�tulo"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID 	= 	formatar("ID",1,2)
sql="select * from LIVROS where ID_livro="&ID
set rs= conexao.execute(sql)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_editar_livro" method="post" action="acoes.asp?acao=editar" onSubmit="return checa(this);" enctype="multipart/form-data">
	<input type="hidden" name="ID" value="<%=ID%>" />
	<tr>
		<td class="titulo_02" width="50%"><b>T�tulo</b></td>
		<td class="titulo_02" width="50%"><b>Status</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="titulo" style="width:100%" value="<%=Server.HTMLEncode(rs("titulo_livro"))%>" />
		</td>
		<td>
			<input type="radio" name="ativo" value="<%=verdade%>"<%if rs("status_livro")=true then%> checked<%end if%> /> Ativo 
			<input type="radio" name="ativo" value="<%=falso%>"<%if rs("status_livro")=false then%> checked<%end if%> /> Inativo
		</td>
	</tr>
    <tr>
		<td class="titulo_02" width="50%"><b>Autores e Informa��es</b></td>
		<td class="titulo_02" width="50%"><b>Status</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="chamada" style="width:100%" value="<%=rs("chamada")%>" />
		</td>
		<td>
			<input type="file" name="foto" style="width:100%" /><BR />
            JPG ou GIF / 118px de largura por 150px de altura
            <%if not rs("foto")=empty then%>
				<BR />Arquivo atual: <a href="../../methodus/_arquivos_home/<%=rs("foto")%>" target="_blank"><%=rs("foto")%></a>
			<%end if%>
		</td>
	</tr>
    <tr>
		<td class="titulo_02" width="50%"><b>Editora</b></td>
		<td class="titulo_02" width="50%"><b>&nbsp;</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="editora" style="width:100%" value="<%=rs("editora")%>" />
		</td>
		<td>&nbsp;</td>
	</tr>
    <tr>
		<td class="titulo_02" width="50%"><b>Categoria</b></td>
		<TD><b>Aparece em maladireta?</b></TD>
	</tr>
	<tr>
		<td>
			<%
			sql="select titulo_tema, ID_tema from TEMAS where status_tema="&verdade&""
			set rs_tema= conexao.execute(sql)
			%>
			<select name="tema" style="width:100%">
				<option value="0">Sem Categoria</option>
				<%do while not rs_tema.EOF%>
					<option value="<%=rs_tema("ID_tema")%>"<%if rs_tema("ID_tema")=rs("ID_tema") then%> selected="selected"<%end if%>><%=rs_tema("titulo_tema")%></option>
				<%rs_tema.movenext:loop%>
			</select>
		</td>
		<td>
			<input type="radio" name="mm" value="<%=verdade%>"<%if rs("mm")=true then%> checked<%end if%> /> Sim 
			<input type="radio" name="mm" value="<%=falso%>"<%if rs("mm")=false or isnull(rs("mm")) then%> checked<%end if%> /> N�o
		</td>
	</tr>
	<tr>
		<td class="titulo_02" colspan="2"><b>Conte�do</b></td>
	</tr>
	<tr>
		<td colspan="2">
			<%
			Dim oFCKeditor
			Set oFCKeditor = New FCKeditor
			oFCKeditor.BasePath = "../_FCKeditor/"
			oFCKeditor.ToolbarSet = "Padrao"
			oFCKeditor.Height = "240"
			oFCKeditor.Value = rs("corpo_livro")
			
			oFCKeditor.Create "corpo"
			%>	
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Editar" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->