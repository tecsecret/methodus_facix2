<!-- #include file="../_base/config.asp" -->
<%
call checalogado()

acao 			= 	formatar("acao",1,2)
condicao 		= 	formatar("condicao",1,2)

titulo 			=	formatar("titulo",1,1)
sub_categoria	=	formatar("sub_categoria",1,1)
tipo			=	formatar("tipo",1,1)
link 			=	formatar("link",1,1)
ativo 			=	formatar("ativo",1,1)
classe 			=	formatar("classe",1,1)

ID 				= 	formatar("ID",1,3)
ID1 			= 	formatar("ID1",1,2)
ID2 			= 	formatar("ID2",1,2)

'INCLUS�O
if acao="incluir" then

	if titulo=empty or sub_categoria=empty or ativo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql="select top 1 posicao_categoria from CATEGORIAS where SUB_categoria="&sub_categoria&" order by posicao_categoria desc"
	set rs= conexao.execute(sql)
	
	if not rs.EOF then
		posicao = rs("posicao_categoria")+1
	else
		posicao = 1
	end if
	
	sql="insert into CATEGORIAS (titulo_categoria,SUB_categoria,posicao_categoria,link_categoria,status_categoria,tipo_categoria, classe_categoria)"&_
		" values ('"&titulo&"',"&sub_categoria&","&posicao&",'"&link&"',"&ativo&","&tipo&",'"&classe&"')"
	conexao.execute(sql)

	session("sucesso") = "0001"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	
'EXCLUIR
elseif acao="excluir" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if
	
	sql="delete from CONTEUDOS where ID_categoria="&ID
	conexao.execute(sql)
	
	sql="select * from CATEGORIAS where SUB_categoria="&ID
	set rs= conexao.execute(sql)
	
	do while not rs.EOF
		sql="delete from CONTEUDOS where ID_categoria="&rs("ID_categoria")
		conexao.execute(sql)
	rs.movenext:loop
	
	sql="delete from CATEGORIAS where SUB_categoria="&ID
	conexao.execute(sql)
		
	sql="delete from CATEGORIAS where ID_categoria="&ID
	conexao.execute(sql)
	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))


'EDITAR
elseif acao="editar" then

	if ID=empty or titulo=empty or sub_categoria=empty or ativo=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql="select SUB_categoria, posicao_categoria from CATEGORIAS where ID_categoria="&ID
	set rs= conexao.execute(sql)
	
	if sub_categoria=cstr(rs("SUB_categoria")) then
		posicao = rs("posicao_categoria")
	else
		SUB_antiga = cstr(rs("SUB_categoria"))
	
		sql="select top 1 posicao_categoria from CATEGORIAS where SUB_categoria="&sub_categoria&" order by posicao_categoria desc"
		set rs= conexao.execute(sql)
	
		if not rs.EOF then
			posicao = rs("posicao_categoria")+1
		else
			posicao = 1
		end if
	end if
	
	sql="update CATEGORIAS set titulo_categoria='"&titulo&"',SUB_categoria="&sub_categoria&",link_categoria='"&link&"',posicao_categoria="&posicao&",status_categoria="&ativo&",tipo_categoria="&tipo&", classe_categoria='"&classe&"'"&_
		" where ID_categoria="&ID
	conexao.execute(sql)
	
	if not SUB_antiga=empty then
		sql="select ID_categoria from CATEGORIAS where SUB_categoria="&SUB_antiga&" order by posicao_categoria"
		set rs= conexao.execute(sql)
		
		i=1
		do while not rs.EOF
			sql="update CATEGORIAS set posicao_categoria="&i&" where ID_categoria="&rs("ID_categoria")
			conexao.execute(sql)
			i = i + 1
		rs.movenext:loop
	end if

	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
	
'MOVER POSI��O
elseif acao="mover" then

	if ID1=empty or ID2=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql="select posicao_categoria, SUB_categoria from CATEGORIAS where ID_categoria="&ID1
	set rs= conexao.execute(sql)
	
	posicao1 		= rs("posicao_categoria")
	
	sql="select posicao_categoria, SUB_categoria from CATEGORIAS where ID_categoria="&ID2
	set rs= conexao.execute(sql)
	
	posicao2 		= rs("posicao_categoria")
	
	sql="update CATEGORIAS set posicao_categoria="&posicao2&" where ID_categoria="&ID1
	conexao.execute(sql)
	
	sql="update CATEGORIAS set posicao_categoria="&posicao1&" where ID_categoria="&ID2
	conexao.execute(sql)
	

	'session("sucesso") = "0002"
	response.Redirect(session("voltar"))
	
	
	
'MOVER POSI��O
elseif acao="mover2" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql="select posicao_categoria, SUB_categoria from CATEGORIAS where ID_categoria="&ID
	set rs= conexao.execute(sql)
	
	posicao_atual 		= rs("posicao_categoria")
	
	if condicao="subir" then
		nova_posicao	= posicao_atual-1
	else
		nova_posicao	= posicao_atual+1
	end if
	
	sql="select ID_categoria from CATEGORIAS where SUB_categoria="&rs("SUB_categoria")&" and posicao_categoria="&nova_posicao
	set rs= conexao.execute(sql)
	
	
	sql="update CATEGORIAS set posicao_categoria="&nova_posicao&" where ID_categoria="&ID
	conexao.execute(sql)
	
	if not rs.EOF then
	
		sql="update CATEGORIAS set posicao_categoria="&posicao_atual&" where ID_categoria="&rs("ID_categoria")
		conexao.execute(sql)
		
	end if

	'session("sucesso") = "0002"
	response.Redirect(session("voltar"))
	

'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>