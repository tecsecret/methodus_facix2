<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/cls_webservice.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Incluir Data de Treinamento@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE DATA DE TREINAMENTO
function checa(formulario)
{
	campo=[
			formulario.inicio_dia,
			formulario.inicio_mes,
			formulario.inicio_ano,
			formulario.fim_dia,
			formulario.fim_mes,
			formulario.fim_ano,
			formulario.horario,
			formulario.inscricoes_dia,
			formulario.inscricoes_mes,
			formulario.inscricoes_ano,
			formulario.vagas
		  ];
	n_campo=[
			"Dia de in�cio",
			"M�s de in�cio",
			"Ano de in�cio",
			"Dia de t�rmino",
			"M�s de t�rmino",
			"Ano de t�rmino",
			"Hor�rio",
			"Dia de t�rmino das inscri��es",
			"M�s de t�rmino das inscri��es",
			"Ano de t�rmino das inscri��es",
			"Vagas"
			];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
	if (checa_caracter(campo[0],n_campo[0],"1234567890")==false)
		return(false);
	if (checa_nulo(campo[1],n_campo[1])==false)
		return(false);
	if (checa_caracter(campo[1],n_campo[1],"1234567890")==false)
		return(false);
	if (checa_nulo(campo[2],n_campo[2])==false)
		return(false);
	if (checa_caracter(campo[2],n_campo[2],"1234567890")==false)
		return(false);
		
	if (checa_nulo(campo[3],n_campo[3])==false)
		return(false);
	if (checa_caracter(campo[3],n_campo[3],"1234567890")==false)
		return(false);
	if (checa_nulo(campo[4],n_campo[4])==false)
		return(false);
	if (checa_caracter(campo[4],n_campo[4],"1234567890")==false)
		return(false);
	if (checa_nulo(campo[5],n_campo[5])==false)
		return(false);
	if (checa_caracter(campo[5],n_campo[5],"1234567890")==false)
		return(false);
		
	if (checa_nulo(campo[6],n_campo[6])==false)
		return(false);
		
	if (checa_nulo(campo[7],n_campo[7])==false)
		return(false);
	if (checa_caracter(campo[7],n_campo[7],"1234567890")==false)
		return(false);
	if (checa_nulo(campo[8],n_campo[8])==false)
		return(false);
	if (checa_caracter(campo[8],n_campo[8],"1234567890")==false)
		return(false);
	if (checa_nulo(campo[9],n_campo[9])==false)
		return(false);
	if (checa_caracter(campo[9],n_campo[9],"1234567890")==false)
		return(false);
		
	if (checa_nulo(campo[10],n_campo[10])==false)
		return(false);
	if (checa_caracter(campo[10],n_campo[10],"1234567890")==false)
		return(false);
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID 	= 	formatar("ID",1,2)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_incluir_data" method="post" action="curso_acoes.asp?acao=incluir" onSubmit="return checa(this);">
	<input type="hidden" name="ID" value="<%=ID%>" />
	<tr>
		<td class="titulo_02" width="50%"><b>In�cio</b></td>
		<td class="titulo_02" width="50%"><b>Fim</b></td>
	</tr>
	<tr>
		<TD>
			<input type="text" name="inicio_dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" /> / 
			<input type="text" name="inicio_mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" /> / 
			<input type="text" name="inicio_ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" />
		</TD>
		<TD>
			<input type="text" name="fim_dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" /> / 
			<input type="text" name="fim_mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" /> / 
			<input type="text" name="fim_ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" />
		</TD>
	</tr>
	<tr>
		<td class="titulo_02" width="50%"><b>Hor�rio</b></td>
		<td class="titulo_02" width="50%"><b>Esgotado?</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="horario" style="width:100%" maxlength="50" />
		</td>
		<td>
			<input type="radio" name="ativo" value="<%=verdade%>" checked /> N�o 
			<input type="radio" name="ativo" value="<%=falso%>" /> Sim
		</td>
	</tr>
	<tr>
		<td colspan="2"><b>Descri��o</b></td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="descricao" style="width:100%; height:120px;"></textarea>	
		</td>
	</tr>
    <tr>
		<td class="titulo_02" width="50%"><b>Data de fim das Inscri��es</b></td>
		<td class="titulo_02" width="50%"><b>N�mero de Vagas</b></td>
	</tr>
	<tr>
		<TD>
			<input type="text" name="inscricoes_dia" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" /> / 
			<input type="text" name="inscricoes_mes" style="width:25px" maxlength="2" onKeyUp="return autoTab(this, 2, event);" /> / 
			<input type="text" name="inscricoes_ano" style="width:47px" maxlength="4" onKeyUp="return autoTab(this, 4, event);" />
		</TD>
		<TD>
			<input type="text" name="vagas" style="width:50x;" maxlength="3" />
		</TD>
	</tr>
    <tr>
		<td class="titulo_02" width="50%"><b>Tipo</b></td>
		<td class="titulo_02" width="50%"><b>Matricular no Instudo na turma</b></td>
	</tr>
	<tr>
		<td>
			<input type="checkbox" name="presencial" value="1" checked /> Presencial 
			<input type="checkbox" name="online" value="1" /> Online
		</td>
		<td>
        	<%
			gravar_turmas = RESGATAR_TURMAS_INSTUDO()
			
			if gravar_turmas <> "1" then
			%>
			<div style="color:#FF0000;">
				Um erro ocorreu ao tentar recuperar os dados mais atuais dos cursos, a lista abaixo pode estar desatualizada. ERRO: <%=gravar_turmas%>
			</div>
			<%
			end if
			%>
			<select name="ID_instudo" style="width:100%">
            	<option value="null">-- N�o cadastrar no Instudo --</option> 
				<%
                '' Ler XML de turmas, fazer loop e gravar no banco se necess�rio
                call CONEXAO_XML( "../_xml/turmas_instudo.xml" )
                set registros 	= xmlDoc.documentElement
                
                for each registro in registros.childNodes
                    
                    codigo_turma 	= registro.selectSingleNode("@id").text
                    titulo_turma 	= registro.selectSingleNode("titulo").text
                    %>
                    <option value="<%=codigo_turma%>"><%=titulo_turma%></option> 
                    <%
                    
                next
                %>
            </select>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<BR />
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Incluir" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->