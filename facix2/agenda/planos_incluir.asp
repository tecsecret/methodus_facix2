<!-- #include file="../_base/config.asp" -->
<!-- #include file="../_base/layout_dialogo_01.asp" -->
<!-- IN�CIO DO CONTE�DO -->

	<%
	' Cria Menu migalha de p�o da p�gina '
	response.Write( cria_titulo("Incluir Plano de inscri��o@") )
	''''''''''''''''''''''''''''''''''''''
	
	checalogado()
	%>


<script type="text/javascript">
//#########################################################//
//                    VALIDA��O DE FORMS                   //
//#########################################################//

// INCLUS�O DE PLANO
function checa(formulario)
{
	campo=[formulario.preco, formulario.participantes, formulario.desconto1, formulario.desconto2, formulario.desconto3, formulario.parcelas];
	n_campo=["Pre�o", "Participantes", "Desconto 1", "Desconto 2", "Desconto 3", "Parcelas"];
	
	if (checa_nulo(campo[0],n_campo[0])==false)
		return(false);
		
	if (checa_caracter(campo[0],n_campo[0],"0123456789.,")==false)
		return(false);
	if (campo[0].value <= 0 || campo[0].value == "0,00" || campo[0].value == "0,0" || campo[0].value == "0,")
	{
		alert("O pre�o precisa ser maior que R$ 0,00");
		return(false);
	}
	
	
	if (checa_nulo(campo[1],n_campo[1])==false)
		return(false);
		
	if (checa_caracter(campo[1],n_campo[1],"0123456789")==false)
		return(false);
	if (campo[1].value <= 0)
	{
		alert("O n�mero de participantes precisa ser maior que 0");
		return(false);
	}
	
	if (checa_caracter(campo[2],n_campo[2],"0123456789")==false)
		return(false);
	if (checa_caracter(campo[3],n_campo[3],"0123456789")==false)
		return(false);
	if (checa_caracter(campo[4],n_campo[4],"0123456789")==false)
		return(false);
		
	if (checa_caracter(campo[5],n_campo[5],"0123456789")==false)
		return(false);
	
	return(true);
}
/////////////////////////////////////////////////////////////
</script>

<%
ID 	=	formatar("ID",1,2)
%>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
	<form name="a_incluir_plano" method="post" action="planos_acoes.asp?acao=incluir" onSubmit="return checa(this);">
	<input type="hidden" name="ID_curso" value="<%=ID%>" />
	<tr>
		<td width="50%"><b>Empresarial</b></td>
		<td width="50%"><b>Status</b></td>
	</tr>
	<tr>
		<td>
			<select name="empresarial" style="width:100%">
				<option value="0" selected="selected">Pessoa F�sica</option>
				<option value="1">Empresarial</option>
			</select>
		</td>
		<td>
			<input type="radio" name="ativo" value="<%=verdade%>" checked /> Ativo 
			<input type="radio" name="ativo" value="<%=falso%>" /> Inativo
		</td>
	</tr>
	<tr>
		<td><b>N�mero m�ximo de participantes</b></td>
        <td><b>Pre�o (por pessoa)</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="participantes" style="width:80px" maxlength="10" />
		</td>
        <td>
			R$ <input type="text" name="preco" style="width:80px" maxlength="10" />
		</td>
	</tr>
    <tr>
		<td><b>Desconto para pagto � vista</b></td>
        <td><b>Desconto para 2 ou mais pessoas</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="desconto1" style="width:80px" maxlength="2" /> %
		</td>
        <td>
			<input type="text" name="desconto2" style="width:80px" maxlength="2" /> %
		</td>
	</tr>
    <tr>
		<td><b>Desconto para 2 ou mais pessoas � prazo</b></td>
        <td><b>Quantidade de Parcelas</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" name="desconto3" style="width:80px" maxlength="2" /> %
		</td>
        <td>
			<input type="text" name="parcelas" style="width:60px" maxlength="2" />
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<BR />
			<input type="button" value="Cancelar" style="width:75px;" onClick="return confirmar_cancelar();" />
			<input type="submit" value="Incluir" style="width:150px;" />
		</td>
	</tr>
	</form>
</table>


<!-- FIM DO CONTE�DO -->
<!-- #include file="../_base/layout_dialogo_02.asp" -->