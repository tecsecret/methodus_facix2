<!-- #include file="../_base/config.asp" -->
<%
call checalogado()

acao 			= 	formatar("acao",1,2)

inicio_dia 		=	formatar("inicio_dia",1,1)
inicio_mes 		=	formatar("inicio_mes",1,1)
inicio_ano 		=	formatar("inicio_ano",1,1)
fim_dia 		=	formatar("fim_dia",1,1)
fim_mes 		=	formatar("fim_mes",1,1)
fim_ano 		=	formatar("fim_ano",1,1)
horario			=	formatar("horario",1,1)
ativo			=	formatar("ativo",1,1)
inscricoes_dia 	=	formatar("inscricoes_dia",1,1)
inscricoes_mes 	=	formatar("inscricoes_mes",1,1)
inscricoes_ano 	=	formatar("inscricoes_ano",1,1)
vagas			=	formatar("vagas",1,1)
foto			=	formatar("foto",1,1)
excluir_foto	=	formatar("excluir_foto",1,1)
descricao		=	formatar("descricao",2,1)
presencial		=	formatar("presencial",1,1)
online			=	formatar("online",1,1)
ID_instudo		=	formatar("ID_instudo",1,1)
ID_curso		= 	formatar("ID_curso",1,3)

ID 				= 	formatar("ID",1,3)

data_inicio		=	inicio_dia &"/"& inicio_mes &"/"& inicio_ano
data_fim		=	fim_dia &"/"& fim_mes &"/"& fim_ano
data_inscricoes	=	inscricoes_dia &"/"& inscricoes_mes &"/"& inscricoes_ano

if online = empty then
	online = "0"
end if
if presencial = empty then
	presencial = "0"
end if

'INCLUS�O
if acao="incluir" then

	if ID=empty or horario=empty or ativo=empty or isdate(data_inicio)=false or isdate(data_fim)=false then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	sql="insert into CURSOS_DATAS (ID_curso, inicio_data, fim_data, horario_data, status_data, fim_inscricoes_data, vagas_data, descricao_data, ID_turma_instudo, presencial_data, online_data)"&_
		" values ("&ID&",'"&DATA_BD( data_inicio )&"','"&DATA_BD( data_fim )&"','"&horario&"',"&ativo&",'"&DATA_BD( data_inscricoes )&"',"&vagas&",'"&descricao&"',"&ID_instudo&","&presencial&","&online&")"
	conexao.execute(sql)
	
	'' remover cache do site
	call EXCLUIR_ARQUIVO( "../../api/xml/cursos/curso_"&ID&".xml" )

	session("sucesso") = "0001"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
	
'EXCLUIR
elseif acao="excluir" then

	if ID=empty then
		session("erro") = "0001"
		response.Redirect(session("voltar"))
	end if
	
	
	sql="select ID_inscricao from CURSOS_INSCRICOES where ID_data="&ID
	set rs = conexao.execute(sql)
	
	do while not rs.EOF
		
		sql="delete from CURSOS_INSCRICOES_EMPRESAS where ID_inscricao="&rs("ID_inscricao")
		conexao.execute(sql)
		
		sql="delete from CURSOS_INSCRICOES_USUARIOS where ID_inscricao="&rs("ID_inscricao")
		conexao.execute(sql)
		
	rs.movenext:loop
	
	sql="delete from CURSOS_INSCRICOES where ID_data="&ID
	conexao.execute(sql)
	
	sql="delete from CURSOS_DATAS where ID_data="&ID
	conexao.execute(sql)
	
	'' remover cache do site
	call EXCLUIR_ARQUIVO( "../../api/xml/cursos/curso_"&ID_curso&".xml" )

	
	session("sucesso") = "0003"
	response.Redirect(session("voltar"))

'EDITAR
elseif acao="editar" then

	if ID=empty or horario=empty or ativo=empty or isdate(data_inicio)=false or isdate(data_fim)=false then
		session("erro") = "0001"
		response.Redirect("../_base/layout_dialogo_voltar.asp")
	end if
	
	if excluir_foto = "1" and foto=empty then
		foto = "null"
	elseif foto=empty then
		foto = "null"
	else
		call FORMATA_IMAGEM( "../../asa/_arquivos_turmas/"&foto, "../../asa/_arquivos_turmas/thumb_"&foto, 210, 147, 3 )
		call FORMATA_IMAGEM( "../../asa/_arquivos_turmas/"&foto, "../../asa/_arquivos_turmas/"&foto, 672, 378, 3 )
		foto = "'"&foto&"'"
	end if
	
	sql="update CURSOS_DATAS set inicio_data='"&DATA_BD( data_inicio )&"', fim_data='"&DATA_BD( data_fim )&"', horario_data='"&horario&"', status_data="&ativo&",fim_inscricoes_data='"&DATA_BD( data_inscricoes )&"',vagas_data="&vagas&", foto_data="&foto&", descricao_data='"&descricao&"', ID_turma_instudo="&ID_instudo&", presencial_data="&presencial&", online_data="&online&" "&_
		" where ID_data="&ID
	conexao.execute(sql)
	
	'' remover cache do site
	call EXCLUIR_ARQUIVO( "../../api/xml/cursos/curso_"&ID_curso&".xml" )

	session("sucesso") = "0002"
	response.Redirect("../_base/layout_dialogo_voltar.asp")

'SE A A��O N�O FOI IDENTIFICADA
else
	session("erro") = "0004"
	response.Redirect("../_base/layout_dialogo_voltar.asp")
end if
%>