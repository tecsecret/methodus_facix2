<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><%=ws_titulo%> - �rea do Aluno</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../_base/css/et-line-icons.css" rel="stylesheet" type="text/css" media="all">
    <link href="../_base/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="../_base/css/themify-icons.css" rel="stylesheet" type="text/css" media="all">
    <link href="../_base/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <link href="../_base/css/flexslider.css" rel="stylesheet" type="text/css" media="all">
    <link href="../_base/css/theme-hyperblue.css" rel="stylesheet" type="text/css" media="all">
    <link href="../_base/css/custom.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600" rel="stylesheet" type="text/css">
    <style>
		@media (max-width: 768px) {
			img {max-width: 300px !important; margin:0 auto 30px;}
		}
		p{
			color:#000 !important;	
		}
		a:not(.botao_padrao){
			color:#1f367e !important;		
		}
	</style>
    
    <script language="JavaScript" type="text/JavaScript" src="../../facix2/_base/js/script_01.js"></script>
	<script language="JavaScript" type="text/JavaScript" src="../_base/js/script_01.js"></script>
</head>
        
<body class="scroll-assist">
        <div class="nav-container">
            <a id="top"></a>
            <nav></nav>
        </div>
        <div class="main-container">
            <section class="cover fullscreen image-bg overlay">
                <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../_img/login.jpg">
                </div>
                <div class="container v-align-transform">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2">
                            <div class="feature bordered text-center" style="background-color: #FFF;">
                              
                                <a href="http://www.methodus.com.br/"><img src="../_img/logo methodus menor-res.png" class="img-responsive mb30" style="max-width:200px;margin: 0 auto 30px !important;"></a>
                                
                                
                                <%if session("aluno") > 0 then%>
                                    <div style="color: #1f367e; float:right;">
                                        <a href="../home/default.asp" style="color: #1f367e;">MEUS CURSOS</a> | 
                                        <a href="#" onClick="javascript: confirmar( '../login/acoes.asp?acao=logout', 'Quer mesmo sair do Ambiente de Aula?' )" style="color: #1f367e;"> VOLTAR PARA O SITE</a>
                                    </div>
                                    <BR />
                                <%else%>
                                	<h4 class="uppercase" style="color: #1f367e;">�rea do Aluno</h4>
                                <%end if%>